package com.jiudaifu.moxademo;

import com.telink.TelinkApplication;

/**
 * author: mojinshu
 * date: 2018/8/14 上午10:08
 * description: MoxaDemo自定义Application，继承TelinkApplication
 */
public class MoxaDemoApplication extends TelinkApplication {
}
