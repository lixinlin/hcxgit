package com.jiudaifu.moxademo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiudaifu.moxademo.R;
import com.jiudaifu.moxademo.view.ShadowImageView;
import com.telink.bluetooth.light.ConnectionStatus;
import com.telink.ibluetooth.model.Channel;
import com.telink.ibluetooth.model.Channels;
import com.telink.ibluetooth.ui.BatteryView;
import com.telink.ibluetooth.utils.VoiceUtils;

/**
 * author: mojinshu
 * date: 2018/8/20 上午9:28
 * description:
 */
public class ChannelAdapter extends RecyclerView.Adapter<ChannelAdapter.ViewHolder>{

    private LayoutInflater mInflater;

    private RecyclerView.LayoutParams mLp;

    private Context mContext;

    public ChannelAdapter(Context mContext) {
        this.mContext = mContext;
        mInflater = LayoutInflater.from(mContext);
        mLp = new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
    }

    public void setItemHeight(int newItemHeight){
        if(mLp.height != newItemHeight) {
            mLp.height = newItemHeight;
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return Channels.getInstance().size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final Channel channel = Channels.getInstance().get(position);
        final Channel.Info targetState = channel.getTargetState();
        if (channel.getConnectionStatus() != ConnectionStatus.OFFLINE && channel.isWorking()) {
            if (targetState.getTemp() >= VoiceUtils.WARNNING_WENDU) {
                holder.mChannelIcon.setImageResource(R.mipmap.moxi_channel_i9_red);
                holder.mTempView.setTextColor(mContext.getResources().getColor(R.color.chl_text_red));
            } else if (targetState.getTemp() >= VoiceUtils.WARNNING_TEMP_ORANGE) {
                holder.mChannelIcon.setImageResource(R.mipmap.moxi_channel_i9_orange);
                holder.mTempView.setTextColor(mContext.getResources().getColor(R.color.chl_text_orange));
            } else {
                holder.mChannelIcon.setImageResource(R.mipmap.moxi_channel_i9_blue);
                holder.mTempView.setTextColor(mContext.getResources().getColor(R.color.chl_text_normal));
            }
        } else {
            holder.mChannelIcon.setImageResource(R.mipmap.moxi_channel_i9_normal);
            holder.mTempView.setTextColor(mContext.getResources().getColor(R.color.chl_text_normal));
        }
        //显示编号
        int num = channel.getNumber();
        holder.mNumImgView.setVisibility(View.GONE);
        holder.mNumView.setVisibility(View.VISIBLE);
        holder.mNumView.setText(num <= 0?"":String.valueOf(channel.getNumber()));

        holder.mTempView.setText(channel.getTargetState().getTemp()+"℃");

        //显示状态
        if (channel.getConnectionStatus() == ConnectionStatus.OFFLINE){
            holder.mStatusView.setText(R.string.moxa_nolink_ble_hint);
        } else {
            if (channel.isWorking()) {
                int time = targetState.getTimeSeconds();
                holder.mStatusView.setText(String.format("%02d:%02d",new Object[]{time/60,time%60}));
                holder.mShadowView.clearAnim();
            } else {
                holder.mStatusView.setText(R.string.unwork);
                holder.mShadowView.startAnim();
            }
        }
        //显示电量
        final int battery = targetState.getBattery();
        holder.mBatteryView.setPower(battery);

        if (holder.itemView.getLayoutParams().height != mLp.height) {
            holder.itemView.setLayoutParams(mLp);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View root = mInflater.inflate(R.layout.moxa_channel_grid_item, null);
        root.setLayoutParams(mLp);
        ViewHolder holder = new ViewHolder(root);
        holder.mIndicator = (ImageView) root.findViewById(R.id.curr_indicator);
        holder.mBatteryView = (BatteryView) root.findViewById(R.id.battery_view);
        holder.mNumView = (TextView) root.findViewById(R.id.num_view);
        holder.mNumImgView = (ImageView) root.findViewById(R.id.num_img_view);
        holder.mStatusView = (TextView) root.findViewById(R.id.status_view);
        holder.mTempView = (TextView) root.findViewById(R.id.temp_view);
        holder.mContainer = root.findViewById(R.id.container);
        holder.mChannelIcon = (ImageView) root.findViewById(R.id.chl_icon);
        holder.mShadowView = (ShadowImageView) root.findViewById(R.id.image_switch);
        return holder;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        BatteryView mBatteryView;
        ImageView mIndicator;
        TextView mNumView;
        ImageView mNumImgView; //用于显示特殊编号图片，如：金、木、水、火、土
        ImageView mChannelIcon;

        ShadowImageView mShadowView;

        TextView mTempView;
        TextView mStatusView;

        View mContainer;

        public ViewHolder(View v) {
            super(v);
        }

    }
}
