package com.jiudaifu.moxademo.jiuliaoData;

/**
 * Created by lixinlin on 2018/10/30.
 */

public class ActiviteInfo {
    private String member;
    private String deviceSn;
    private String activiteStatus;
    private String activiteTime;

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public String getDeviceSn() {
        return deviceSn;
    }

    public void setDeviceSn(String deviceSn) {
        this.deviceSn = deviceSn;
    }

    public String getActiviteStatus() {
        return activiteStatus;
    }

    public void setActiviteStatus(String activiteStatus) {
        this.activiteStatus = activiteStatus;
    }

    public String getActiviteTime() {
        return activiteTime;
    }

    public void setActiviteTime(String activiteTime) {
        this.activiteTime = activiteTime;
    }

    @Override
    public String toString() {
        return "ActiviteInfo{" +
                "member='" + member + '\'' +
                ", deviceSn='" + deviceSn + '\'' +
                ", activiteStatus='" + activiteStatus + '\'' +
                ", activiteTime='" + activiteTime + '\'' +
                '}';
    }
}
