package com.hxlm.android.hcy.user;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyphone.event.EventUserLogin;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import static com.hxlm.android.hcy.user.LoginControllor.getChoosedChildMember;

public class ChooseMemberDialog extends AlertDialog {
    private final static String TAG = "ChooseMemberDialog";
    private final OnCompleteListener onCompleteListener;
    private final UserManager userManager;

    private ListView lv_members;
    private List<ChildMember> childMembers;
    private TextView tv_username;
    private Context context;

    public ChooseMemberDialog(Context context, OnCompleteListener listener) {
        super(context);
        this.context = context;
        this.onCompleteListener = listener;
        userManager = new UserManager();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_choose_member);

        initView();
        initData();
    }

    private void initView() {
        tv_username = (TextView) findViewById(R.id.tv_username);
        lv_members = (ListView) findViewById(R.id.lv_members);

        //家庭成员点击项
        lv_members.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LoginControllor.setChoosedChildMember(childMembers.get(position));

                Logger.d(TAG, Integer.toString(childMembers.get(position).getId()));

                ChooseMemberDialog.this.dismiss();
                EventBus.getDefault().post(new EventUserLogin());
                if (null != onCompleteListener) {
                    onCompleteListener.onComplete();
                }
            }
        });
    }

    private void initData() {
        if (LoginControllor.isLogin()) {
            //当前用户
            String userName = LoginControllor.getLoginMember().getUserName();
            if(userName.length()>20){
                userName = LoginControllor.getWeixinNickName();
            }
            tv_username.setText(context.getString(R.string.dialog_choose_member_welcome) + userName);
            childMembers = LoginControllor.getLoginMember().getMengberchild();
           int size=childMembers.size();
            for(int i=0;i<size;i++)
            {
                ChildMember childMember=childMembers.get(i);
                if(childMember.getMobile().equals(LoginControllor.getLoginMember().getUserName()))
                {
                    childMembers.add(0,childMember);
                    childMembers.remove(i+1);
                }
            }

            MemberAdapter adapter = new MemberAdapter(getContext(), childMembers);
            lv_members.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }


    class MemberAdapter extends BaseAdapter {
        private List<ChildMember> list;
        private LayoutInflater inflater;
        private Holder holder;

        private String sex;//性别


        public MemberAdapter(Context context, List<ChildMember> list) {
            inflater = LayoutInflater.from(context);
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                holder = new Holder();

                convertView = inflater.inflate(R.layout.choose_member_item, null);
                holder.iv_is_choosed_status_image = (ImageView) convertView
                        .findViewById(R.id.iv_is_choosed_status_image);
                holder.tv_name = (TextView) convertView
                        .findViewById(R.id.tv_name);
                holder.tv_sex = (TextView) convertView
                        .findViewById(R.id.tv_sex);
                holder.tv_age = (TextView) convertView
                        .findViewById(R.id.tv_age);
                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }

            ChildMember childMember = list.get(position);
            if (getChoosedChildMember() != null &&
                    childMember.getId() == getChoosedChildMember().getId()) {
                holder.iv_is_choosed_status_image.setImageResource(R.drawable.member_choosed);
            } else {
                holder.iv_is_choosed_status_image.setImageResource(R.drawable.member_not_choosed);
            }
            String childMemberName = childMember.getName();
            if(childMemberName.length()>20){
                holder.tv_name.setText(LoginControllor.getWeixinNickName());
            }else {
                holder.tv_name.setText(childMember.getName());
            }

            if(LoginControllor.getLoginMember().getUserName().equals(childMember.getMobile()))
            {
                sex=LoginControllor.getLoginMember().getGender();
                if ("male".equals(sex)) {
                    sex = BaseApplication.getContext().getString(R.string.man);
                } else if ("female".equals(sex)) {
                    sex = BaseApplication.getContext().getString(R.string.woman);
                } else {
                    sex = BaseApplication.getContext().getString(R.string.my_families_weizhi);
                }
            }else
            {
                sex = childMember.getGender();
                if ("male".equals(sex)) {
                    sex = BaseApplication.getContext().getString(R.string.man);
                } else if ("female".equals(sex)) {
                    sex = BaseApplication.getContext().getString(R.string.woman);
                } else {
                    sex = BaseApplication.getContext().getString(R.string.my_families_weizhi);
                }
            }


            holder.tv_sex.setText(sex);


            if (!TextUtils.isEmpty(childMember.getBirthday())) {
                holder.tv_age.setText(userManager.getAge(childMember) + BaseApplication.getContext().getString(R.string.my_families_sui));
            }else
            {
                holder.tv_age.setText(BaseApplication.getContext().getString(R.string.my_families_weizhi));
            }

            return convertView;
        }

    }

    class Holder {
        ImageView iv_is_choosed_status_image;
        TextView tv_name;
        TextView tv_sex;
        TextView tv_age;
    }
}
