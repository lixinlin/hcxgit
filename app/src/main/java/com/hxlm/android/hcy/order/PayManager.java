package com.hxlm.android.hcy.order;

import android.text.TextUtils;
import android.util.Log;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.bean.Payment;
import com.hxlm.hcyandroid.bean.PaymentAndOrder;
import com.loopj.android.http.RequestParams;

import java.util.List;

public class PayManager {
    /**
     * 生成订单的接口
     *
     * @param reserveType     预定的服务类型
     * @param orderId         订单id
     * @param payType         交易类型
     * @param paymentPluginId 所使用的支付插件的id
     * @param cardFees        如果没使用现金卡，可不传；每张现金卡的id及扣款金额，客户端计算好之后传上来，形式cardFees=1:100;2:50
     * @param balance         余额，订单支付总金额减去现金卡支付的金额，如果全部为现金卡支付或者不用现金卡，可不传
     * @param isInvoice       是否开发票，不开发票可不用传
     * @param invoiceTitle    发票抬头，不开发票不传
     * @param memo            订单备注，没有备注可不传
     */
    void createOrder(int reserveType, int orderId, String payType,
                     String paymentPluginId, String cardFees, double balance,
                     boolean isInvoice, String invoiceTitle, String memo,
                     AbstractDefaultHttpHandlerCallback callback) {

        String url = "/member/order/create_pay.jhtml";

        RequestParams params = new RequestParams();

        params.put("reserveType", reserveType);
        params.put("orderId", orderId);
        params.put("type", payType);
        if (!TextUtils.isEmpty(paymentPluginId)) {
            params.put("paymentPluginId", paymentPluginId);
        }
        if (!TextUtils.isEmpty(cardFees)) {
            params.put("cardFees", cardFees);
        }
        params.put("balance", balance);
        if (isInvoice) {
            params.put("isInvoice", true);
        }
        if (!TextUtils.isEmpty(invoiceTitle)) {
            params.put("invoiceTitle", invoiceTitle);
        }
        if (!TextUtils.isEmpty(memo)) {
            params.put("memo", memo);
        }

        if(Constant.DEBUG)
        {
            Log.i("Log.i","请求地址-->"+Constant.BASE_URL + url);
            Log.i("Log.i", "支付参数--> "+params);
        }

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                return JSON.parseObject(content, PaymentAndOrder.class);
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

    void getOrderId(String ids, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/resources/reserve.jhtml";

        RequestParams params = new RequestParams();
        params.put("ids", ids);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                JSONObject data = JSON.parseObject(content);

                JSONObject order = data.getJSONObject("order");
                return order.getInteger("id");
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

    /**
     * 生成收款单，用于健康商城的支付
     *
     * @param orderSn         订单的sn
     * @param paymentPluginId 支付方式
     */
    void createPayment(String orderSn, String paymentPluginId, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/mobile/order/payment/submit.jhtml";
        RequestParams params = new RequestParams();
        params.put("sn", orderSn);
        params.put("paymentPluginId", paymentPluginId);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {

                return JSON.parseObject(content, Payment.class);
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

    String getCardsPayInfoStr(List<Card> cardsToPay) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < cardsToPay.size(); i++) {
            if (i == 0) {
                stringBuilder.append(cardsToPay.get(i).getId()).append(":").
                        append(cardsToPay.get(i).getDeductMoney());
            } else {
                stringBuilder.append(";").append(cardsToPay.get(i).getId()).append(":").
                        append(cardsToPay.get(i).getDeductMoney());
            }
        }
        return stringBuilder.toString();
    }
}
