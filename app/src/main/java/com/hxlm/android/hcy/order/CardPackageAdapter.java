package com.hxlm.android.hcy.order;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.bean.CardArrayDataBean;
import com.hxlm.hcyandroid.Constant;

import java.util.Calendar;
import java.util.List;

/**
 * @author Lirong
 * @date 2018/12/5 0005.
 * @description
 */

public class CardPackageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<CardArrayDataBean> list;
    private int CARD_LIST = 0;
    private int CARD_PACKAGE = 1;
    private final boolean isCheckButtonGone;

    public CardPackageAdapter(Context context,boolean isCheckButtonGone) {
        this.context = context;
        this.isCheckButtonGone = isCheckButtonGone;
    }

    public void setData(List<CardArrayDataBean> list){
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        if (viewType == CARD_LIST){
//            View inflate = LayoutInflater.from(context).inflate(R.layout.item_card_package, parent, false);
////            View inflate = LayoutInflater.from(context).inflate(R.layout.reservation_card_item, parent, false);
//            CardListViewHolder cardListViewHolder = new CardListViewHolder(inflate);
//            return cardListViewHolder;
//        }else {
            View inflate = LayoutInflater.from(context).inflate(R.layout.item_card_package, parent, false);
            CardPackageViewHolder cardPackageViewHolder = new CardPackageViewHolder(inflate);
            return cardPackageViewHolder;
//        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
//        if (holder instanceof CardListViewHolder){
//            CardListViewHolder holder2 = (CardListViewHolder) holder;
//            CardArrayDataBean cardArrayDataBean = list.get(position);
//            holder2.item_card_title.setText(cardArrayDataBean.getCard_name());
//            Log.e("retrofit","===卡包列表blance=="+cardArrayDataBean.getBalance());
//            holder2.item_card_introduce.setText("余额："+Constant.MONEY_PREFIX + cardArrayDataBean.getBalance());
//            holder2.item_card_unActivate.setVisibility(View.GONE);
//            String exprise_time = cardArrayDataBean.getExprise_time();
//            if (!TextUtils.isEmpty(exprise_time)) {
//                holder2.item_card_date.setText(exprise_time + "到期");
//            } else {
//                holder2.item_card_date.setText("永久有效");
//            }
//            //被激活卡不显示提示图片
//            if (cardArrayDataBean.getStatus().equals("2")) {
//                holder2.item_card_unActivate.setVisibility(View.GONE);
//            } else if (cardArrayDataBean.getStatus().equals("1")) {
//                holder2.item_card_unActivate.setVisibility(View.VISIBLE);
//            }
//            holder2.tv_card_name.setText(cardArrayDataBean.getCard_name());
//            // 余额
//            holder2.tv_balance.setText(Constant.MONEY_PREFIX + cardArrayDataBean.getBalance());
//            // 总额
//            holder2.tv_balance2.setText("总额："+cardArrayDataBean.getAmount() + "元");
//
            // 有效期：
//            Calendar calendar = Calendar.getInstance();
//
//            int expiredDays = cardArrayDataBean.getExpiredTime();
//
//            if (expiredDays == 0) {
//                calendar.setTimeInMillis(cardArrayDataBean.getEndDate());
//            } else {
//                calendar.setTimeInMillis(cardArrayDataBean.getBindDate());
//                calendar.add(Calendar.DAY_OF_MONTH, expiredDays);
//            }
//            holder2.item_card_date.setText(new StringBuilder().append(calendar.get(Calendar.YEAR)).append("-").
//                    append(calendar.get(Calendar.MONTH)+1).append("-").append(calendar.get(Calendar.DAY_OF_MONTH)).append("到期"));
//
//            // 判断是否快过期
//            if (cardArrayDataBean.isDated()) {
//                holder2.tv_valid_is_date.setText("(快过期)");
//                holder2.tv_valid_is_date.setTextColor(context.getResources().getColor(R.color.fc5959));
//            }
//
//            if(isCheckButtonGone){
//                holder2.iv_card_checked_status.setVisibility(View.GONE);
//            }else {
//                if (cardArrayDataBean.isChoosed()) {
//                    holder2.iv_card_checked_status.setImageResource(R.drawable.card_checked);
//                } else {
//                    holder2.iv_card_checked_status.setImageResource(R.drawable.card_not_checked);
//                }
//            }
//        }else if (holder instanceof CardPackageViewHolder) {
            CardPackageViewHolder holder1 = (CardPackageViewHolder) holder;
            CardArrayDataBean cardArrayDataBean = list.get(position);
            holder1.item_card_title.setText(cardArrayDataBean.getCard_name());
            int tag = cardArrayDataBean.getTag();
            if (tag == 1){
                holder1.item_card_introduce.setText(context.getString(R.string.card_package_yue)+ Constant.MONEY_PREFIX + cardArrayDataBean.getBalance());
                holder1.item_card_unActivate.setVisibility(View.GONE);
                // 有效期：
                Calendar calendar = Calendar.getInstance();

                int expiredDays = cardArrayDataBean.getExpiredTime();

                if (expiredDays == 0) {
                    calendar.setTimeInMillis(cardArrayDataBean.getEndDate());
                } else {
                    calendar.setTimeInMillis(cardArrayDataBean.getBindDate());
                    calendar.add(Calendar.DAY_OF_MONTH, expiredDays);
                }
                holder1.item_card_date.setText(new StringBuilder().append(calendar.get(Calendar.YEAR)).append("-").
                        append(calendar.get(Calendar.MONTH)+1).append("-").append(calendar.get(Calendar.DAY_OF_MONTH)).append(context.getString(R.string.card_package_daoqi)));
            }else if (tag == 2){
                holder1.item_card_introduce.setText(cardArrayDataBean.getDescription());
                String exprise_time = cardArrayDataBean.getExprise_time();
                if (!TextUtils.isEmpty(exprise_time)) {
                    holder1.item_card_date.setText(exprise_time + context.getString(R.string.card_package_daoqi));
                } else {
                    holder1.item_card_date.setText(context.getString(R.string.card_package_yongjiuyouxiao));
                }
                //被激活卡不显示提示图片
                if (!TextUtils.isEmpty(cardArrayDataBean.getStatus())) {
                    if (cardArrayDataBean.getStatus().equals("2")) {
                        holder1.item_card_unActivate.setVisibility(View.GONE);
                    } else if (cardArrayDataBean.getStatus().equals("1")) {
                        holder1.item_card_unActivate.setVisibility(View.VISIBLE);
                    }
                }
                //设置点击监听
                holder1.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onItemClick.onItem(holder.getPosition(), list.get(position));
                    }
                });
            }
//        }
    }

//    @Override
//    public int getItemViewType(int position) {
//        if (position == 0){
//            return CARD_PACKAGE;
//        }else {
//            return CARD_LIST;
//        }
//    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CardListViewHolder extends RecyclerView.ViewHolder{
//        private final ImageView iv_card_icon;
//        private final TextView tv_card_name;
//        private final TextView tv_balance;
//        private final TextView tv_balance2;
//        private final TextView tv_valid_date;
//        private final ImageView iv_card_checked_status;
//        private final TextView tv_valid_is_date;
        private final TextView item_card_title;
        private final TextView item_card_introduce;
        private final TextView item_card_date;
        private final ImageView item_card_unActivate;
        public CardListViewHolder(View itemView) {
            super(itemView);
//            iv_card_icon = itemView.findViewById(R.id.iv_card_icon);
//            tv_card_name = itemView.findViewById(R.id.tv_card_name);
//            tv_balance = itemView.findViewById(R.id.tv_balance);
//            tv_balance2 = itemView.findViewById(R.id.tv_balance2);
//            tv_valid_date = itemView.findViewById(R.id.tv_valid_date);
//            iv_card_checked_status = itemView.findViewById(R.id.iv_card_checked_status);
//            tv_valid_is_date = itemView.findViewById(R.id.tv_valid_is_date);
            item_card_title = itemView.findViewById(R.id.item_card_title);
            item_card_introduce = itemView.findViewById(R.id.item_card_introduce);
            item_card_date = itemView.findViewById(R.id.item_card_date);
            item_card_unActivate = itemView.findViewById(R.id.item_card_unActivate);
        }
    }

    class CardPackageViewHolder extends RecyclerView.ViewHolder{

        private final TextView item_card_title;
        private final TextView item_card_introduce;
        private final TextView item_card_date;
        private final ImageView item_card_unActivate;

        public CardPackageViewHolder(View itemView) {
            super(itemView);
            item_card_title = itemView.findViewById(R.id.item_card_title);
            item_card_introduce = itemView.findViewById(R.id.item_card_introduce);
            item_card_date = itemView.findViewById(R.id.item_card_date);
            item_card_unActivate = itemView.findViewById(R.id.item_card_unActivate);
        }
    }

    private OnItemClick onItemClick;

    public void setOnItemClick(OnItemClick onItemClick) {
        this.onItemClick = onItemClick;
    }

    public interface OnItemClick{
        void onItem(int position, CardArrayDataBean card);
    }

}
