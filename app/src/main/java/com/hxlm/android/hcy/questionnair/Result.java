package com.hxlm.android.hcy.questionnair;

/**
 * 评估结果
 * Created by Zhenyu on 2016/4/11.
 */
public class Result {
    private int score;
    private String categorySn;
    private String subjectSn;
    private String description;

    public Result(String categorySn, int score) {
        this.categorySn = categorySn;
        this.score = score;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getSubjectSn() {
        return subjectSn;
    }

    public void setSubjectSn(String subjectSn) {
        this.subjectSn = subjectSn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategorySn() {
        return categorySn;
    }

    public void setCategorySn(String categorySn) {
        this.categorySn = categorySn;
    }

    @Override
    public String toString() {
        return "Result{" +
                "score=" + score +
                ", categorySn='" + categorySn + '\'' +
                ", subjectSn='" + subjectSn + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
