package com.hxlm.android.hcy.content;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.health.AbstractBaseActivity;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.ReserveType;
import com.hxlm.hcyandroid.callback.MyCallBack.OnChangeListener;
import com.hxlm.android.hcy.order.GouWuCheActivity;
import com.hxlm.hcyandroid.util.BroadcastUtil;
import com.hxlm.hcyandroid.util.ToastUtil;

import java.util.List;

public class YueyaojiesuanActivity extends AbstractBaseActivity implements OnClickListener {
    private static List<ResourceItem> list;
    private YueYaoJiesuanListAdapter adapter;
    private TextView mPrice;
    private ListView mListView;

    private double getPrice() {
        double price = 0;
        for (ResourceItem item : list) {
            price += item.getPrice();
        }
        return price;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_pay:
                LoginControllor.requestLogin(this, new OnCompleteListener() {
                    @Override
                    public void onComplete() {

                        //从乐药跳转
                        BroadcastUtil.BroadreserveID = ReserveType.YUEYAO;
                        //从乐药跳转
                        BroadcastUtil.sendTypeToPayResult(YueyaojiesuanActivity.this,
                                ReserveType.TYPE_STATIC_RESOURCE);

                        Intent intent = new Intent(YueyaojiesuanActivity.this, GouWuCheActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                break;
            default:
                break;
        }
    }

    @Override
    public void setContentView() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_yueyaojiesuan);
    }

    @Override
    public void initViews() {

        TitleBarView titleView = new TitleBarView();
        titleView.init(this, getString(R.string.yyjs_title), titleView, 1);
        adapter = new YueYaoJiesuanListAdapter(this, new OnChangeListener() {
            @Override
            public void onChange() {
                mPrice.setText(getString(R.string.yueyao_total)+"￥" + Constant.decimalFormat.format(getPrice()));

            }
        });
        mPrice = (TextView) findViewById(R.id.tv_appointment_moey_to_pay_down);
        TextView mPay = (TextView) findViewById(R.id.tv_pay);
        mListView = (ListView) findViewById(R.id.jisuan_list);
        mPay.setOnClickListener(this);


        mListView.setAdapter(adapter);
        list = Constant.LIST;
        adapter.setDatas(list);
        mPrice.setText(getString(R.string.yueyao_total)+"￥" + Constant.decimalFormat.format(getPrice()));
    }

    @Override
    public void initDatas() {

    }
}
