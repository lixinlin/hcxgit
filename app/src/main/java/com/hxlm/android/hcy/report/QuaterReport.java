package com.hxlm.android.hcy.report;

/**
 * Created by lixinlin on 2018/12/18.
 */

public class QuaterReport {

    /**
     * id : 1
     * createDate : 1544491587000
     * modifyDate :
     *
     * year : 2018
     * quarter : 3
     * jlbsjson : 1
     * jlbsremark : 2
     * tzbsjson : 2
     * tzbsremark : 2
     * zfbsjson : 2
     * zfbsremark : 2
     * ecgjson : 34
     * ecgremark : 5
     * oxygenjson : 5
     * oxygenremark : 6
     * bloodjson : 6
     * bloodremark : 7
     * bodytemjson : 8
     * bodytemremark : 9
     * breathejson : 9
     * breatheremark : 007
     * status : 0
     * modify_user : 0
     */

    private int id;
    private long createDate;
    private long modifyDate;
    private String year;
    private int quarter;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getQuarter() {
        return quarter;
    }

    public void setQuarter(int quarter) {
        this.quarter = quarter;
    }
}
