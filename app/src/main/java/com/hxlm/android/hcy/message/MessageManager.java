package com.hxlm.android.hcy.message;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.hxlm.android.hcy.asynchttp.AbstractHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.hcyandroid.BaseApplication;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

public class MessageManager {
    private static final String SP_MSG_COUNT_NAME = "count";
    private static final String SP_MSG_PREFIX_NAME = "msg_";
    private static final String SP_MSG_STORE_PREFIX_NAME = "msg_store_";
    private static final String SP_MSG_LAST_FETCH_NAME = "last_fetch_time";

    //从Share得到消息
    public List<HcyMessage> getHcyMessagesFromSp() {
        //首先得到所有的消息
        List<HcyMessage> hcyMessages = getSpMessages(HcyMessage.TYPE_ALL);

        //如果所有消息不为空，再去获取个人消息添加进去 否则只是添加个人消息
        if (hcyMessages != null) {
            List<HcyMessage> typeOneMessages = getSpMessages(HcyMessage.TYPE_ONE);

            if (typeOneMessages != null) {
                hcyMessages.addAll(typeOneMessages);
            }
        } else {
            hcyMessages = getSpMessages(HcyMessage.TYPE_ONE);
        }

        return hcyMessages;
    }

    /**
     * 获取消息接口
     * 如果不传时间和用户id,返回的是所有的系统消息
     * 如果不传时间，传用户id,返回的是当前用户的所有消息
     */
    public void fetchMessage(AbstractHttpHandlerCallback callback) {
        String url = "/user_message/messages.jhtml";

        RequestParams params = new RequestParams();

        //最后一次请求消息时间
        long lastQueryTime;
        //判断是否登录
        if (LoginControllor.isLogin()) {
            params.put(LoginControllor.MEMBER, LoginControllor.getLoginMember().getId());
            lastQueryTime = getLastFetchTime(HcyMessage.TYPE_ONE);
        } else {
            lastQueryTime = getLastFetchTime(HcyMessage.TYPE_ALL);
        }

        if (lastQueryTime > 0) {
            params.put("date", lastQueryTime);
        }

        HcyHttpClient.submit(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            public Object contentParse(String content) {
                return JSON.parseArray(content, HcyMessage.class);
            }
        });
    }

    /**
     * 保存消息到SharedPreferences，按照个人和全局两个区域保存，未登录时可以查看全局信息，对收到的消息自动进行排重
     *
     * @param aHcyMessages 需要保存的消息列表，从服务器fetch获得
     */
    public void saveMessagesToSp(List<HcyMessage> aHcyMessages) {
        //首先得到Share里面的消息
        List<HcyMessage> hcyMessagesInSp = getHcyMessagesFromSp();

        //然后将原来的消息和得到的消息保存到Share
        saveMessagesToSp(hcyMessagesInSp, aHcyMessages, HcyMessage.TYPE_ALL);
        saveMessagesToSp(hcyMessagesInSp, aHcyMessages, HcyMessage.TYPE_ONE);
    }

    /**
     * 保存消息到SharedPreferences到某个指定区域，按照个人和全局两个区域保存，
     * 未登录时可以查看全局信息，对收到的消息自动进行排重
     *
     * @param hcyMessagesInSp 已经获得的消息
     * @param aHcyMessages    需要保存的消息列表，从服务器fetch获得
     * @param type            保存区域编码
     */
    private void saveMessagesToSp(List<HcyMessage> hcyMessagesInSp, List<HcyMessage> aHcyMessages, String type) {
        //获得某个指定保存区域的SharedPreferences句柄
        SharedPreferences sp = getSharedPreferences(type);

        if (sp != null) {
            //获取消息的数量
            int count = sp.getInt(SP_MSG_COUNT_NAME, 0);

            SharedPreferences.Editor editor = sp.edit();

            long lastFetchTime = 0;
            //遍历从服务器获得的消息
            for (HcyMessage hcyMessage : aHcyMessages) {
                //如果为某一个消息类型并且在指定的消息集合中查看指定的消息是否存在，不存在的话
                if (type.equals(hcyMessage.getType()) && !isExist(hcyMessagesInSp, hcyMessage)) {
                    //设置顺序号？
                    hcyMessage.setOrder(count);
                    //保存为json串
                    editor.putString(SP_MSG_PREFIX_NAME + count, JSON.toJSONString(hcyMessage));

                    //更新请求时间
                    if (hcyMessage.getCreateDate() > lastFetchTime) {
                        lastFetchTime = hcyMessage.getCreateDate();
                    }

                    count++;
                }
            }

            //保存数量
            editor.putInt(SP_MSG_COUNT_NAME, count);

            //保存更新的时间
            if(lastFetchTime>0)
            {
                editor.putLong(SP_MSG_LAST_FETCH_NAME, lastFetchTime);
            }


            editor.apply();
        }
    }

    /**
     * 在指定的消息集合中查看指定的消息是否存在
     *
     * @param hcyMessagesInSp 已经获得的消息
     * @param aHcyMessage     需要确认是否在已经获得的消息中存在的消息
     */
    private boolean isExist(List<HcyMessage> hcyMessagesInSp, HcyMessage aHcyMessage) {
        if (hcyMessagesInSp != null) {
            for (HcyMessage hcyMessage : hcyMessagesInSp) {
                if (hcyMessage.getId() == aHcyMessage.getId()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 在指定的消息集合中是否存在未读消息
     */
    public boolean isAllRead(List<HcyMessage> aHcyMessages) {
        if (aHcyMessages != null && aHcyMessages.size() > 0) {
            for (HcyMessage hcyMessage : aHcyMessages) {
                if (!hcyMessage.isRead()) {
                    return false;
                }
            }
        }
        return true;
    }

    //获取上一次请求时间
    private long getLastFetchTime(String type) {
       // 获得某个指定保存区域的SharedPreferences句柄
        SharedPreferences sp = getSharedPreferences(type);
        return sp.getLong(SP_MSG_LAST_FETCH_NAME, -1);
    }

    /**
     * 查询某个指定区域的消息
     *
     * @param type 保存区域编码
     */
    private List<HcyMessage> getSpMessages(String type) {
        List<HcyMessage> messages = null;
        //获得某个指定保存区域的SharedPreferences句柄
        SharedPreferences sp = getSharedPreferences(type);

        if (sp != null) {
            //获取消息数量
            int count = sp.getInt(SP_MSG_COUNT_NAME, 0);

            for (int i = 0; i < count; i++) {
                //获取json串
                String jsonStr = sp.getString(SP_MSG_PREFIX_NAME + i, null);
                //如果不为空,解析
                if (!TextUtils.isEmpty(jsonStr)) {
                    HcyMessage message = JSON.parseObject(jsonStr, HcyMessage.class);
                    if (messages == null) {
                        messages = new ArrayList<>();
                    }
                    messages.add(message);
                }
            }
        }
        return messages;
    }

    /**
     * 获得某个指定保存区域的SharedPreferences句柄
     *
     * @param type 保存区域编码
     */
    private SharedPreferences getSharedPreferences(String type) {
        SharedPreferences sp = null;
        switch (type) {
            case HcyMessage.TYPE_ALL:
                sp = BaseApplication.getContext().getSharedPreferences(SP_MSG_STORE_PREFIX_NAME + HcyMessage.TYPE_ALL,
                    Context.MODE_PRIVATE);
                break;
            case HcyMessage.TYPE_ONE:
                if (LoginControllor.isLogin()) {
                    sp = BaseApplication.getContext().getSharedPreferences(
                            SP_MSG_STORE_PREFIX_NAME + LoginControllor.getLoginMember().getId(), Context.MODE_PRIVATE);
                }
                break;
        }
        return sp;
    }

    /**
     * 更新某条消息的已读状态
     *
     * @param hcyMessage 需要更新状态的消息
     */
    void updateReadStatus(HcyMessage hcyMessage) {
        SharedPreferences sp = getSharedPreferences(hcyMessage.getType());

        if (sp != null) {
            //设置为已读状态
            hcyMessage.setRead(true);

            SharedPreferences.Editor editor = sp.edit();
            editor.putString(SP_MSG_PREFIX_NAME + hcyMessage.getOrder(), JSON.toJSONString(hcyMessage));
            editor.apply();
        }
    }
}

