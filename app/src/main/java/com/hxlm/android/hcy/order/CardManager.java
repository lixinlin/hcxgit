package com.hxlm.android.hcy.order;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.bean.CardArrayDataBean;
import com.hxlm.android.hcy.bean.CardListBean;
import com.hxlm.android.hcy.user.LoginControllor;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;


public class CardManager {

    /**
     * 获取卡包列表
     * @param callback
     */
    public void getCards(final AbstractDefaultHttpHandlerCallback callback) {
        LoginControllor.requestLogin(callback.getContext(), new OnCompleteListener() {
            @Override
            public void onComplete() {
                String url = "/member/cashcard/list/" + LoginControllor.getLoginMember().getId() + ".jhtml";
                RequestParams params = new RequestParams();
                HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {

                    private List<CardArrayDataBean> cardrrayDataBeans = new ArrayList<>();
                    private CardListBean card = new CardListBean();

                    @Override
                    protected Object contentParse(String content) {
                        Log.e("retrofit","卡包列表信息："+content);

                        try {
                            card = JSON.parseObject(content, CardListBean.class);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return card;
                    }
                }, LoginControllor.MEMBER, callback.getContext());
            }
        });
    }

    void getCardsByProduct(final String ids, final AbstractDefaultHttpHandlerCallback callback) {
        LoginControllor.requestLogin(callback.getContext(), new OnCompleteListener() {
            @Override
            public void onComplete() {
                String url = "/member/cashcard/getcards.jhtml";
                RequestParams params = new RequestParams();
                params.put("productids", ids);

                HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params,
                        new HcyHttpResponseHandler(callback) {
                    @Override
                    protected Object contentParse(String content) {
                        return JSON.parseArray(content, Card.class);
                    }
                }, LoginControllor.MEMBER, callback.getContext());
            }
        });
    }

    /**
     * 绑定现金卡  (添加卡)
     *
     * @param cardCode 现金卡卡号
     */
    public void bindCard(String cardCode, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/cashcard/bind.jhtml";

        RequestParams params = new RequestParams();
        params.put("memberId",LoginControllor.getLoginMember().getId());
        params.put("code", cardCode);
        Log.e("retrofit","===id==="+LoginControllor.getLoginMember().getId());

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                Log.e("retrofit","==添加新卡接口："+content);
                return content;
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

    /**
     * 服务卡激活
     * @param callback
     */
    public void activeCard(String card_no,AbstractDefaultHttpHandlerCallback callback){
        String url = "/md/service_card/active.jhtml";
        RequestParams params = new RequestParams();
        params.put("memberId",LoginControllor.getLoginMember().getId());
        params.put("card_no",card_no);
        Log.i("retrofit","==卡激活卡号==="+card_no);
        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                Log.e("retrofit","==服务卡激活=="+content);
                return content;
            }
        },LoginControllor.MEMBER,callback.getContext());
    }

    /**
     * 卡消费记录
     * @param callback
     */
    public void cardRecord(String card_no,AbstractDefaultHttpHandlerCallback callback){

        String url = "/md/user_record.jhtml";
        RequestParams params = new RequestParams();
        params.put("memberId",LoginControllor.getLoginMember().getId());
        params.put("card_no",card_no);
        Log.e("retrofit","===卡消费记录card_no==="+card_no);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                Log.e("retrofit","卡消费记录："+content);
                return content;
            }
        },LoginControllor.MEMBER,callback.getContext());
    }

    /**
     * 扫描二维码
     * @param code
     * @param callback
     */
    public void getImageCode(String code,AbstractDefaultHttpHandlerCallback callback){
        String url = "/member/cashcard/getCard.jhtml";
        RequestParams requestParams = new RequestParams();
        requestParams.put("imageCode",code);
        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, requestParams, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                Log.e("retrofit","==二维码==："+content);
                return content;
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

}
