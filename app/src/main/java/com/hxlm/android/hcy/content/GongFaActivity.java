package com.hxlm.android.hcy.content;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.report.RecordManager;
import com.hxlm.android.hcy.report.Report;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.IdentityCategory;
import com.hxlm.hcyandroid.bean.GongFaDatas;
import com.hxlm.hcyandroid.view.ImageListAdapter;
import com.hxlm.hcyandroid.view.VoiceGallery;

import net.sourceforge.simcpux.Constants;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class GongFaActivity extends BaseActivity implements OnClickListener {
    private final String tag = getClass().getSimpleName();
    private MediaPlayer mediaPlayer;
    private TitleBarView mTitleView;
    private VoiceGallery mBigGallery;
    private ImageListAdapter mBigAdapter;//adapter
    private TextView mGongFaNameView;
    private TextView gfExplain;// 功法说明
    private TextView mCountView;//顶部显示的是用户选择招式之后的总共有几页
    private TextView mZhaoshi;//顶部显示的是用户选择的第几式
    private ImageView mPlay;
    private ImageView mShifan1;
    private LinearLayout mBofang;
    private ImageView iv_bofangyueyao;
    private ImageView iv_up;
    private LinearLayout mGongfazhaoshi;//顶部的popwindow
    private int selectPostion;
    private int songIndex = 0;
    private int isPlaying = 2;//  1:正在播放 2：停止
    private List<String> audioFiles;
    //用于判断播放哪个示范音
    private int index = 0;
    //判断运动示范音是否播放  1.正在播放  2.停止播放
    private int isPlaySport = 2;

    private List<GongFaType> gongFaTypeList = new ArrayList<GongFaType>();
    private String advice;

    // 获取当前目录下所有的mp3文件
    private List<String> getYueYaoFiles() {
        List<String> vecFile = null;
        File file = new File(Constant.YUEYAO_PATH);

        if (file.exists()) {
            File[] subFile = file.listFiles();

            for (File aSubFile : subFile) {
                // 判断是否为文件夹
                if (!aSubFile.isDirectory()) {
                    String filename = aSubFile.getName();
                    // 判断是否为MP3结尾
                    if (filename.endsWith(".mp3")) {
                        if (vecFile == null) {
                            vecFile = new ArrayList<String>();
                        }
                        vecFile.add(Constant.YUEYAO_PATH + "/" + filename);
                    }
                }
            }
        }
        return vecFile;
    }

    @Override
    public void setContentView() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_gongfa);
    }

    @Override
    public void initViews() {
        if (mTitleView == null) {
            mTitleView = new TitleBarView();
            mTitleView.init(GongFaActivity.this, getString(R.string.gongfa_title), mTitleView, 1, new OnCompleteListener() {
                @Override
                public void onComplete() {
                    //选择完之后，根据子账户获取经络辨识结果，只需要第一条数据
                    int memberChildId = LoginControllor.getChoosedChildMember().getId();
                    new RecordManager().getReportBySn(memberChildId, IdentityCategory.MERIDIAN_IDENTITY,
                            new AbstractDefaultHttpHandlerCallback(GongFaActivity.this) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    List<Report> listReports = (List<Report>) obj;

                                    if (listReports != null && listReports.size() > 0) {
                                        Report report = listReports.get(0);

                                        for (int i = 0; i < gongFaTypeList.size(); i++) {
                                            if (gongFaTypeList.get(i).getType().equals(report.getSubject().getName())) {
                                                //根据经络类型跳转不同的运动样式
                                                refreshByIndex(gongFaTypeList.get(i).getIndex(), false);
                                            }
                                        }
                                    } else {
                                        //如果用户还没有进行经络辨识则默认显示全部
                                        int receivedIndex = getIntent().getIntExtra("gongfa_index", -1) + 1;
                                        refreshByIndex(receivedIndex, false);
                                    }
                                }
                            });
                }
            });
        }


        mBofang = (LinearLayout) findViewById(R.id.bofang);
        mPlay = (ImageView) findViewById(R.id.gongfa_bofang);
        mShifan1 = (ImageView) findViewById(R.id.gongfa_shifan);
        mCountView = (TextView) findViewById(R.id.count_view);
        mZhaoshi = (TextView) findViewById(R.id.gongfa_zhaoshi_zhanshi);
        gfExplain = (TextView) findViewById(R.id.gf_explain);
        mBigGallery = (VoiceGallery) findViewById(R.id.big_gallery);
        LinearLayout mShifan = (LinearLayout) findViewById(R.id.shifan);
        LinearLayout mBofangyueyao = (LinearLayout) findViewById(R.id.bofangyueyao);
        iv_bofangyueyao = (ImageView) findViewById(R.id.gongfa_bofangyueyao);
        iv_up = (ImageView) findViewById(R.id.up_iv);
        mGongFaNameView = (TextView) findViewById(R.id.gf_name);

        mShifan.setOnClickListener(this);
        mBofangyueyao.setOnClickListener(this);
        iv_up.setOnClickListener(this);
        mBofang.setOnClickListener(this);

        //根据不同的经络类型跳转不同的运动样式
        getGongFaTypeList();

        //基本Gallery的设置
        initBigGallery();
        //popwindow的一些设置
        initRedFlag();
        //iv_up遮挡了招式选择
        iv_up.setVisibility(View.GONE);
        //按钮-监听电话
        createPhoneListener();

        //默认第0式选中
        // int receivedIndex = getIntent().getIntExtra("gongfa_index", -1) + 1;
        // refreshByIndex(receivedIndex, false);
    }

    @Override
    public void initDatas() {
        audioFiles = getYueYaoFiles();
        Intent intent = getIntent();
        advice = intent.getStringExtra("advice");
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (!TextUtils.isEmpty(advice)) {
            if (!TextUtils.isEmpty(Constants.YUNDONG_SHI)) {

                for (int i = 0; i < gongFaTypeList.size(); i++) {
                    if (gongFaTypeList.get(i).getType().equals(Constants.YUNDONG_SHI)) {
                        //根据经络类型跳转不同的运动样式
                        refreshByIndex(gongFaTypeList.get(i).getIndex(), false);
                    }
                }
            } else {
                //如果用户还没有进行经络辨识则默认显示全部
                int receivedIndex = getIntent().getIntExtra("gongfa_index", -1) + 1;
                refreshByIndex(receivedIndex, false);
            }
//        }


    }

    private void initBigGallery() {
        if(mBigAdapter==null){
            mBigAdapter = new ImageListAdapter(this);
            mBigGallery.setAdapter(mBigAdapter);
        }
        refreshByIndex( selectPostion,false);
        //Gallery的点击事件以及轮滑事件
        mBigGallery.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int realPosition = mBigAdapter.getRealPosition(position);
                if (TextUtils.isEmpty(GongFaDatas.GONG_FA_EXPLAIN[realPosition])) {
                    gfExplain.setVisibility(View.GONE);
                } else {
                    gfExplain.setText(GongFaDatas.GONG_FA_EXPLAIN[realPosition]);
                    if (gfExplain.getVisibility() == View.VISIBLE) {
                        gfExplain.setVisibility(View.GONE);
                    } else if (gfExplain.getVisibility() == View.GONE) {
                        gfExplain.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        //滑动显示图片下面的内容
        mBigGallery.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                int realPosition = mBigAdapter.getRealPosition(position);

                mGongFaNameView.setText(GongFaDatas.GONG_FA_NAME[realPosition]);
                if (TextUtils.isEmpty(GongFaDatas.GONG_FA_EXPLAIN[realPosition])) {
                    gfExplain.setVisibility(View.GONE);
                } else {
                    gfExplain.setVisibility(View.VISIBLE);
                    gfExplain.setText(GongFaDatas.GONG_FA_EXPLAIN[realPosition]);
                }

                int count = position + 1;
                mCountView.setText(count + "/" + mBigAdapter.getCount());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
              int a = 0;
            }
        });

        mCountView.setText("1/" + mBigAdapter.getCount());
    }

    private void initRedFlag() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.gf_tp_yubeidongzuo_0);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);

        int galleryHeight = dm.heightPixels - getStatusBarHeight() - dip2px(53 + 49);
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        int margin = ((dm.widthPixels - width * galleryHeight / height) / 2);

        params.leftMargin = margin + 5;
        params.bottomMargin = dm.heightPixels - 92 - bitmap.getHeight();

        mGongfazhaoshi = (LinearLayout) findViewById(R.id.gongfa_zhaoshi);
        mGongfazhaoshi.setLayoutParams(params);
        mGongfazhaoshi.setOnClickListener(this);
    }

    private int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private int dip2px(float dpValue) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //点击选择第几式popwindow
            case R.id.gongfa_zhaoshi:
                collapseRedFlag(mZhaoshi);
                break;
            //运动示范音
            case R.id.shifan:
                mZhaoshi.setText(GongFaDatas.mGongFaTypeIcons[0]);//招式名称,这里应该显示“全部”
//                if (isPlaySport == 1){
//                    Log.e("retrofit","停止播放");
//                    mShifan1.setImageResource(R.drawable.gongfa_shifan1);
//                    mBigGallery.isPlayingDemo = false;
//                    mBigGallery.stop();
//                    isPlaySport = 2;
//                }else {
//                    Log.e("retrofit","正在播放");
//                    if (isPlaying == 1) {
//                        stopPlayer();//停止播放
//                        iv_bofangyueyao.setImageResource(R.drawable.gongfa_bofang1);
//                    }
//                    mShifan1.setImageResource(R.drawable.gongfa_shifan2);
//                    mPlay.setImageResource(R.drawable.gongfa_bofang);
//                    mBigGallery.isPlayingDemo = true;
//                    isPlaySport = 1;
//                }
//                mBigGallery.setType(VoiceGallery.CAN_AUTO_SCROLL);
//                initBigGallery();
//                mBofang.setClickable(true);
//                mGongfazhaoshi.setClickable(true);

                if (iv_up.getVisibility() == View.GONE) {
                    if (isPlaying == 1) {
                        stopPlayer();//停止播放
                        iv_bofangyueyao.setImageResource(R.drawable.gongfa_bofang1);
                    }

                    //播放运动示范音的同时，其他功能也可以操作
                    mBofang.setClickable(true);
                    mPlay.setImageResource(R.drawable.gongfa_bofang);
                    iv_up.setVisibility(View.VISIBLE);
                    mShifan1.setImageResource(R.drawable.gongfa_shifan2);
                    mGongfazhaoshi.setClickable(true);//顶部的popwindow不可点击
                    mBigGallery.isPlayingDemo = true;


                } else {
                    iv_up.setVisibility(View.GONE);
                    mShifan1.setImageResource(R.drawable.gongfa_shifan1);
                    mBofang.setClickable(true);
                    mGongfazhaoshi.setClickable(true);
                    mBigGallery.isPlayingDemo = false;
                    mBigGallery.stop();
                }

                mBigGallery.setType(VoiceGallery.CAN_AUTO_SCROLL);
                initBigGallery();

                break;

            //播放乐药
            case R.id.bofangyueyao:
                if (isPlaying == 2) {
                    isPlaySport = 2;
                    iv_up.setVisibility(View.GONE);
                    mShifan1.setImageResource(R.drawable.gongfa_shifan1);
                    mGongfazhaoshi.setClickable(true);
                    mBigGallery.isPlayingDemo = false;
                    mBigGallery.stopVoice();

                    if (audioFiles != null && audioFiles.size() > 0) {
                        //播放乐药
                        songplay();
                    } else {
                        Toast.makeText(GongFaActivity.this, getString(R.string.gongfa_to_download), Toast.LENGTH_SHORT).show();
                        iv_bofangyueyao.setImageResource(R.drawable.gongfa_bofang1);
                    }
                } else {
                    stopPlayer();
                    iv_bofangyueyao.setImageResource(R.drawable.gongfa_bofang1);
                }
                break;

            //抢夺焦点，关闭手动滑动gallery的点击
            case R.id.up_iv:
                break;
            //点击轮播暂停按钮，进行切换是播放还是停止
            case R.id.bofang:
                if (mBigGallery.getType() == VoiceGallery.CAN_AUTO_SCROLL) {
                    mBigGallery.setType(VoiceGallery.CANNOT_AUTO_SCROLL);
                    mPlay.setImageResource(R.drawable.gongfa_zanting);
                } else {
                    mBigGallery.setType(VoiceGallery.CAN_AUTO_SCROLL);
                    mBigGallery.recoverAutoScroll();
                    mPlay.setImageResource(R.drawable.gongfa_bofang);
                }
                break;
            default:
                break;
        }
    }

    private void stopPlayer() {
        isPlaying = 2;
        isPlaySport = 2;
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    private void nextsong() {
        if (songIndex < audioFiles.size() - 1) {
            songIndex = songIndex + 1;
            songplay();
        } else {
            audioFiles.clear();
            songIndex = 0;
        }
    }

    private void songplay() {

        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnErrorListener(new OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    mediaPlayer.reset();
                    return false;
                }
            });
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer.start();
                }
            });
            mediaPlayer.setOnCompletionListener(new CompletionListener());

        } else {
            mediaPlayer.reset();
        }
        try {
            if (audioFiles != null && audioFiles.size() != 0) {
                isPlaying = 1;
                mediaPlayer.setDataSource(audioFiles.get(songIndex));

                mediaPlayer.prepareAsync();
                iv_bofangyueyao.setImageResource(R.drawable.gongfa_bofang2);
            }
        } catch (IOException e) {
            Logger.e(tag, e);
        }
    }

    /**
     * 按钮-监听电话
     */
    public void createPhoneListener() {
        TelephonyManager telephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephony.listen(new OnePhoneStateListener(),
                PhoneStateListener.LISTEN_CALL_STATE);
    }

    @Override
    protected void onDestroy() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mBigGallery.stop();
        super.onDestroy();
    }


    private void refreshByIndex(int index, boolean isClick) {
        Log.e("retrofit", "===运动示范音index===" + index);
        this.index = index;
        //ToastUtil.invokeShortTimeToast(GongFaActivity.this,"isClick-->"+isClick);
        // 如果是点击 菜单切换功法，则不自动轮播
        if (isClick) {
            mBigGallery.setType(VoiceGallery.CANNOT_AUTO_SCROLL);
            mPlay.setImageResource(R.drawable.gongfa_zanting);//显示暂停页面
        }

        mBigAdapter.setGroup_index(index);
        mBigGallery.setSelection(0);
        mBigAdapter.notifyDataSetChanged();

        mGongFaNameView.setText(GongFaDatas.GONG_FA_NAME[mBigAdapter.getRealPosition(0)]);
        mZhaoshi.setText(GongFaDatas.mGongFaTypeIcons[index]);//招式名称
        mCountView.setText("1/" + mBigAdapter.getCount());//总共有多少招式
    }

    /**
     * 显示功法选择的popupwindow
     */
    private void collapseRedFlag(View anchor) {
        LinearLayout convertView = (LinearLayout) LayoutInflater.from(this)
                .inflate(R.layout.popwindow_gongfa,
                        (ViewGroup) getWindow().getDecorView(), false);

        final PopupWindow popwindow = new PopupWindow(convertView, WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT, true);
        popwindow.setAnimationStyle(R.style.dialog);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, 1);

        //显示宫法的名称
        for (int m = 0; m < GongFaDatas.GONGFA_GROUP_NAME.length; m++) {
            final int position = m;

            TextView tv = new TextView(this);

            tv.setBackgroundResource(R.drawable.selector_pop_item);
            tv.setText(GongFaDatas.GONGFA_GROUP_NAME[m]);
            tv.setTextSize(16);
            tv.setPadding(5, 5, 5, 10);
            tv.setLayoutParams(params);
            tv.setTextColor(Color.BLACK);

            ImageView iv = new ImageView(this);
            iv.setLayoutParams(lparams);
            iv.setBackgroundResource(R.drawable.gongfa_pop_item_sep);

            convertView.addView(tv);
            convertView.addView(iv);

            tv.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    popwindow.dismiss();
                    //设置哪一式被选中
                    refreshByIndex(position, true);
                    selectPostion = position;
                    //当前招式+招式数目作为endPosiion；todo
                    int endposition = GONGFA_GROUP[position][0]+GONGFA_GROUP[position][1];
                    mBigGallery.setEndPosion(endposition);
                    mBigGallery.nowPlayingGF =  GONGFA_GROUP[position][0];
                }
            });
        }
        popwindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popwindow.showAsDropDown(anchor, 0, -26);
    }
    public  final int[][] GONGFA_GROUP = {{0, 35}, {0, 2}, {2, 2}, {4, 5}, {9, 5},
            {14, 5}, {19, 5}, {24, 5}, {29, 4}, {33, 2}};

    private final class CompletionListener implements OnCompletionListener {
        @Override
        public void onCompletion(MediaPlayer mp) {
            nextsong();
        }

    }

    /**
     * 电话状态监听.
     */
    class OnePhoneStateListener extends PhoneStateListener {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING://来电状态，电话铃声响起的那段时间或正在通话又来新电，新来电话不得不等待的那段时间
                    if (mediaPlayer != null) {
                        mediaPlayer.pause();
                    }
                    mBigGallery.pause();
                    break;
                case TelephonyManager.CALL_STATE_IDLE://空闲状态，没有任何活动
                    if (mediaPlayer != null) {
                        mediaPlayer.start();
                    }
                    mBigGallery.start();
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK://摘机状态，至少有个电话活动。该活动或是拨打（dialing）或是通话
                    if (mediaPlayer != null) {
                        mediaPlayer.pause();
                    }
                    mBigGallery.pause();
                    break;
                default:
                    break;
            }
            super.onCallStateChanged(state, incomingNumber);
        }
    }


    //根据不同的经络类型跳转不同的运动样式
    private void getGongFaTypeList() {

        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_gong_da), 7));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_gong_jia), 7));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_gong_shang), 7));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_gong_shao), 7));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_gong_zuo), 7));

        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_shang_shang), 3));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_shang_shao), 3));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_shang_tai), 3));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_shang_you), 3));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_shang_zuo), 3));

        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_jue_da), 6));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_jue_pan), 6));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_jue_shang), 6));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_jue_shao), 6));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_jue_tai), 6));

        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_zhi_pan), 5));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_zhi_shang), 5));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_zhi_shao), 5));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_zhi_you), 5));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_zhi_zhi), 5));

        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_yu_da), 4));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_yu_shang), 4));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_yu_shao), 4));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_yu_zhi), 4));
        gongFaTypeList.add(new GongFaType(getString(R.string.gongfa_yu_zhong), 4));

    }
}
