package com.hxlm.android.hcy.message;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.tabbar.usercenter.MyHealthLectureActivity;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * 消息中心
 *
 * @author l
 */
public class MessagesActivity extends BaseActivity implements OnClickListener {
    private ListView listviewnews;
    private RelativeLayout rl_no_news;// 没有消息

    private MessagesAdapter messagesAdapter;
    private List<HcyMessage> hcyMessages;
    private MessageManager messageManager;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_news);
    }

    public void initViews() {
        LinearLayout linear_back_news = (LinearLayout) findViewById(R.id.linear_back_news);
        linear_back_news.setOnClickListener(this);

        rl_no_news = (RelativeLayout) findViewById(R.id.rl_no_news);
        listviewnews = (ListView) findViewById(R.id.listviewnews);
        messagesAdapter = new MessagesAdapter(this);
        // 消息listview
        listviewnews.setAdapter(messagesAdapter);

        // item点击事件
        listviewnews.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HcyMessage hcyMessage = hcyMessages.get(position);

                //更新为已读状态
                messageManager.updateReadStatus(hcyMessage);

                //在指定的消息集合中是否存在未读消息,如果全部已阅读
                if (messageManager.isAllRead(hcyMessages)) {
                    for (WeakReference<TitleBarView> weakTitleBarview : Constant.titleBarViews) {
                        if (weakTitleBarview.get() != null) {
                            weakTitleBarview.get().refreshUnread(false);
                        }
                    }
                }

                switch (hcyMessage.getSendType()) {
//                    case HcyMessage.SEND_TYPE_BESPOKE:
//                        startActivity(new Intent(MessagesActivity.this, MyReservationActivity.class));
//                        MessagesActivity.this.finish();
//                        break;
//                    case HcyMessage.SEND_TYPE_VIDEO:
//                        startActivity(new Intent(MessagesActivity.this, MyVideoReservationActivity.class));
//                        MessagesActivity.this.finish();
//                        break;
                    case HcyMessage.SEND_TYPE_LECTURE:
                        startActivity(new Intent(MessagesActivity.this, MyHealthLectureActivity.class));
                        MessagesActivity.this.finish();
                        break;
                    case HcyMessage.SEND_TYPE_SYSTEM:
                        messagesAdapter.notifyDataSetChanged();
                        break;
                    default:
                        break;
                }
            }
        });
    }

    @Override
    public void initDatas() {
        messageManager = new MessageManager();
        hcyMessages = messageManager.getHcyMessagesFromSp();

        Log.i("HcyHttpResponseHandler", "消息-->" + hcyMessages);

        //进行排序
        if (hcyMessages != null) {
            Collections.sort(hcyMessages);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (hcyMessages == null || hcyMessages.size() == 0) {
            listviewnews.setVisibility(View.GONE);
            rl_no_news.setVisibility(View.VISIBLE);
        } else {
            listviewnews.setVisibility(View.VISIBLE);
            rl_no_news.setVisibility(View.GONE);
            messagesAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linear_back_news:
                finish();
                break;

            default:
                break;
        }
    }

    // 适配器
    class MessagesAdapter extends BaseAdapter {
        LayoutInflater inflater;

        public MessagesAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            if (hcyMessages != null && hcyMessages.size() > 0) {
                return hcyMessages.size();
            } else {
                return 0;
            }

        }

        @Override
        public Object getItem(int position) {
            return hcyMessages.get(position);
        }

        @Override
        public long getItemId(int position) {
            return hcyMessages.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup arg2) {
            Honlder honlder;

            if (convertView == null) {
                honlder = new Honlder();
                convertView = inflater.inflate(R.layout.list_news_item, null);

                honlder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
                honlder.content = (TextView) convertView.findViewById(R.id.tv_content);
                honlder.date = (TextView) convertView.findViewById(R.id.tv_date);
                honlder.img_news = (ImageView) convertView.findViewById(R.id.img_news);
                honlder.iv_right = (ImageView) convertView.findViewById(R.id.iv_right);

                convertView.setTag(honlder);
            } else {
                honlder = (Honlder) convertView.getTag();
            }

            HcyMessage hcyMessage = hcyMessages.get(position);
            // 标题
            honlder.tv_title.setText(hcyMessage.getTitle());
            // 内容
            honlder.content.setText(hcyMessage.getContent());

            // 时间的long
            long createDate = hcyMessage.getCreateDate();
            // 需要当前的年月日，时和分
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date(createDate);
            String strDate = sdf.format(date);
            honlder.date.setText(strDate);

            // 如果是系统消息
            if (HcyMessage.SEND_TYPE_SYSTEM.equals(hcyMessage.getSendType())) {
                honlder.iv_right.setVisibility(View.GONE);
            } else {
                honlder.iv_right.setVisibility(View.VISIBLE);
            }

            if (!hcyMessage.isRead()) {
                honlder.img_news.setVisibility(View.VISIBLE);
            } else {
                honlder.img_news.setVisibility(View.INVISIBLE);
            }
            return convertView;
        }

        class Honlder {
            ImageView img_news;// 新消息
            TextView tv_title;// 标题
            TextView content;// 内容
            TextView date;// 时间
            ImageView iv_right;// 右侧的箭头
        }
    }
}
