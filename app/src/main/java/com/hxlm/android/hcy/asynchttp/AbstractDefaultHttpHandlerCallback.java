package com.hxlm.android.hcy.asynchttp;

import android.app.Dialog;
import android.content.Context;
import android.os.Message;
import android.view.Window;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.ResponseStatus;
import com.hxlm.hcyandroid.util.DialogUtils;
import com.hxlm.hcyandroid.util.ToastUtil;

/**
 * 默认实现基类的部分功能，大多数场景可用
 * Created by Zhenyu on 2016/4/16.
 */
public abstract class AbstractDefaultHttpHandlerCallback extends AbstractHttpHandlerCallback {
    private final String tag = getClass().getSimpleName();
    private final Context context;
    private Dialog waittingDialog;

    //构造方法
    public AbstractDefaultHttpHandlerCallback(Context context) {
        this.context = context;
    }

    //dialog消失
    @Override
    public boolean handleMessage(Message msg) {
        dismissWaittingingDialog();
        super.handleMessage(msg);
        return false;
    }

    private void dismissWaittingingDialog() {
        if (waittingDialog != null && waittingDialog.isShowing()) {
            try {
                waittingDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public Context getContext() {
        return context;
    }

    //弹出加载圈
    @Override
    public void preSubmit() {
        try {
            if (waittingDialog == null&&context!=null) {
                waittingDialog = DialogUtils.displayWaitDialog(context);
            }
            if (!waittingDialog.isShowing()) {
                Window window = waittingDialog.getWindow();
                if(window!=null){
                    waittingDialog.show();
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    //请求错误
    @Override
    protected void onResponseError(int errorCode, String errorDesc) {
        Logger.e(tag, "HCY System Response Error. errcode = " + errorCode + "      desc = " + errorDesc);

        //如果返回44错误，并且请求不为空
        if (errorCode == ResponseStatus.UNLOGIN && httpRequest != null) {
            LoginControllor.clearLastLoginInfo();//清空用户个人信息
            HcyHttpClient.submitAutoLogin(httpRequest, context);//重新登录并提交信息
        } else {
            ToastUtil.invokeShortTimeToast(context, errorDesc);//否则弹出错误信息
        }
    }


    @Override
    protected void onHttpError(int httpErrorCode) {
        Logger.e(tag, "Http Response Error. http status code = " + httpErrorCode);
        ToastUtil.invokeShortTimeToast(context, context.getString(R.string.server_error));
    }

    @Override
    protected void onNetworkError() {
        Logger.e(tag, "Network Error.");
        ToastUtil.invokeShortTimeToast(context, context.getString(R.string.check_net));
    }

    @Override
    protected void onProgress(int bytesWritten, int totalSize) {}
}
