package com.hxlm.android.hcy.content;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.health.AbstractBaseActivity;
import com.hxlm.hcyandroid.BaseActivity;

/**
 * Created by l on 2016/12/21.
 */
public class YueYaoFirstSplashActivity extends AbstractBaseActivity implements View.OnClickListener{
    private ImageView img_yueyao_know;
    @Override
    public void setContentView() {
        setContentView(R.layout.layput_yueyao_first_splash);
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, "", titleBar, 1);

        img_yueyao_know=(ImageView)findViewById(R.id.img_yueyao_know);
        img_yueyao_know.setOnClickListener(this);
    }

    @Override
    public void initDatas() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.img_yueyao_know:
                YueYaoFirstSplashActivity.this.finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
        }
    }
}