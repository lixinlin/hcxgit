package com.hxlm.hcyandroid.tabbar.expertconsult;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.tabbar.expertconsult.ImageGridAdapter.TextCallback;
import com.hxlm.hcyandroid.util.AlbumHelper;
import com.hxlm.hcyandroid.util.Bimp;
import com.hxlm.hcyandroid.view.ImageItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


/**
 * 显示文件夹里面的具体图片
 *
 * @author dell
 */

public class ImageGridActivity extends Activity {
    public static final String EXTRA_IMAGE_LIST = "imagelist";

    // ArrayList<Entity> dataList;
    List<ImageItem> dataList;
    GridView gridView;
    ImageGridAdapter adapter;
    AlbumHelper helper;
    TextView bt;

    ImageView img_back;

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    Toast.makeText(ImageGridActivity.this, "最多选择100张图片", Toast.LENGTH_LONG).show();
                    break;

                default:
                    break;
            }
        }
    };

    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_image_grid);

        sp = getSharedPreferences("pub", Context.MODE_PRIVATE);

        helper = AlbumHelper.getHelper();
        helper.init(getApplicationContext());

        dataList = (List<ImageItem>) getIntent().getSerializableExtra(
                EXTRA_IMAGE_LIST);

        initView();
        // 点击完成按钮
        bt = (TextView) findViewById(R.id.bt);
        bt.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                ArrayList<String> list = new ArrayList<String>();
                Collection<String> c = adapter.map.values();
                Iterator<String> it = c.iterator();
                for (; it.hasNext(); ) {
                    list.add(it.next());
                }

                if (Bimp.act_bool) {
                    /*
                     * Intent intent = new
					 * Intent(ImageGridActivity.this,PublishedActivity.class);
					 * Bundle bundle = new Bundle();
					 * bundle.putBoolean("pubsort", true);
					 * intent.putExtras(bundle); startActivity(intent);
					 */
                    Editor editor = sp.edit();
                    editor.putBoolean("pubsort", true);
                    editor.commit();

                    Bimp.act_bool = false;
                }
                for (int i = 0; i < list.size(); i++) {
                    //这里限制上传图片的个数，而且在ImageGridAdapter也必须要设置
                    if (Bimp.drr.size() < 100) {
                        Bimp.drr.add(list.get(i));
                    }
                }
                finish();
            }

        });
    }

    // 初始化信息
    private void initView() {

        // 点击返回按钮
        img_back = (ImageView) findViewById(R.id.img_back);
        img_back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                //清空数据
                /*Bimp.bmp.clear();
				Bimp.drr.clear();
				Bimp.max = 0;
				FileUtils.deleteDir();*/

                if (Bimp.act_bool) {

					/*
					 * Intent intent = new
					 * Intent(ImageGridActivity.this,PublishedActivity.class);
					 * Bundle bundle = new Bundle();
					 * bundle.putBoolean("pubsort", true);
					 * intent.putExtras(bundle); startActivity(intent);
					 */
                    SharedPreferences sp = getSharedPreferences("pub",
                            Context.MODE_PRIVATE);
                    Editor editor = sp.edit();
                    editor.putBoolean("pubsort", true);
                    editor.commit();

                    Bimp.act_bool = false;
                }

                finish();

            }
        });

        gridView = (GridView) findViewById(R.id.gridview);
        gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));

        // 适配
        adapter = new ImageGridAdapter(ImageGridActivity.this, dataList,
                mHandler);
        gridView.setAdapter(adapter);
        adapter.setTextCallback(new TextCallback() {
            public void onListen(int count) {
                if (count == 0) {

                    bt.setText("完成");

                } else {
                    bt.setText("完成" + "(" + count + ")");
                }

            }
        });

        // item的点击项
        gridView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // if(dataList.get(position).isSelected()){
                // dataList.get(position).setSelected(false);
                // }else{
                // dataList.get(position).setSelected(true);
                // }

                adapter.notifyDataSetChanged();
            }

        });

    }
}
