package com.hxlm.hcyandroid.tabbar.home.visceraidentity;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.*;

public class ICD10OpenHelper extends SQLiteOpenHelper {
    private static String DB_NAME = "ICD10.db";
    private static String ASSETS_NAME = "ICD10.db";
    private Context context;

    public ICD10OpenHelper(Context context) {
        super(context, "ICD10.db", null, 1);
        this.context = context;
        createDataBase();
    }

    public void createDataBase() {
        boolean isDBExist = checkDataBase();
        if (!isDBExist) {
            File dir = new File(getDataBasePath());
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File DBFile = null;
            try {
                DBFile = new File(getDataBasePath() + DB_NAME);
                if (DBFile.exists()) {
                    DBFile.delete();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            SQLiteDatabase.openOrCreateDatabase(DBFile, null);
            copyDataBase();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
    }

    /**
     * 获取数据库路径
     *
     * @return 数据库路径
     */
    public String getDataBasePath() {
        String packageName = context.getPackageName();
        return "/data/data/" + packageName + "/databases/";
    }

    /**
     * 检查本地是否存在该数据库
     *
     * @return 如果存在返回true，否则返回false
     */
    public boolean checkDataBase() {
        String path = getDataBasePath();
        SQLiteDatabase db = null;
        try {
            db = SQLiteDatabase.openDatabase(path + DB_NAME, null,
                    SQLiteDatabase.OPEN_READONLY);
        } catch (SQLiteException e) {
            // TODO: handle exception
        }

        if (db != null) {
            db.close();
        }
        return db != null;
    }

    private void copyDataBase() {
        try {
            InputStream myInput = context.getAssets().open(ASSETS_NAME);
            String outFileName = getDataBasePath() + DB_NAME;
            OutputStream myOutput = new FileOutputStream(outFileName);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }
            myOutput.flush();
            myOutput.close();
            myInput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
