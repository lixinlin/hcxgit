package com.hxlm.hcyandroid.tabbar.sicknesscheck;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.bean.CheckStep;

import java.util.List;

public class StepAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private List<CheckStep> checkSteps;


    public StepAdapter(Context context, List<CheckStep> checkSteps) {
        super();
        this.context = context;
        this.checkSteps = checkSteps;
    }

    @Override
    public int getCount() {
        if (checkSteps.size() == 0) {
            return 0;
        } else {
            return checkSteps.size();
        }


    }

    @Override
    public Object getItem(int position) {
        return checkSteps.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            holder = new Holder();
            inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.ecg_step_item, null);
            holder.iv_step_number = (ImageView) convertView.findViewById(R.id.iv_step_number);
            holder.iv_check_step_img = (ImageView) convertView.findViewById(R.id.iv_check_step_img);
            holder.tv_check_text = (TextView) convertView.findViewById(R.id.tv_check_text);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        CheckStep step = checkSteps.get(position);
        holder.iv_step_number.setImageResource(step.getStepNumber());
        holder.tv_check_text.setText(step.getStepText());

        return convertView;
    }

    class Holder {
        ImageView iv_step_number;
        ImageView iv_check_step_img;
        TextView tv_check_text;
    }

}
