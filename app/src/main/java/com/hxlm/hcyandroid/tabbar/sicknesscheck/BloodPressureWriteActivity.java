package com.hxlm.hcyandroid.tabbar.sicknesscheck;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.report.UploadManager;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.ChooseMemberDialog;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.CheckedDataType;
import com.hxlm.hcyandroid.bean.BloodPressureData;
import com.hxlm.hcyandroid.tabbar.MyHealthFileBroadcast;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ContainsEmojiEditText;
import com.hxlm.hcyphone.MainActivity;

import java.util.List;

public class BloodPressureWriteActivity extends BaseActivity implements
        OnClickListener {

    private ContainsEmojiEditText et_ssy;// 收缩压
    private ContainsEmojiEditText et_szy;// 舒张压
    private ContainsEmojiEditText et_mb;// 脉搏
    private ImageView iv_save;// 保存

    private Context context;
    private String strssy;
    private String strszy;
    private String strmb;
    private String isNormal;// 是否正常
    private UploadManager uploadManager;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_blood_pressure_write);
        context = BloodPressureWriteActivity.this;
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.bpw_title), titleBar, 1);
        et_ssy = (ContainsEmojiEditText) findViewById(R.id.et_ssy);
        et_szy = (ContainsEmojiEditText) findViewById(R.id.et_szy);
        et_mb = (ContainsEmojiEditText) findViewById(R.id.et_mb);
        iv_save = (ImageView) findViewById(R.id.iv_save);
        iv_save.setOnClickListener(this);
    }

    @Override
    public void initDatas() {
        uploadManager = new UploadManager();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_save:

                // 高压收缩压
                strssy = et_ssy.getText().toString();
                // 低压舒张压
                strszy = et_szy.getText().toString();

                strmb = et_mb.getText().toString();

                if (TextUtils.isEmpty(strssy)) {
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.bpw_tips_shousuoya_not_empty));
                }else if(strssy.equals("0")){
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.bpw_tips_input_error));
                } else if (TextUtils.isEmpty(strszy)) {
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.bpw_tips_shuzhangya_not_empty));
                }else if(strszy.equals("0")){
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.bpw_tips_input_error));
                } else if (TextUtils.isEmpty(strmb)) {
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.bpw_tips_maibo_not_empty));
                }else if(strmb.equals("0")){
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.bpw_tips_input_error));
                }
                //如果高压不能超过250，低压不能超过150 脉搏不能超过150
                else
                {
                    if (Integer.parseInt(strssy) > 250) {
                        ToastUtil.invokeShortTimeToast(context, getString(R.string.bpc_range_gaoya));
                    }else if(Integer.parseInt(strszy) > 150)
                    {
                        ToastUtil.invokeShortTimeToast(context, getString(R.string.bpc_range_diya));
                    }else if(Integer.parseInt(strmb)>150)
                    {
                        ToastUtil.invokeShortTimeToast(context, getString(R.string.bpc_range_maibo));
                    }else {
                    // 高压收缩压
                    int intssy = Integer.parseInt(strssy);
                    // 低压舒张压
                    int intszy = Integer.parseInt(strszy);

                    if ((intssy >= 180 && intszy >= 110)
                            || (intssy >= 180 || intszy >= 110)) {
                        isNormal = getString(R.string.bpc_isNormal1);
                    /*
                     * ToastUtil.invokeShortTimeToast(context,
					 * "血压值严重偏高。请注意及时就诊，配合治疗");
					 */
                    } else if (((intssy >= 160 && intssy <= 179) && (intszy >= 100 && intszy <= 109))
                            || ((intssy >= 160 && intssy <= 179) || (intszy >= 100 && intszy <= 109))) {

                        isNormal = getString(R.string.bpc_isNormal2);
                    /*
					 * ToastUtil.invokeShortTimeToast(context,
					 * "血压值过高，请严格调整作息，控制饮食。若身体不适，请注意及时就诊");
					 */
                    } else if (((intssy >= 140 && intssy <= 159) && (intszy >= 90 && intszy <= 99))
                            || ((intssy >= 140 && intssy <= 159) || (intszy >= 90 && intszy <= 99))) {

                        isNormal = getString(R.string.bpc_isNormal3);
					/*
					 * ToastUtil.invokeShortTimeToast(context,
					 * "血压值偏高，请戒烟限酒，限制钠盐的摄入，加强锻炼");
					 */
                    } else if (((intssy >= 120 && intssy <= 139) && (intszy >= 80 && intszy <= 89))
                            || ((intssy >= 120 && intssy <= 139) || (intszy >= 80 && intszy <= 89))) {
                        isNormal = getString(R.string.bpc_isNormal4);
					/*
					 * ToastUtil.invokeShortTimeToast(context,
					 * "血压值正常稍高，请采取健康的生活方式，戒烟限酒，限制钠盐的摄入，加强锻炼，密切关注血压");
					 */
                    } else if ((intssy >= 90 && intssy < 120)
                            && (intszy >= 60 && intszy < 80)) {
                        isNormal = getString(R.string.bpc_isNormal5);
					/*
					 * ToastUtil.invokeShortTimeToast(context,
					 * "血压值正常，建议继续保持当下的健康生活方式，并定期测量血压");
					 */
                    } else if ((intssy > 0 && intssy < 90)
                            && (intszy > 0 && intszy < 60)
                            || ((intssy > 0 && intssy < 90) || (intszy > 0 && intszy < 60))) {
                        isNormal = getString(R.string.bpc_isNormal6);
					/*
					 * ToastUtil.invokeShortTimeToast(context,
					 * "血压值偏低，建议均衡营养，坚持锻炼，改善体质");
					 */
                    }

                    LoginControllor.requestLogin(BloodPressureWriteActivity.this, new OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            List<ChildMember> childMembers = LoginControllor.getLoginMember().getMengberchild();
                            int size=childMembers.size();
                            if(true){
                                BloodPressureData bloodPressureData = new BloodPressureData();
                                bloodPressureData.setHighPressure(strssy);
                                bloodPressureData.setLowPressure(strszy);
                                bloodPressureData.setPulse(strmb);

                                // 输入正常值才上传数据
                                uploadManager.uploadCheckedData(CheckedDataType.BLOOD_PRESSURE, bloodPressureData,0,
                                        new AbstractDefaultHttpHandlerCallback(BloodPressureWriteActivity.this) {
                                            @Override
                                            protected void onResponseSuccess(Object obj) {
                                                new BloodPressureDialog(BloodPressureWriteActivity.this,  getString(R.string.bpw_dialog_text_dangqianmaibo)+ strmb
                                                        + getString(R.string.bpc_unit_count), strssy + "mmHg", strszy + "mmHg", isNormal).show();

                                            }
                                        });
                            }else{
                                new ChooseMemberDialog(BloodPressureWriteActivity.this, new OnCompleteListener() {
                                    @Override
                                    public void onComplete() {


                                        BloodPressureData bloodPressureData = new BloodPressureData();
                                        bloodPressureData.setHighPressure(strssy);
                                        bloodPressureData.setLowPressure(strszy);
                                        bloodPressureData.setPulse(strmb);

                                        // 输入正常值才上传数据
                                        uploadManager.uploadCheckedData(CheckedDataType.BLOOD_PRESSURE, bloodPressureData,0,
                                                new AbstractDefaultHttpHandlerCallback(BloodPressureWriteActivity.this) {
                                                    @Override
                                                    protected void onResponseSuccess(Object obj) {
                                                        new BloodPressureDialog(BloodPressureWriteActivity.this, getString(R.string.bpw_dialog_text_dangqianmaibo) + strmb
                                                                + getString(R.string.bpc_unit_count), strssy + "mmHg", strszy + "mmHg", isNormal).show();

                                                    }
                                                });


                                    }
                                }).show();
                            }

                        }
                    });


                }
                }
                break;

            default:
                break;
        }
    }

    public class BloodPressureDialog extends AlertDialog implements
            OnClickListener {

        Context context;
        String mb;
        String ssy;
        String szy;
        String isnormal;
        String str_tishi = "";



        public BloodPressureDialog(Context context, String mb, String ssy,
                                   String szy, String isnormal) {
            super(context);
            this.context = context;
            this.mb = mb;
            this.ssy = ssy;
            this.szy = szy;
            this.isnormal = isnormal;

        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.bloodpressure_submit_prompt);

            TextView tv_breath_prompt_tishi_mb = (TextView) findViewById(R.id.tv_breath_prompt_tishi_mb);
            tv_breath_prompt_tishi_mb.setText(mb);

            TextView tv_breath_prompt_tishi_ssy = (TextView) findViewById(R.id.tv_breath_prompt_tishi_ssy);
            tv_breath_prompt_tishi_ssy.setText(ssy);

            TextView tv_breath_prompt_tishi_szy = (TextView) findViewById(R.id.tv_breath_prompt_tishi_szy);
            tv_breath_prompt_tishi_szy.setText(szy);

            // 血压
            TextView tv_is_normal = (TextView) findViewById(R.id.tv_is_normal);
            //tv_is_normal.setText("脉搏：60－100次/分");

            //显示高压范围
            TextView text_gaoya=(TextView)findViewById(R.id.text_gaoya);
            text_gaoya.setText("90 < "+getString(R.string.bpw_text_range_gaoya)+"< 140");
            //显示低压范围
            TextView text_diya=(TextView)findViewById(R.id.text_diya);
            text_diya.setText("60 <"+getString(R.string.bpw_text_range_gaoya)+"< 90");


            // 返回检测
            TextView text_back = (TextView) findViewById(R.id.text_back);
            text_back.setOnClickListener(this);

            //查看档案
            TextView text_see_dangan=(TextView) findViewById(R.id.text_see_dangan);
            text_see_dangan.setOnClickListener(this);


//            if ("偏低".equals(isnormal)) {
//                str_tishi = "血压值偏低，建议均衡营养，坚持锻炼，改善体质。";
//                SpannableStringBuilder style = new SpannableStringBuilder(
//                        str_tishi);
//                style.setSpan(
//                        new ForegroundColorSpan(getResources().getColor(
//                                R.color.submit_not_normal)), 3, 5,
//                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                tv_is_normal.setText(style);
//
//            } else if ("正常".equals(isnormal)) {
//                str_tishi = "血压值正常，建议继续保持当下的健康生活方式，并定期测量血压。";
//                SpannableStringBuilder style = new SpannableStringBuilder(
//                        str_tishi);
//                style.setSpan(
//                        new ForegroundColorSpan(getResources().getColor(
//                                R.color.submit_normal)), 3, 5,
//                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                tv_is_normal.setText(style);
//
//            } else if ("正常稍高".equals(isnormal)) {
//                str_tishi = "血压值正常稍高，请采取健康的生活方式，戒烟限酒，限制钠盐的摄入，加强锻炼，密切关注血压。";
//                SpannableStringBuilder style = new SpannableStringBuilder(
//                        str_tishi);
//                style.setSpan(
//                        new ForegroundColorSpan(getResources().getColor(
//                                R.color.submit_not_normal)), 3, 7,
//                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                tv_is_normal.setText(style);
//            } else if ("偏高".equals(isnormal)) {
//                str_tishi = "血压值偏高，请戒烟限酒，限制钠盐的摄入，加强锻炼。";
//                SpannableStringBuilder style = new SpannableStringBuilder(
//                        str_tishi);
//                style.setSpan(
//                        new ForegroundColorSpan(getResources().getColor(
//                                R.color.submit_not_normal)), 3, 5,
//                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                tv_is_normal.setText(style);
//            } else if ("过高".equals(isnormal)) {
//                str_tishi = "血压值过高，请严格调整作息，控制饮食。若身体不适，请注意及时就诊。";
//                SpannableStringBuilder style = new SpannableStringBuilder(
//                        str_tishi);
//                style.setSpan(
//                        new ForegroundColorSpan(getResources().getColor(
//                                R.color.submit_not_normal)), 3, 5,
//                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                tv_is_normal.setText(style);
//            } else if ("严重偏高".equals(isnormal)) {
//                str_tishi = "血压值严重偏高。请注意及时就诊，配合治疗。";
//                SpannableStringBuilder style = new SpannableStringBuilder(
//                        str_tishi);
//                style.setSpan(
//                        new ForegroundColorSpan(getResources().getColor(
//                                R.color.submit_not_normal)), 3, 7,
//                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                tv_is_normal.setText(style);
//            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                // 返回检测
                case R.id.text_back:
                    this.dismiss();
                    BloodPressureWriteActivity.this.finish();
                    break;
                //查看档案
                case R.id.text_see_dangan:
                    this.dismiss();
                    // 动态注册广播使用隐士Intent
                    Intent intent=new Intent(MyHealthFileBroadcast.ACTION);
                    intent.putExtra("ArchivesFragment", "3");
                    intent.putExtra("otherReport", "true");
                    intent.putExtra("aaa","123");
                    intent.putExtra("Jump",6);
                    intent.putExtra("tag","blood_pressure");
                    BloodPressureWriteActivity.this.sendBroadcast(intent);

                    Intent intent2 = new Intent(BloodPressureWriteActivity.this, MainActivity.class);
//                    intent2.putExtra("mainId",2);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//清除MainActivity之前所有的activity
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP); //沿用之前的MainActivity
                    startActivity(intent2);
                    break;
                    default:
                        break;

            }
        }

    }

}
