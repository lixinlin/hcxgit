package com.hxlm.hcyandroid.tabbar.sicknesscheck;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.hcy.ky3h.R;
import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.comm.Error;
import com.hxlm.android.comm.Error_English;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.report.UploadManager;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.ChooseMemberDialog;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;
import com.hxlm.android.health.device.message.ecg.EcgWaveOutputCommand;
import com.hxlm.android.health.device.message.spo2.SpO2DataMessage;
import com.hxlm.android.health.device.model.ChargePal;
import com.hxlm.android.health.device.model.HcyPhone;
import com.hxlm.hcyandroid.CheckedDataType;
import com.hxlm.hcyphone.MainActivity;
import com.hxlm.hcyandroid.tabbar.MyHealthFileBroadcast;
import com.hxlm.hcyandroid.util.ToastUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 血氧检测
 *
 * @author l
 */
public class SPO2HCheckActivity extends AbstractDeviceActivity implements
        OnClickListener {
    private String TAG = "SPO2HCheckActivity";

    private LinearLayout linear_checkxueyang;
    private TextView tv_xueyang;// 当前血氧
    private TextView tv_start_check;// 开始检测
    private ImageView iv_restart_check;// 重新检测

    private ImageView iv_feishebei;// 非设备检测
    private ImageView iv_use_specification;// 使用规范
    private int xueyang;
    private String isNormal;// 是否正常

    private UploadManager uploadManager;
    private ImageView iv_ecg_connect_status;
    private TextView tv_ecg_connect_status;
    private List<Integer> SPO2Datas = new ArrayList<Integer>();
    private HcyPhone hcyPhone;


    private String defaultMiao="30";
    private int defaultMiaoInt=30;
    // 倒计时
    private LinearLayout linear_jishi;// 开始计时
    private TextView tv_time_miao;// 30秒
    private TextView tv_time_s;// 尾数s
    private TextView tv_time_end;// 计时结束
    private Timer timer;
    // 计时器
    private TimerTask timerTask;
    private int time;// 计时


    private boolean isFirstTanTou=true;//血氧探头第一次脱落
    private boolean isFirstWindow=true;//血氧监测窗口
   // private double idenXueYang=0;//标识用户在上传之前的最后一次数据
    private boolean isTrueDisplay=true;//当提交之后时候还要显示接收到的数据

    private int myCount=0;//蓝牙返回数据的次数

    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case -10:
                    tv_time_miao.setText(String.valueOf(time));
                    if (time <= 0) {
                        stopTimer();
                        // 计时结束
                        linear_jishi.setVisibility(View.GONE);
                        tv_time_end.setVisibility(View.VISIBLE);

                        //计时结束之后，提交数据
                        submitSpo2H();
                    }
                    break;
                default:
                    break;
            }
            return false;
        }
    });



    @Override
    public void setContentView() {
        //保持背光常亮的设置方法
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_spo2h_check);

        hcyPhone = new HcyPhone();

        if (hcyPhone.getPowerStatus() != HcyPhone.PowerStatus.UNSUPPORT) {


            try {
                hcyPhone.setPowerStatus(HcyPhone.PowerStatus.ON);

                ioSession = hcyPhone.getIOSession(this);


            } catch (IOException e) {
                ioSession = new ChargePal().getIOSession(this);
                onExceptionCaught(e);
            }
        } else {
            ioSession = new ChargePal().getIOSession(this);
        }
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.spo2_title), titleBar, 1);

        linear_checkxueyang = findViewById(R.id.linear_checkxueyang);
        tv_xueyang = findViewById(R.id.tv_xueyang);
        tv_start_check = findViewById(R.id.tv_start_check);
        iv_restart_check = findViewById(R.id.iv_restart_check);

        iv_feishebei = findViewById(R.id.iv_feishebei);
        iv_feishebei.setOnClickListener(this);
        tv_ecg_connect_status = findViewById(R.id.tv_ecg_connect_status);
        iv_ecg_connect_status = findViewById(R.id.iv_ecg_connect_status);

        linear_jishi = findViewById(R.id.linear_jishi);
        tv_time_miao = findViewById(R.id.tv_time_miao);
        tv_time_s = findViewById(R.id.tv_time_s);
        tv_time_end = findViewById(R.id.tv_time_end);



        iv_use_specification = findViewById(R.id.iv_use_specification);
        iv_use_specification.setOnClickListener(this);


        tv_start_check.setOnClickListener(this);
        iv_restart_check.setOnClickListener(this);
    }

    // 开始计时
    public void startTimer() {
        time = defaultMiaoInt;
        if (timer == null) {
            timer = new Timer();
        }
        if (timerTask == null) {
            timerTask = new TimerTask() {

                @Override
                public void run() {
                    time--;
                    Message message = handler.obtainMessage(-10);
                    handler.sendMessage(message);
                }
            };
        }

        if (timer != null)
            timer.schedule(timerTask, 1000, 1000);
    }

    // 停止计时
    public void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        if (timerTask != null) {
            timerTask.cancel();
            timerTask = null;
        }

        time = 0;
    }

    @Override
    public void initDatas() {
        uploadManager = new UploadManager();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStop() {



        if(timer!=null)
        {
            //关闭计时器
            stopTimer();
        }
        super.onStop();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            // 开始检测
            case R.id.tv_start_check:
                myCount=0;//重新进行设置

                if (ioSession == null) {
                    ToastUtil.invokeShortTimeToast(SPO2HCheckActivity.this, getString(R.string.spo2_equip_unconnect));
                }else if (ioSession != null) {

                 if (ioSession.status != AbstractIOSession.Status.CONNECTED) {
                       // ToastUtil.invokeShortTimeToast(this, "开始连接血氧检测设备......");
                     iv_ecg_connect_status.setImageResource(R.drawable.ecg_not_connect);
                     tv_ecg_connect_status.setText(getString(R.string.spo2_connecting));
                        ioSession.connect();

                    }else
                    {
                        if(getString(R.string.no_access).equals(tv_ecg_connect_status.getText().toString()))
                        {
                            ToastUtil.invokeShortTimeToast(SPO2HCheckActivity.this,getString(R.string.spo2_equip_unconnect));
                        }else if(getString(R.string.bpc_connected).equals(tv_ecg_connect_status.getText().toString()))
                        {

                            isTrueDisplay=true;//显示接收数据

                            tv_start_check.setVisibility(View.GONE);
                            linear_checkxueyang.setVisibility(View.VISIBLE);
                            iv_restart_check.setVisibility(View.VISIBLE);//重新检测

                            tv_time_miao.setText(defaultMiao);
                            if(timer!=null)
                            {
                                //关闭计时器
                                stopTimer();
                            }
                            // 开始计时
                            startTimer();
                            tv_time_end.setVisibility(View.GONE);
                            linear_jishi.setVisibility(View.VISIBLE);


                        }
                    }
                }
                break;
            // 非设备检测
            case R.id.iv_feishebei:
                Intent i = new Intent(SPO2HCheckActivity.this,
                        SPO2HWriteActivity.class);
                startActivity(i);
                break;
            //重新检测
            case R.id.iv_restart_check:

                myCount=0;//重新进行设置

                tv_xueyang.setText("");
                if (ioSession == null) {
                    iv_ecg_connect_status.setImageResource(R.drawable.ecg_not_connect);
                    tv_ecg_connect_status.setText(getString(R.string.no_access));
                }else  if (ioSession != null) {
                    if (ioSession.status != AbstractIOSession.Status.CONNECTED) {
                        iv_ecg_connect_status.setImageResource(R.drawable.ecg_not_connect);
                        tv_ecg_connect_status.setText(getString(R.string.spo2_connecting));
                        ioSession.connect();
                    }else
                    {
                        if(getString(R.string.no_access).equals(tv_ecg_connect_status.getText().toString()))
                        {
                           // ToastUtil.invokeShortTimeToast(SPO2HCheckActivity.this,"血氧检测设备未连接！");
                            iv_ecg_connect_status.setImageResource(R.drawable.ecg_not_connect);
                            tv_ecg_connect_status.setText(getString(R.string.no_access));
                        }else if(getString(R.string.bpc_connected).equals(tv_ecg_connect_status.getText().toString()))
                        {

                            isTrueDisplay=true;//显示接收数据

                            // 当前的倒计时没有结束，又点击重新检测，先将当前的time设置为0
                            if (time > 0) {
                                // 先停止计时
                                stopTimer();

                                tv_time_miao.setText(defaultMiao);
                                // 在重新计时
                                startTimer();

                                tv_time_end.setVisibility(View.GONE);
                                linear_jishi.setVisibility(View.VISIBLE);

                            }
                            // 当前的计时已经结束
                            else {

                                tv_time_miao.setText(defaultMiao);

                                // 重新开始计时
                                startTimer();
                                tv_time_end.setVisibility(View.GONE);
                                linear_jishi.setVisibility(View.VISIBLE);

                            }
                        }

                    }
                }
                break;
            // 使用规范
            case R.id.iv_use_specification:
                Intent intent = new Intent(SPO2HCheckActivity.this,
                        SPO2HUseSpecificatioinActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    //倒计时结束，提交数据
    private void submitSpo2H()
    {
        String str_xueyang = tv_xueyang.getText().toString();

        if (TextUtils.isEmpty(str_xueyang)) {
            ToastUtil.invokeShortTimeToast(this, getString(R.string.spo2_empty));

        }  else {
           // idenXueYang = Double.parseDouble(str_xueyang);//上传之前的最后一次数据

            isTrueDisplay=false;//不显示接收数据

            int SPO2Sum = 0;
            for (int data : SPO2Datas) {
                SPO2Sum += data;
            }
            //  xueyang = SPO2Sum / SPO2Datas.size() + SPO2Sum % SPO2Datas.size();
            xueyang = SPO2Sum / SPO2Datas.size();


            xueyang=Math.round(xueyang);//进行四舍五入
            tv_xueyang.setText(xueyang+"");
            if (xueyang >= 95 && xueyang <= 100) {
                isNormal = getString(R.string.bpc_isNormal5);
            } else if (xueyang >= 0 && xueyang < 95) {
                isNormal = getString(R.string.bpc_isNormal6);
            }

            LoginControllor.requestLogin(SPO2HCheckActivity.this, new OnCompleteListener() {
                @Override
                public void onComplete() {
                    List<ChildMember> childMembers = LoginControllor.getLoginMember().getMengberchild();
                    int size=childMembers.size();
                    if(true){
                        // 输入正常值才上传数据
                        uploadManager.uploadCheckedData(CheckedDataType.SPO2H, String.valueOf(xueyang/100.0),0,
                                new AbstractDefaultHttpHandlerCallback(SPO2HCheckActivity.this) {
                                    @Override
                                    protected void onResponseSuccess(Object obj) {
                                        SPO2HWriteDialog spo2HWriteDialog =new SPO2HWriteDialog(SPO2HCheckActivity.this,
                                                getString(R.string.spo2_oxygen_value) + xueyang, isNormal);
                                        spo2HWriteDialog.setCanceledOnTouchOutside(false);
                                        spo2HWriteDialog.show();

                                    }
                                });
                    }else{
                        new ChooseMemberDialog(SPO2HCheckActivity.this, new OnCompleteListener() {
                            @Override
                            public void onComplete() {

                                // 输入正常值才上传数据
                                uploadManager.uploadCheckedData(CheckedDataType.SPO2H, String.valueOf(xueyang/100),0,
                                        new AbstractDefaultHttpHandlerCallback(SPO2HCheckActivity.this) {
                                            @Override
                                            protected void onResponseSuccess(Object obj) {
                                                SPO2HWriteDialog spo2HWriteDialog =new SPO2HWriteDialog(SPO2HCheckActivity.this,
                                                        getString(R.string.spo2_oxygen_value) + xueyang, isNormal);
                                                spo2HWriteDialog.setCanceledOnTouchOutside(false);
                                                spo2HWriteDialog.show();

                                            }
                                        });

                            }
                        }).show();
                    }

                }
            });



        }
    }

    @Override
    public void onConnected() {
        //ToastUtil.invokeShortTimeToast(this, "设备已连接");
        ioSession.sendMessage(new EcgWaveOutputCommand(false));

        iv_ecg_connect_status.setImageResource(R.drawable.ecg_connect);
        tv_ecg_connect_status.setText(getString(R.string.bpc_connected));


        isTrueDisplay=true;//显示接收数据
        //连接之后开始计时
        tv_start_check.setVisibility(View.GONE);
        linear_checkxueyang.setVisibility(View.VISIBLE);
        iv_restart_check.setVisibility(View.VISIBLE);//重新检测

        tv_time_miao.setText(defaultMiao);
        if(timer!=null)
        {
            //关闭计时器
            stopTimer();
        }
        // 开始计时
        startTimer();
        tv_time_end.setVisibility(View.GONE);
        linear_jishi.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDisconnected() {
       // ToastUtil.invokeShortTimeToast(SPO2HCheckActivity.this, "设备已断开");
        iv_ecg_connect_status.setImageResource(R.drawable.ecg_not_connect);
        tv_ecg_connect_status.setText(getString(R.string.no_access));

        tv_start_check.setVisibility(View.VISIBLE);
        linear_checkxueyang.setVisibility(View.GONE);
        iv_restart_check.setVisibility(View.GONE);
        if(timer!=null)
        {
            stopTimer();
        }
        linear_jishi.setVisibility(View.GONE);
        tv_time_end.setVisibility(View.VISIBLE);
        try {
            hcyPhone.setPowerStatus(HcyPhone.PowerStatus.OFF);
        } catch (IOException e) {
            onExceptionCaught(e);
        }
    }

    @Override
    public void onExceptionCaught(Throwable e) {
        Log.e(TAG, e.getMessage());
    }

    @Override
    public void onMessageReceived(AbstractMessage message) {

       // Log.d(TAG, "message received..........." + message.toString());

        ioSession.sendMessage(new EcgWaveOutputCommand(false));

        switch ((HealthDeviceMessageType) message.getMessageType()) {
            case SPO2_DATA:
                SpO2DataMessage spO2DataMessage = (SpO2DataMessage) message;
                Log.d(TAG, "spO2DataMessage.getItemStatus() = " + spO2DataMessage.getStatus());
                if (spO2DataMessage.getStatus() == 0x01) {
                    if(isFirstTanTou)
                    {
                       // Toast.makeText(SPO2HCheckActivity.this, "血氧探头脱落", Toast.LENGTH_SHORT).show();
                        isFirstTanTou=false;
                    }

                } else if (spO2DataMessage.getStatus() == 0x02) {
                    if(isFirstWindow)
                    {
                        Toast.makeText(SPO2HCheckActivity.this, getString(R.string.spo2_tips), Toast.LENGTH_SHORT).show();
                        isFirstWindow=false;
                    }

                } else if (spO2DataMessage.getStatus() == 0x03) {
//                    Toast.makeText(SPO2HCheckActivity.this, "正在搜索脉搏",
//                            Toast.LENGTH_SHORT).show();
                } else if (spO2DataMessage.getStatus() == 0x04) {
//                    Toast.makeText(SPO2HCheckActivity.this, "脉搏搜索时间过长",
//                            Toast.LENGTH_SHORT).show();
                } else if (spO2DataMessage.getStatus() == 0x00) {
                    if (spO2DataMessage.getSaturate() > 0) {


                        if(isTrueDisplay==true)//为true显示接收数据
                        {
                            isFirstTanTou=true;
                            isFirstWindow=true;
                            tv_xueyang.setText(String.valueOf(Math.round(spO2DataMessage.getSaturate())));
                            SPO2Datas.add(spO2DataMessage.getSaturate());


                            myCount++;
                            if(myCount==5)
                            {
                                if(timer!=null)
                                {
                                    //关闭计时器
                                    stopTimer();
                                }
                                // 计时结束
                                linear_jishi.setVisibility(View.GONE);
                                tv_time_end.setVisibility(View.VISIBLE);

                                //计时结束之后，提交数据
                                submitSpo2H();
                            }
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            hcyPhone.setPowerStatus(HcyPhone.PowerStatus.OFF);
        } catch (IOException e) {
            onExceptionCaught(e);
        }


    }

    @Override
    public void onConnectFailed(Error error) {
        if (hcyPhone.getPowerStatus() == HcyPhone.PowerStatus.ON) {
            try {
                hcyPhone.setPowerStatus(HcyPhone.PowerStatus.OFF);
            } catch (IOException e) {
                onExceptionCaught(e);
            }

            Toast.makeText(this, error.getDesc(), Toast.LENGTH_SHORT).show();

            ioSession = new ChargePal().getIOSession(this);
            ioSession.connect();
        } else {
            Toast.makeText(this, error.getDesc(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onConnectFailedEnglist(Error_English error) {
        if (hcyPhone.getPowerStatus() == HcyPhone.PowerStatus.ON) {
            try {
                hcyPhone.setPowerStatus(HcyPhone.PowerStatus.OFF);
            } catch (IOException e) {
                onExceptionCaught(e);
            }

            Toast.makeText(this, error.getDesc(), Toast.LENGTH_SHORT).show();

            ioSession = new ChargePal().getIOSession(this);
            ioSession.connect();
        } else {
            Toast.makeText(this, error.getDesc(), Toast.LENGTH_SHORT).show();
        }
    }

    public class SPO2HWriteDialog extends AlertDialog implements
            OnClickListener {

        Context context;
        String tishi;
        String isnormal;
        String str_tishi = "";

        public SPO2HWriteDialog(Context context, String tishi, String isnormal) {
            super(context);
            this.context = context;
            this.tishi = tishi;
            this.isnormal = isnormal;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.spo2hwrite_submit_prompt);

            TextView tv_breath_prompt_tishi = findViewById(R.id.tv_breath_prompt_tishi);
            tv_breath_prompt_tishi.setText(tishi);

            // 血氧
            TextView tv_is_normal = findViewById(R.id.tv_is_normal);

            // 返回检测
            TextView text_back = findViewById(R.id.text_back);
            text_back.setOnClickListener(this);

            //查看档案
            TextView text_see_dangan= findViewById(R.id.text_see_dangan);
            text_see_dangan.setOnClickListener(this);

//            if ("正常".equals(isnormal)) {
//                str_tishi = "血氧饱和度正常";
//                SpannableStringBuilder style = new SpannableStringBuilder(
//                        str_tishi);
//                style.setSpan(
//                        new ForegroundColorSpan(getResources().getColor(
//                                R.color.submit_normal)), 5, 7,
//                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                tv_is_normal.setText(style);
//
//            } else if ("偏低".equals(isnormal)) {
//                str_tishi = "血氧饱和度偏低。建议监测，若持续偏低，请及时到医院就诊。";
//                SpannableStringBuilder style = new SpannableStringBuilder(
//                        str_tishi);
//                style.setSpan(
//                        new ForegroundColorSpan(getResources().getColor(
//                                R.color.submit_not_normal)), 5, 7,
//                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                tv_is_normal.setText(style);
//            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                // 返回检测
                case R.id.text_back:
                    this.dismiss();

                    break;
                //查看档案
                case R.id.text_see_dangan:
                    this.dismiss();
                    // 动态注册广播使用隐士Intent
                    Intent intent=new Intent(MyHealthFileBroadcast.ACTION);
                    intent.putExtra("ArchivesFragment", "3");
                    intent.putExtra("otherReport", "true");
                    intent.putExtra("Jump",7);
                    SPO2HCheckActivity.this.sendBroadcast(intent);

                    Intent intent2 = new Intent(SPO2HCheckActivity.this, MainActivity.class);

                    intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//清除MainActivity之前所有的activity
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP); //沿用之前的MainActivity
                    startActivity(intent2);
                    break;
                default:
                    break;
            }
        }
    }
}
