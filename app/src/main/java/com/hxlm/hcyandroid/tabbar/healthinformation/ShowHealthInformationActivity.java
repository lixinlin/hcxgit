package com.hxlm.hcyandroid.tabbar.healthinformation;

import android.content.Intent;
import android.webkit.WebView;
import android.widget.ProgressBar;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.bean.HealthInformation;
import com.hxlm.hcyandroid.util.WebViewUtil;

public class ShowHealthInformationActivity extends BaseActivity {

    private ProgressBar progressBar;
    private WebView wv_show;
    private HealthInformation information;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_show_health_information);
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.show_health_title) ,titleBar, 1);

    }

    @Override
    public void initDatas() {
        progressBar=(ProgressBar)findViewById(R.id.progressbar);
        wv_show = (WebView) findViewById(R.id.wv_show);
        Intent intent = getIntent();
        information = intent.getParcelableExtra("healthInformation");

//        WebSettings settings = wv_show.getSettings();
//        settings.setUseWideViewPort(true);
//        settings.setLoadWithOverviewMode(true);
//        settings.setJavaScriptEnabled(true);
//        settings.setSupportZoom(true);

//        wv_show.loadUrl(Constant.BASE_URL + information.getPath());
//        wv_show.setWebViewClient(new WebViewClient() {
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl(url);
//                return true;
//            }
//        });

        new WebViewUtil().setWebViewInit(wv_show,progressBar,this,Constant.BASE_URL + information.getPath());
    }

}

