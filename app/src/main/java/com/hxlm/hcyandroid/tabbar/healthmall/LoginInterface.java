package com.hxlm.hcyandroid.tabbar.healthmall;

import android.os.Handler;
import android.webkit.JavascriptInterface;

public class LoginInterface {
    public static final int LOGIN = 100;
    private Handler handler;

    public LoginInterface(Handler handler) {
        super();
        this.handler = handler;
    }

    @JavascriptInterface
    public void login(final String url) {
        handler.obtainMessage(LOGIN, url).sendToTarget();
    }
}
