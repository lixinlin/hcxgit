package com.hxlm.hcyandroid.tabbar.sicknesscheck;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.report.RecordManager;
import com.hxlm.android.hcy.report.UploadManager;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.ChooseMemberDialog;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.CheckedDataType;
import com.hxlm.hcyandroid.bean.BloodSugar;
import com.hxlm.hcyandroid.bean.BloodSugarData;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ContainsEmojiEditText;
import com.hxlm.hcyandroid.view.XRTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 血糖检测
 *
 * @author l
 */
public class BloodSugarCheckActivity extends BaseActivity implements
        OnClickListener {

    double value_fasting = 0;// 空腹
    int num_fasting = -1;// 空腹血糖是否正常
    double value_after_meal = 0;// 饭后
    int num_after_meal = -1;// 饭后血糖是否正常
    private XRTextView tv_bloodsugar_prompt_cishu;//提交次数
    private XRTextView tv_bloodsugar_prompt_xuetang;//血糖值
    private ImageView iv_no_eating;
    private ImageView iv_before_eating;
    private ImageView iv_after_eating;
    private ContainsEmojiEditText et_blood_sugar_value;
    private ImageView iv_commit;
    private ImageView iv_use_specification;//使用规范
    private ListView lv_sick_BloodSugar;// 血糖
    private List<BloodSugarText> list;
    private Context context;
    private int index = 0;// 设置下标
    private int index_fasting = 0;// 空腹
    private int index_after_meal = 0;// 餐后
    private int num_submit = 0;// 记录用户提交的次数
    private int fasting_num = 0;// 空腹提交次数

    private int after_meal_num = 0;// 饭后提交次数
    private int normal_fasting = 0;// 空腹正常次数
    private int not_normal_fasting = 0;// 空腹不正常次数

    private int normal_after_meal = 0;// 饭后正常次数
    private int not_normal_after_meal = 0;// 饭后不正常次数

    private Dialog waittingDialog;

    private String isNormal;// 是否正常

    private Map<String, List<BloodSugar>> map;
    private List<BloodSugar> emptyData;// 空腹值
    private List<BloodSugar> fullData;// 饭后值

    private RecordManager recordManager;
    private UploadManager uploadManager;

    private BloodSugarData bloodSugarData;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_blood_sugar_check);
        context = BloodSugarCheckActivity.this;
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.bsc_title), titleBar, 1);
        iv_no_eating = (ImageView) findViewById(R.id.iv_no_eating);
        // iv_before_eating = (ImageView) findViewById(R.id.iv_before_eating);
        iv_after_eating = (ImageView) findViewById(R.id.iv_after_eating);
        et_blood_sugar_value = (ContainsEmojiEditText) findViewById(R.id.et_blood_sugar_value);
        iv_commit = (ImageView) findViewById(R.id.iv_commit);
        iv_use_specification = (ImageView) findViewById(R.id.iv_use_specification);
        iv_use_specification.setOnClickListener(this);

        iv_no_eating.setOnClickListener(this);
        iv_no_eating
                .setImageResource(R.drawable.blood_sugar_check_no_eating_checked);
        // iv_before_eating.setOnClickListener(this);
        iv_after_eating.setOnClickListener(this);
        iv_commit.setOnClickListener(this);


        index = 1;// 默认为空腹

    }

    @Override
    public void initDatas() {
        recordManager = new RecordManager();
        uploadManager = new UploadManager();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //使用规范
            case R.id.iv_use_specification:
                Intent intent = new Intent(BloodSugarCheckActivity.this, BloodSugarUseSpecificationActivity.class);
                startActivity(intent);
                break;

            case R.id.iv_no_eating:
                index = 1;
                cleanChecked();
                iv_no_eating
                        .setImageResource(R.drawable.blood_sugar_check_no_eating_checked);
                et_blood_sugar_value.setText("");
                break;
        /*
         * case R.id.iv_before_eating: index=2; cleanChecked();
		 * iv_before_eating.
		 * setImageResource(R.drawable.blood_sugar_check_before_eating_checked);
		 * break;
		 */
            case R.id.iv_after_eating:
                index = 2;
                cleanChecked();
                iv_after_eating
                        .setImageResource(R.drawable.blood_sugar_check_after_eating_checked);
                et_blood_sugar_value.setText("");
                break;
            // 提交
            case R.id.iv_commit:

                String str_blood_sugar_value = et_blood_sugar_value.getText().toString();
                if (!TextUtils.isEmpty(str_blood_sugar_value)) {
                    if (Double.parseDouble(str_blood_sugar_value) > 100) {
                        ToastUtil.invokeShortTimeToast(context, getString(R.string.bsc_tips_sugar_more_range));
                    }
                    // 点击提交
                    else {
                        num_submit++;// 次数加1
                        // 将总提交次数保存
                        SharedPreferenceUtil.saveString("num_submit", num_submit + "");

                        bloodSugarData = new BloodSugarData();

                        // 只是选择了空腹提交
                        if (index == 1) {
                            fasting_num++;
                            // 空腹提交总次数
                            SharedPreferenceUtil.saveString("fasting_num", fasting_num + "");

                            // 记录空腹数据
                            final double double_blood_sugar_value1 = Double.parseDouble(str_blood_sugar_value);
                            value_fasting = double_blood_sugar_value1;

                            bloodSugarData.setType(BloodSugarData.BLOOD_SUGAR_TYPE_EMPTY);
                            bloodSugarData.setLevels(double_blood_sugar_value1);

                            if (double_blood_sugar_value1 > 0 && double_blood_sugar_value1 < 3.9) {

                                isNormal = getString(R.string.bsc_isNormal1);
                                not_normal_fasting++;
                                SharedPreferenceUtil.saveString("not_normal_fasting", not_normal_fasting + "");

                                bloodSugarData.setAbnormity(true);

                            } else if (double_blood_sugar_value1 >= 3.9 && double_blood_sugar_value1 <= 6.1) {
                                isNormal = getString(R.string.bsc_isNormal2);

                                normal_fasting++;
                                SharedPreferenceUtil.saveString("normal_fasting", normal_fasting + "");

                                bloodSugarData.setAbnormity(false);

                            } else if (double_blood_sugar_value1 > 6.1) {

                                isNormal = getString(R.string.bsc_isNormal3);
                                not_normal_fasting++;
                                SharedPreferenceUtil.saveString("not_normal_fasting", not_normal_fasting + "");

                                bloodSugarData.setAbnormity(true);
                            }
                        } else if (index == 2) {
                            after_meal_num++;
                            // 饭后提交总次数
                            SharedPreferenceUtil.saveString("after_meal_num", after_meal_num + "");

                            // 饭后数据
                            final double double_blood_sugar_value2 = Double.parseDouble(str_blood_sugar_value);
                            value_after_meal = double_blood_sugar_value2;

                            bloodSugarData.setType(BloodSugarData.BLOOD_SUGAR_TYPE_FULL);
                            bloodSugarData.setLevels(double_blood_sugar_value2);

                            if (double_blood_sugar_value2 > 0 && double_blood_sugar_value2 <= 7.8) {
                                isNormal = getString(R.string.bsc_isNormal2);

                                normal_after_meal++;
                                SharedPreferenceUtil.saveString("normal_after_meal", normal_after_meal + "");

                                bloodSugarData.setAbnormity(false);

                            } else if (double_blood_sugar_value2 > 7.8) {
                                isNormal = getString(R.string.bsc_isNormal3);

                                not_normal_after_meal++;
                                SharedPreferenceUtil.saveString("not_normal_after_meal", not_normal_after_meal + "");

                                bloodSugarData.setAbnormity(true);
                            }
                        }

                        LoginControllor.requestLogin(BloodSugarCheckActivity.this, new OnCompleteListener() {
                            @Override
                            public void onComplete() {
                                List<ChildMember> childMembers = LoginControllor.getLoginMember().getMengberchild();
                                int size=childMembers.size();
                                if(true){
                                    uploadManager.uploadCheckedData(CheckedDataType.BLOOD_SUGAR, bloodSugarData, 0,
                                            new AbstractDefaultHttpHandlerCallback(BloodSugarCheckActivity.this) {
                                                @Override
                                                protected void onResponseSuccess(Object obj) {
                                                    // 提交成功之后，获取空腹和餐后列表
                                                    // 测试请求接口
                                                    recordManager.getBloodSugarReport(new AbstractDefaultHttpHandlerCallback(
                                                            BloodSugarCheckActivity.this) {
                                                        @Override
                                                        protected void onResponseSuccess(Object obj) {
                                                            map = (Map<String, List<BloodSugar>>) obj;
                                                            emptyData = map.get("empty");
                                                            fullData = map.get("full");

                                                            // 异常空腹数据
                                                            List<BloodSugar> listemptyDataTrue = new ArrayList<>();
                                                            for (BloodSugar anEmptyData1 : emptyData) {
                                                                if (anEmptyData1.isAbnormity()) {
                                                                    listemptyDataTrue.add(anEmptyData1);
                                                                }
                                                            }
                                                            // 异常饭后数据
                                                            List<BloodSugar> listfullDataTrue = new ArrayList<>();
                                                            for (BloodSugar aFullData : fullData) {
                                                                if (aFullData.isAbnormity()) {
                                                                    listfullDataTrue.add(aFullData);
                                                                }
                                                            }
                                                            //正常空腹数据
                                                            List<BloodSugar> listNormalEmpty = new ArrayList<>();
                                                            for (BloodSugar anEmptyData : emptyData) {
                                                                if (!anEmptyData.isAbnormity()) {
                                                                    listNormalEmpty.add(anEmptyData);
                                                                }
                                                            }
                                                            //正常饭后数据
                                                            List<BloodSugar> listNormalFull = new ArrayList<>();
                                                            for (BloodSugar aFullData : fullData) {
                                                                if (!aFullData.isAbnormity()) {
                                                                    listNormalFull.add(aFullData);
                                                                }
                                                            }

                                                            int allNumber = emptyData.size() + fullData.size();//所有
                                                            int normalNumber = listNormalEmpty.size() + listNormalFull.size();//正常
                                                            int abnaormalkNumber = listemptyDataTrue.size() + listfullDataTrue.size();//异常

                                                            if (index == 1) {
                                                                BloodSugarDialog dialog = new BloodSugarDialog(
                                                                        BloodSugarCheckActivity.this, value_fasting + "", allNumber,
                                                                        normalNumber, abnaormalkNumber, isNormal, listemptyDataTrue,
                                                                        listfullDataTrue);
                                                                dialog.show();
                                                            } else if (index == 2) {
                                                                BloodSugarDialog dialog = new BloodSugarDialog(
                                                                        BloodSugarCheckActivity.this, value_after_meal + "", allNumber,
                                                                        normalNumber, abnaormalkNumber, isNormal, listemptyDataTrue,
                                                                        listfullDataTrue);
                                                                dialog.show();
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                }else{
                                    new ChooseMemberDialog(BloodSugarCheckActivity.this, new OnCompleteListener() {
                                        @Override
                                        public void onComplete() {

                                            uploadManager.uploadCheckedData(CheckedDataType.BLOOD_SUGAR, bloodSugarData, 0,
                                                    new AbstractDefaultHttpHandlerCallback(BloodSugarCheckActivity.this) {
                                                        @Override
                                                        protected void onResponseSuccess(Object obj) {
                                                            // 提交成功之后，获取空腹和餐后列表
                                                            // 测试请求接口
                                                            recordManager.getBloodSugarReport(new AbstractDefaultHttpHandlerCallback(
                                                                    BloodSugarCheckActivity.this) {
                                                                @Override
                                                                protected void onResponseSuccess(Object obj) {
                                                                    map = (Map<String, List<BloodSugar>>) obj;
                                                                    emptyData = map.get("empty");
                                                                    fullData = map.get("full");

                                                                    // 异常空腹数据
                                                                    List<BloodSugar> listemptyDataTrue = new ArrayList<>();
                                                                    for (BloodSugar anEmptyData1 : emptyData) {
                                                                        if (anEmptyData1.isAbnormity()) {
                                                                            listemptyDataTrue.add(anEmptyData1);
                                                                        }
                                                                    }
                                                                    // 异常饭后数据
                                                                    List<BloodSugar> listfullDataTrue = new ArrayList<>();
                                                                    for (BloodSugar aFullData : fullData) {
                                                                        if (aFullData.isAbnormity()) {
                                                                            listfullDataTrue.add(aFullData);
                                                                        }
                                                                    }
                                                                    //正常空腹数据
                                                                    List<BloodSugar> listNormalEmpty = new ArrayList<>();
                                                                    for (BloodSugar anEmptyData : emptyData) {
                                                                        if (!anEmptyData.isAbnormity()) {
                                                                            listNormalEmpty.add(anEmptyData);
                                                                        }
                                                                    }
                                                                    //正常饭后数据
                                                                    List<BloodSugar> listNormalFull = new ArrayList<>();
                                                                    for (BloodSugar aFullData : fullData) {
                                                                        if (!aFullData.isAbnormity()) {
                                                                            listNormalFull.add(aFullData);
                                                                        }
                                                                    }

                                                                    int allNumber = emptyData.size() + fullData.size();//所有
                                                                    int normalNumber = listNormalEmpty.size() + listNormalFull.size();//正常
                                                                    int abnaormalkNumber = listemptyDataTrue.size() + listfullDataTrue.size();//异常

                                                                    if (index == 1) {
                                                                        BloodSugarDialog dialog = new BloodSugarDialog(
                                                                                BloodSugarCheckActivity.this, value_fasting + "", allNumber,
                                                                                normalNumber, abnaormalkNumber, isNormal, listemptyDataTrue,
                                                                                listfullDataTrue);
                                                                        dialog.show();
                                                                    } else if (index == 2) {
                                                                        BloodSugarDialog dialog = new BloodSugarDialog(
                                                                                BloodSugarCheckActivity.this, value_after_meal + "", allNumber,
                                                                                normalNumber, abnaormalkNumber, isNormal, listemptyDataTrue,
                                                                                listfullDataTrue);
                                                                        dialog.show();
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });

                                        }
                                    }).show();
                                }

                            }
                        });
                    }
                } else {
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.bsc_tips_not_input));
                }
                break;
            default:
                break;
        }
    }

    public void cleanChecked() {
        iv_no_eating.setImageResource(R.drawable.blood_sugar_check_no_eating_normal);
        // iv_before_eating.setImageResource(R.drawable.blood_sugar_check_before_eating_normal);
        iv_after_eating.setImageResource(R.drawable.blood_sugar_check_after_eating_normal);
    }

    // 实体类
    class BloodSugarText {

        private int id;
        private String text;

        public BloodSugarText(int id, String text) {
            super();
            this.id = id;
            this.text = text;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }

    public class BloodSugarDialog extends AlertDialog implements
            OnClickListener {

        Context context;

        int bloodAllNumber;//所有的血糖次数
        int normalNumber;//正常次数
        int abnormalNumber;//异常次数

        String isnormal;
        String str_tishi = "";
        List<BloodSugar> emptyData;// 空腹值
        List<BloodSugar> fullData;// 饭后值


        public BloodSugarDialog(Context context, String xuetangzhi, int bloodAllNumber, int normalNumber, int abnormalNumber,
                                String isnormal, List<BloodSugar> emptyData,
                                List<BloodSugar> fullData) {
            super(context);
            this.context = context;
            this.bloodAllNumber = bloodAllNumber;
            this.normalNumber = normalNumber;
            this.abnormalNumber = abnormalNumber;
            this.isnormal = isnormal;
            this.emptyData = emptyData;
            this.fullData = fullData;

        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.bloodsugar_submit_prompt);


            final ScrollView scrollview = (ScrollView) findViewById(R.id.scrollview);
            scrollview.post(new Runnable() {
                // 让scrollview跳转到顶部，必须放在runnable()方法中
                @Override
                public void run() {
                    scrollview.scrollTo(0, 0);
                }
            });

            // 次数
            tv_bloodsugar_prompt_cishu = (XRTextView) findViewById(R.id.tv_bloodsugar_prompt_cishu);

            String str = getString(R.string.bsc_dialog_text1) + bloodAllNumber + getString(R.string.bsc_dialog_text2)
                    + normalNumber + getString(R.string.bsc_dialog_text3)
                    + abnormalNumber + getString(R.string.bsc_dialog_text4);
            String source = str;
            SpannableString ss = new SpannableString(source);
            tv_bloodsugar_prompt_cishu.setMText(ss);

            tv_bloodsugar_prompt_cishu.setTextSize(14);
            tv_bloodsugar_prompt_cishu.setTextColor(getResources().getColor(
                    R.color.submit_chengse));
            tv_bloodsugar_prompt_cishu.invalidate();


            // 血糖值
            tv_bloodsugar_prompt_xuetang = (XRTextView) findViewById(R.id.tv_bloodsugar_prompt_xuetang);
            // 血糖
            TextView tv_is_normal = (TextView) findViewById(R.id.tv_is_normal);
            // 确定
            ImageView image_submit = (ImageView) findViewById(R.id.image_submit);
            image_submit.setOnClickListener(this);

            // 血糖是否正常
            LinearLayout linearlayout = (LinearLayout) findViewById(R.id.linearlayout);
            // 空腹
            ListView lv_kongfu = (ListView) findViewById(R.id.lv_kongfu);
            // 饭后
            ListView lv_canhou = (ListView) findViewById(R.id.lv_canhou);

            DialogLvAdapterEmpty adapter1 = new DialogLvAdapterEmpty(
                    BloodSugarCheckActivity.this, emptyData);
            lv_kongfu.setAdapter(adapter1);

            setListHeight(lv_kongfu);

            DialogLvAdapterFull adapter2 = new DialogLvAdapterFull(
                    BloodSugarCheckActivity.this, fullData);
            lv_canhou.setAdapter(adapter2);

            setListHeight(lv_canhou);


            if (index == 1) {
                String strkonghfu = getString(R.string.bsc_dialog_measure_kongfu_value)
                        + value_fasting + "mmol/L";
                String source2 = strkonghfu;
                SpannableString ss2 = new SpannableString(source2);
                tv_bloodsugar_prompt_xuetang.setMText(ss2);

                tv_bloodsugar_prompt_xuetang.setTextSize(14);
                tv_bloodsugar_prompt_xuetang.setTextColor(getResources().getColor(
                        R.color.submit_chengse));
                tv_bloodsugar_prompt_xuetang.invalidate();

                if (getString(R.string.bsc_isNormal2).equals(isnormal)) {
                    str_tishi = getString(R.string.bsc_dialog_normal2_result);
                    SpannableStringBuilder style = new SpannableStringBuilder(
                            str_tishi);
                    style.setSpan(new ForegroundColorSpan(getResources()
                                    .getColor(R.color.submit_normal)), 4, 6,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tv_is_normal.setText(style);

                    linearlayout.setVisibility(View.GONE);

                } else if (getString(R.string.bsc_isNormal1).equals(isnormal)) {
                    str_tishi = getString(R.string.bsc_dialog_normal1_result1) + value_fasting
                            + "mmol/L，"+getString(R.string.bsc_dialog_normal1_result2);
                    SpannableStringBuilder style = new SpannableStringBuilder(
                            str_tishi);
                    style.setSpan(new ForegroundColorSpan(getResources()
                                    .getColor(R.color.submit_not_normal)), 5, 7,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tv_is_normal.setText(style);

                    linearlayout.setVisibility(View.VISIBLE);

                } else if (getString(R.string.bsc_isNormal3).equals(isnormal)) {
                    str_tishi = getString(R.string.bsc_dialog_normal3_result1) + value_fasting
                            + "mmol/L，"+getString(R.string.bsc_dialog_normal3_result2);
                    SpannableStringBuilder style = new SpannableStringBuilder(
                            str_tishi);
                    style.setSpan(new ForegroundColorSpan(getResources()
                                    .getColor(R.color.submit_not_normal)), 5, 7,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tv_is_normal.setText(style);

                    linearlayout.setVisibility(View.VISIBLE);
                }

            } else if (index == 2) {
                String strfanhou = getString(R.string.bsc_dialog_fanhou_value)
                        + value_after_meal + "mmol/L";
                String source3 = strfanhou;
                SpannableString ss3 = new SpannableString(source3);
                tv_bloodsugar_prompt_xuetang.setMText(ss3);

                tv_bloodsugar_prompt_xuetang.setTextSize(14);
                tv_bloodsugar_prompt_xuetang.setTextColor(getResources().getColor(
                        R.color.submit_chengse));
                tv_bloodsugar_prompt_xuetang.invalidate();

                if (getString(R.string.bsc_isNormal2).equals(isnormal)) {
                    str_tishi = getString(R.string.bsc_dialog_normal2_result);
                    SpannableStringBuilder style = new SpannableStringBuilder(
                            str_tishi);
                    style.setSpan(new ForegroundColorSpan(getResources()
                                    .getColor(R.color.submit_normal)), 4, 6,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tv_is_normal.setText(style);

                    linearlayout.setVisibility(View.GONE);

                } else if (getString(R.string.bsc_isNormal3).equals(isnormal)) {
                    str_tishi = getString(R.string.bsc_dialog_normal3_fanhou) + value_after_meal
                            + "mmol/L，"+getString(R.string.bsc_dialog_normal3_result2);
                    SpannableStringBuilder style = new SpannableStringBuilder(
                            str_tishi);
                    style.setSpan(new ForegroundColorSpan(getResources()
                                    .getColor(R.color.submit_not_normal)), 8, 10,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tv_is_normal.setText(style);

                    linearlayout.setVisibility(View.VISIBLE);
                }
            }

        }

        private void showSuger(String str) {

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                // 确定
                case R.id.image_submit:
                    et_blood_sugar_value.setText("");
                    this.dismiss();
                    break;

            }
        }

        // 实现两个ListView在Scrollview中同时滑动
        private void setListHeight(ListView listView) {
            ListAdapter list = listView.getAdapter();
            if (list == null) {
                return;
            }
            int height = 0;
            for (int i = 0; i < list.getCount(); i++) {
                View listItem = list.getView(i, null, listView);
                listItem.measure(0, 0); // 计算子项View 的宽高
                height += listItem.getMeasuredHeight(); // 统计所有子项的总高度
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = height
                    + (listView.getDividerHeight() * (list.getCount() - 1));
            // listView.getDividerHeight()获取子项间分隔符占用的高度
            // params.height最后得到整个ListView完整显示需要的高度
            listView.setLayoutParams(params);
        }

        // 空腹Dialog的适配器
        class DialogLvAdapterEmpty extends BaseAdapter {
            Context context;
            List<BloodSugar> list;// 空腹值
            LayoutInflater inflater;

            public DialogLvAdapterEmpty(Context context, List<BloodSugar> list) {
                this.context = context;
                this.list = list;
                inflater = LayoutInflater.from(context);
            }

            @Override
            public int getCount() {
                if (list == null) {
                    return 0;
                }
                return list.size();
            }

            @Override
            public Object getItem(int arg0) {
                return list.get(arg0);
            }

            @Override
            public long getItemId(int arg0) {
                return arg0;
            }

            @Override
            public View getView(int position, View view, ViewGroup arg2) {
                HonderEmpty honder = null;
                if (view == null) {
                    honder = new HonderEmpty();
                    view = inflater.inflate(R.layout.dialog_list_item_empty,
                            null);
                    honder.tvtime = (TextView) view.findViewById(R.id.tvtime);
                    honder.tvxuetang = (TextView) view
                            .findViewById(R.id.tvxuetang);

                    view.setTag(honder);

                } else {
                    honder = (HonderEmpty) view.getTag();
                }

                BloodSugar bloodSugar = list.get(position);
                // 时间的long
                long createDate = bloodSugar.getCreateDate();

                // 需要当前的年月日，时和分
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
                Date date = new Date(createDate);

                String strDate = sdf.format(date);

                // 保存当天的时期
                SharedPreferenceUtil.saveString("now_date", strDate);

                // 只需要当前的时和分
                SimpleDateFormat sdfhm = new SimpleDateFormat("HH:mm");
                Date datehm = new Date(createDate);

                String strDatehm = sdfhm.format(date);


                // isAbnormity: true//是否异常，true代表异常，false代表正常
                if (bloodSugar.isAbnormity()) {
                    honder.tvtime.setText(strDatehm);
                    honder.tvxuetang.setText(bloodSugar.getLevels() + "mmol/L");
                }

                return view;
            }

        }

        class HonderEmpty {
            TextView tvtime;// 时间
            TextView tvxuetang;// 血糖值异常
        }

        // 饭后Dialog的适配器
        class DialogLvAdapterFull extends BaseAdapter {
            Context context;
            List<BloodSugar> list;// 空腹值
            LayoutInflater inflater;

            public DialogLvAdapterFull(Context context, List<BloodSugar> list) {
                this.context = context;
                this.list = list;
                inflater = LayoutInflater.from(context);
            }

            @Override
            public int getCount() {
                if (list == null) {
                    return 0;
                }
                return list.size();
            }

            @Override
            public Object getItem(int arg0) {
                return list.get(arg0);
            }

            @Override
            public long getItemId(int arg0) {
                return arg0;
            }

            @Override
            public View getView(int position, View view, ViewGroup arg2) {
                HonderFull honder = null;
                if (view == null) {
                    honder = new HonderFull();
                    view = inflater.inflate(R.layout.dialog_list_item_full,
                            null);
                    honder.tvtime = (TextView) view
                            .findViewById(R.id.tvtimefull);
                    honder.tvxuetang = (TextView) view
                            .findViewById(R.id.tvxuetangfull);

                    view.setTag(honder);

                } else {
                    honder = (HonderFull) view.getTag();
                }

                BloodSugar bloodSugar = list.get(position);
                // 时间的long
                long createDate = bloodSugar.getCreateDate();

                // 需要当前的年月日，时和分
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
                Date date = new Date(createDate);

                String strDate = sdf.format(date);

                // 保存当天的时期
                SharedPreferenceUtil.saveString("now_date", strDate);

                // 只需要当前的时和分
                SimpleDateFormat sdfhm = new SimpleDateFormat("HH:mm");
                Date datehm = new Date(createDate);

                String strDatehm = sdfhm.format(date);


                // isAbnormity: true//是否异常，true代表异常，false代表正常
                if (bloodSugar.isAbnormity()) {
                    honder.tvtime.setText(strDatehm);
                    honder.tvxuetang.setText(bloodSugar.getLevels() + "mmol/L");
                }

                return view;
            }

        }

        class HonderFull {
            TextView tvtime;// 时间
            TextView tvxuetang;// 血糖值异常
        }
    }

}
