package com.hxlm.hcyandroid.tabbar.home.visceraidentity;

import android.webkit.WebView;
import android.widget.ProgressBar;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.util.WebViewUtil;

public class VisceraIdentityReportActivity extends BaseActivity {
    private ProgressBar progressBar;
    private  WebView wv_report;
    @Override
    public void setContentView() {
        setContentView(R.layout.activity_webview);
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.report_title_zf), titleBar, 1);
    }

    @Override
    public void initDatas() {
        wv_report = (WebView) findViewById(R.id.wv);
        progressBar=(ProgressBar)findViewById(R.id.progressbar);
        String url = getIntent().getStringExtra("url");
        new WebViewUtil().setWebViewInit(wv_report,progressBar,this,url);
    }
}
