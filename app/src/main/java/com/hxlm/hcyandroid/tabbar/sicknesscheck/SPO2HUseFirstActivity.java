package com.hxlm.hcyandroid.tabbar.sicknesscheck;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.bean.CheckStep;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 血氧首次进入界面
 *
 * @author l
 */
public class SPO2HUseFirstActivity extends BaseActivity implements OnClickListener {

    private ListView lv_tips;
    private List<CheckStep> steps;

    private ImageView iv_now_check;//立即检测
    private ImageView iv_no_prompt;//今后不再提示

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_spo2_huse_first);

    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.spo2_title), titleBar, 1);
        lv_tips = (ListView) findViewById(R.id.lv_tips);
        iv_now_check = (ImageView) findViewById(R.id.iv_now_check);
        iv_now_check.setOnClickListener(this);
        iv_no_prompt = (ImageView) findViewById(R.id.iv_no_prompt);
        iv_no_prompt.setOnClickListener(this);

        steps = new ArrayList<CheckStep>();
        steps.add(new CheckStep(R.drawable.check_step_number_bg1, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg2, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg3, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg4, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg5, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg6, "", getString(R.string.bpuf_tips)));

        lv_tips.setAdapter(new StepAdapter(this, steps));
    }

    @Override
    public void initDatas() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //立即检测
            case R.id.iv_now_check:
                SPO2HUseFirstActivity.this.finish();
                Intent intent = new Intent(SPO2HUseFirstActivity.this, SPO2HCheckActivity.class);
                startActivity(intent);

                break;
            //今后不再提示
            case R.id.iv_no_prompt:
                SPO2HUseFirstActivity.this.finish();
                SharedPreferenceUtil.saveString("SPO2H", "0");
                Intent intent2 = new Intent(SPO2HUseFirstActivity.this, SPO2HCheckActivity.class);
                startActivity(intent2);
                break;
            default:
                break;
        }
    }
}
