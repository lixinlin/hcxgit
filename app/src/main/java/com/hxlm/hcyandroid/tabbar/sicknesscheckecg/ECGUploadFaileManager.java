package com.hxlm.hcyandroid.tabbar.sicknesscheckecg;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import com.alibaba.fastjson.JSON;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.CheckedDataType;
import com.hxlm.hcyandroid.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by l on 2016/10/21.
 * 心电上传数据失败保存本地
 */
public class ECGUploadFaileManager  {

    private static final String SP_MSG_COUNT_NAME = "count";//保存的数量
    private static final String SP_MSG_PREFIX_NAME = "ecg_msg_";//每一条消息设置的标识
    private static final String SP_MSG_STORE_PREFIX_NAME = "ecg_upload_faile_";

    //从Share得到消息
    public List<UploadFailedData> getECGFaileDataFromSp() {
        //首先得到所有的消息
        List<UploadFailedData> hcyMessages = getSpDatas(CheckedDataType.ECG);
        if(Constant.DEBUG) {
            Log.i("ECGReviewActivity", "得到所有的消息-->" + hcyMessages);
        }

        return hcyMessages;
    }

    /**
     * 保存心电数据到SharedPreferences
     */
    public void saveECGFaileDataToSp(List<UploadFailedData> aHcyMessages) {
        //首先得到Share里面的消息
        List<UploadFailedData> hcyMessagesInSp = getECGFaileDataFromSp();
        if(Constant.DEBUG)
        {
            Log.i("ECGReviewActivity","需要保存的消息-->"+aHcyMessages);
        }

        //然后将原来的数据和得到的数据保存到Share
        saveECGFaileDataToSp(hcyMessagesInSp, aHcyMessages, CheckedDataType.ECG);
          }
    /**
     * 保存消息到SharedPreferences到某个指定区域，按照个人和全局两个区域保存，
     * 未登录时可以查看全局信息，对收到的消息自动进行排重
     *
     * @param hcyMessagesInSp 已经获得的消息
     * @param aHcyMessages    需要保存的消息列表，从服务器fetch获得
     * @param type            保存区域编码
     */
    private void saveECGFaileDataToSp(List<UploadFailedData> hcyMessagesInSp, List<UploadFailedData> aHcyMessages, int type) {
        //获得某个指定保存区域的SharedPreferences句柄
        SharedPreferences sp = getSharedPreferences(type);

        if (sp != null) {
            //获取消息的数量
            int count = sp.getInt(SP_MSG_COUNT_NAME, 0);

            SharedPreferences.Editor editor = sp.edit();
            //遍历从服务器获得的消息
            for (UploadFailedData hcyMessage : aHcyMessages) {
                //如果为某一个消息类型并且在指定的消息集合中查看指定的消息是否存在，不存在的话
                if (!isExist(hcyMessagesInSp, hcyMessage)) {
                    //设置顺序号？
                    hcyMessage.setOrder(count);
                    //保存为json串
                    editor.putString(SP_MSG_PREFIX_NAME + count, JSON.toJSONString(hcyMessage));
                    count++;
                }
            }

            //保存数量
            editor.putInt(SP_MSG_COUNT_NAME, count);

            editor.apply();
        }
    }

    /**
     * 在指定的消息集合中查看指定的消息是否存在
     *
     * @param hcyMessagesInSp 已经获得的消息
     * @param aHcyMessage     需要确认是否在已经获得的消息中存在的消息
     */
    private boolean isExist(List<UploadFailedData> hcyMessagesInSp, UploadFailedData aHcyMessage) {
        if (hcyMessagesInSp != null) {
            for (UploadFailedData hcyMessage : hcyMessagesInSp) {
                /**
                 * 第一种情况，是上传file失败
                 * 第二种情况，上传数据失败
                 * 被保存的数据  aHcyMessage
                 */
                //先判断被保存的数据aHcyMessage的file是否为空
                //第一种情况，是上传file失败
                if(!TextUtils.isEmpty(aHcyMessage.getFile().getAbsolutePath()))
                {
                    //保存的文件相同 不在保存
                    return aHcyMessage.getFile().getAbsolutePath().equals(hcyMessage.getFile().getAbsolutePath());
                }
                //第二种情况，上传数据失败 path不为空
                else
                {
                    if(!TextUtils.isEmpty(aHcyMessage.getEcgData().getPath()))
                    {
                        //如果已经保存路径，不在保存
                        return aHcyMessage.getEcgData().getPath().equals(hcyMessage.getEcgData().getPath());
                    }
                }

            }
        }
        return false;
    }

    /**
     * 获得某个指定保存区域的SharedPreferences句柄
     *
     * @param type 保存区域编码
     */
    private SharedPreferences getSharedPreferences(int type) {
        SharedPreferences sp = null;
        switch (type) {
            case CheckedDataType.ECG:
                if (LoginControllor.isLogin()) {
                    sp = BaseApplication.getContext().getSharedPreferences(
                            SP_MSG_STORE_PREFIX_NAME + LoginControllor.getLoginMember().getId()+"_"+ LoginControllor.getChoosedChildMember().getId(), Context.MODE_PRIVATE);
                }
                break;
        }
        return sp;
    }

    /**
     * 查询某个指定区域的消息
     *
     * @param type 保存区域编码
     */
    private List<UploadFailedData> getSpDatas(int type) {
        List<UploadFailedData> messages = null;
        //获得某个指定保存区域的SharedPreferences句柄
        SharedPreferences sp = getSharedPreferences(type);

        if (sp != null) {
            //获取消息数量
            int count = sp.getInt(SP_MSG_COUNT_NAME, 0);

            for (int i = 0; i < count; i++) {
                //获取json串
                String jsonStr = sp.getString(SP_MSG_PREFIX_NAME + i, null);
                Logger.i("ECGReviewActivity","jsonStr字符串-->"+jsonStr);
                //如果不为空,解析
                if (!TextUtils.isEmpty(jsonStr)) {
                    UploadFailedData message = JSON.parseObject(jsonStr, UploadFailedData.class);
                    if (messages == null) {
                        messages = new ArrayList<>();
                    }
                    messages.add(message);
                }
            }
        }
        return messages;
    }


    /**
     * 文件上传成功之后，应该删除之前保存的失败记录
     *
     * @param uploadFailedData 需要删除的数据
     */
   public void deleteEcgFile(UploadFailedData uploadFailedData) {
        SharedPreferences sp = getSharedPreferences(CheckedDataType.ECG);

        if (sp != null) {

            //先得到所有的保存的失败记录
//            List<UploadFailedData> uploadFailedDataList=getECGFaileDataFromSp();
//
//
//            for(int i=0;i<uploadFailedDataList.size();i++)
//            {
//                if(uploadFailedData.getFile().getAbsolutePath().equals(uploadFailedDataList.get(i).getFile().getAbsolutePath()))
//                {
//                    if(Constant.DEBUG)
//                    {
//                        Log.i("ECGReviewActivity","绝对路径-->"+uploadFailedData.getFile().getAbsolutePath());
//                    }
//                    uploadFailedDataList.remove(uploadFailedData);
//
//                }
//            }
//
//            Log.i("ECGReviewActivity","deleteEcgFile--uploadFailedDataList.size()-->"+uploadFailedDataList.size());

            Logger.i("ECGReviewActivity","deleteEcgFile--getOrder()-->"+uploadFailedData.getOrder());

            //设置为已读状态
            uploadFailedData.setStatus(0);//状态设置为0
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(SP_MSG_PREFIX_NAME + uploadFailedData.getOrder(), null);
            editor.apply();

        }
    }

    /**
     * 数据上传成功之后，更新之前保存的path数据
     * @param uploadFailedData
     */
    public void deleteEcgPath(UploadFailedData uploadFailedData) {
        SharedPreferences sp = getSharedPreferences(CheckedDataType.ECG);

        if (sp != null) {

            Logger.i("ECGReviewActivity","deleteEcgPath--getOrder()-->"+uploadFailedData.getOrder());

            //设置为已读状态
            uploadFailedData.setStatus(0);//状态设置为0
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(SP_MSG_PREFIX_NAME + uploadFailedData.getOrder(), null);
            editor.apply();
        }
    }

}
