package com.hxlm.hcyandroid.tabbar.sicknesscheckecg;

import android.util.Log;
import com.hxlm.android.health.device.view.ECGDrawWaveManager;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.Constant;

import java.io.File;

/**
 * Created by juzhang on 2016/10/23.
 *  //替换保存在本地的文件名
 */
public class ECGRenameToFile {

    //替换文件名
    public void renameToFile(File ecgDataFile, String pathFile)
    {
        //-----------------获取保存在本地的文件名与上传之后返回的文件名，进行替换
        //保存在本地的文件名
        String filePath=ecgDataFile.getAbsolutePath();//保存路径
        int intPath=filePath.lastIndexOf("/");
        //获取保存的文件名
        String strFileName=filePath.substring(intPath+1);
        Logger.i("ECGReviewActivity","保存在本地的文件名-->"+strFileName);

        //得到上传之后返回的文件名
        int intPath2=pathFile.lastIndexOf("/");
        String strPathName=pathFile.substring(intPath2+1);
        Logger.i("ECGReviewActivity","上传之后返回的文件名-->"+strPathName);

        //保存的文件路径
        String strToRename= Constant.BASE_PATH+ ECGDrawWaveManager.ECG_FILE_PATH+"/"+strPathName;
        //替换文件名
        File fileToRename=new File(strToRename);
        if(ecgDataFile.exists()&&!fileToRename.exists())
        {
            //进行替换
            if(ecgDataFile.renameTo(fileToRename))
            {
                Logger.i("ECGReviewActivity","修改成功");
                Logger.i("ECGReviewActivity","修改成功之后的路径-->"+fileToRename.getAbsolutePath());

            }else
            {
                Logger.i("ECGReviewActivity","修改失败");
            }
        }
    }
}
