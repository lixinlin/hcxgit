package com.hxlm.hcyandroid.tabbar.home_jczs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hcy_futejia.activity.BandingPhoneNumberActivity;
import com.hcy_futejia.activity.FtjBloodPressureCheckActivity;
import com.hcy_futejia.activity.FtjTuWenActivity;
import com.hcy_futejia.activity.FtjYueYaoActivity;
import com.hhmedic.activity.CallSelectorAct;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.content.GongFaActivity;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.Member;
import com.hxlm.android.hcy.utils.DeviceUtils;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.tabbar.sicknesscheck.BloodPressureUseFirstActivity;
import com.hxlm.hcyandroid.tabbar.sicknesscheck.ECGUseFirstActivity;
import com.hxlm.hcyandroid.tabbar.sicknesscheck.SPO2HCheckActivity;
import com.hxlm.hcyandroid.tabbar.sicknesscheck.SPO2HUseFirstActivity;
import com.hxlm.hcyandroid.tabbar.sicknesscheckecg.ECGCheckActivity;
import com.hxlm.hcyandroid.util.IsMobile;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.hxlm.hcyandroid.util.StatusBarUtils;
import com.jiudaifu.moxademo.activity.MainActivityJDF;

/**
 * Created by l on 2016/10/18.
 */
public class MenuActiviy extends Activity implements View.OnClickListener {
    private boolean areMenusShowing;
    private ViewGroup menusWrapper;
    private ImageView imageViewPlus;
    private ImageView menu_auricular;
    private ImageView menu_sport;
    private ImageView menu_music;
    private ImageView menu_heart;
    private ImageView menu_blood_pressure;
    private ImageView menu_video;
    private ImageView menu_message;
    private ImageView menu_phone;
    private ImageView menu_oxygen;

    //    private View shrinkRelativeLayout;
//    private RelativeLayout layoutMain;
//    private View btn_menu_dimiss;
    // 顺时针旋转动画
    private Animation animRotateClockwise;
    // 逆时针旋转动画
    private Animation animRotateAntiClockwise;


    private SharedPreferences sp;


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 111) {

                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                MenuActiviy.this.finish();
            }
        }
    };
    private RelativeLayout mRlThree;
    private RelativeLayout mRlFour;
    private ImageView mIvPhone;
    private ImageView mIvJiu;
    private ImageView mIvJiu2;
    private ImageView menu_shipin;
    private ImageView menu_oxygen_en;

    @Override
    protected void onResume() {
        super.onResume();
        StatusBarUtils.setWindowStatusBarColor(MenuActiviy.this,R.color.white);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_menu_dialog);


        sp = getSharedPreferences("pub", Context.MODE_PRIVATE);
        // 初始化
        initViews();

    }

    // 初始化
    private void initViews() {
        imageViewPlus = (ImageView) findViewById(R.id.imageview_plus);
        menu_auricular = (ImageView) findViewById(R.id.menu_auricular);
        menu_sport = (ImageView) findViewById(R.id.menu_sport);
        menu_heart = (ImageView) findViewById(R.id.menu_heart);
//        menu_video = (ImageView) findViewById(R.id.menu_video);
        menu_phone = (ImageView) findViewById(R.id.menu_phone);
        menu_message = (ImageView) findViewById(R.id.menu_message);
        menu_music = (ImageView) findViewById(R.id.menu_music);
        menu_oxygen = (ImageView) findViewById(R.id.menu_oxygen);
        menu_blood_pressure = (ImageView) findViewById(R.id.menu_blood_pressure);
        mIvPhone = (ImageView) findViewById(R.id.menu_phone_else);
        mIvJiu = (ImageView) findViewById(R.id.menu_jiu);
        mIvJiu2 = (ImageView) findViewById(R.id.menu_jiu2);
        mRlThree = (RelativeLayout) findViewById(R.id.rl_three);
        mRlFour = (RelativeLayout) findViewById(R.id.rl_four);
        menu_shipin = findViewById(R.id.menu_shipin);
        menu_oxygen_en = findViewById(R.id.menu_oxygen_en);

        menu_phone.setOnClickListener(this);
        menu_shipin.setOnClickListener(this);
        menu_auricular.setOnClickListener(this);
        menu_sport.setOnClickListener(this);
        menu_heart.setOnClickListener(this);
        mIvPhone.setOnClickListener(this);
//        menu_video.setOnClickListener(this);
        menu_phone.setOnClickListener(this);
        menu_message.setOnClickListener(this);
        menu_music.setOnClickListener(this);
        menu_blood_pressure.setOnClickListener(this);
        menu_oxygen.setOnClickListener(this);
        imageViewPlus.setOnClickListener(this);
        mIvJiu.setOnClickListener(this);
        mIvJiu2.setOnClickListener(this);
        menu_oxygen_en.setOnClickListener(this);


//        menusWrapper = (ViewGroup) findViewById(R.id.menus_wrapper);
//        shrinkRelativeLayout = findViewById(R.id.relativelayout_shrink);
//        layoutMain = (RelativeLayout) findViewById(R.id.layout_content);
//        btn_menu_dimiss= findViewById(R.id.btn_menu_dimiss);


//        //点击关闭
//        btn_menu_dimiss.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showLinearMenus();
//            }
//        });

        //手机制造商
        String deviceManufacturer = DeviceUtils.getDeviceManufacturer();
        if (deviceManufacturer.equals("HC03")) {
            //定制手机
            menu_heart.setVisibility(View.VISIBLE);
            menu_oxygen.setVisibility(View.VISIBLE);
//            mIvPhone.setVisibility(View.GONE);
            mRlFour.setVisibility(View.VISIBLE);
//            mRlThree.setVisibility(View.VISIBLE);

            if (Constant.isEnglish){
                menu_shipin.setVisibility(View.GONE);
                menu_oxygen.setVisibility(View.GONE);
                menu_oxygen_en.setVisibility(View.VISIBLE);
            }

        } else {
            //非定制手机
            menu_heart.setVisibility(View.GONE);
            menu_oxygen.setVisibility(View.GONE);
            mRlFour.setVisibility(View.GONE);
            mRlThree.setVisibility(View.VISIBLE);
        }


        //顺时针旋转45度
        animRotateClockwise = AnimationUtils.loadAnimation(
                this, R.anim.rotate_open);
        //逆时针返回
        animRotateAntiClockwise = AnimationUtils.loadAnimation(
                this, R.anim.rotate_close);


        //默认应该展开
//        showLinearMenus();

//        shrinkRelativeLayout.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                showLinearMenus();
//            }
//        });
//
//        for (int i = 0; i < menusWrapper.getChildCount(); i++) {
//            //分布式菜单点击事件
//            menusWrapper.getChildAt(i).setOnClickListener(
//                    new SpringMenuLauncher(intentActivity[i]));
//        }
    }

    @Override
    public void onClick(View v) {
        Intent  intent = null;
        switch (v.getId()) {
            case R.id.menu_auricular:
                Intent yueleyiIntent = new Intent(MenuActiviy.this, FtjYueYaoActivity.class);
                yueleyiIntent.putExtra("title", getString(R.string.web_leluoyi));
                startActivity(yueleyiIntent);
                MenuActiviy.this.finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case R.id.menu_blood_pressure:
                //是否需要进入血压首次进入界面 0表示不显示
                if ("0".equals(SharedPreferenceUtil.getBloodPressure())) {
                    startActivity(new Intent(this, FtjBloodPressureCheckActivity.class));
                } else {
                    startActivity(new Intent(this, BloodPressureUseFirstActivity.class));
                }
                MenuActiviy.this.finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case R.id.menu_heart:
                //是否需要进入心电首次进入界面 0表示不显示
                if ("0".equals(SharedPreferenceUtil.getECG())) {
                    //控制是否显示两条线
                    startActivity(new Intent(this, ECGCheckActivity.class));
                    // startActivity(new Intent(this, ECGCompareActivity.class));
                } else {
                    startActivity(new Intent(this, ECGUseFirstActivity.class));
                }
                MenuActiviy.this.finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;

            case R.id.menu_phone_else:
                call400();
                break;

            case R.id.menu_message:
                LoginControllor.requestLogin(MenuActiviy.this, new OnCompleteListener() {
                    @Override
                    public void onComplete() {

//                        Intent intent = new Intent(MenuActiviy.this,
//                                TuWenActivity.class);
                        Intent intent = new Intent(MenuActiviy.this,
                                FtjTuWenActivity.class);
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putBoolean("pubsort", true);
                        editor.commit();
                        startActivity(intent);
                    }
                });
                MenuActiviy.this.finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case R.id.menu_music:
//                Intent yueyaoIntent = new Intent(MenuActiviy.this, YueYaoActivity.class);
                Intent yueyaoIntent = new Intent(MenuActiviy.this, FtjYueYaoActivity.class);
                yueyaoIntent.putExtra("title", getString(R.string.web_leyao));
                startActivity(yueyaoIntent);
                MenuActiviy.this.finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case R.id.menu_phone:
                call400();
                break;
            case R.id.menu_sport:

                startActivity(new Intent(MenuActiviy.this, GongFaActivity.class));
                MenuActiviy.this.finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case R.id.menu_oxygen:
                //是否需要进入血氧首次进入界面 0表示不显示
                if ("0".equals(SharedPreferenceUtil.getSPO2H())) {
                    startActivity(new Intent(this, SPO2HCheckActivity.class));
                } else {
                    startActivity(new Intent(this, SPO2HUseFirstActivity.class));
                }
                MenuActiviy.this.finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
////            case R.id.menu_video:
////                startActivity(new Intent(MenuActiviy.this, VideoConsultActivity.class));
//                break;
            case R.id.menu_oxygen_en:
                //是否需要进入血氧首次进入界面 0表示不显示
                if ("0".equals(SharedPreferenceUtil.getSPO2H())) {
                    startActivity(new Intent(this, SPO2HCheckActivity.class));
                } else {
                    startActivity(new Intent(this, SPO2HUseFirstActivity.class));
                }
                MenuActiviy.this.finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case R.id.imageview_plus:
                imageViewPlus.startAnimation(animRotateAntiClockwise);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Message message = new Message();
                        message.what = 111;
                        handler.sendMessage(message);
                    }
                }, 450);
                MenuActiviy.this.finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case R.id.menu_jiu:
                 intent = new Intent(this, MainActivityJDF.class);
                ChildMember choosedChildMember = LoginControllor.getChoosedChildMember();
                if(choosedChildMember!=null){
                    intent.putExtra("userid",choosedChildMember.getId());
                }
                startActivity(intent);
                MenuActiviy.this.finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case R.id.menu_jiu2:
                 intent = new Intent(this, MainActivityJDF.class);
                ChildMember choosedChildMember2 = LoginControllor.getChoosedChildMember();
                if(choosedChildMember2!=null){
                    intent.putExtra("userid",choosedChildMember2.getId());
                }
                startActivity(intent);
                MenuActiviy.this.finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            //视频
            case R.id.menu_shipin:
                Intent intent2 = new Intent(MenuActiviy.this, CallSelectorAct.class);
                startActivity(intent2);
                MenuActiviy.this.finish();
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            default:
                break;
        }

    }

    private void call400() {
        if (Constant.isEnglish){
            toCallPhone();
        }else {
            Member loginMember = LoginControllor.getLoginMember();
            if (loginMember != null) {
                String phone = loginMember.getUsername();
                boolean mobileNO = IsMobile.isMobileNO(phone);
                if (mobileNO) {
                    toCallPhone();
                } else {
                    //提示绑定手机号
                    setDialog_binding_phone();
                }
            }
        }
    }

    private void toCallPhone() {
        //绑定了手机号
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_DIAL);//表示打开拨打电话窗口，但还未拨出电话
        intent.setData(Uri.parse("tel:" + "4006776668"));
        startActivity(intent);
        MenuActiviy.this.finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    /**
     * 设置提示弹窗
     */
    private Dialog dialog_bind_phone;
    public void setDialog_binding_phone(){
        dialog_bind_phone = new Dialog(this,R.style.dialog);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_activate, null);
        dialog_bind_phone.setContentView(view);
        //初始化控件
        Button btn_dialog_activate_no = view.findViewById(R.id.btn_dialog_activate_no);
        Button btn_dialog_activate_yes = view.findViewById(R.id.btn_dialog_activate_yes);
        TextView tv_dialog_text = view.findViewById(R.id.tv_dialog_text);

        tv_dialog_text.setText(R.string.ftj_callSelectorAct_glsj);

        btn_dialog_activate_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_bind_phone.dismiss();
            }
        });

        btn_dialog_activate_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //跳转至绑手机号页面
                Intent intent = new Intent(MenuActiviy.this, BandingPhoneNumberActivity.class);
                startActivity(intent);
                dialog_bind_phone.dismiss();
            }
        });
        dialog_bind_phone.show();
        dialog_bind_phone.setCanceledOnTouchOutside(false);
        dialog_bind_phone.setCancelable(true);
    }

    /**
     * 以直线型展开菜单
     */
    {
//    private void showLinearMenus() {
//        int[] size = DeviceUtility.getScreenSize(this);
//
//        if (!areMenusShowing) {
//            SpringAnimation.startAnimations(
//                    this.menusWrapper, ZoomAnimation.Direction.SHOW, size);
//            this.imageViewPlus.startAnimation(this.animRotateClockwise);
//        } else {
//            //关闭动画
//            SpringAnimation.startAnimations(
//                    this.menusWrapper,ZoomAnimation.Direction.HIDE,size);


//        }
//
//        areMenusShowing = !areMenusShowing;
//    }

    // 分布菜单事件监听器
//    private class SpringMenuLauncher implements View.OnClickListener {
//        private final Class<?> cls;
//
//        private SpringMenuLauncher(Class<?> c) {
//            this.cls = c;
//        }
//
//
//        public void onClick(View v) {
//            // TODO Auto-generated method stub
//            MenuActiviy.this.startSpringMenuAnimations(v);
//            //layoutMain.setBackgroundResource(resource);
//            switch (v.getId()) {
//                case R.id.menu_video:
//                    startActivity(new Intent(MenuActiviy.this, VideoConsultActivity.class));
//                    break;
//                case R.id.menu_message:
//                    LoginControllor.requestLogin(MenuActiviy.this, new OnCompleteListener() {
//                        @Override
//                        public void onComplete() {
//
//                            Intent intent = new Intent(MenuActiviy.this,
//                                    TuWenActivity.class);
//                            SharedPreferences.Editor editor = sp.edit();
//                            editor.putBoolean("pubsort", true);
//                            editor.commit();
//                            startActivity(intent);
//                        }
//                    });
//                    break;
//                case R.id.menu_phone:
//                    Intent intent = new Intent();
//                    intent.setAction(Intent.ACTION_DIAL);//表示打开拨打电话窗口，但还未拨出电话
//                    intent.setData(Uri.parse("tel:" + "4008118899"));
//                    startActivity(intent);
//                    break;
//            }
//
////            MenuDialog.this.startActivity(
////                    new Intent(
////                            MenuDialog.this,
////                            MenuDialog.SpringMenuLauncher.this.cls));
//
//
//        }
//    }
//
//    /**
//     * 点击第二项的动画效果
//     * 展现菜单动画效果
//     *
//     * @param view
//     */
//    private void startSpringMenuAnimations(View view) {
//        areMenusShowing = true;
//        Animation shrinkOut1 = new ShrinkAnimationOut(300);
//        Animation growOut = new EnlargeAnimationOut(300);
//        shrinkOut1.setInterpolator(new AnticipateInterpolator(2.0F));
//        shrinkOut1.setAnimationListener(new Animation.AnimationListener() {
//
//            public void onAnimationEnd(Animation animation) {
//                // TODO Auto-generated method stub
//                MenuActiviy.this.imageViewPlus.clearAnimation();
//            }
//
//            public void onAnimationRepeat(Animation animation) {
//                // TODO Auto-generated method stub
//
//            }
//
//            public void onAnimationStart(Animation animation) {
//                // TODO Auto-generated method stub
//
//            }
//        });
//
//        view.startAnimation(growOut);
//    }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        MenuActiviy.this.finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        return super.onKeyDown(keyCode, event);
    }
}
