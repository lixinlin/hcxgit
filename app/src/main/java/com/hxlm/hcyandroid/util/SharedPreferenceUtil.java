package com.hxlm.hcyandroid.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.hcyandroid.BaseApplication;

public class SharedPreferenceUtil {

    private static SharedPreferences sp = BaseApplication.getContext()
            .getSharedPreferences("user_info", Context.MODE_PRIVATE);

    public static String getString(String string) {
        return sp.getString(string, "");
    }

    public static void saveString(String key, String value) {
        Editor editor = sp.edit();
        editor.putString(key, value);
        editor.apply();
    }

    // 用户退出程序之后删除需要跳转到的fragment知己档案
    public static void removeArchivesFragment() {
        Editor editor = sp.edit();
        editor.remove("ArchivesFragment");
        editor.apply();
    }

    // 得到登录输入的用户名
    public static String getUser_Name() {
        return sp.getString("user_name", "");
    }

    // 得到登录输入的密码
    public static String getUser_Pass() {
        return sp.getString("user_pass", "");
    }

    // 呼吸首次显示界面是否还需要显示 0代表不显示
    public static String getBreath() {
        return sp.getString("Breath", "");
    }

    // 体温首次显示界面是否还需要显示 0代表不显示
    public static String getTemperature() {
        return sp.getString("Temperature", "");
    }

    // 血压首次显示界面是否还需要显示 0代表不显示
    public static String getBloodPressure() {
        return sp.getString("BloodPressure", "");
    }

    // 心电首次显示界面是否还需要显示 0代表不显示
    public static String getECG() {
        return sp.getString("ECG", "");
    }

    // 血糖首次显示界面是否还需要显示 0代表不显示
    public static String getBloodSugar() {
        return sp.getString("BloodSugar", "");
    }

    // 血氧首次显示界面是否还需要显示 0代表不显示
    public static String getSPO2H() {
        return sp.getString("SPO2H", "");
    }

    // 一说页面是否首次进入，true :是， false ：不是
    public static String getOneSay() {
        return sp.getString("oneSay", "true");
    }

    // 一写页面是否首次进入，true :是， false ：不是
    public static String getOneWrite() {
        return sp.getString("oneWrite", "true");
    }

    // 一点页面是否首次进入，true :是， false ：不是
    public static String getOneClock() {
        return sp.getString("oneClick", "true");
    }

    // 当前用户姓名
    public static String getUserName() {
        return sp.getString("userName", "");
    }

    // 当前用户手机号
    public static String getUserPhone() {
        return sp.getString("phone", "");
    }

    public static String getCurrentMemberName() {
        return sp.getString("currentMemberName", sp.getString("userName", ""));
    }

    public static String getMemberId() {
        if (LoginControllor.getLoginMember() == null) {
            return null;
        } else {
            return String.valueOf(LoginControllor.getLoginMember().getId());
        }
    }


    /**
     * 得到memberChildId
     */
    public static String getCurrnetMemberId() {
        if (LoginControllor.getChoosedChildMember() == null) {
            return null;
        } else {
            return String.valueOf(LoginControllor.getChoosedChildMember().getId());
        }
    }

    public static void saveCity(String city) {
        Editor editor = sp.edit();
        editor.putString("city", city);
        editor.apply();
    }

    public static void saveProvince(String province) {
        Editor editor = sp.edit();
        editor.putString("privonce", province);
        editor.apply();
    }

    public static String getProvince() {
        return sp.getString("privonce", "");
    }

    public static void logoff() {
        LoginControllor.clearLastLoginInfo();
    }
}
