package com.hxlm.hcyandroid.bean;

import java.util.Date;

public class VisceraIdentityResult {
    private String id;
    private long createDate;
    private long modifyDate;
    private String zhengzhuang_id;
    private String zz_name;
    private String name;
    private String zz_level;
    private String icd_id;
    private String icd_code;
    private String cust_id;
    private String physique_id;
    private String icd_name;
    private String icd_name_str;
    private String zz_name_str;
    private Date createTime;
    private String link;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getZhengzhuang_id() {
        return zhengzhuang_id;
    }

    public void setZhengzhuang_id(String zhengzhuang_id) {
        this.zhengzhuang_id = zhengzhuang_id;
    }

    public String getZz_name() {
        return zz_name;
    }

    public void setZz_name(String zz_name) {
        this.zz_name = zz_name;
    }

    public String getZz_level() {
        return zz_level;
    }

    public void setZz_level(String zz_level) {
        this.zz_level = zz_level;
    }

    public String getIcd_id() {
        return icd_id;
    }

    public void setIcd_id(String icd_id) {
        this.icd_id = icd_id;
    }

    public String getIcd_code() {
        return icd_code;
    }

    public void setIcd_code(String icd_code) {
        this.icd_code = icd_code;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getPhysique_id() {
        return physique_id;
    }

    public void setPhysique_id(String physique_id) {
        this.physique_id = physique_id;
    }

    public String getIcd_name() {
        return icd_name;
    }

    public void setIcd_name(String icd_name) {
        this.icd_name = icd_name;
    }

    public String getIcd_name_str() {
        return icd_name_str;
    }

    public void setIcd_name_str(String icd_name_str) {
        this.icd_name_str = icd_name_str;
    }

    public String getZz_name_str() {
        return zz_name_str;
    }

    public void setZz_name_str(String zz_name_str) {
        this.zz_name_str = zz_name_str;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
