package com.hxlm.hcyandroid.bean;

public class IdentityResult {
    private int id;
    private long createDate;
    private long modifyDate;
    private Subject subject;

    //脏腑的数据
    private String zz_name_str;




    public int getId() {
        return id;
    }

    public String getZz_name_str() {
        return zz_name_str;
    }

    public void setZz_name_str(String zz_name_str) {
        this.zz_name_str = zz_name_str;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

}
