package com.hxlm.hcyandroid;

import android.content.Intent;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.tabbar.healthmall.PayInterface;
import com.hxlm.hcyandroid.util.WebViewUtil;

public class WebViewAboutHealthMallActivity extends BaseActivity {

    private ProgressBar progressBar;
    private WebView wv;
    private ProgressBar progressbar;
    private String url;
    private String title;//标题
    TitleBarView titleBar;
    private LinearLayout linear_request_error;//网络请求错误提示页面

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_webview);
    }

    @Override
    public void initViews() {


    }

    @Override
    public void initDatas() {

        Intent intent = WebViewAboutHealthMallActivity.this.getIntent();
        title=intent.getStringExtra("title");
        url = intent.getStringExtra("url");

        titleBar = new TitleBarView();
        titleBar.init(this, title, titleBar, 1);
        progressBar=(ProgressBar)findViewById(R.id.progressbar);
        wv = (WebView) findViewById(R.id.wv);
        linear_request_error = (LinearLayout) findViewById(R.id.linearmy_request_error);


        wv.addJavascriptInterface(new PayInterface(WebViewAboutHealthMallActivity.this), "payInterface");

        //new WebViewUtil().setWebView(wv, WebViewAboutHealthMallActivity.this);

        LoginControllor.requestLogin(WebViewAboutHealthMallActivity.this, new OnCompleteListener() {
            @Override
            public void onComplete() {
                //重新定义
                new WebViewUtil().setWebViewInit(wv, progressBar,WebViewAboutHealthMallActivity.this, url);
                
            }
        });

            //判断是否连接网络
//        if(NetWorkUtils.isNetworkConnected(WebViewAboutHealthMallActivity.this))
//        {
//
//
//            linear_request_error.setVisibility(View.GONE);
//            wv.setVisibility(View.VISIBLE);
//
//            LoginControllor.requestLogin(WebViewAboutHealthMallActivity.this, new OnCompleteListener() {
//                @Override
//                public void onComplete() {
//                    WebViewUtil.syncCookie(WebViewAboutHealthMallActivity.this);
//                    wv.loadUrl(url);
//                }
//            });
//        }else
//        {
//            titleBar.init(this, "网络出错", titleBar, 1);
//
//            linear_request_error.setVisibility(View.VISIBLE);
//            wv.setVisibility(View.GONE);
//        }

    }


        @Override
        public boolean onKeyDown ( int keyCode, KeyEvent event){
            if (event.getAction() == KeyEvent.ACTION_DOWN
                    && keyCode == KeyEvent.KEYCODE_BACK) {
                if (wv.canGoBack()) {
                    wv.goBack();
                } else {
                    this.finish();
                }
            }
            return true;
        }

    }
