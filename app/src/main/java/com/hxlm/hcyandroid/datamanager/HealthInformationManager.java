package com.hxlm.hcyandroid.datamanager;

import android.os.Handler;
import android.os.Message;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hxlm.android.hcy.asynchttp.AbstractHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.order.Card;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.Member;
import com.hxlm.hcyandroid.MessageType;
import com.hxlm.hcyandroid.ResponseStatus;
import com.hxlm.hcyandroid.bean.HealthInformation;
import com.hxlm.hcyandroid.bean.HealthInformationCategory;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

public class HealthInformationManager {
    /**
     * 获取最新资讯接口
     */
    public void getNewHealthInformation(final AbstractHttpHandlerCallback callback) {
        String url = "/article/healthArticleList.jhtml";

        RequestParams params = new RequestParams();

        HcyHttpClient.submit(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            public Object contentParse(String content) {
                return JSON.parseArray(content, HealthInformation.class);
            }
        });
    }
    /**
     * 获取健康资讯中的分类
     */
    public void getHealthInformationCategory(final AbstractHttpHandlerCallback callback) {
        String url = "/article/healthCategoryList.jhtml";
        RequestParams params = new RequestParams();


        HcyHttpClient.submit(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content)
            {
                return JSON.parseArray(content, HealthInformationCategory.class);
            }
        });

    }

    /**
     * 根据健康资讯分类id获取相应的资讯
     *
     * @param id 健康资讯分类id
     */
    public void getHealthInformationById(int id,final AbstractHttpHandlerCallback callback) {
        String url = "/article/healthListByCategory/" + id + ".jhtml";
        RequestParams params = new RequestParams();
        params.put("id", id);

        HcyHttpClient.submit(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content)
            {
                return JSON.parseArray(content, HealthInformation.class);
            }
        });
    }

}
