package com.hxlm.hcyandroid.view;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.bean.MyHealthLecture;
import com.hxlm.hcyandroid.bean.ReserveSn;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ShowReserveSnDialog extends AlertDialog {

    private MyHealthLecture myHealthLecture;
    private Context context;
    private ListView lv_reserve_sn;
    private List<ReserveSn> reserveSns;

    public ShowReserveSnDialog(Context context, MyHealthLecture myHealthLecture) {
        super(context);
        this.context = context;
        this.myHealthLecture = myHealthLecture;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_show_reserve_sn);
        initView();
        initData();
    }

    private void initView() {
        lv_reserve_sn = (ListView) findViewById(R.id.lv_reserve_sn);
    }

    private void initData() {
        reserveSns = myHealthLecture.getReserveSns();
        ReserveSnAdapter adapter = new ReserveSnAdapter(context, reserveSns);
        lv_reserve_sn.setAdapter(adapter);
    }

    class ReserveSnAdapter extends BaseAdapter {

        private Context context;
        private List<ReserveSn> list;
        private LayoutInflater inflater;
        private Holder holder;

        public ReserveSnAdapter(Context context, List<ReserveSn> list) {
            super();
            this.context = context;
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                holder = new Holder();
                inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.reserve_sn_item, null);

                holder.tv_reserve_sn = (TextView) convertView.findViewById(R.id.tv_reserve_sn);
                holder.tv_reserve_time = (TextView) convertView.findViewById(R.id.tv_reserve_sn_time);
                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }

            ReserveSn reserveSn = list.get(position);
            holder.tv_reserve_sn.setText(reserveSn.getReserveSn());

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String time = format.format(new Date(reserveSn.getCreateDate()));
            holder.tv_reserve_time.setText(time);

            return convertView;
        }

    }

    class Holder {
        TextView tv_reserve_sn;
        TextView tv_reserve_time;
    }

}
