package com.hxlm.hcyandroid.view;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.hcy.ky3h.R;

public class TuWenCloseBackDialog extends AlertDialog implements
        View.OnClickListener {

    private ImageView tv_tuwen_sure;
    private ImageView tv_tuwen_cancle;
    private OnChoosedBackListener listener;

    public TuWenCloseBackDialog(Context context, OnChoosedBackListener onChoosedListener) {
        super(context);
        listener = onChoosedListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_tuwen_back);
        tv_tuwen_sure = (ImageView) findViewById(R.id.tv_tuwen_sure);
        tv_tuwen_cancle = (ImageView) findViewById(R.id.tv_tuwen_cancle);
        tv_tuwen_sure.setOnClickListener(this);
        tv_tuwen_cancle.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //确定
            case R.id.tv_tuwen_sure:
                listener.onChoosed(1);
                this.dismiss();
                break;

            //取消
            case R.id.tv_tuwen_cancle:
                this.dismiss();
                break;
        }
    }

    public interface OnChoosedBackListener {
        void onChoosed(int index);

    }
}
