package com.hxlm.hcyandroid.view;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.BaseApplication;

public class ChooseSexDialog extends AlertDialog implements
        View.OnClickListener {
    private TextView tv_man;
    private TextView tv_women;
    private String sex;
    private OnChoosedListener listener;

    public ChooseSexDialog(Context context, OnChoosedListener onChoosedListener) {
        super(context);
        listener = onChoosedListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_choose_sex);
        tv_man = (TextView) findViewById(R.id.tv_man);
        tv_women = (TextView) findViewById(R.id.tv_woman);
        tv_man.setOnClickListener(this);
        tv_women.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_man:
                sex = BaseApplication.getContext().getString(R.string.man);
                listener.onChoosedSex(sex);
                this.dismiss();
                break;
            case R.id.tv_woman:
                sex = BaseApplication.getContext().getString(R.string.woman);
                listener.onChoosedSex(sex);
                this.dismiss();
                break;
            default:
                break;
        }
    }

    public interface OnChoosedListener {
        void onChoosedSex(String sex);

    }

}
