package com.hxlm.hcyphone.harmonypackage;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hcy_futejia.activity.BandingPhoneNumberActivity;
import com.hcy_futejia.activity.FtjMeasureInfoActivity;
import com.hhmedic.activity.CallSelectorAct;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.Member;
import com.hxlm.android.hcy.utils.DeviceUtils;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.tabbar.home_jczs.DetectionJump;
import com.hxlm.hcyandroid.tabbar.home_jczs.InformationActivity;
import com.hxlm.hcyandroid.tabbar.home_jczs.WebViewActivity;
import com.hxlm.hcyandroid.util.IsMobile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 干预方案
 * Created by JQ on 2017/6/26.
 */
public class CombingStateActivity extends BaseActivity implements View.OnClickListener {
    private List<String> slztItemList;
    private Map<String, Integer> jlslImagesMap;
    private Map<String, Bundle> slztActivityMap;
    private ImageView one_dai, one_zhan;
    private ImageView one_eat, one_sit;
    private ImageView one_drink;
    private TextView all_jingluo;
    private ImageView one_ce, one_hu, one_help, one_yue;
    private ImageView one_sit_big;
    private LinearLayout ll_one_sit_big;
    private RelativeLayout rl_one;


    @Override
    public void setContentView() {
        setContentView(R.layout.activity_combing_state);
    }

    @Override
    public void initViews() {
        //初始化titleBar
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.harmony_package_scheme), titleBar, 1);
        titleBar.setIv_familiesUnable();
        //经络梳理,全部跳转
        one_zhan = (ImageView) findViewById(R.id.iv_one_station);
        one_dai = (ImageView) findViewById(R.id.iv_one_wear);
        all_jingluo = (TextView) findViewById(R.id.tv_jingluo_all);
        one_zhan.setOnClickListener(this);
        one_dai.setOnClickListener(this);
        all_jingluo.setOnClickListener(this);
        //脏腑调理
        one_eat = (ImageView) findViewById(R.id.iv_one_eat);
        one_sit = (ImageView) findViewById(R.id.iv_one_sit);
        one_sit.setOnClickListener(this);
        one_eat.setOnClickListener(this);
        //一坐调整
        ll_one_sit_big = findViewById(R.id.ll_one_zuo_big);
        rl_one = findViewById(R.id.rl_one);
        one_sit_big = findViewById(R.id.iv_one_zuo_big);
        one_sit_big.setOnClickListener(this);
        //接收值
        String oneSit = getIntent().getStringExtra("OneSit");
        if (!TextUtils.isEmpty(oneSit)){
            if (DetectionJump.GAN_YU_FANG_AN.equals(oneSit)){
                ll_one_sit_big.setVisibility(View.VISIBLE);
                rl_one.setVisibility(View.GONE);
            }else {
                rl_one.setVisibility(View.VISIBLE);
                ll_one_sit_big.setVisibility(View.GONE);
            }
        }
        //体质调理
        one_drink = (ImageView) findViewById(R.id.iv_one_drink);
        one_drink.setOnClickListener(this);
        //状态护理
        one_ce = (ImageView) findViewById(R.id.iv_one_test);
        one_hu = (ImageView) findViewById(R.id.iv_one_hu);
        one_help = (ImageView) findViewById(R.id.iv_one_help);
        one_yue = (ImageView) findViewById(R.id.iv_one_yue);
        one_ce.setOnClickListener(this);
        one_hu.setOnClickListener(this);
        one_help.setOnClickListener(this);
        one_yue.setOnClickListener(this);
    }

    @Override
    public void initDatas() {
        //init ImageSrc
        jlslImagesMap = new HashMap<>();
        jlslImagesMap.put("yd", R.drawable.one_wear);
        jlslImagesMap.put("y_ting", R.drawable.one_hear);
        jlslImagesMap.put("y_zhan", R.drawable.one_station);
        jlslImagesMap.put("y_tie", R.drawable.one_attach);
        jlslImagesMap.put("yx", R.drawable.one_select);
        jlslImagesMap.put("yb", R.drawable.one_bian);
        jlslImagesMap.put("yj", R.drawable.one_jiu);
        jlslImagesMap.put("y_tui", R.drawable.one_push);
        jlslImagesMap.put("yg", R.drawable.one_scrape);
        // init ActivitySrc
        initActivitySrc("yd", getString(R.string.home_ear_prescription), "/member/service/view/fang/JLBS/1/","yidai");
        initActivitySrc("y_zhan", getString(R.string.home_sport_prescription), "/member/service/view/fang/JLBS/1/","yizhan");
        initActivitySrc("yy", getString(R.string.home_tizhi_prescription), "/member/service/view/fang/TZBS/1/","yiyin");
        initActivitySrc("yx", getString(R.string.home_shiliao_prescription), "/member/service/zf_chufang/","yichi");
        initActivitySrc("y_zuo", getString(R.string.home_zf_sport_prescription), "/member/service/view/fang/sn/1/","yizuo");
        initActivitySrc("y_ting", getString(R.string.combing_state_tizhengjiance), "/member/service/view/fang/sn/1/","yice");
        initActivitySrc("y_tie", getString(R.string.other_pay_yuyueguahao), "/member/service/view/fang/sn/1/","yihu");
        initActivitySrc("yb", getString(R.string.combing_state_shipinwenzhen), "/member/service/view/fang/sn/1/","yizhu");
        initActivitySrc("yj", getString(R.string.info_title), "/member/service/view/fang/sn/1/","yiyue");
        //todo 获得经络梳理信息（从上个页面获取或网络请求）
        slztItemList = new ArrayList<>();
        slztItemList.add("yd");
        slztItemList.add("y_zhan");

    }

    private void initActivitySrc(String key, String title, String type,String fangType) {
        if (slztActivityMap == null) {
            slztActivityMap = new HashMap<>();
        }
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("type", type);
        bundle.putString("fangType", fangType);

        slztActivityMap.put(key, bundle);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_one_wear:
                jumpActivity(slztItemList.get(0));
                break;
            case R.id.iv_one_station:
                jumpActivity(slztItemList.get(1));
                break;
            //经络全部
            case R.id.tv_jingluo_all:
                Intent intent = new Intent(this, CombingMeridianActivity.class);
                startActivity(intent);
                break;
            case R.id.iv_one_eat:
                jumpActivity("yx");
                break;
            //一坐小图
            case R.id.iv_one_sit:
                jumpActivity("y_zuo");
                break;
            //一坐大图
            case R.id.iv_one_zuo_big:
                jumpActivity("y_zuo");
                break;
            case R.id.iv_one_drink:
                jumpActivity("yy");
                break;
            //一测
            case R.id.iv_one_test:
                //手机制造商
                String deviceManufacturer = DeviceUtils.getDeviceManufacturer();
                if (deviceManufacturer.equals("HC03")) {
                    Intent intent2 = new Intent(this, MeasureInfoActivity.class);
                    startActivity(intent2);
                } else {
                    Intent intent2 = new Intent(this, FtjMeasureInfoActivity.class);
                    startActivity(intent2);
                }
                break;

            case R.id.iv_one_hu:
                //一呼
//                Intent intent3 = new Intent(this, CallExpertActivity.class);
//                Intent intent3 = new Intent(this, MainActivityHH.class);
//                startActivity(intent3);
                Intent intent3 = new Intent(this, CallSelectorAct.class);
//                Intent intent3 = new Intent(this, OnlineConsultingActivity.class);
                startActivity(intent3);

                break;
            case R.id.iv_one_help:
                //一助
                if (Constant.isEnglish){
                    Intent intent4 = new Intent(this, HelphospitalActivity.class);
                    startActivity(intent4);
                }else {
                    Member loginMember = LoginControllor.getLoginMember();
                    if (loginMember != null) {
                        String phone = loginMember.getUsername();
                        boolean mobileNO = IsMobile.isMobileNO(phone);
                        if (mobileNO) {
                            //绑定了手机号
                            Intent intent4 = new Intent(this, HelphospitalActivity.class);
                            startActivity(intent4);
                        } else {
                            //提示绑定手机号
                            setDialog_binding_phone();
                        }

                    }
                }

                break;
            case R.id.iv_one_yue:
                //一阅InformationActivity
                Intent intent5 = new Intent(this, InformationActivity.class);
                startActivity(intent5);
                break;
            default:
                break;
        }
    }

    private void jumpActivity(String key) {
        if (key != null) {
            Intent intent = new Intent(this, WebViewActivity.class);
            Bundle bundle = slztActivityMap.get(key);
            intent.putExtra("title", bundle.getString("title"));
            intent.putExtra("type", bundle.getString("type"));
            intent.putExtra("fangType", bundle.getString("fangType"));
            startActivity(intent);
        }
    }

    /**
     * 设置提示弹窗
     */
    private Dialog dialog_bind_phone;
    public void setDialog_binding_phone(){
        dialog_bind_phone = new Dialog(this,R.style.dialog);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_activate, null);
        dialog_bind_phone.setContentView(view);
        //初始化控件
        Button btn_dialog_activate_no = view.findViewById(R.id.btn_dialog_activate_no);
        Button btn_dialog_activate_yes = view.findViewById(R.id.btn_dialog_activate_yes);
        TextView tv_dialog_text = view.findViewById(R.id.tv_dialog_text);

        tv_dialog_text.setText(R.string.ftj_callSelectorAct_glsj);

        btn_dialog_activate_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_bind_phone.dismiss();
            }
        });

        btn_dialog_activate_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //跳转至绑手机号页面
                Intent intent = new Intent(CombingStateActivity.this, BandingPhoneNumberActivity.class);
                startActivity(intent);
                dialog_bind_phone.dismiss();
            }
        });
        dialog_bind_phone.show();
        dialog_bind_phone.setCanceledOnTouchOutside(false);
        dialog_bind_phone.setCancelable(true);
    }
}
