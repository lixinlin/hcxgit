package com.hxlm.hcyphone.bean;

/**
 * Created by lixinlin on 2017/6/19.
 */
public class HealthMonitorSuggest {
  private String monitorSuggest;
  private String hearRateMonitorFreqency;
  private String bloodPressureMonitorFreqency;
  private String bloodGlucoseMonitorFreqency;
  private String bloodOxygenMonitorFreqency;
  private String breatheMonitorFreqency;
  private String thermometerMonitorFreqency;

    public HealthMonitorSuggest() {
    }

    public HealthMonitorSuggest(String monitorSuggest, String hearRateMonitorFreqency, String bloodPressureMonitorFreqency, String bloodGlucoseMonitorFreqency, String bloodOxygenMonitorFreqency, String breatheMonitorFreqency, String thermometerMonitorFreqency) {
        this.monitorSuggest = monitorSuggest;
        this.hearRateMonitorFreqency = hearRateMonitorFreqency;
        this.bloodPressureMonitorFreqency = bloodPressureMonitorFreqency;
        this.bloodGlucoseMonitorFreqency = bloodGlucoseMonitorFreqency;
        this.bloodOxygenMonitorFreqency = bloodOxygenMonitorFreqency;
        this.breatheMonitorFreqency = breatheMonitorFreqency;
        this.thermometerMonitorFreqency = thermometerMonitorFreqency;
    }

    public String getMonitorSuggest() {
        return monitorSuggest;
    }

    public void setMonitorSuggest(String monitorSuggest) {
        this.monitorSuggest = monitorSuggest;
    }

    public String getHearRateMonitorFreqency() {
        return hearRateMonitorFreqency;
    }

    public void setHearRateMonitorFreqency(String hearRateMonitorFreqency) {
        this.hearRateMonitorFreqency = hearRateMonitorFreqency;
    }

    public String getBloodPressureMonitorFreqency() {
        return bloodPressureMonitorFreqency;
    }

    public void setBloodPressureMonitorFreqency(String bloodPressureMonitorFreqency) {
        this.bloodPressureMonitorFreqency = bloodPressureMonitorFreqency;
    }

    public String getBloodGlucoseMonitorFreqency() {
        return bloodGlucoseMonitorFreqency;
    }

    public void setBloodGlucoseMonitorFreqency(String bloodGlucoseMonitorFreqency) {
        this.bloodGlucoseMonitorFreqency = bloodGlucoseMonitorFreqency;
    }

    public String getBloodOxygenMonitorFreqency() {
        return bloodOxygenMonitorFreqency;
    }

    public void setBloodOxygenMonitorFreqency(String bloodOxygenMonitorFreqency) {
        this.bloodOxygenMonitorFreqency = bloodOxygenMonitorFreqency;
    }

    public String getBreatheMonitorFreqency() {
        return breatheMonitorFreqency;
    }

    public void setBreatheMonitorFreqency(String breatheMonitorFreqency) {
        this.breatheMonitorFreqency = breatheMonitorFreqency;
    }

    public String getThermometerMonitorFreqency() {
        return thermometerMonitorFreqency;
    }

    public void setThermometerMonitorFreqency(String thermometerMonitorFreqency) {
        this.thermometerMonitorFreqency = thermometerMonitorFreqency;
    }

    @Override
    public String toString() {
        return "HealthMonitorSuggest{" +
                "monitorSuggest='" + monitorSuggest + '\'' +
                ", hearRateMonitorFreqency='" + hearRateMonitorFreqency + '\'' +
                ", bloodPressureMonitorFreqency='" + bloodPressureMonitorFreqency + '\'' +
                ", bloodGlucoseMonitorFreqency='" + bloodGlucoseMonitorFreqency + '\'' +
                ", bloodOxygenMonitorFreqency='" + bloodOxygenMonitorFreqency + '\'' +
                ", breatheMonitorFreqency='" + breatheMonitorFreqency + '\'' +
                ", thermometerMonitorFreqency='" + thermometerMonitorFreqency + '\'' +
                '}';
    }
}
