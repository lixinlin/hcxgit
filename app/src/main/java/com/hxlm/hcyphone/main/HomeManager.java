package com.hxlm.hcyphone.main;

import com.alibaba.fastjson.JSON;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.hcyphone.bean.IndexPicBean;
import com.loopj.android.http.RequestParams;

/**
 * Created by Lirong on 2018/12/14.
 */

public class HomeManager {

    /**
     * 获取首页图片
     * @param callback
     */
    public void getIndexPic(AbstractDefaultHttpHandlerCallback callback){
        String url = "/mobile/index/newIndexpic.jhtml";

        RequestParams params = new RequestParams();

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                return JSON.parseArray(content, IndexPicBean.class);
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

}
