package com.hxlm.database;

import android.app.Application;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.hxlm.database.dao.DaoMaster;
import com.hxlm.database.dao.DaoSession;


public class GreenDaoManager {
    private static final String DB_NAME = "ICD10.db";
    private static DaoSession mDaoSession;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public static void setDatabase(Application application) {

        MySQLiteOpenHelper mSQLiteOpenHelper = new MySQLiteOpenHelper(application, DB_NAME, null);
        DaoMaster mDaoMaster = new DaoMaster(mSQLiteOpenHelper.getWritableDatabase());
        mSQLiteOpenHelper.setWriteAheadLoggingEnabled(false);
        mSQLiteOpenHelper.getWritableDatabase().disableWriteAheadLogging();
        mDaoSession = mDaoMaster.newSession();
    }

    public static DaoSession getDaoSession() {
        return mDaoSession;
    }
}
