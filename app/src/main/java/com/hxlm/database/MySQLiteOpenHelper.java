package com.hxlm.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import com.github.yuweiguocn.library.greendao.MigrationHelper;
import com.hxlm.database.dao.DaoMaster;
import com.hxlm.database.dao.ICD10Dao;

import org.greenrobot.greendao.database.Database;


public class MySQLiteOpenHelper extends DaoMaster.OpenHelper {

    public MySQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {

        MigrationHelper.migrate(db,
                new MigrationHelper.ReCreateAllTableListener() {
                    @Override
                    public void onCreateAllTables(Database db, boolean ifNotExists) {
                        Log.e("YanHuang", "onCreateAllTables");
                        DaoMaster.createAllTables(db, true);
                    }

                    @Override
                    public void onDropAllTables(Database db, boolean ifExists) {
                        Log.e("YanHuang", "onDropAllTables");
                        DaoMaster.dropAllTables(db, ifExists);
                    }
                }, ICD10Dao.class);
    }
}