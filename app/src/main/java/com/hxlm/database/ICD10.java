package com.hxlm.database;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;

/**
 * @author Lirong
 * @date 2019/7/10.
 * @description
 */

@Entity(createInDb = false)
public class ICD10 {
    private Long rowid;
    private int SEX;
    private String content;
    private String content_en;
    private String MICD;
    private String MTJI;
    @Generated(hash = 769653222)
    public ICD10(Long rowid, int SEX, String content, String content_en,
            String MICD, String MTJI) {
        this.rowid = rowid;
        this.SEX = SEX;
        this.content = content;
        this.content_en = content_en;
        this.MICD = MICD;
        this.MTJI = MTJI;
    }
    @Generated(hash = 556123173)
    public ICD10() {
    }
    public Long getRowid() {
        return this.rowid;
    }
    public void setRowid(Long rowid) {
        this.rowid = rowid;
    }
    public int getSEX() {
        return this.SEX;
    }
    public void setSEX(int SEX) {
        this.SEX = SEX;
    }
    public String getContent() {
        return this.content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public String getContent_en() {
        return this.content_en;
    }
    public void setContent_en(String content_en) {
        this.content_en = content_en;
    }
    public String getMICD() {
        return this.MICD;
    }
    public void setMICD(String MICD) {
        this.MICD = MICD;
    }
    public String getMTJI() {
        return this.MTJI;
    }
    public void setMTJI(String MTJI) {
        this.MTJI = MTJI;
    }
}
