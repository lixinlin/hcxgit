package com.hcy_futejia.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyphone.utils.GlideApp;

public class BigPhotoActivity extends BaseActivity {

    private ImageView iv_photo;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_big_photo);
    }

    @Override
    public void initViews() {
        iv_photo = findViewById(R.id.iv_photo);

        String photo = getIntent().getStringExtra("photo");
        GlideApp.with(this).load(photo).into(iv_photo);
    }

    @Override
    public void initDatas() {

    }
}
