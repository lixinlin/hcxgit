package com.hcy_futejia.activity;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hcy.ky3h.R;
import com.hcy_futejia.manager.UserEnglishManager;
import com.hcy_futejia.widget.VerifyCodeView;
import com.hxlm.android.hcy.AbstractBaseActivity;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.UserManager;
import com.hxlm.android.hcy.utils.SpUtils;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.util.StatusBarUtils;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyphone.MainActivity;

import java.util.Timer;
import java.util.TimerTask;

public class FtjVerifyCodeActivity extends AbstractBaseActivity implements View.OnClickListener {
    VerifyCodeView verifyCodeView;
    private TextView tv_number;
    private Button tv_problem;
    private UserEnglishManager userEnglishManager;
    private int codeType;
    private TextView tv_have_account;
    private int userType;

    public static OnCompleteListener onCompleteListener;


    @Override
    protected void setContentView() {
        StatusBarUtils.setWindowStatusBarColor(this, R.color.white);
        setContentView(R.layout.activity_ftj_verify_code);
    }

    @Override
    protected void initViews() {

        tv_number = findViewById(R.id.tv_number);
        tv_problem = findViewById(R.id.tv_problem);
        tv_have_account = findViewById(R.id.tv_have_account);
        tv_problem.setVisibility(View.GONE);

        tv_have_account.setOnClickListener(this);
        tv_problem.setOnClickListener(this);
        userEnglishManager = new UserEnglishManager();
        Intent intent = getIntent();
        // 1手机号注册  2邮箱注册
        codeType = intent.getIntExtra("codeType", 0);
        String createAccount = (String) SpUtils.get(FtjVerifyCodeActivity.this, "createAccount", "");
        tv_number.setText("Enter the code sent to +1 "+createAccount);

        verifyCodeView = (VerifyCodeView) findViewById(R.id.verify_code_view);
        verifyCodeView.setInputCompleteListener(new VerifyCodeView.InputCompleteListener() {
            @Override
            public void inputComplete() {
                if (codeType == 1){
                    String createAccount = (String) SpUtils.get(FtjVerifyCodeActivity.this, "createAccount", "");
                    String createPwd = (String) SpUtils.get(FtjVerifyCodeActivity.this,"createPwd","");
                    String editContent = verifyCodeView.getEditContent();
                    Log.e("retrofit","======短信验证码1====="+editContent);
                    userEnglishManager.getPhoneLogin(createAccount, createPwd, editContent, new AbstractDefaultHttpHandlerCallback(FtjVerifyCodeActivity.this) {
                        @Override
                        protected void onResponseSuccess(Object obj) {
                            BaseApplication.destoryActivity("loginActivity");
                            BaseApplication.destoryActivity("accountActivity");
                            BaseApplication.destoryActivity("passwordActivity");
                            goLoginMM(createAccount,createPwd);
                        }
                    });
                }else if (codeType == 2){
                    String createAccount = (String) SpUtils.get(FtjVerifyCodeActivity.this, "createAccount", "");
                    String createPwd = (String) SpUtils.get(FtjVerifyCodeActivity.this,"createPwd","");
                    String editContent = verifyCodeView.getEditContent();
                    Log.e("retrofit","======短信验证码2====="+editContent);
                    userEnglishManager.getEmailLogin(createAccount, createPwd, editContent, new AbstractDefaultHttpHandlerCallback(FtjVerifyCodeActivity.this) {
                        @Override
                        protected void onResponseSuccess(Object obj) {
                            BaseApplication.destoryActivity("loginActivity");
                            BaseApplication.destoryActivity("accountActivity");
                            BaseApplication.destoryActivity("passwordActivity");
                            goLoginMM(createAccount,createPwd);

                        }
                    });
                }
//                Toast.makeText(FtjVerifyCodeActivity.this, "inputComplete: " + verifyCodeView.getEditContent(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void invalidContent() {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tv_problem.setVisibility(View.VISIBLE);
            }
        }, 10000);
    }

    //登录
    private void goLoginMM(final String userName, final String password) {

            new UserManager().login(userName, password, new AbstractDefaultHttpHandlerCallback(FtjVerifyCodeActivity.this) {
                @Override
                protected void onResponseSuccess(Object obj) {
                    LoginControllor.setAutoLoginByMiMa(true, userName, password);
                    LoginControllor.setIsAutologinBySms(false, "");
                    LoginControllor.setAutoLoginByWeiChart(false, "", "", "", "");
                    if (null != onCompleteListener) {
                        onCompleteListener.onComplete();
                    }
                    Constant.loginType = 1;
                    //跳转主界面
                    toMainActivity();
                }

            });

    }

    private void toMainActivity() {
        Intent intentMain = new Intent(FtjVerifyCodeActivity.this, MainActivity.class);
        startActivity(intentMain);
        FtjVerifyCodeActivity.this.finish();
    }

    @Override
    protected void initDatas() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_have_account:
                Intent intent = new Intent(FtjVerifyCodeActivity.this, EnglishLoginActivity.class);
                startActivity(intent);
                FtjVerifyCodeActivity.this.finish();
                break;
            //获取不到验证码的时候
            case R.id.tv_problem:
                getCode();
                break;
            default:
                break;
        }
    }

    private void getCode () {
        //userType 1 发送手机验证码  2发送邮箱验证码
        String createAccount = (String) SpUtils.get(FtjVerifyCodeActivity.this, "createAccount", "");
        Intent intent = getIntent();
        userType = intent.getIntExtra("codeType", 0);
        if (userType == 1) {
            userEnglishManager.getPhoneCodeCreate(createAccount, new AbstractDefaultHttpHandlerCallback(FtjVerifyCodeActivity.this) {
                @Override
                protected void onResponseSuccess(Object obj) {
                    ToastUtil.invokeShortTimeToast(FtjVerifyCodeActivity.this,"Verification code sent successfully");
                    Log.e("retrofit", "====外国手机号重新获取验证码接口===" + obj.toString());
                }
            });
        } else if (userType == 2) {
            userEnglishManager.getEmailCodeCreate(createAccount, new AbstractDefaultHttpHandlerCallback(FtjVerifyCodeActivity.this) {
                @Override
                protected void onResponseSuccess(Object obj) {
                    ToastUtil.invokeShortTimeToast(FtjVerifyCodeActivity.this,"Verification code sent successfully");
                    Log.e("retrofit", "====邮箱重新获取验证码接口===" + obj.toString());
                }
            });
        }
    }

}