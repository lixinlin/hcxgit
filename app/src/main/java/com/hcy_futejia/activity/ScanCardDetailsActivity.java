package com.hcy_futejia.activity;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.google.gson.Gson;
import com.hcy.ky3h.R;
import com.hcy_futejia.adapter.ScanCardDetailsAdapter;
import com.hcy_futejia.bean.ScanCardDetailsBean;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.bean.CardRecordBean;
import com.hxlm.android.hcy.bean.CardServiceBean;
import com.hxlm.android.hcy.order.CardManager;
import com.hxlm.android.hcy.order.card.CardDetailsActivity;
import com.hxlm.android.hcy.order.card.CardDetailsAdapter;
import com.hxlm.android.hcy.order.card.ConsumptionRecordsAdapter;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.util.ToastUtil;

import java.io.Serializable;
import java.util.List;

public class ScanCardDetailsActivity extends BaseActivity implements View.OnClickListener {

    private TextView tvTitle;
    private TextView tvIntroduce;

    private RecyclerView rvRecord;
    private RecyclerView rvCardDetails;

    private TextView tv_service;
    private TextView tv_record;
    private Intent intent;
    private ImageView iv_record;
    private Dialog dialog;
    private List<CardRecordBean> cardRecordBeans;
    private Button btn_add_card_package;
    private CardManager cardManager;
    private ScanCardDetailsBean detailsBean;
    private TextView tvNum;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_card_details);
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.kapianxinxi), titleBar, 1);
        //初始化控件
        tv_service = findViewById(R.id.tv_service);
        tv_record = findViewById(R.id.tv_record);
        tvTitle = findViewById(R.id.tv_card_details_title);
        tvIntroduce = findViewById(R.id.tv_card_details_introduce);
        tvNum = findViewById(R.id.tv_card_details_num);
        rvCardDetails = findViewById(R.id.rv_card_details);
        rvRecord = findViewById(R.id.rv_card_details_record);

        iv_record = findViewById(R.id.iv_record);
        iv_record.setVisibility(View.GONE);

        btn_add_card_package = findViewById(R.id.btn_add_card_package);
        btn_add_card_package.setOnClickListener(this);
        btn_add_card_package.setVisibility(View.VISIBLE);

        //获取卡号
        intent = getIntent();
        detailsBean = (ScanCardDetailsBean) intent.getSerializableExtra("detailsBean");
        Log.i("retrofit","===消费记录卡描述信息11111111=="+detailsBean.getCode());


        //设置标题
        tvTitle.setText(detailsBean.getName());
        if (!TextUtils.isEmpty(detailsBean.getCode())) {
            tvNum.setText(getString(R.string.card_details_card_no)+detailsBean.getCode());
        }else {
            tvNum.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(detailsBean.getDescription())){
            tvIntroduce.setText(detailsBean.getDescription());
        }else{
            tvIntroduce.setVisibility(View.GONE);
        }

        //剩余服务
        rvCardDetails.setLayoutManager(new LinearLayoutManager(this));

        //消费记录
        rvRecord.setLayoutManager(new LinearLayoutManager(this));
        rvRecord.setHasFixedSize(true);
        rvRecord.setNestedScrollingEnabled(false);
    }

    @Override
    public void initDatas() {
                //data   集合1：剩余服务    集合2：消费记录
                //剩余服务
        cardManager = new CardManager();
        List<ScanCardDetailsBean.LmMdServiceAttrBean> lmMdServiceAttr = detailsBean.getLmMdServiceAttr();
        Log.e("retrofit","===二维码扫描后详情的剩余服务==="+detailsBean.toString());
        if (lmMdServiceAttr != null && lmMdServiceAttr.size() != 0) {
                    tv_service.setVisibility(View.VISIBLE);
                    rvCardDetails.setVisibility(View.VISIBLE);
                ScanCardDetailsAdapter cardDetailsAdapter = new ScanCardDetailsAdapter(ScanCardDetailsActivity.this, lmMdServiceAttr);
                    rvCardDetails.setAdapter(cardDetailsAdapter);
                }else {
                    tv_service.setVisibility(View.GONE);
                    rvCardDetails.setVisibility(View.GONE);
                }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            //添加至卡包
            case R.id.btn_add_card_package:
                Log.e("retrofit","===点击了添加至卡包==="+detailsBean.getCode());
                cardManager.bindCard(detailsBean.getCode(), new AbstractDefaultHttpHandlerCallback(this) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        ToastUtil.invokeShortTimeToast(ScanCardDetailsActivity.this,getString(R.string.input_add_card_tips_success));
                        ScanCardDetailsActivity.this.finish();
                    }

                });
                break;
            default:
                break;
        }
    }
}
