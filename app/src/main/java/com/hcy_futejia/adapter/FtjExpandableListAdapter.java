package com.hcy_futejia.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.bean.Symptom;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;
import retrofit2.http.POST;

public class FtjExpandableListAdapter extends BaseExpandableListAdapter {


    private Context context;
    private Map<String, List<String>> part;
    private List<String> partNames;
    private List<Symptom> symptomsToShow;
    private int showTag = 0;

    private int groupClickTemp = 0;
    private int childClickTemp = 0;

    public FtjExpandableListAdapter(Context context, Map<String, List<String>> part, List<String> partNames) {
        this.context = context;
        this.part = part;
        this.partNames = partNames;
        // this.listdata1 = listdata1;
    }

    public void setSymptomsToShowData(List<Symptom> symptomsToShow) {
        showTag = 1;
        this.symptomsToShow = symptomsToShow;
    }

    /**
     * 获取一级标签总数
     */
    @Override
    public int getGroupCount() {
        return part.size();
    }

    /**
     * 获取一级标签下二级标签的总数
     */
    @Override
    public int getChildrenCount(int groupPosition) {
        return part.get(partNames.get(groupPosition)).size();
    }

    /**
     * 获取一级标签内容
     */
    @Override
    public Object getGroup(int groupPosition) {
        // return list.get(groupPosition).get("txt");
        return part.get(partNames.get(groupPosition));
        // return listdata1.get(groupPosition).getData1();
    }

    /**
     * 获取一级标签下二级标签的内容
     */
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        // return child_text_array[groupPosition][childPosition];
        return part.get(partNames.get(groupPosition)).get(childPosition);
        // return listdata1.get(groupPosition).getListData2().get(childPosition)
        // .getData2();
    }

    /**
     * 获取一级标签的ID
     */
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    /**
     * 获取二级标签的ID
     */
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    /**
     * 指定位置相应的组视图
     */
    @Override
    public boolean hasStableIds() {
        return true;
    }

    public void setGroupSelection(int groupPosition) {
        this.groupClickTemp = groupPosition;
    }

    public void setChildSelection(int childPosition) {
        this.childClickTemp = childPosition;
    }

    /**
     * 对一级标签进行设置
     */
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        ViewHolderGroup viewHolderGroup;
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_ftj_expandable_list, parent, false);
            viewHolderGroup = new ViewHolderGroup();
            viewHolderGroup.tv_group_title = convertView.findViewById(R.id.tv_title);
            convertView.setTag(viewHolderGroup);
        } else {
            viewHolderGroup = (ViewHolderGroup) convertView.getTag();
        }

        if (groupClickTemp == groupPosition) {
            viewHolderGroup.tv_group_title.setBackgroundResource(R.drawable.radius_viscera_identity_tab_selected);
            viewHolderGroup.tv_group_title.setTextColor(Color.parseColor("#ffffff"));
        } else {
            viewHolderGroup.tv_group_title.setBackgroundResource(R.drawable.radius_expandable_group_white);
            viewHolderGroup.tv_group_title.setTextColor(Color.parseColor("#8e8e93"));
        }

        viewHolderGroup.tv_group_title.setText(partNames.get(groupPosition));

        return convertView;
    }

    /**
     * 对一级标签下的二级标签进行设置
     */
    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolderChild viewHolderChild;
        if (null == convertView) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_ftj_expandable_child, parent, false);
            viewHolderChild = new ViewHolderChild();
            viewHolderChild.tv_text_child = convertView.findViewById(R.id.tv_child_text);
            QBadgeView qBadgeView = new QBadgeView(context);
            viewHolderChild.qBadgeView = qBadgeView;
            convertView.setTag(viewHolderChild);
        } else {
            viewHolderChild = (ViewHolderChild) convertView.getTag();
        }
        if (childClickTemp == childPosition) {
            viewHolderChild.tv_text_child.setTextColor(Color.parseColor("#ffa200"));
        } else {
            viewHolderChild.tv_text_child.setTextColor(Color.parseColor("#8e8e93"));
        }
       viewHolderChild.qBadgeView.bindTarget(viewHolderChild.tv_text_child).setBadgeNumber(0);
        viewHolderChild.tv_text_child.setText(part.get(partNames.get(groupPosition)).get(
                childPosition));
        if (symptomsToShow!=null&&symptomsToShow.size() != 0) {
            handle_something(viewHolderChild, viewHolderChild.qBadgeView);
        }



        return convertView;
    }

    private void handle_something(ViewHolderChild viewHolderChild, QBadgeView qBadgeView) {
        String text = viewHolderChild.tv_text_child.getText().toString();
        Log.d("handle_something", ": ***********************");
        Log.d("handle_something", "tv_text_child" + text);
        Map<String, Integer> map = new HashMap<>();
        for (Symptom symptom : symptomsToShow) {
            Log.d("handle_something", "symptom" + symptom.getPart());
            String key = symptom.getPart();
            if (map.containsKey(key)) {
                Integer integer = map.get(key);
                map.put(key, integer + 1);
            } else {
                map.put(key, 1);
            }
        }

        if (map.containsKey(text)) {
            qBadgeView.bindTarget(viewHolderChild.tv_text_child).setBadgeNumber(map.get(text));
            qBadgeView.setBadgeTextColor(Color.parseColor("#ffffff"));
            qBadgeView.setBadgeBackgroundColor(Color.parseColor("#D81E06"));
        }


    }

    /**
     * 当选择子节点的时候，调用该方法
     */
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    class ViewHolderGroup {
        TextView tv_group_title;
    }

    class ViewHolderChild {
        TextView tv_text_child;
        QBadgeView qBadgeView;
    }

}
