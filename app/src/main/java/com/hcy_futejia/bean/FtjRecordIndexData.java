package com.hcy_futejia.bean;

import java.util.Date;

/**
 * @author Lirong
 * @date 2019/5/5.
 * @description
 */

public class FtjRecordIndexData {

    private String category;
    private String result;
    private long time;
    private int imageResourceId;
    private Date createTime;
    private int highPressure;
    private int lowPressure;
    private int pulse;
    private Date inputDate;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getInputDate() {
        return inputDate;
    }

    public void setInputDate(Date inputDate) {
        this.inputDate = inputDate;
    }

    public int getHighPressure() {
        return highPressure;
    }

    public void setHighPressure(int highPressure) {
        this.highPressure = highPressure;
    }

    public int getLowPressure() {
        return lowPressure;
    }

    public void setLowPressure(int lowPressure) {
        this.lowPressure = lowPressure;
    }

    public int getPulse() {
        return pulse;
    }

    public void setPulse(int pulse) {
        this.pulse = pulse;
    }

    public FtjRecordIndexData(Date inputDate,int imageResourceId, String category, String result, long time,int highPressure,int lowPressure,int pulse,String type) {
        super();
        this.inputDate = inputDate;
        this.imageResourceId = imageResourceId;
        this.category = category;
        this.result = result;
        this.time = time;
        this.highPressure = highPressure;
        this.lowPressure = lowPressure;
        this.pulse = pulse;
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }


    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "FtjRecordIndexData{" +
                "category='" + category + '\'' +
                ", result='" + result + '\'' +
                ", time=" + time +
                ", imageResourceId=" + imageResourceId +
                ", createTime=" + createTime +
                ", highPressure=" + highPressure +
                ", lowPressure=" + lowPressure +
                ", pulse=" + pulse +
                ", inputDate=" + inputDate +
                '}';
    }
}
