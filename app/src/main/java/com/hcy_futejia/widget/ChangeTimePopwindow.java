package com.hcy_futejia.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hcy_futejia.widget.wheelview.OnWheelChangedListener;
import com.hcy_futejia.widget.wheelview.OnWheelScrollListener;
import com.hcy_futejia.widget.wheelview.WheelView;
import com.hcy_futejia.widget.wheelview.adapters.AbstractWheelTextAdapter1;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * @author :  Chen.yuan
 * Email:   hubeiqiyuan2010@163.com
 * Date:    2016/7/28 17:37
 * Description:日期选择window
 */
public class ChangeTimePopwindow extends PopupWindow implements View.OnClickListener {

	private Context context;
	private WheelView wvYear;
	private WheelView wvMonth;
	private WheelView wvDay;

	private TextView btnSure;
	private TextView btnCancel;

	private CalendarTextAdapter mYearAdapter;
	private CalendarTextAdapter mMonthAdapter;
	private CalendarTextAdapter mDaydapter;

	private ArrayList<String> arry_apm = new ArrayList<String>();
	private ArrayList<String> arry_hour = new ArrayList<String>();
	private ArrayList<String> arry_minute = new ArrayList<String>();
	private String currentApm = getApm();
	private String currentHour = getHour();
	private String currentMinute = getMinute();

	private int maxTextSize = 16;
	private int minTextSize = 14;

	private boolean issetdata = false;

	private String selectYear;
	private String selectMonth;
	private String selectDay;

	private OnBirthListener onBirthListener;

	public ChangeTimePopwindow(final Context context) {
		super(context);
		this.context = context;
		View view= View.inflate(context, R.layout.dialog_change_birth,null);
		wvYear =  view.findViewById(R.id.wv_birth_year);
		wvMonth =  view.findViewById(R.id.wv_birth_month);
		wvDay =  view.findViewById(R.id.wv_birth_day);
		btnSure = view.findViewById(R.id.btn_birth_finish);
		btnCancel =  view.findViewById(R.id.btn_birth_cancel);

		//设置SelectPicPopupWindow的View
		this.setContentView(view);
		//设置SelectPicPopupWindow弹出窗体的宽
		this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
		//设置SelectPicPopupWindow弹出窗体的高
		this.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
		//设置SelectPicPopupWindow弹出窗体可点击
		this.setFocusable(true);
		//设置SelectPicPopupWindow弹出窗体动画效果
//		this.setAnimationStyle(R.style.AnimBottom);
		//实例化一个ColorDrawable颜色为半透明
		ColorDrawable dw = new ColorDrawable(0xb0000000);
		//设置SelectPicPopupWindow弹出窗体的背景
		this.setBackgroundDrawable(dw);

		btnSure.setOnClickListener(this);
		btnCancel.setOnClickListener(this);

		if (!issetdata) {
			initData();
		}

		initApm();
		mYearAdapter = new CalendarTextAdapter(context, arry_apm, 0, maxTextSize, minTextSize);
		wvYear.setVisibleItems(5);
		wvYear.setViewAdapter(mYearAdapter);
		wvYear.setCurrentItem(setApm(currentApm));

		initHour();
		mMonthAdapter = new CalendarTextAdapter(context, arry_hour, 0, maxTextSize, minTextSize);
		wvMonth.setVisibleItems(5);
		wvMonth.setViewAdapter(mMonthAdapter);
		wvMonth.setCurrentItem(setHour(currentHour));

		initMinute();
		mDaydapter = new CalendarTextAdapter(context, arry_minute, 0, maxTextSize, minTextSize);
		wvDay.setVisibleItems(5);
		wvDay.setViewAdapter(mDaydapter);
		wvDay.setCurrentItem(setMinute(currentMinute));

		wvYear.addChangingListener(new OnWheelChangedListener() {

			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				// TODO Auto-generated method stub
				String currentText = (String) mYearAdapter.getItemText(wheel.getCurrentItem());
				selectYear = currentText;
				setTextviewSize(currentText, mYearAdapter);
			}
		});

		wvYear.addScrollingListener(new OnWheelScrollListener() {

			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				String currentText = (String) mYearAdapter.getItemText(wheel.getCurrentItem());
				setTextviewSize(currentText, mYearAdapter);
			}
		});

		wvMonth.addChangingListener(new OnWheelChangedListener() {


			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				String currentText = (String) mMonthAdapter.getItemText(wheel.getCurrentItem());
				selectMonth = currentText;
				setTextviewSize(currentText, mMonthAdapter);
			}
		});

		wvMonth.addScrollingListener(new OnWheelScrollListener() {

			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				String currentText = (String) mMonthAdapter.getItemText(wheel.getCurrentItem());
				setTextviewSize(currentText, mMonthAdapter);
			}
		});

		wvDay.addChangingListener(new OnWheelChangedListener() {

			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				// TODO Auto-generated method stub
				String currentText = (String) mDaydapter.getItemText(wheel.getCurrentItem());
				setTextviewSize(currentText, mDaydapter);
				selectDay = currentText;
			}
		});

		wvDay.addScrollingListener(new OnWheelScrollListener() {

			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				String currentText = (String) mDaydapter.getItemText(wheel.getCurrentItem());
				setTextviewSize(currentText, mDaydapter);
			}
		});
	}

	/**
	 * 获取apm下标
	 * @param apm
	 * @return
	 */
	private int setApm(String apm){
		Log.e("retrofit","======时间====上午还是下午呢===="+apm);
		int apmIndex = 0;
		for (int i = 0; i < arry_apm.size(); i++) {
			if (Integer.parseInt(apm) == i){
				return apmIndex;
			}
			apmIndex++;
		}
		return apmIndex++;
	}

	/**
	 * 获取hour下标
	 */
	private int setHour(String hour){
		int hourIndex = 0;
		for (int i = 1; i < arry_hour.size(); i++) {
			if (i == Integer.parseInt(hour)){
				return hourIndex;
			}
			hourIndex++;
		}
		return hourIndex;
	}

	/**
	 * 获取minute下标
	 */
	private int setMinute(String minute){
		int minuteIndex = 0;
		for (int i = 0; i < arry_minute.size(); i++) {
			if (Integer.parseInt(minute) == i){
				return minuteIndex;
			}
			minuteIndex++;
		}
		return minuteIndex;
	}

	public void initApm(){
		arry_apm.clear();
		arry_apm.add(context.getString(R.string.shangwu));
		arry_apm.add(context.getString(R.string.xiawu));
	}

	public void initHour(){
		arry_hour.clear();
		for (int i = 1; i <= 12; i++) {
			arry_hour.add(i+"");
		}
	}

	public void initMinute(){
		arry_minute.clear();
		for (int i = 0; i < 60; i++) {
			if (i<10){
				arry_minute.add("0"+i);
			}else {
				arry_minute.add(i+"");
			}
		}
	}

	private class CalendarTextAdapter extends AbstractWheelTextAdapter1 {
		ArrayList<String> list;

		protected CalendarTextAdapter(Context context, ArrayList<String> list, int currentItem, int maxsize, int minsize) {
			super(context, R.layout.item_birth_year, NO_RESOURCE, currentItem, maxsize, minsize);
			this.list = list;
			setItemTextResource(R.id.tempValue);
		}

		@Override
		public View getItem(int index, View cachedView, ViewGroup parent) {
			View view = super.getItem(index, cachedView, parent);
			TextView tv = view.findViewById(R.id.tempValue);
			setTextviewSize(tv.getText().toString(),this);
			return view;

		}

		@Override
		public int getItemsCount() {
			return list.size();
		}

		@Override
		protected CharSequence getItemText(int index) {
			return list.get(index) + "";
		}
	}

	public void setBirthdayListener(OnBirthListener onBirthListener) {
		this.onBirthListener = onBirthListener;
	}

	@Override
	public void onClick(View v) {

		if (v == btnSure) {
			if (onBirthListener != null) {
				onBirthListener.onClick(selectYear, selectMonth, selectDay);
				Log.d("cy",""+selectYear+""+selectMonth+""+selectDay);
			}
		} else if (v == btnSure) {

		}  else {
			dismiss();
		}
		dismiss();

	}

	public interface OnBirthListener {
		public void onClick(String year, String month, String day);
	}

	/**
	 * 设置字体大小
	 *
	 * @param curriteItemText
	 * @param adapter
	 */
	public void setTextviewSize(String curriteItemText, CalendarTextAdapter adapter) {
		ArrayList<View> arrayList = adapter.getTestViews();
		int size = arrayList.size();
		String currentText;
		for (int i = 0; i < size; i++) {
			TextView textvew = (TextView) arrayList.get(i);
			currentText = textvew.getText().toString();
			if (curriteItemText.equals(currentText)) {
				textvew.setTextSize(maxTextSize);
//				textvew.setTextColor(Color.parseColor("#ffa200"));
			} else {
				textvew.setTextSize(minTextSize);
//				textvew.setTextColor(Color.parseColor("#000000"));
			}
		}
	}

	public String getApm() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.AM_PM)+"";
	}

	public String getHour() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.HOUR) +"";
	}

	public String getMinute() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.MINUTE)+"";
	}

	public void initData() {
		setDate(getApm(), getHour(), getMinute());
		this.currentApm = getApm() +"";
		this.currentHour = getHour()+"";
		this.currentMinute = getMinute()+"";
	}

	/**
	 * 设置年月日
	 *
	 * @param year
	 * @param month
	 * @param day
	 */
	public void setDate(String year, String month, String day) {
		selectYear = year;
		selectMonth = month;
		selectDay = day ;
		issetdata = true;
		this.currentApm = year;
		this.currentHour = month;
		this.currentMinute = day;

	}
}