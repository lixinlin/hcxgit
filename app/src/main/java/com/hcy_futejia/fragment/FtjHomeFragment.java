package com.hcy_futejia.fragment;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.hcy.ky3h.R;
import com.hcy.ky3h.collectdata.CDDeviceUtils;
import com.hcy.ky3h.collectdata.CDRequestUtils;
import com.hcy_futejia.activity.FtjBodyQuestionActivity;
import com.hcy_futejia.activity.FtjUserInfoActivity;
import com.hcy_futejia.activity.FtjVisceraIdentityActivity;
import com.hcy_futejia.adapter.TaskAdapter;
import com.hcy_futejia.utils.DateUtil;
import com.hcy_futejia.utils.PackageUtils;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.questionnair.QuestionnaireManager;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.Member;
import com.hxlm.android.hcy.user.SharedPreferenceTempSave;
import com.hxlm.android.hcy.user.UserManager;
import com.hxlm.android.hcy.voicediagnosis.BianShiActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.bean.HealthInformation;
import com.hxlm.hcyandroid.bean.Subject;
import com.hxlm.hcyandroid.bean.Task;
import com.hxlm.hcyandroid.tabbar.healthinformation.ShowHealthInformationActivity;
import com.hxlm.hcyandroid.tabbar.home_jczs.InformationActivity;
import com.hxlm.hcyandroid.tabbar.home_jczs.OneClockActivity;
import com.hxlm.hcyandroid.tabbar.home_jczs.OneSayActivity;
import com.hxlm.hcyandroid.tabbar.home_jczs.OneWriteActivity;
import com.hxlm.hcyandroid.tabbar.home_jczs.WebViewActivity;
import com.hxlm.hcyandroid.util.ImageLoaderUtil;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.hxlm.hcyandroid.util.StatusBarUtils;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyphone.BaseFragment;
import com.hxlm.hcyphone.MainActivity;
import com.hxlm.hcyphone.adapter.IndexReadAdapter;
import com.hxlm.hcyphone.bean.IndexPicBean;
import com.hxlm.hcyphone.bean.NewInsBean;
import com.hxlm.hcyphone.main.HomeActiveDetailsActivity;
import com.hxlm.hcyphone.main.HomeManager;
import com.hxlm.hcyphone.manager.HarmonyPackageManager;
import com.hxlm.hcyphone.manager.TaskManager;
import com.hxlm.hcyphone.utils.GlideApp;
import com.hxlm.hcyphone.widget.ListViewInScroll;

import net.sourceforge.simcpux.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by KY3H on 2019/4/4.
 */

public class FtjHomeFragment extends BaseFragment implements View.OnClickListener{
    private String TAG = "FtjHomeFragment";
    private ImageView iv_hcb_bg;
    private LinearLayout ll_hcb;
    private ImageView iv_view_write;
    private ImageView iv_view_speak;
    private ImageView iv_view_click;
    private TextView tv_package_text;
    private ImageView iv_activity;
    private TextView tv_remind_time;
    private ListViewInScroll task_list;
    private ImageView gengduoyuedu;
    private RecyclerView rcv_read;

    private List<IndexPicBean> indexPicBeanList;
    //首页活动详情链接获取
    private String indexPicBeanLink;
    private int memberChildId;
    private TaskManager mTaskmanager;
    private List<Task> tasks;
    private int day;
    private TaskAdapter taskAdapter;
    private LinearLayoutManager linearLayoutManager;
    private List<HealthInformation> healthInformations;
    private UserManager userManager;
    private IndexReadAdapter indexReadAdapter;
    private HarmonyPackageManager harmonyPackageManager;
    private SwipeRefreshLayout sw;
    private RelativeLayout rl_title_bar_main;
    private boolean white;
    private ScrollView scrollView;
    private ImageView mIvFamilies;
    private int collectAge;

    @Override
    protected int getContentViewResource() {
        return R.layout.ftj_fragment_home;
    }

    @Override
    protected void initView(View view) {
        Log.d(TAG, "initView: ");
        mIvFamilies = view.findViewById(R.id.iv_families);
        scrollView = view.findViewById(R.id.parent_scroll_view);
        rl_title_bar_main = view.findViewById(R.id.include);
        TextView viewById = view.findViewById(R.id.tv_title);
        iv_hcb_bg = view.findViewById(R.id.iv_hcb_bg);
//        让scroll中最上面的控件获取焦点，整个scroll就固定在最上面了
        iv_hcb_bg.setFocusable(true);
        iv_hcb_bg.setFocusableInTouchMode(true);
        iv_hcb_bg.requestFocus();

        ll_hcb = view.findViewById(R.id.ll_hcb);
        tv_package_text = view.findViewById(R.id.tv_package_text);
        iv_activity = view.findViewById(R.id.iv_activity);
        tv_remind_time = view.findViewById(R.id.tv_remind_time);
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = sDateFormat.format(new java.util.Date());
        tv_remind_time.setText(date);
        task_list = view.findViewById(R.id.task_list);
        gengduoyuedu = view.findViewById(R.id.gengduoyuedu);
        rcv_read = view.findViewById(R.id.rcv_read);
        rcv_read.setFocusable(false);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rcv_read.setLayoutManager(linearLayoutManager);
        sw = view.findViewById(R.id.sw);
        sw.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh_task_hcb();
            }
        });

        iv_view_write = view.findViewById(R.id.iv_view_write);
        iv_view_speak = view.findViewById(R.id.iv_view_speak);
        iv_view_click = view.findViewById(R.id.iv_view_click);

        ll_hcb.setOnClickListener(this);
        iv_activity.setOnClickListener(this);
        iv_view_write.setOnClickListener(this);
        iv_view_speak.setOnClickListener(this);
        iv_view_click.setOnClickListener(this);
        gengduoyuedu.setOnClickListener(this);
        mIvFamilies.setOnClickListener(this);

        getInfoDevice();

    }

    /**
     * 获得渠道名称
     * @return
     */
    private String getChannel() {
        try {
            PackageManager pm = getActivity().getPackageManager();
            ApplicationInfo appInfo = pm.getApplicationInfo(getActivity().getPackageName(), PackageManager.GET_META_DATA);
            String CHANNEL =  appInfo.metaData.getString("CHANNEL");
            Log.d(TAG, "getChannel: "+CHANNEL);
            return CHANNEL;
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return "";
    }

    /**
     * 获取用户信息，设备信息的接口
     */
    private void getInfoDevice() {
        try {
            Member member = LoginControllor.getLoginMember();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date dateAge = formatter.parse( member.getBirthday());
                collectAge = getAge(dateAge);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //collectGender  0未知 1男 2女
            int collectGender = 0;
            if (!TextUtils.isEmpty(member.getGender())) {
                if (member.getGender().equals("male")) {
                    collectGender = 1;
                } else if (member.getGender().equals("female")) {
                    collectGender = 2;
                } else {
                    collectGender = 0;
                }
            }else{
                collectGender = 0;
            }
            CDRequestUtils.getInfo(member.getId()+"",collectAge,collectGender,"1","");

            //获取连网方式
            String networkState = CDDeviceUtils.getNetworkState(getActivity());
            String networkType = "0";
            if (networkState.equals("WIFI")){
                networkType = "1";
            }else if (networkState.equals("2G") || networkState.equals("3G") || networkState.equals("4G")){
                networkType = "2";
            }
            CDRequestUtils.getDevice(member.getId()+"","1",
                    CDDeviceUtils.getDeviceBrand(),CDDeviceUtils.getDeviceModel(),
                    "安卓",CDDeviceUtils.getScreenDisplay(getActivity()),CDDeviceUtils.getOperatorType(getActivity())
                    ,networkType,"");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据生日计算年龄
     * @param birthDay
     * @return
     * @throws Exception
     */
    public static int getAge(Date birthDay) throws Exception {
        Calendar cal = Calendar.getInstance();
        if (cal.before(birthDay)) {
            throw new IllegalArgumentException("The birthDay is before Now.It's unbelievable!");
        }
        int yearNow = cal.get(Calendar.YEAR);
        int monthNow = cal.get(Calendar.MONTH);
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
        cal.setTime(birthDay);

        int yearBirth = cal.get(Calendar.YEAR);
        int monthBirth = cal.get(Calendar.MONTH);
        int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);

        int age = yearNow - yearBirth;

        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                if (dayOfMonthNow < dayOfMonthBirth) {
                    age--;
                }
            } else {
                age--;
            }
        }
        return age;
    }

    /**
     * 初始化准备工作
     */
    @Override
    protected void initStartup() {
        Log.d(TAG, "initStartup: ");
        mTaskmanager = new TaskManager();
        harmonyPackageManager = new HarmonyPackageManager();
        if (LoginControllor.getChoosedChildMember() != null) {
            memberChildId = LoginControllor.getChoosedChildMember().getId();
        }
    }

    /**
     * 获取和畅包，活动，合唱提醒，推荐阅读
     */
    @Override
    protected void initData() {
        Log.d(TAG, "initData: ");
        //和畅包与task不能并发，两者如果都受登录失败影响就受到各自的自动登录影响
        fetch_activity();
        fetch_task();
        fetch_recommend_read(true);
        fetch_hcb_package();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_families:
                startActivity(new Intent(getActivity(), FtjUserInfoActivity.class));
                break;
//            一说
            case R.id.iv_view_speak:
                String oneSay = SharedPreferenceUtil.getOneSay();
                if (oneSay != null && oneSay.equals("true")) {
                    Intent intent1 = new Intent(getActivity(), OneSayActivity.class);
                    startActivity(intent1);
                } else {
                    Intent intentSay = new Intent(getActivity(), BianShiActivity.class);
                    startActivity(intentSay);
                }
//                show_hide_hcb_view(true);
                break;
//                一写
            case R.id.iv_view_write:
                String oneWrite = SharedPreferenceUtil.getOneWrite();
                if (oneWrite != null && oneWrite.equals("true")) {
                    Intent intent = new Intent(getActivity(), OneWriteActivity.class);
                    startActivity(intent);
                } else {
                    Intent intentWrite = new Intent(getActivity(), FtjVisceraIdentityActivity.class);
                    startActivity(intentWrite);
                }
//                show_hide_hcb_view(false);
                break;
//                一点
            case R.id.iv_view_click:
                String oneClock = SharedPreferenceUtil.getOneClock();
                if (oneClock != null && oneClock.equals("true")) {
                    Intent intent2 = new Intent(getActivity(), OneClockActivity.class);
                    startActivity(intent2);
                } else {
                    Intent intentClick = new Intent(getActivity(), FtjBodyQuestionActivity.class);
                    intentClick.putExtra("title", getString(R.string.home_physical_identification));
                    intentClick.putExtra("categorySn", QuestionnaireManager.SN_TZBS);
                    startActivity(intentClick);
                }
                break;
//                进入和畅包
            case R.id.ll_hcb:
                    Intent intent3 = new Intent(getActivity(), WebViewActivity.class);
                    intent3.putExtra("title", getString(R.string.main_harmony_package));
                    intent3.putExtra("type", "/member/service/home/1/");
                    startActivity(intent3);
                break;
//                首页活动跳转
            case R.id.iv_activity:
                if (!TextUtils.isEmpty(indexPicBeanLink) ) {
                    Intent intent = new Intent(getActivity(), HomeActiveDetailsActivity.class);
                    intent.putExtra("indexLink", indexPicBeanLink);
                    startActivity(intent);
                }
                break;
//          推荐阅读更多
            case R.id.gengduoyuedu:
                Intent jingluo = new Intent(getActivity(), InformationActivity.class);
                startActivity(jingluo);
                break;

            default:
                break;

        }

    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            //隐藏时使用
        } else {
            //显示时使用
            if(white){
                set_status_bar_color(R.color.white);
            }else {
                set_status_bar_color(R.color.new_title_bar_bg);
            }
}
    }

@Override
public void onStop() {
        super.onStop();
        set_status_bar_color(R.color.new_title_bar_bg);
        }

@Override
public void onResume() {
        super.onResume();
    //更新头像
    Member loginMember = LoginControllor.getLoginMember();
    if (loginMember != null) {
        String memberImage = loginMember.getMemberImage();
        if (memberImage != null && !memberImage.equals("")) {
            ImageLoaderUtil.displayImage(getContext(), mIvFamilies, memberImage, R.drawable.main_family, R.drawable.main_family, R.drawable.main_family);
        }
    }
        if(white){
        set_status_bar_color(R.color.white);
        }else {
        set_status_bar_color(R.color.new_title_bar_bg);
        }
        if(Constants.needRefesh){
        refresh_task_hcb2();
        }

        }

private void refresh_task_hcb(){
        fetch_activity();
        fetch_task();
        fetch_hcb_package();
        sw.setRefreshing(false);
        }

    private void refresh_task_hcb2(){
        fetch_task();
        fetch_hcb_package();
        sw.setRefreshing(false);
    }


    private void show_hide_hcb_view(boolean show) {
        if (show) {
            white = false;
            iv_hcb_bg.setVisibility(View.VISIBLE);
            ll_hcb.setVisibility(View.VISIBLE);
            rl_title_bar_main.setBackgroundColor(Color.parseColor("#4FADEA"));
            set_status_bar_color(R.color.new_title_bar_bg);
        } else {
            white = true;
            iv_hcb_bg.setVisibility(View.GONE);
            ll_hcb.setVisibility(View.GONE);
            rl_title_bar_main.setBackgroundColor(Color.parseColor("#ffffff"));
            set_status_bar_color(R.color.white);
        }
    }

  private void   set_status_bar_color(int colorid){
      StatusBarUtils.setWindowStatusBarColor(getActivity(), colorid);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN|View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
      }
    }

    /**
     * 獲取活動
     */
    private void fetch_activity() {
        //首页活動图片数据接口
        if (indexPicBeanList == null) {
            new HomeManager().getIndexPic(new AbstractDefaultHttpHandlerCallback(getActivity()) {
                @Override
                protected void onResponseSuccess(Object obj) {
                    indexPicBeanList = (List<IndexPicBean>) obj;
                    if (indexPicBeanList != null && indexPicBeanList.size() > 0) {
                        for (int i = 0; i < indexPicBeanList.size(); i++) {
                            IndexPicBean indexPicBean = indexPicBeanList.get(i);
                            switch (indexPicBean.getType()) {
                                case 2:
                                    if (getActivity() != null) {
                                        String picurl = indexPicBean.getPicurl();
                                        indexPicBeanLink = indexPicBean.getLink();
                                        if (TextUtils.isEmpty(indexPicBeanLink) || TextUtils.isEmpty(picurl)) {
                                            iv_activity.setVisibility(View.GONE);
                                        }else {
                                            try {
                                                GlideApp.with(getActivity())
                                                        .load(Constant.BASE_URL + indexPicBean.getPicurl())
                                                        .placeholder(R.color.white)
                                                        .into(iv_activity);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            iv_activity.setVisibility(View.VISIBLE);
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                }

            });
        }
    }

    /**
     * 获取和畅任务
     */
    private void fetch_task() {
        if(memberChildId!=0){
                mTaskmanager.getUserTask_ftj( memberChildId, new AbstractDefaultHttpHandlerCallback(getActivity()) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        Constants.needRefesh = false;
                        tasks = (List<Task>) obj;
                        handleTaskInfo(tasks);
                        //本地状态缓存：
                        cacheTask(true);
                    }
                });
            }
    }

    private void handleTaskInfo(List<Task> tasks) {
        if(tasks!=null){
            if(taskAdapter==null){
                taskAdapter = new TaskAdapter(getActivity());
            }
            task_list.setAdapter(taskAdapter);
            handle_tasks(tasks);
            taskAdapter.setTasks(tasks);
            task_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Task task = tasks.get(position);
                    String type = task.getType();
                    Intent intent = null;
                    if ("yizhan".equals(type)) {
                        intent = new Intent(getActivity(), WebViewActivity.class);
                        intent.putExtra("title", getString2(R.string.home_sport_prescription));
                        intent.putExtra("type", "/member/service/view/fang/JLBS/1/");
                        intent.putExtra("fangType", "yizhan");
                        intent.putExtra("advice",task.getAdvice());
                        startActivity(intent);
                    } else if ("yiting".equals(type)) {
                        intent = new Intent(getActivity(), WebViewActivity.class);
                        intent.putExtra("title", getString2(R.string.home_music_prescription));
                        intent.putExtra("type", "/member/service/view/fang/JLBS/1/");
                        intent.putExtra("fangType", "yiting");
                        startActivity(intent);
                    } else if ("yidai".equals(type)) {
                        intent = new Intent(getActivity(), WebViewActivity.class);
                        intent.putExtra("title", getString2(R.string.home_ear_prescription));
                        intent.putExtra("type", "/member/service/view/fang/JLBS/1/");
                        intent.putExtra("fangType", "yidai");
                        startActivity(intent);
                    }else if ("yishuo".equals(type)){
                        String oneSay = SharedPreferenceUtil.getOneSay();
                        if (oneSay != null && oneSay.equals("true")) {
                            Intent intent1 = new Intent(getActivity(), OneSayActivity.class);
                            startActivity(intent1);
                        } else {
                            Intent intentSay = new Intent(getActivity(), BianShiActivity.class);
                            startActivity(intentSay);
                        }
                    }else if ("yixie".equals(type)){
                        String oneWrite = SharedPreferenceUtil.getOneWrite();
                        if (oneWrite != null && oneWrite.equals("true")) {
                            intent = new Intent(getActivity(), OneWriteActivity.class);
                            startActivity(intent);
                        } else {
                            Intent intentWrite = new Intent(getActivity(), FtjVisceraIdentityActivity.class);
                            startActivity(intentWrite);
                        }
                    }else if ("yidian".equals(type)){

                        String oneClock = SharedPreferenceUtil.getOneClock();
                        if (oneClock != null && oneClock.equals("true")) {
                            Intent intent2 = new Intent(getActivity(), OneClockActivity.class);
                            startActivity(intent2);
                        } else {
                            Intent intentClick = new Intent(getActivity(), FtjBodyQuestionActivity.class);
                            intentClick.putExtra("title", getString2(R.string.home_physical_identification));
                            intentClick.putExtra("categorySn", QuestionnaireManager.SN_TZBS);
                            startActivity(intentClick);
                        }
                    }else if ("yitui".equals(type)){
                            intent = new Intent(getActivity(), WebViewActivity.class);
                            intent.putExtra("title", getString(R.string.combing_meridian_massage_prescription));
                            intent.putExtra("type", "/member/service/view/fang/JLBS/1/");
                            intent.putExtra("fangType", "yitui");
                            startActivity(intent);

                    }
                    if(!task.isDone()){

                        if(!("yishuo".equals(type)||"yixie".equals(type)||"yidian".equals(type))){
                            task.setDone(true);
                            //只在标记是未完成时才提交完成状态
                            mTaskmanager.submitTaskCompleted(getActivity(), memberChildId,task.getId() , new AbstractDefaultHttpHandlerCallback(getActivity()) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                }
                            });
                            //更新缓存
                            cacheTask(true);
                        }else {
                            //说写点不做缓存，取消使用缓存，因为不好确定是否做完，所以这三个动作后就要重新请求服务器数据。
                           //说写点在此处不修改状态，需要获得报告才判定完成，完成与否通过刷新接口获取，
                            cacheTask(false);
                        }
                    }
//                    把当前任务挪到最后
                    if(task.isDone()){
                        tasks.remove(task);
                        tasks.add(task);
                    }

                    taskAdapter.notifyDataSetChanged();
                }
            });
            for(Task task :tasks){
                if("yizhan".equals(task.getType())){
                    Subject subject = task.getSubject();
                    if(!TextUtils.isEmpty(subject.getName())){
                        Constants.YUNDONG_SHI = subject.getName();
                    }
                }
            }
        }

    }

    /**
     * 对任务按照完成未完成进行排序
     * @param tasks
     */
    private void handle_tasks(List<Task> tasks) {
        List<Task> completedtask = new ArrayList<>();
        for (Task task :tasks){
            if(task.isDone()){
                completedtask.add(task);
            }
        }
        tasks.removeAll(completedtask);
        for (Task task :completedtask){
            if("yishuo".equalsIgnoreCase(task.getType())||"yixie".equalsIgnoreCase(task.getType())||"yidian".equalsIgnoreCase(task.getType())){
                completedtask.remove(task);
            }
        }
        tasks.addAll(completedtask);
    }

    /**
     * 获取推荐阅读
     * @param b 是否使用缓存
     */
    private void fetch_recommend_read(boolean b) {
        Long info_date = SharedPreferenceTempSave.getLong("info_date");
        long l = System.currentTimeMillis();
        boolean interval = (l - info_date) / 3600 / 24 > 3;
        if (!interval&&b) {   //缓存未超过3天，b:是否使用缓存
            try {
                SharedPreferenceTempSave.getString("informations");
                healthInformations = JSON.parseArray(SharedPreferenceTempSave.getString("informations"),HealthInformation.class);
                handleInformation(healthInformations);
            } catch (Exception e) {
                fetch_recommend_read(false);
                e.printStackTrace();
            }


        } else {
            userManager = new UserManager();
            userManager.getNewInformation2(getActivity(),1, new AbstractDefaultHttpHandlerCallback(getActivity()) {
                @Override
                protected void onResponseSuccess(Object obj) {
                    handleInformation((List<HealthInformation>) obj);
                }
            });
        }
    }

    /**
     * 展示推荐阅读
     * @param healthInformations
     */
    private void handleInformation(List<HealthInformation> healthInformations) {
        if (getActivity() != null) {
            this.healthInformations = healthInformations;
            if (indexReadAdapter == null) {
                indexReadAdapter = new IndexReadAdapter(getActivity(),healthInformations);
                rcv_read.setAdapter(indexReadAdapter);
                Log.e("retrofit", "==推荐阅读集合==" + healthInformations.size());
                indexReadAdapter.setOnItemClick(new IndexReadAdapter.OnItemClick() {
                    @Override
                    public void onItem(int position, HealthInformation healthInformation) {
                        Intent intent = new Intent(getActivity(),
                                ShowHealthInformationActivity.class);
                        intent.putExtra("healthInformation",
                                healthInformations.get(position));
                        startActivity(intent);
                    }
                });
            }else {
                indexReadAdapter.notifyDataSetChanged();
            }
        }
    }

    private void fetch_hcb_package() {
        //和畅包与task不能并发，两者如果都受登录失败影响就受到各自的自动登录影响
        harmonyPackageManager.getCurrentState(getActivity(), memberChildId, new AbstractDefaultHttpHandlerCallback(getActivity()) {
            @Override
            protected void onResponseSuccess(Object obj) {
//                fetch_task();
                String data = (String) obj;
                hadle_hcb_Info(data);
            }
        });
    }
    private void hadle_hcb_Info(String data) {
        NewInsBean newInsBean = new NewInsBean();
        if(!TextUtils.isEmpty(data)){
            try {
                newInsBean = new Gson().fromJson(data, NewInsBean.class);
            } catch (JsonSyntaxException e) {
                newInsBean.setName("");
                newInsBean.setNum(111);
            }
        }
        setNewCurrentState(newInsBean.getNum(), newInsBean.getName());
    }

    /**
     * 和畅包状态赋值
     *
     * @param stateNum
     */
    public void setNewCurrentState(int stateNum, String type) {

        if (stateNum != 111){
            if (stateNum == 0) {
                tv_package_text.setText(getString2(R.string.home_status_1) + type + getString2(R.string.home_status));
            } else if (stateNum == 1) {
                tv_package_text.setText(getString2(R.string.home_status_2) + type +getString2(R.string.home_status));
            } else if (stateNum == 2) {
                tv_package_text.setText(getString2(R.string.home_status_2) + type +getString2(R.string.home_status));
            } else if (stateNum == 3) {
                tv_package_text.setText(getString2(R.string.home_status_3) + type + getString2(R.string.home_status));
            } else if (stateNum == 4) {
                tv_package_text.setText(getString2(R.string.home_status_4) + type + getString2(R.string.home_status));
            } else if (stateNum == 5) {
                tv_package_text.setText(getString2(R.string.home_status_5) + type +getString2(R.string.home_status));
            } else if (stateNum == 6) {
                tv_package_text.setText(getString2(R.string.home_status_6) + type + getString2(R.string.home_status));
            } else if (stateNum == 7) {
                tv_package_text.setText(getString2(R.string.home_status_3) + type + getString2(R.string.home_status));
            } else if (stateNum == 8) {
                tv_package_text.setText(getString2(R.string.home_status_4) + type + getString2(R.string.home_status));
            } else if (stateNum == 9) {
                tv_package_text.setText(getString2(R.string.home_status_6) + type + getString2(R.string.home_status));
            } else if (stateNum == 10) {
                tv_package_text.setText(getString2(R.string.home_status_3) + type + getString2(R.string.home_status));
            } else if (stateNum == 11) {
                tv_package_text.setText(getString2(R.string.home_status_4) + type + getString2(R.string.home_status));
            }
            white = false;
            show_hide_hcb_view(true);
        }else {
            if (getActivity()!= null) {
                show_hide_hcb_view(false);
                tv_package_text.setText(getString2(R.string.home_unfinish_medical));
                white = true;
            }
        }
    }

    private String getString2(int str_id){
        if(getActivity()!=null){
            return getString(str_id);
        }
        return "";
    }

    private void cacheTask(boolean usercache) {
        if(tasks!=null){
            SharedPreferenceTempSave.saveBoolean("usercache",usercache);
            if(usercache){
                String task_json = JSON.toJSONString(tasks);
                SharedPreferenceTempSave.saveString("task",task_json);
                SharedPreferenceTempSave.saveLong("date",System.currentTimeMillis());
                SharedPreferenceTempSave.saveInt("memberid",memberChildId);
            }
        }
    }

}
