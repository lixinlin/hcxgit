package com.hcy_futejia.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hcy.ky3h.R;

import com.hcy_futejia.activity.FtjMyConsultActivity;
import com.hcy_futejia.activity.FtjSettingActivity;
import com.hcy_futejia.activity.FtjUserInfoActivity;
import com.hcy_futejia.manager.AmountManager;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.content.ShiFanYinActivity;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.WebViewAboutHealthMallActivity;
import com.hxlm.hcyandroid.tabbar.home.visceraidentity.AlertDialogHealthConstant;
import com.hxlm.hcyandroid.tabbar.home.visceraidentity.AlertDialogVersion;
import com.hxlm.hcyandroid.tabbar.usercenter.MyCardPackageActivity;
import com.hxlm.hcyandroid.view.CircleImageView;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

/**
 * @author Lirong
 * @date 2019/4/3.
 * @description
 */

public class FtjMyFragment  extends Fragment implements View.OnClickListener {

    private String title;
    private CircleImageView iv_user_icon;
    private TextView tv_user_name;
    private LinearLayout ll_collect;
    private TextView tv_collect_count;
    private LinearLayout ll_card_pack;
    private TextView tv_card_package_count;
    private LinearLayout ll_jifen;
    private TextView tv_jifen_count;
    private LinearLayout ll_daifukuan;
    private LinearLayout ll_daipingjia;
    private LinearLayout ll_tuikuan;
    private LinearLayout ll_all_dingdan;
    private LinearLayout ll_health_consultant_team;
    private LinearLayout ll_consulting_records;
    private LinearLayout ll_address_management;
    private LinearLayout ll_sport_shifanyin;
    private LinearLayout ll_setting;

    public FtjMyFragment() {
        super();
    }

    public void getInstance(Bundle bundle){
        this.title = bundle.getString("title");

    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            //隐藏时使用
        } else {
            //显示时使用
            setUserInfo();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ftj_my_fragment, container, false);

        iv_user_icon = view.findViewById(R.id.iv_user_icon);
        tv_user_name = view.findViewById(R.id.tv_user_name);
        ll_collect = view.findViewById(R.id.ll_collect);
        tv_collect_count = view.findViewById(R.id.tv_collect_count);
        ll_card_pack = view.findViewById(R.id.ll_card_pack);
        tv_card_package_count = view.findViewById(R.id.tv_card_package_count);
        ll_jifen = view.findViewById(R.id.ll_jifen);
        tv_jifen_count = view.findViewById(R.id.tv_jifen_count);
        ll_daifukuan = view.findViewById(R.id.ll_daifukuan);
        ll_daipingjia = view.findViewById(R.id.ll_daipingjia);
        ll_tuikuan = view.findViewById(R.id.ll_tuikuan);
        ll_all_dingdan = view.findViewById(R.id.ll_all_dingdan);
        ll_health_consultant_team = view.findViewById(R.id.ll_health_consultant_team);
        ll_consulting_records = view.findViewById(R.id.ll_consulting_records);
        ll_address_management = view.findViewById(R.id.ll_address_management);
        ll_sport_shifanyin = view.findViewById(R.id.ll_sport_shifanyin);
        ll_setting = view.findViewById(R.id.ll_setting);

        iv_user_icon.setOnClickListener(this);
        ll_collect.setOnClickListener(this);
        ll_card_pack.setOnClickListener(this);
        ll_jifen.setOnClickListener(this);
        ll_daifukuan.setOnClickListener(this);
        ll_daipingjia.setOnClickListener(this);
        ll_tuikuan.setOnClickListener(this);
        ll_all_dingdan.setOnClickListener(this);
        ll_health_consultant_team.setOnClickListener(this);
        ll_consulting_records.setOnClickListener(this);
        ll_address_management.setOnClickListener(this);
        ll_sport_shifanyin.setOnClickListener(this);
        ll_setting.setOnClickListener(this);

        setUserInfo();
        return view;
    }

    /**
     * 设置用户信息
     */
    public void setUserInfo(){
        String userName = (!TextUtils.isEmpty(LoginControllor.getLoginMember().getName())) ? LoginControllor.getLoginMember().getName() : LoginControllor.getLoginMember().getUserName();
        if(!TextUtils.isEmpty(userName)){
            if(userName.length()>20){
                String weixinNickName = LoginControllor.getWeixinNickName();
                userName = weixinNickName;
            }
        }
        //当前用户
        tv_user_name.setText(userName);

        // 用户头像
        String memberImage = LoginControllor.getLoginMember().getMemberImage();

        if (!TextUtils.isEmpty(memberImage)) {
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .cacheOnDisk(true)
                    .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                    .cacheInMemory(false).build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                    .denyCacheImageMultipleSizesInMemory()
                    .threadPriority(Thread.NORM_PRIORITY - 2)
                    .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                    .tasksProcessingOrder(QueueProcessingType.LIFO)
                    .memoryCache(new LruMemoryCache(8 * 1024 * 1024))
                    .diskCacheSize(50 * 1024 * 1024)
                    .threadPoolSize(3).build();

            ImageLoader loader = ImageLoader.getInstance();
            loader.init(config);
            loader.displayImage(memberImage, iv_user_icon, options);
        }else {
            iv_user_icon.setImageResource(R.drawable.main_family);
        }

        new AmountManager().getAmount(new AbstractDefaultHttpHandlerCallback(getActivity()) {
            @Override
            protected void onResponseSuccess(Object obj) {
                JSONObject jsonObject = JSON.parseObject((String) obj);
                long integrals = jsonObject.getLongValue("integrals");
                long collect = jsonObject.getLongValue("collect");
                long card = jsonObject.getLongValue("card");
                tv_card_package_count.setText(card+"");
                tv_collect_count.setText(collect+"");
                tv_jifen_count.setText(integrals+"");
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            //头像
            case R.id.iv_user_icon:
                LoginControllor.requestLogin(getActivity(), new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        setUserInfo();
//                        Intent intent = new Intent(getActivity(), UserInfoActivity.class);
                        Intent intent = new Intent(getActivity(), FtjUserInfoActivity.class);
                        startActivity(intent);
                    }
                });
                break;
            //收藏
            case R.id.ll_collect:
                LoginControllor.requestLogin(getActivity(), new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        setUserInfo();
                        Intent intent = new Intent(getActivity(), WebViewAboutHealthMallActivity.class);
                        intent.putExtra("title",getString2(R.string.myfrag_shoucang));
                        intent.putExtra("url", Constant.BASE_URL + "/member/mobile/focus_ware/list.jhtml");
                        startActivity(intent);
                    }
                });
                break;
            //卡包
            case R.id.ll_card_pack:
                LoginControllor.requestLogin(getActivity(), new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        setUserInfo();
                        Intent intent = new Intent(getActivity(), MyCardPackageActivity.class);
                        startActivity(intent);
                    }
                });
                break;
            //积分
            case R.id.ll_jifen:
                LoginControllor.requestLogin(getActivity(), new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        setUserInfo();
                        Intent intent = new Intent(getActivity(), WebViewAboutHealthMallActivity.class);
                        intent.putExtra("title",getString2(R.string.myfrag_wodejifen));
                        intent.putExtra("url", Constant.BASE_URL + "/member/mobile/order/pointList.jhtml");
                        startActivity(intent);
                    }
                });
                break;
            //待付款
            case R.id.ll_daifukuan:
                LoginControllor.requestLogin(getActivity(), new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        setUserInfo();
                        Intent intent = new Intent(getActivity(), WebViewAboutHealthMallActivity.class);
                        intent.putExtra("title",getString2(R.string.myfrag_daifukuan));
                        intent.putExtra("url", Constant.BASE_URL + "/member/mobile/order/list.jhtml?type=unpaid");
                        startActivity(intent);
                    }
                });
                break;
            //待评价
            case R.id.ll_daipingjia:
                LoginControllor.requestLogin(getActivity(), new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        setUserInfo();
                        Intent intent = new Intent(getActivity(), WebViewAboutHealthMallActivity.class);
                        intent.putExtra("title",getString2(R.string.myfrag_daipingjia));
                        intent.putExtra("url", Constant.BASE_URL + "/member/mobile/order/list.jhtml?type=completed");
                        startActivity(intent);
                    }
                });
                break;
            //退款/售后
            case R.id.ll_tuikuan:
                LoginControllor.requestLogin(getActivity(), new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        setUserInfo();
                        Intent intent = new Intent(getActivity(), WebViewAboutHealthMallActivity.class);
                        intent.putExtra("title",getString2(R.string.myfrag_tuikuanjilu));
                        intent.putExtra("url", Constant.BASE_URL + "/member/mobile/order/list.jhtml?type=refund");
                        startActivity(intent);
                    }
                });
                break;
            //全部订单
            case R.id.ll_all_dingdan:
                LoginControllor.requestLogin(getActivity(), new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        setUserInfo();
                        Intent intent = new Intent(getActivity(), WebViewAboutHealthMallActivity.class);
                        intent.putExtra("title",getString2(R.string.myfrag_quanbudingdan));
                        intent.putExtra("url", Constant.BASE_URL + "/member/mobile/order/list.jhtml?type=all");
                        startActivity(intent);
                    }
                });
                break;
            //健康顾问团队
            case R.id.ll_health_consultant_team:
                AlertDialogHealthConstant cloce = new AlertDialogHealthConstant(getActivity());
                Dialog dialogcloce = cloce.createAlartDialog(getString(R.string.shangweikaifang), "");
                dialogcloce.show();
                dialogcloce.setCanceledOnTouchOutside(true);
                break;
            //咨询记录
            case R.id.ll_consulting_records:
                LoginControllor.requestLogin(getActivity(), new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        setUserInfo();
                      Intent intent = new Intent(getActivity(), FtjMyConsultActivity.class);
                        startActivity(intent);
                    }
                });
                break;
            //地址管理
            case R.id.ll_address_management:
                LoginControllor.requestLogin(getActivity(), new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        setUserInfo();
                        Intent intent = new Intent(getActivity(), WebViewAboutHealthMallActivity.class);
                        intent.putExtra("title",getString2(R.string.myfrag_shouhuodizhi));
                        intent.putExtra("url", Constant.BASE_URL + "/member/mobile/order/receiverlist.jhtml");
                        startActivity(intent);
                    }
                });
                break;
            //运动示范音
            case R.id.ll_sport_shifanyin:
                LoginControllor.requestLogin(getActivity(), new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        setUserInfo();
                        Intent intent = new Intent(getActivity(), ShiFanYinActivity.class);
                        startActivity(intent);
                    }
                });
                break;
            //设置
            case R.id.ll_setting:
//                Intent intent = new Intent(getActivity(), SettingActivity.class);
                Intent intent = new Intent(getActivity(), FtjSettingActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUserInfo();

    }

    private String getString2(int id){
        if(getActivity()!=null){
            return getString(id);
        }else {
            return "";
        }
    }
}
