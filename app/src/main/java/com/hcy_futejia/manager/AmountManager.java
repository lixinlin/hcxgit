package com.hcy_futejia.manager;

import android.util.Log;

import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.user.LoginControllor;
import com.loopj.android.http.RequestParams;

/**
 * @author Lirong
 * @date 2019/5/7.
 * @description
 */

public class AmountManager {

    /**
     * 我的界面数量
     * @param callback
     */
    public void getAmount(AbstractDefaultHttpHandlerCallback callback){
        String url = "/member/mobile/focus_ware/getAmount.jhtml";
        RequestParams requestParams = new RequestParams();
        requestParams.put("memberChildId",LoginControllor.getChoosedChildMember().getId());
        requestParams.put("memberId",LoginControllor.getLoginMember().getId());
        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, requestParams, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                Log.e("retrofit","==我的界面数量==："+content);
                return content;
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

}
