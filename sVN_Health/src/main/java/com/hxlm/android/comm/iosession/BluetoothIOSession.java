package com.hxlm.android.comm.iosession;

import android.bluetooth.*;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.text.TextUtils;
import com.hxlm.android.comm.AbstractCodec;
import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.Error;
import com.hxlm.android.utils.Logger;
import com.hxlm.android.utils.PairUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This class does all the work for setting up and managing Bluetooth
 * connections with other devices. It has a thread that listens for incoming
 * connections, a thread for connecting with a device, and a thread for
 * performing data transmissions when connected.
 */
public class BluetoothIOSession extends AbstractIOSession {
    private static final String ACTION_PAIRING_REQUEST = "android.bluetooth.device.action.PAIRING_REQUEST";//自动配对
    private static final UUID BT_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private final String deviceName;
    private final String devicePin;//一般是0000 或者 1234

    // Member fields
    private BluetoothAdapter mAdapter;
    private BluetoothDevice mDevice;
    private BluetoothSocket mSocket;

    private InputStream mInStream;
    private OutputStream mOutStream;

    private ExecutorService readExecutor;

    private BluetoothHeadset mBluetoothHeadset;
    private BluetoothA2dp mBluetoothA2dp;

    private boolean isConnectA2DP = false;
    private boolean isConnectHeadset = false;
    private boolean isConnectRFComm = true;
    private boolean isAutoPairing = true;//是否已经配对

    private BluetoothProfile.ServiceListener mProfileListener = new BluetoothProfile.ServiceListener() {
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            try {
                if (profile == BluetoothProfile.HEADSET && isConnectHeadset) {
                    //Service连接成功，获得BluetoothHeadset
                    mBluetoothHeadset = (BluetoothHeadset) proxy;
                    //连接BluetoothHeadset
                    if (mBluetoothHeadset.getConnectionState(mDevice) != BluetoothProfile.STATE_CONNECTED) {
                        Logger.i(tag, "连接蓝牙Headset服务。。。。。。");
                        mBluetoothHeadset.getClass().getMethod("connect", BluetoothDevice.class)
                                .invoke(mBluetoothHeadset, mDevice);
                    }
                } else if (profile == BluetoothProfile.A2DP && isConnectA2DP) {
                    //Service连接成功，获得BluetoothA2DP
                    mBluetoothA2dp = (BluetoothA2dp) proxy;
                    //连接BluetoothA2DP
                    if (mBluetoothA2dp.getConnectionState(mDevice) != BluetoothProfile.STATE_CONNECTED) {
                        Logger.i(tag, "连接蓝牙A2DP服务。。。。。。");
                        mBluetoothA2dp.getClass().getMethod("connect", BluetoothDevice.class)
                                .invoke(mBluetoothA2dp, mDevice);
                    }
                }
            } catch (Exception e) {
                Logger.e(tag, e);
            }
        }

        public void onServiceDisconnected(int profile) {
            if (profile == BluetoothProfile.HEADSET) {
                mBluetoothHeadset = null;
            } else if (profile == BluetoothProfile.A2DP) {
                mBluetoothA2dp = null;
            }
        }
    };

    //构造方法
    public BluetoothIOSession(AbstractDeviceActivity anActivity, AbstractCodec abstractCodec,
                              final String name, final String pin) throws IOException {
        super(anActivity, abstractCodec);
        mAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mAdapter == null) {
            throw new IOException("该手机不支持蓝牙连接！");
        }

        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(pin)) {
            throw new IllegalArgumentException("必须提供设备的蓝牙名称和PIN码！");
        }

        IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);//开始
        intentFilter.addAction(BluetoothDevice.ACTION_FOUND);//设备找到
        intentFilter.addAction(ACTION_PAIRING_REQUEST);//自动配对
        intentFilter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);//配对状态改变
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);//搜索结束
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);//蓝牙电源状态变化

        intentFilter.addAction(BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED);//A2DP连接状态变化
        intentFilter.addAction(BluetoothA2dp.ACTION_PLAYING_STATE_CHANGED);//A2DP播放状态变化

        intentFilter.addAction(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED);//Headset连接状态变化

        // 注册广播接收器，接收并处理搜索结果
        activity.registerReceiver(mReceiver, intentFilter);

        //蓝牙是否打开，已打开返回true，否则，返回false
        if (!mAdapter.isEnabled()) {
            mAdapter.enable();//打开蓝牙，这个方法打开蓝牙不会弹出提示
        }

        deviceName = name;
        devicePin = pin;
    }

    //广播
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            BluetoothDevice device;

            switch (intent.getAction()) {
                // 处理A2DP连接状态变化事件
                case BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED:
                    switch (intent.getIntExtra(BluetoothA2dp.EXTRA_STATE, -1)) {
                        case BluetoothA2dp.STATE_CONNECTING:
                            Logger.i(tag, "A2DP 服务连接中。。。。。。");
                            break;
                        case BluetoothA2dp.STATE_CONNECTED:
                            Logger.i(tag, "A2DP 服务已连接");
                            break;
                        case BluetoothA2dp.STATE_DISCONNECTING:
                            Logger.i(tag, "A2DP 服务连接断开中。。。。。。");
                            break;
                        case BluetoothA2dp.STATE_DISCONNECTED:
                            Logger.i(tag, "A2DP 服务已断开");
                            break;
                        default:
                            break;
                    }
                    break;

                // 处理A2DP播放状态变化事件
                case BluetoothA2dp.ACTION_PLAYING_STATE_CHANGED:
                    int state = intent.getIntExtra(BluetoothA2dp.EXTRA_STATE, -1);
                    switch (state) {
                        case BluetoothA2dp.STATE_PLAYING:
                            Logger.i(tag, "A2DP播放状态 --- 播放中");
                            break;
                        case BluetoothA2dp.STATE_NOT_PLAYING:
                            Logger.i(tag, "A2DP播放状态 --- 未播放");
                            break;
                        default:
                            Logger.i(tag, "A2DP播放状态 --- 未知");
                            break;
                    }
                    break;

                // 处理Headset连接状态变化事件
                case BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED:
                    switch (intent.getIntExtra(BluetoothHeadset.EXTRA_STATE, -1)) {
                        case BluetoothHeadset.STATE_CONNECTING:
                            Logger.i(tag, "Headset 服务连接中。。。。。。");
                            break;
                        case BluetoothHeadset.STATE_CONNECTED:
                            Logger.i(tag, "Headset 服务已连接");
                            break;
                        case BluetoothHeadset.STATE_DISCONNECTING:
                            Logger.i(tag, "Headset 服务连接断开中。。。。。。");
                            break;
                        case BluetoothHeadset.STATE_DISCONNECTED:
                            Logger.i(tag, "Headset 服务已断开");
                            break;
                        default:
                            break;
                    }
                    break;

                // 处理找到蓝牙设备事件
                case BluetoothDevice.ACTION_FOUND:
                    device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                    if (device != null) {
                        Logger.d(tag, "查找到的蓝牙设备 --> " + device.getName());

                        //如果查找到的设备符合要连接的设备，处理
                        if (deviceName.equalsIgnoreCase(device.getName())) {
                            //判断当前是否正在查找设备，是返回true
                            if (mAdapter.isDiscovering()) {
                                //取消查找
                                mAdapter.cancelDiscovery();
                            }

                            mDevice = device;
                            onDeviceFound();
                        }
                    }
                    break;

                // 处理配对请求
                case ACTION_PAIRING_REQUEST:
                    if (mDevice != null) {
                        Logger.d(tag, "配对开始，系统版本为 ：" + Build.VERSION.SDK_INT);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            mDevice.setPairingConfirmation(true);
                        } else {
                            try {
                                PairUtils.setPin(mDevice, devicePin);
                            } catch (Exception e) {
                                Logger.e(tag, e);
                                onConnectFailed(Error.PAIRING_SET_PIN_FAILED);
                            }
                        }
                    }
                    break;

                // 处理蓝牙配对状态改变事件
                case BluetoothDevice.ACTION_BOND_STATE_CHANGED:
                    if (mDevice != null) {
                        switch (mDevice.getBondState()) {
                            case BluetoothDevice.BOND_BONDING:
                                Logger.d(tag, "正在配对");
                                break;
                            case BluetoothDevice.BOND_BONDED:
                                Logger.d(tag, "配对成功");
                                onDeviceBound();
                                break;
                            case BluetoothDevice.BOND_NONE:
                                Logger.d(tag, "删除配对");
                                break;
                        }
                    }
                    break;

                // 处理蓝牙设备搜索结束时间
                case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                    if (mDevice == null) {
                        onConnectFailed(Error.DEVICE_NOT_FOUND);
                    }
                    break;

                case BluetoothAdapter.ACTION_STATE_CHANGED:
                    state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
                    switch (state) {
                        case BluetoothAdapter.STATE_TURNING_ON:
                            Logger.i(tag, "正在打开蓝牙。。。。。。");
                            break;
                        case BluetoothAdapter.STATE_ON:
                            Logger.i(tag, "蓝牙已打开");
                            //蓝牙已打开，开始搜索并连接service
                            break;
                        case BluetoothAdapter.STATE_TURNING_OFF:
                            Logger.i(tag, "正在关闭蓝牙。。。。。。");
                            break;
                        case BluetoothAdapter.STATE_OFF:
                            Logger.i(tag, "蓝牙已关闭.");
                            break;
                    }
                    break;
            }
        }
    };

    //连接
    @Override
    protected void doConnect() {
        // 判断当前是否正在查找设备，是返回true
        if (mAdapter.isDiscovering()) {
            //取消查找
            mAdapter.cancelDiscovery();
        }

        //首先检查需要连接的设备是否已经配对
        final Set<BluetoothDevice> pairedDevices = mAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (deviceName.equals(device.getName())) {
                    mDevice = device;
                    Logger.i(tag, "在已配对设备中找到指定设备。");

                    onDeviceBound();
                    return;
                }
            }
        }

        if (isAutoPairing && mDevice == null) {
            //开始搜索蓝牙设备
            mAdapter.startDiscovery();
            Logger.i(tag, "开始查找周边蓝牙设备，请注意放开搜索功能。");
        } else {
            onConnectFailed(Error.AUTO_PAIRING_NOT_SUPPORT);
        }
    }

    //发送数据
    @Override
    protected void doSendMessage(byte[] packet) {
        try {
            mOutStream.write(packet);
            mOutStream.flush();
        } catch (IOException e) {
            Logger.e(tag, e);
        }
    }

    //关闭
    @Override
    protected void doClose() {
        mAdapter.closeProfileProxy(BluetoothProfile.HEADSET, mBluetoothHeadset);
        mAdapter.closeProfileProxy(BluetoothProfile.A2DP, mBluetoothA2dp);

        //如果不为空，进行注销
        if (mReceiver != null) {
            activity.unregisterReceiver(mReceiver);
            mReceiver = null;
        }

        //关闭
        if (mSocket != null) {
            try {
                mSocket.close();
            } catch (IOException e) {
                Logger.e(tag, e);
            }
        }

        if (readExecutor != null) {
            readExecutor.shutdownNow();
        }
    }

    @Override
    public int read(byte[] buffer) throws IOException {
        return mInStream.read(buffer);
    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     * 开始connectthread发起一个连接到一个远程设备。
     */
    private synchronized void onDeviceFound() {
        if (mDevice != null) {
            //已配对设备 直接进行链接
            if (mDevice.getBondState() == BluetoothDevice.BOND_BONDED) {
                onDeviceBound();
            } else if (mDevice.getBondState() == BluetoothDevice.BOND_NONE) {
                if (isAutoPairing) {
                    try {
                        PairUtils.createBond(mDevice);
                    } catch (Exception e) {
                        Logger.e(tag, e);
                        onConnectFailed(Error.PAIRING_INIT_FAILED);
                    }
                } else {
                    onConnectFailed(Error.AUTO_PAIRING_NOT_SUPPORT);
                }
            }
        }
    }

    /**
     * 处理蓝牙设备绑定后的操作，去连接设备。
     */
    private synchronized void onDeviceBound() {
        if (mDevice != null) {
            if (isConnectHeadset) {
                mAdapter.getProfileProxy(activity, mProfileListener, BluetoothProfile.HEADSET);
            }
            if (isConnectA2DP) {
                mAdapter.getProfileProxy(activity, mProfileListener, BluetoothProfile.A2DP);
            }
            if (isConnectRFComm) {
                new Thread(new ConnectRunnable()).start();
            }
        }
    }

    public void setConnectA2DP(boolean connectA2DP) {
        isConnectA2DP = connectA2DP;
    }

    public void setConnectHeadset(boolean connectHeadset) {
        isConnectHeadset = connectHeadset;
    }

    public void setConnectRFComm(boolean connectRFComm) {
        isConnectRFComm = connectRFComm;
    }

    public void setAutoPairing(boolean autoPairing) {
        isAutoPairing = autoPairing;
    }


    /**
     * 此线程在一个远程设备的连接中运行。它处理所有传入和传出传输。
     */
    private class ConnectRunnable implements Runnable {
        public void run() {
            Logger.i(tag, "-----------------------------启动普通蓝牙连接线程");
            try {
                if (Build.VERSION.SDK_INT >= 10) {
                    mSocket = mDevice.createInsecureRfcommSocketToServiceRecord(BT_UUID);
                } else {
                    mSocket = mDevice.createRfcommSocketToServiceRecord(BT_UUID);
                }

                mSocket.connect();
            } catch (IOException e) {

                Logger.e(tag, "连接失败原因-->"+e.getMessage());

                Logger.d(tag, "用反射方式连接");

                try {
                    mSocket.close();

                    Method m = mDevice.getClass().getMethod("createRfcommSocket", int.class);
                    mSocket = (BluetoothSocket) m.invoke(mDevice, 1);

                    mSocket.connect();
                } catch (Exception e1) {

                    Logger.e(tag, "使用反射连接失败原因-->"+e1.getMessage());

                    onConnectFailed(Error.CONNECTING_IO_EXCEPTION);

                    return;
                }
            }

            if (mSocket.isConnected()) {
                try {
                    mInStream = mSocket.getInputStream();
                    mOutStream = mSocket.getOutputStream();

                    readExecutor = Executors.newSingleThreadExecutor();
                    readExecutor.execute(new ReadRunnable(BluetoothIOSession.this));

                    onConnected();//设备连接成功
                } catch (IOException e) {
                    Logger.e(tag, e);

                    onConnectFailed(Error.IO_INIT_FAILED);
                }
            }

            Logger.i(tag, "-----------------------------退出普通蓝牙连接线程");
        }
    }
}
