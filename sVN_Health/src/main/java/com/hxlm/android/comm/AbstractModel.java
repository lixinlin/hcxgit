package com.hxlm.android.comm;

import com.hxlm.android.health.device.model.BodyTemperGunModel;

/**
 * 协议处理抽象类。
 * <p/>
 * Created by Zhenyu on 2015/11/23.
 */
@SuppressWarnings("unused")
public abstract class AbstractModel {
    protected String name;
    private FunctionType[] functions;

    public AbstractModel(String name, FunctionType[] functionTypes) {
        this.name = name;
        this.functions = functionTypes;
    }

    public abstract AbstractIOSession getIOSession(AbstractDeviceActivity activity);

    public FunctionType[] getFunctions() {
        return functions;
    }

    public void setFunctions(FunctionType[] functions) {
        this.functions = functions;
    }

    public enum FunctionType {
        ECG, // 心电
        TEMPERATURE, // 体温
        BLOOD_PRESSUER, // 血压
        SPO2, // 血氧
        BLOOD_SUGAR, // 血糖
        BREATH, // 呼吸
        BOARD,// 站板
        MASSAGE_CHAIR,// 按摩椅
        AURICULAR_BULE,//耳针仪
        BODYTEMPERATUREGU,  //体温枪
        AIR_QUALITY//空气质量检测

        }
}
