package com.hxlm.android.comm;

/**
 * Created by lixinlin on 2019/3/18.
 */

public enum  Error_English {
    DRIVER_NOT_FOUND("Connection driver is not found and this connection method is unsupported."),
    ADAPTER_INIT_FAILED("System adapter failed to start"),
    DRIVER_INIT_FAILED("Connection driver failed to start"),
    DEVICE_NOT_FOUND("The device requesting connection is not found. Please confirm the device is powered on."),
    NEED_PERMISSION("System authorization is needed to connect this device"),
    SERVICE_NOT_SUPPORT("The device does not support the service type requested"),
    AUTO_PAIRING_NOT_SUPPORT("The device can be connected only after pairing in system"),
    PAIRING_INIT_FAILED("Failed to start pairing with device"),
    PAIRING_SET_PIN_FAILED("Automatically set PIN code for device pairing failed"),
    CONNECTING_IO_EXCEPTION("A communication error occurred during connection"),
    IO_INIT_FAILED("Failed to create data input/output communication interface"),
    PERMISSION_DENY("User denied the system permission and device connection port cannot be opened"),
    EMPTY(""),
            ;

    private final String desc;

    Error_English(String aDesc) {
        desc = aDesc;
    }

    public String getDesc() {
        return desc;
    }

    @Override
    public String toString() {
        return desc;
    }
}
