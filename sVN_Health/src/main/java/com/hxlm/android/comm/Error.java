package com.hxlm.android.comm;

/**
 * 记录连接中的各种错误信息
 * Created by Zhenyu on 2016/11/18.
 */
public enum Error {
    DRIVER_NOT_FOUND("连接驱动未找到，无法支持该种连接方式"),
    ADAPTER_INIT_FAILED("系统适配器启动失败"),
    DRIVER_INIT_FAILED("连接驱动启动失败"),
    DEVICE_NOT_FOUND("请求连接的设备未找到，请确认设备是否开机"),
    NEED_PERMISSION("连接该设备需要获得系统授权"),
    SERVICE_NOT_SUPPORT("请求的服务类型该设备无法支持"),
    AUTO_PAIRING_NOT_SUPPORT("设备需要在系统中先完成配对才能进行连接"),
    PAIRING_INIT_FAILED("启动与设备的配对失败"),
    PAIRING_SET_PIN_FAILED("自动设置设备配对的PIN码失败"),
    CONNECTING_IO_EXCEPTION("连接中出现通信错误"),
    IO_INIT_FAILED("创建数据输入输出通信接口失败"),
    PERMISSION_DENY("用户拒绝授予系统权限，无法打开设备连接端口"),
    EMPTY(""),
        ;

    private final String desc;

    Error(String aDesc) {
        desc = aDesc;
    }

    public String getDesc() {
        return desc;
    }

    @Override
    public String toString() {
        return desc;
    }
}
