package com.hxlm.android.comm.iosession;

import android.annotation.SuppressLint;
import android.bluetooth.*;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import com.hxlm.android.comm.AbstractCodec;
import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.Error;
import com.hxlm.android.utils.ByteUtil;
import com.hxlm.android.utils.Logger;

import java.io.IOException;
import java.util.UUID;

@SuppressLint("NewApi")
public class BluetoothBleIOSession extends AbstractIOSession {
    private final static long SCAN_PERIOD = 10000;

    // 蓝牙BLE的状态回调
    private final BluetoothGattCallback bluetoothGattCallback;
    private final BluetoothAdapter.LeScanCallback leScanCallback;

    private UUID uuidService;
    private UUID uuidCharacterReceive;
    private UUID uuidClientCharacterConfig;
    private UUID uuidCharaterSend;

    private BluetoothAdapter btAdapter;
    private BluetoothGatt bluetoothGatt;

    private boolean isScanning = false;
    private String btName;

    public BluetoothBleIOSession(final AbstractDeviceActivity anActivity,
                                 final AbstractCodec abstractCodec) throws IOException {
        super(anActivity, abstractCodec);

        if (!activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            throw new IOException("本设备无法支持蓝牙BLE功能！");
        }

        final BluetoothManager btManager = (BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE);

        if (btManager == null) {
            throw new IOException("无法初始化BluetoothManager.");
        }

        btAdapter = btManager.getAdapter();
        if (btAdapter == null) {
            throw new IOException("无法初始化BluetoothAdapter.");
        }

        if (!btAdapter.isEnabled()) {
            btAdapter.enable();
        }

        bluetoothGattCallback = new BLECallback();
        leScanCallback = new ScanCallback();
    }

    /**
     * 启动周边蓝牙设备的搜索
     */
    @Override
    public void doConnect() {
        if (status == Status.CONNECTED && bluetoothGatt != null) {

            Logger.i(tag, "---存在已有蓝牙连接，进行重新连接。");
            bluetoothGatt.connect();
            return;
        }

        for (int i = 0; i < 10 && !btAdapter.isEnabled(); i++) {
            Logger.i(tag, "---等待蓝牙启动完成。");

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Logger.e(tag, e);
            }
        }

        if (btAdapter.isEnabled()) {
            // 一定时间后停止蓝牙设备扫描
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isScanning) {
                        btAdapter.stopLeScan(leScanCallback);
                        isScanning = false;
                        onConnectFailed(Error.DEVICE_NOT_FOUND);
                    }
                }
            }, SCAN_PERIOD);

            // 蓝牙设备搜索，只搜索指定服务的蓝牙设备
            btAdapter.startLeScan(leScanCallback);
            isScanning = true;

            Logger.i(tag, "---开始扫描蓝牙设备。");
        } else {
            onConnectFailed(Error.ADAPTER_INIT_FAILED);
        }
    }

    @Override
    public void doClose() {
        if (isScanning) {
            Logger.i(tag, "---停止搜索蓝牙。");
            btAdapter.stopLeScan(leScanCallback);
            isScanning = false;
        }

        if (bluetoothGatt != null) {
            Logger.i(tag, "---准备断开蓝牙。");
            bluetoothGatt.disconnect();
        }
    }

    @Override
    public int read(byte[] buffer) throws IOException {
        return -1;
    }

    @Override
    public void doSendMessage(byte[] packet) {
        if (bluetoothGatt != null) {
            BluetoothGattService gattService = bluetoothGatt.getService(uuidService);

            if (gattService != null) {
                BluetoothGattCharacteristic characteristic = gattService.getCharacteristic(uuidCharaterSend);

                if (characteristic != null) {
                    characteristic.setValue(packet);
                    bluetoothGatt.writeCharacteristic(characteristic);

                    Logger.i(tag, "发送新消息： Send Data = "
                            + ByteUtil.bytesToHexString(packet));
                } else
                    Logger.e(tag, "characteristic为空！");
            } else
                Logger.e(tag, "gattService为空！");
        } else {
            Logger.e(tag, "蓝牙连接不存在，无法发送消息！");
            close();
        }
    }

    public void setBtName(String btName) {
        this.btName = btName;
    }

    public void setUuidService(UUID uuidService) {
        this.uuidService = uuidService;
    }

    public void setUuidCharacterReceive(UUID uuidCharacterReceive) {
        this.uuidCharacterReceive = uuidCharacterReceive;
    }

    public void setUuidClientCharacterConfig(UUID uuidClientCharacterConfig) {
        this.uuidClientCharacterConfig = uuidClientCharacterConfig;
    }

    public void setUuidCharaterSend(UUID uuidCharaterSend) {
        this.uuidCharaterSend = uuidCharaterSend;
    }

    private class BLECallback extends BluetoothGattCallback {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Logger.i(tag, "---蓝牙连接成功。");

                //最好用一个独立线程处理状态发生变化时的回调接口，实践中回调耗时较长时会导致连接出现问题
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        bluetoothGatt.discoverServices();
                    }
                });
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                bluetoothGatt.close();
                close();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (gatt != null) {
                BluetoothGattService service = gatt.getService(uuidService);
                if (service != null) {
                    Logger.i(tag, "---蓝牙服务获取成功。");

                    BluetoothGattCharacteristic characteristic = service.getCharacteristic(uuidCharacterReceive);

                    gatt.setCharacteristicNotification(characteristic, true);

                    BluetoothGattDescriptor descriptor = characteristic.getDescriptor(uuidClientCharacterConfig);

                    descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);

                    gatt.writeDescriptor(descriptor);

                    onConnected();
                } else {
                    onConnectFailed(Error.SERVICE_NOT_SUPPORT);
                }
            } else {
                onConnectFailed(Error.DRIVER_NOT_FOUND);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            if (uuidCharacterReceive.equals(characteristic.getUuid())) {
                byte[] data = characteristic.getValue();
                parseData(data, data.length);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (uuidCharacterReceive.equals(characteristic.getUuid())) {
                byte[] data = characteristic.getValue();
                parseData(data, data.length);
            }
        }
    }

    private final class ScanCallback implements BluetoothAdapter.LeScanCallback {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            if (isScanning) {
                Logger.i(tag, " ---检测到蓝牙设备：" + device.getName() + "\nUUid-->" + device.getUuids() + "\n");
                if (device.getUuids() != null && device.getUuids().length > 0) {
                    for (int i = 0; i < device.getUuids().length; i++) {
                        Log.i(tag, "uuid-->" + device.getUuids()[i] + "\n");
                    }
                }


                if (!TextUtils.isEmpty(device.getName()) && btName.contains(device.getName())) {
                    btAdapter.stopLeScan(leScanCallback);
                    isScanning = false;

                    Logger.i(tag, " ---启动与设备的蓝牙连接：" + device.getName());

                    BluetoothBleIOSession.this.bluetoothGatt =
                            device.connectGatt(activity, false, bluetoothGattCallback);
                }
            }
        }
    }
}
