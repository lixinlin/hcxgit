package com.hxlm.android.health.device.message.board;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * 站版接收数据
 *
 * @author l
 */
public class BoardResponseMessage extends AbstractMessage {
    public enum ResultType {
        SETTING_LEVEL_1_SUCCESS,
        SETTING_LEVEL_2_SUCCESS,
        SETTING_LEVEL_3_SUCCESS,
        SETTING_LEVEL_4_SUCCESS,
        SETTING_LEVEL_5_SUCCESS,
        SETTING_LEVEL_6_SUCCESS
    }

    public ResultType result;

    public BoardResponseMessage() {
        super(HealthDeviceMessageType.BOARD_COMMAND_RESPONSE);
    }

    public ResultType getResult() {
        return result;
    }

    public void setResult(ResultType result) {
        this.result = result;
    }

    @Override
    public String toString() {
        if(result==null)
        {
            return "返回的消息值为空";
        }
        switch (result){
            case SETTING_LEVEL_1_SUCCESS:
                return "站板档位一设置成功";
            case SETTING_LEVEL_2_SUCCESS:
                return "站板档位二设置成功";
            case SETTING_LEVEL_3_SUCCESS:
                return "站板档位三设置成功";
            case SETTING_LEVEL_4_SUCCESS:
                return "站板档位四设置成功";
            case SETTING_LEVEL_5_SUCCESS:
                return "站板档位五设置成功";
            case SETTING_LEVEL_6_SUCCESS:
                return "站板档位六设置成功";
        }
        return "未知消息类型";
    }
}
