package com.hxlm.android.health.device.codec;

import com.hxlm.android.comm.AbstractCodec;
import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;
import com.hxlm.android.health.device.message.board.BoardLevelCommand;
import com.hxlm.android.health.device.message.board.BoardResponseMessage;
import com.hxlm.android.health.device.message.chair.ChairCommand;
import com.hxlm.android.health.device.message.chair.ChairResponseMessage;
import com.hxlm.android.utils.ByteUtil;
import com.hxlm.android.utils.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 按摩椅的协议处理类
 *
 * @author l
 */
public class RongtaiChairCodec extends AbstractCodec {

    private static final String TAG = "RongtaiChairCodec";
    private static final boolean IsTrue = true;

    public RongtaiChairCodec() {
        super(new byte[]{(byte) 0xF0, (byte) 0x01}, 2, 3);
    }

    @Override
    protected int getBodyLength(int bodyStartIndex) {
        return 2;
    }

    /**
     * 按摩椅协议 向设备发送数据
     */
    @Override
    protected byte[] getPacket(AbstractMessage message) {
        byte[] content = encodeMessage(message);// 数据包内容
        byte[] packet = new byte[readPacketHeader.length + content.length + 2];
        System.arraycopy(readPacketHeader, 0, packet, 0, readPacketHeader.length);// 拷贝包头到packet数组
        // 开始标识f0
        // 01
        // 拷贝数据包内容到packet
        System.arraycopy(content, 0, packet, readPacketHeader.length,
                content.length);// 拷贝数据包内容到packet数组
        // 校验和
        int sum = 0;
        for (int i = readPacketHeader.length; i < packet.length - 2; i++) {
            sum += ((packet[i] + (0x01)) & 0x000000FF);// 确保没有负数
        }

        packet[packet.length - 2] = (byte) (((~sum) & 0xff) & 0x7f);// 检验和
        // 校验时，用命令字节加上0X01后取反，并将最高位清零
        // 结束标识
        packet[packet.length - 1] = (byte) 0XF1;


        Logger.d(TAG,
                "按摩椅发送的数据-->" + ByteUtil.bytesToHexString(packet));

        return packet;
    }


    /**
     * 解析设备返回数据
     */
    @Override
    public Collection<AbstractMessage> getMessages(byte[] newData, int dataLength)
            throws InterruptedException {
        //------------------------------------
        final ReentrantLock lock = this.lock;
        lock.lockInterruptibly();

        try {
            writeToBuffer(newData, dataLength);// 缓冲

            ArrayList<AbstractMessage> messages = null;

            while (startIndex < endIndex) {

                // 此处的判断防止剩余未处理的字节数小于包头，如果小于包头，判断包头的方法会抛出异常
                if ((endIndex - startIndex) < (1 + 2)) { //返回一个整体包 f0 01 f1
                    break;
                }

                // 判断包头 0xf0
                if (this.isPacketHeader(startIndex)) { // isPacketHeader(dataBuffer,
                    // index)

                    int bodyStartIndex = startIndex + 1; // 除开包头外的内容的起点位置 返回的命令值
                    int bodyLength = 2; // 内容的总长度，内容为 0x01 0xf1

                    // 只有当剩余数据能完整包含除包头外的内容时，才进行处理；否则留待下次处理
                    if (bodyStartIndex + bodyLength > endIndex) {
                        break;
                    }

                    if (messages == null) {
                        messages = new ArrayList<AbstractMessage>();
                    }
                    // 传递给解码器的位置索引从内容长度字节开始，也就是命令字
                    AbstractMessage message = decodeMessage(bodyStartIndex);

                    if (message != null) {

                        Logger.d(TAG,
                                "获得新消息:" + message.toString());


                        messages.add(message);
                    }

                    // 不管校验和是否错误，处理指针都移向下一个消息包的起点。
                    startIndex += 1 + bodyLength;
                } else {
                    // 如果不是包头，则向前移动一位
                    startIndex++;
                }
            }

            return messages;
        } finally {
            lock.unlock();
        }


    }

    /**
     * 检查某一字节数组指定位置开始的连续字节是否与指定的消息头字节数组相等。
     * <p/>
     * 待处理的字节数组
     *
     * @param startIndex 本次处理在字节数组的起始位置
     */
    @Override
    public boolean isPacketHeader(int startIndex) {
        try {
            byte[] packetHeader_back = {(byte) 0xF0};
            for (int i = 0; i < packetHeader_back.length; i++) {
                if (packetHeader_back[i] != dataBuffer[startIndex + i]) {
//                    if (IsTrue) {
//                        Log.w(TAG,
//                                "未检测到包头！index = "
//                                        + startIndex
//                                        + "\ndataBuffer = "
//                                        + CommonUtils.bytesToHexString(
//                                        dataBuffer, 0, dataBuffer.length));
//                    }
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            if (IsTrue) {
                Logger.d(TAG, "检查包头出现异常， startIndex = " + startIndex + "， 异常信息为："
                        + e.getMessage());
                Logger.d(TAG,
                        "data = "
                                + ByteUtil.bytesToHexString(dataBuffer, 0,
                                dataBuffer.length));
            }
        }
        return false;
    }


    // 接收设备传回的数据
    @Override
    public AbstractMessage decodeMessage(int bodyStartIndex) {
        AbstractMessage message = null;
        ChairResponseMessage chairResponseMessage = new ChairResponseMessage();
        BoardResponseMessage boardResponseMessage = new BoardResponseMessage();

        switch (dataBuffer[bodyStartIndex]) {
            case 0x00:
                chairResponseMessage.setResult(ChairResponseMessage.ResultType.NONE_SUCCESS);
                message = chairResponseMessage;
                break;
            case 0x01:
                chairResponseMessage.setResult(ChairResponseMessage.ResultType.POWER_SWITCH_SUCCESS);
                message = chairResponseMessage;
                break;
            case 0x02:
                chairResponseMessage.setResult(ChairResponseMessage.ResultType.MASSAGE_PATTERN_1_SUCCESS);
                message = chairResponseMessage;
                break;
            case 0x03:
                chairResponseMessage.setResult(ChairResponseMessage.ResultType.MASSAGE_PATTERN_2_SUCCESS);
                message = chairResponseMessage;
                break;
            case 0x04:
                chairResponseMessage.setResult(ChairResponseMessage.ResultType.MASSAGE_PATTERN_3_SUCCESS);
                message = chairResponseMessage;
                break;
            case 0x05:
                chairResponseMessage.setResult(ChairResponseMessage.ResultType.BACKREST_UP_SUCCESS);
                message = chairResponseMessage;
                break;
            case 0x06:
                chairResponseMessage.setResult(ChairResponseMessage.ResultType.BACKREST_DOWN_SUCCESS);
                message = chairResponseMessage;
                break;
            case 0x07:
                chairResponseMessage.setResult(ChairResponseMessage.ResultType.BOARD_STRETCH_SUCCESS);
                message = chairResponseMessage;
                break;
            case 0x08:
                chairResponseMessage.setResult(ChairResponseMessage.ResultType.BOARD_SHRINK_SUCCESS);
                message = chairResponseMessage;
                break;
            case 0x09:
                boardResponseMessage.setResult(BoardResponseMessage.ResultType.SETTING_LEVEL_1_SUCCESS);
                message = boardResponseMessage;
                break;
            case 0x0A:
                boardResponseMessage.setResult(BoardResponseMessage.ResultType.SETTING_LEVEL_2_SUCCESS);
                message = boardResponseMessage;
                break;
            case 0x0B:
                boardResponseMessage.setResult(BoardResponseMessage.ResultType.SETTING_LEVEL_3_SUCCESS);
                message = boardResponseMessage;
                break;
            case 0x0C:
                boardResponseMessage.setResult(BoardResponseMessage.ResultType.SETTING_LEVEL_4_SUCCESS);
                message = boardResponseMessage;
                break;
            case 0x0D:
                boardResponseMessage.setResult(BoardResponseMessage.ResultType.SETTING_LEVEL_5_SUCCESS);
                message = boardResponseMessage;
                break;
            case 0x0E:
                boardResponseMessage.setResult(BoardResponseMessage.ResultType.SETTING_LEVEL_6_SUCCESS);
                message = boardResponseMessage;
                break;
            //靠背升停接收成功
            case 0x0F:
                chairResponseMessage.setResult(ChairResponseMessage.ResultType.BACKREST_UP_STOP_SUCCESS);
                message = chairResponseMessage;
                break;
            //靠背降停接收成功
            case 0x10:
                chairResponseMessage.setResult(ChairResponseMessage.ResultType.BACKREST_DOWN_STOP_SUCCESS);
                message = chairResponseMessage;
                break;
            //零重力键
            case 0x11:
                chairResponseMessage.setResult(ChairResponseMessage.ResultType.MESSAGE_ZERO_GRAVITY_SUCCESS);
                message = chairResponseMessage;
                break;
            //暂停功能接收成功
            case 0x13:
                chairResponseMessage.setResult(ChairResponseMessage.ResultType.BOARD_PAUSE_SUCCESS);
                message = chairResponseMessage;
                break;
            //复位-站板0档
            case 0x12:
                chairResponseMessage.setResult(ChairResponseMessage.ResultType.LEGPLANK_RESET_SUCCESS);
                message = chairResponseMessage;
                break;
            //电源关闭接收成功
            case 0x14:
                chairResponseMessage.setResult(ChairResponseMessage.ResultType.POWER_SWITCH_CLOSE_SUCCESS);
                message = chairResponseMessage;
                break;
            //全部复位接收成功
            case 0x15:
                chairResponseMessage.setResult(ChairResponseMessage.ResultType.ALL_RESET_SUCCESS);
                message = chairResponseMessage;
                break;
        }
        return message;
    }

    // 向设备发送数据
    @Override
    public byte[] encodeMessage(AbstractMessage message) {
        byte[] packBody = new byte[1];

        // 数据包内容
        switch ((HealthDeviceMessageType) message.getMessageType()) {
            case CHAIR_COMMAND:
                switch (((ChairCommand) message).getCommandType()) {
                    //无按键按下时
                    case NONE:
                        packBody[0] = (byte) 0x7F;
                        break;
                    case POWER_SWITCH:
                        packBody[0] = (byte) 0x01;
                        break;
                    case BACKREST_UP_START:
                        packBody[0] = (byte) 0x64;
                        break;
                    case BACKREST_UP_STOP:
                        packBody[0] = (byte) 0x65;
                        break;
                    case BACKREST_DOWN_START:
                        packBody[0] = (byte) 0x66;
                        break;
                    case BACKREST_DOWN_STOP:
                        packBody[0] = (byte) 0x67;
                        break;
                    case BOARD_STRETCH_START:
                        packBody[0] = (byte) 0x68;
                        break;
                    case BOARD_STRETCH_STOP:
                        packBody[0] = (byte) 0x69;
                        break;
                    case BOARD_SHRINK_START:
                        packBody[0] = (byte) 0x6A;
                        break;
                    case BOARD_SHRINK_STOP:
                        packBody[0] = (byte) 0x6B;
                        break;
                    case MASSAGE_PATTERN_1:
                        packBody[0] = (byte) 0x2E;
                        break;
                    case MASSAGE_PATTERN_2:
                        packBody[0] = (byte) 0x2D;
                        break;
                    case MASSAGE_PATTERN_3:
                        packBody[0] = (byte) 0x2F;
                        break;
                    case MESSAGE_ZERO_GRAVITY:
                        packBody[0] = (byte) 0x70;
                        break;
                    //复位-站板0档
                    case LEGPLANK_RESET:
                        packBody[0] = (byte) 0x58;
                        break;
                    //按摩椅暂停功能接收
                    case MASSAGE_PAUSE:
                        packBody[0] = (byte) 0x56;
                        break;

                    //按摩恢复
                    case MASSAGE_CONTINUE:
                        packBody[0] = (byte) 0x55;
                        break;
                    //电源关
                    case POWER_SWITCH_CLOSE:
                        packBody[0] = (byte) 0x02;
                        break;
                    //全部复位
                    case ALL_RESET:
                        packBody[0] = (byte) 0x03;
                        break;
                }
                break;
            case BOARD_LEVEL_COMMAND:
                switch (((BoardLevelCommand) message).getCommandType()) {
                    case SETTING_LEVEL_1:
                        packBody[0] = (byte) 0x75;
                        break;
                    case SETTING_LEVEL_2:
                        packBody[0] = (byte) 0x76;
                        break;
                    case SETTING_LEVEL_3:
                        packBody[0] = (byte) 0x77;
                        break;
                    case SETTING_LEVEL_4:
                        packBody[0] = (byte) 0x78;
                        break;
                    case SETTING_LEVEL_5:
                        packBody[0] = (byte) 0x79;
                        break;
                    case SETTING_LEVEL_6:
                        packBody[0] = (byte) 0x7A;
                        break;
                }
        }

        //packBody[1] = (byte) ((~(packBody[0] & 0x000000FF + 0x01)) & 0xFF & 0x7F);
        // 结束标识
        // packBody[2] = (byte) 0XF1;

        return packBody;
    }
}
