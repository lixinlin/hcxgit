package com.hxlm.android.health.device.message.ecg;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 */
public class EcgFilterTypeCommand extends AbstractMessage {

    private int signalFiltrate;         //心电模块滤波    01： 手术模式，1 ~ 25Hz (3dB)；02： 监护模式，0.5 ~ 75Hz (3dB)；03： 诊断模式，0.05 ~ 100Hz (3dB)

    public EcgFilterTypeCommand() {
        super(HealthDeviceMessageType.ECG_FILTER_TYPE_COMMAND);
    }

    public int getSignalFiltrate() {
        return signalFiltrate;
    }

    public void setSignalFiltrate(int signalFiltrate) {
        this.signalFiltrate = signalFiltrate;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("心电模块滤波：");
        switch (signalFiltrate) {
            case 0:
                sb.append("手术模式\n");
                break;
            case 1:
                sb.append("监护模式\n");
                break;
            case 2:
                sb.append("诊断模式\n");
                break;
        }
        return sb.toString();
    }
}
