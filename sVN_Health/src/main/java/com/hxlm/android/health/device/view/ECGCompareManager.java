package com.hxlm.android.health.device.view;

import android.graphics.*;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android_serialport_api.ECGfilter;
import com.hxlm.android.health.device.message.ecg.EcgWaveQueueMessage;
import com.hxlm.android.utils.AssetsUtil;
import com.hxlm.android.utils.IntArrayBlockingQueue;
import com.hxlm.android.utils.Logger;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 用于比较滤波模式和原始数据的画图程序
 * <p/>
 * Created by Zhenyu on 2016/3/30.
 */
public class ECGCompareManager {
    private final static String TAG = "ECGWaveDrawManager";

    private final static int MM_PER_SECONDS = 25; // 基准扫描速度为 25毫米/秒
    private final static int POINTS_PER_TIME = 5; // 每次处理的点的数量
    private final static int DATA_FILTER_COUNT = 1500; //滤波程序每次处理的数据量

    private final int surfaceViewHeight;
    private final SurfaceHolder surfaceHolder;
    private final Paint paint;

    private final IntArrayBlockingQueue inQueue;
    private final IntArrayBlockingQueue filterQueue;
    private final IntArrayBlockingQueue outQueue;

    private final int numberPerMv; // 每毫伏对应的数值，用于换算距离
    private final int pointsPerSecond; // 每秒发送多少个数据

    private final String basePath;
    private final EcgDrawAttrs attrs;
    private ExecutorService executorService;
    private boolean isRunning = false;

    public ECGCompareManager(final SurfaceView sfv, final EcgWaveQueueMessage ecgWaveQueueMessage,
                             String aBasePath) throws IOException {
        basePath = aBasePath;

        File filterFiles = new File(aBasePath + ECGDrawWaveManager.ECG_FILTER_FILE_PATH);
        if (!filterFiles.exists()) {
            try {
                AssetsUtil.copyFilesFromAssets(sfv.getContext(), ECGDrawWaveManager.ECG_FILTER_ASSETS_PATH,
                        aBasePath + ECGDrawWaveManager.ECG_FILTER_FILE_PATH);
            } catch (IOException e) {
                throw new IOException("Unable to copy ECG Filters!" + e.getMessage());
            }
        }

        this.surfaceViewHeight = sfv.getHeight();
        surfaceHolder = sfv.getHolder();
        surfaceHolder.setFormat(PixelFormat.TRANSPARENT);

        attrs = EcgDrawAttrs.getInstance(sfv);

        this.inQueue = ecgWaveQueueMessage.getWaveQueue();
        filterQueue = new IntArrayBlockingQueue(2000);
        outQueue = new IntArrayBlockingQueue(2000);

        numberPerMv = ecgWaveQueueMessage.getNumberPerMv();
        pointsPerSecond = ecgWaveQueueMessage.getPointsPerSecond();
        int maxValue = ecgWaveQueueMessage.getMaxValue();
        int minValue = ecgWaveQueueMessage.getMinValue();

        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(3);
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStyle(Paint.Style.STROKE);
    }

    public void startDraw() {
        isRunning = true;
        executorService = Executors.newFixedThreadPool(4);
        executorService.execute(new FilterRunnable());
        executorService.execute(new DrawWaveRunnable());
    }

    public void stopDraw() {
        isRunning = false;
        if (executorService != null) {
            executorService.shutdownNow();
        }
    }

    private final class FilterRunnable implements Runnable {
        @Override
        public void run() {
            int[] data;

            while (isRunning) {
                data = new int[DATA_FILTER_COUNT];
                for (int i = 0; i < DATA_FILTER_COUNT && isRunning; i++) {
                    try {
                        data[i] = inQueue.take();
                        outQueue.put(data[i]);
                        //Logger.i("心电 处理前==============", data[i] + "");
                    } catch (InterruptedException e) {
                        Logger.e(TAG, e);
                    }
                }
                if (executorService != null && isRunning) {
                    executorService.execute(new ECGfilter(data, filterQueue,
                            basePath + ECGDrawWaveManager.ECG_FILTER_FILE_PATH));
                }
            }
        }
    }

    private final class DrawWaveRunnable implements Runnable {
        private final PointF lastPointOri;
        private final PointF lastPointFilter;

        public DrawWaveRunnable() {
            lastPointOri = new PointF(0, 0);
            lastPointFilter = new PointF(0, 0);
        }

        /**
         * 采用25mm/s的标准扫描速度。不缩放的情况下，纵向需要能显示40小格，即4毫伏的区间。
         * 由于X坐标非常密集，测试发现使不使用贝塞尔曲线对心电图绘制影响不大。
         */
        @Override
        public void run() {
            int displayPointCount = 0;  //控制超出显示尺寸时折回显示
            int displayPointCountFilter = 0;  //控制超出显示尺寸时折回显示

            // 每个数据包对应多少个像素
            final double pixelPerPacket = attrs.xPixelPerMM * MM_PER_SECONDS / pointsPerSecond;
            // 传过来的每个数字的1对应多少像素
            final double pixelPerNumber = attrs.yPixelPerMM * 10 / numberPerMv;
            // 实际需要的高度
//            final double maxHeight = (maxValue - minValue) * pixelPerNumber;
            // 如果要把最大高度时坐标系中值定位在中间的话，需要减去本偏移量
//            final float offset = (float) ((maxHeight - attrs.height) / 2);
            //y轴上如果显示不下时，缩放比例
            // final double yScale = (double) attrs.height / ((maxValue - minValue) * pixelPerNumber);

            final double yScale = 1;

            Path path = new Path();

            Logger.i(TAG, "开始绘制心电图。");

            while (isRunning) {
                //开始绘制心电图
                int cleanerWidth = 20;
                final Canvas canvas = surfaceHolder.lockCanvas(new Rect((int) (lastPointFilter.x), 0,
                        (int) (lastPointFilter.x + POINTS_PER_TIME + cleanerWidth), surfaceViewHeight));

                if (canvas != null) {
                    //清理锁定区域
                    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                    canvas.drawPaint(paint);
                    //设定绘图模式
                    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));

                    path.reset();

                    //每次绘制一定数量的点
                    for (int i = 0; i < POINTS_PER_TIME; i++) {
                        float x = Math.round(displayPointCountFilter * pixelPerPacket);

                        //如果x坐标值已经超出显示区域，则跳出循环，准备从头显示。
                        if (x > attrs.width) {
                            displayPointCountFilter = 0;
                            lastPointFilter.x = attrs.xStart;
                            break;
                        }
                        //显示时需要加上边缘的偏移量
                        x += attrs.xStart;

                        float yFilter = 0;

                        try {
                            //从阻塞队列中获得数据
                            int dataFilter = filterQueue.take();
                            yFilter = attrs.yEnd - (float) (dataFilter * pixelPerNumber * yScale);
                        } catch (InterruptedException e) {
                            Logger.e(TAG, e);
                        }

                        if (lastPointFilter.x != 0 || lastPointFilter.y != 0) {
                            path.moveTo(lastPointFilter.x, lastPointFilter.y);
                            path.lineTo(x, yFilter);
                        }

                        lastPointFilter.x = x;
                        lastPointFilter.y = yFilter;

                        displayPointCountFilter++;
                    }
                    //每次绘制一定数量的点
                    for (int i = 0; i < POINTS_PER_TIME; i++) {
                        float x = Math.round(displayPointCount * pixelPerPacket);

                        //如果x坐标值已经超出显示区域，则跳出循环，准备从头显示。
                        if (x > attrs.width) {
                            displayPointCount = 0;
                            lastPointOri.x = attrs.xStart;
                            break;
                        }
                        //显示时需要加上边缘的偏移量
                        x += attrs.xStart;

                        float yOri = 0;

                        try {
                            int dataOri = outQueue.take();
                            yOri = attrs.yEnd - (float) (dataOri * pixelPerNumber * yScale) - 520;
                        } catch (InterruptedException e) {
                            Logger.e(TAG, e);
                        }


                        if (lastPointOri.x != 0 || lastPointOri.y != 0) {
                            path.moveTo(lastPointOri.x, lastPointOri.y);
                            path.lineTo(x, yOri);
                        }

                        lastPointOri.x = x;
                        lastPointOri.y = yOri;

                        displayPointCount++;
                    }
                    canvas.drawPath(path, paint);
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }
}
