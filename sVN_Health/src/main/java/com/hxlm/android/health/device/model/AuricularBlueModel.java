package com.hxlm.android.health.device.model;

import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.AbstractModel;
import com.hxlm.android.comm.iosession.BluetoothIOSession;
import com.hxlm.android.health.device.codec.AuricularBlueCodec;

import java.io.IOException;

/**
 * Created by l on 2016/9/28.
 * 耳针仪Model 普通蓝牙
 */
public class AuricularBlueModel extends AbstractModel {

    public AuricularBlueModel() {
        super("耳针仪", new FunctionType[]{FunctionType.AURICULAR_BULE});
    }

    @Override
    public AbstractIOSession getIOSession(AbstractDeviceActivity activity) {
        try {
            BluetoothIOSession ioSession = new BluetoothIOSession(activity, new AuricularBlueCodec(), "yueluoyi", "0000");

            ioSession.setConnectHeadset(true);
            ioSession.setConnectA2DP(true);

            ioSession.setConnectRFComm(true);

            ioSession.setAutoPairing(false);
            return ioSession;
        } catch (IOException e) {
            return null;
        }
    }
}
