package com.hxlm.android.health.device.message.board;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * 站版驱动板发送数据
 *
 * @author l
 */
public class BoardLevelCommand extends AbstractMessage {
    public enum CommandType {
        SETTING_LEVEL_1,
        SETTING_LEVEL_2,
        SETTING_LEVEL_3,
        SETTING_LEVEL_4,
        SETTING_LEVEL_5,
        SETTING_LEVEL_6
    }

    private CommandType commandType;//站板档位

    public BoardLevelCommand() {
        super(HealthDeviceMessageType.BOARD_LEVEL_COMMAND);
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public void setCommandType(CommandType commandType) {
        this.commandType = commandType;
    }

    @Override
    public String toString() {
        return "站板档位是 :   " + commandType;
    }
}
