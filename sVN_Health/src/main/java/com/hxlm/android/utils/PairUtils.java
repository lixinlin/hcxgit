package com.hxlm.android.utils;

import android.bluetooth.BluetoothDevice;

import java.lang.reflect.Method;

/**
 * 实现自动配对，不弹出配对框的功能
 */
public class PairUtils {
    /**
     * 与设备配对 参考源码：platform/packages/apps/Settings.git
     * /Settings/src/com/android/settings/bluetooth/CachedBluetoothDevice.java
     */
    static public boolean createBond(BluetoothDevice btDevice)
            throws Exception {
        Method createBondMethod = btDevice.getClass().getMethod("createBond");
        createBondMethod.setAccessible(true);
        return (Boolean) createBondMethod.invoke(btDevice);
    }

    /**
     * 与设备解除配对 参考源码：platform/packages/apps/Settings.git
     * /Settings/src/com/android/settings/bluetooth/CachedBluetoothDevice.java
     */
    static public boolean removeBond(BluetoothDevice btDevice)
            throws Exception {

        Method removeBondMethod = btDevice.getClass().getMethod("removeBond");
        return (Boolean) removeBondMethod.invoke(btDevice);
    }

    static public boolean setPin(BluetoothDevice btDevice,
                                 String str) throws Exception {

        Method removeBondMethod = btDevice.getClass().getDeclaredMethod("setPin", byte[].class);
        return (Boolean) removeBondMethod.invoke(btDevice, new Object[]{str.getBytes()});
    }

    // 取消用户输入
    static public boolean cancelPairingUserInput(BluetoothDevice device) throws Exception {

        Method createBondMethod = device.getClass().getMethod("cancelPairingUserInput");
//        cancelBondProcess(btClass,device);
        return (Boolean) createBondMethod.invoke(device);
    }

    // 取消配对
    static public boolean cancelBondProcess(BluetoothDevice device) throws Exception {

        Method createBondMethod = device.getClass().getMethod("cancelBondProcess");
        return (Boolean) createBondMethod.invoke(device);
    }
}
