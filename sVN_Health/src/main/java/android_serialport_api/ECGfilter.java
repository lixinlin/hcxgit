package android_serialport_api;

import com.hxlm.android.utils.IntArrayBlockingQueue;
import com.hxlm.android.utils.Logger;


public class ECGfilter implements Runnable {
    private final static String TAG = "ECGfilter";

    private final static int SAMPLE_RATE = 250;
    private final int[] data;
    private final IntArrayBlockingQueue outQueue;
    private final String filterPath;

    public ECGfilter(int[] oriData, IntArrayBlockingQueue queue, String aFilterPath) {
        filterPath = aFilterPath;
        data = oriData;
        outQueue = queue;
    }

    // 宣告由C/C++實作的方法
    public native void ecgDrawFilter(int[] oriData, int length, float sampleRate, String filtersPath);

    // 讀取函式庫
    static {
        System.loadLibrary("EcgDrawFilter");
    }

    @Override
    public void run() {
        ecgDrawFilter(data, data.length, SAMPLE_RATE, filterPath);
        try {
            for (int aData : data) {
                outQueue.put(aData);
            }
        } catch (InterruptedException e) {
            Logger.e(TAG, e);
        }
    }
}
