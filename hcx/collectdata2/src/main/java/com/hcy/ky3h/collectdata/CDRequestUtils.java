package com.hcy.ky3h.collectdata;

import android.text.TextUtils;

import com.zhy.http.okhttp.OkHttpUtils;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * @author Lirong
 * @date 2019/6/13.
 * @description
 */

public class CDRequestUtils {

    private static String baseUrl = "http://47.92.160.74:8086/api/v1/user/";
    private static String SYNC_API_KEY = "6e21212ba021e3d09db25d1c4f2bc4b4";
    private static String token;

    private static OkHttpClient getClient(){
        HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
//                Log.d("okHttpLogInfo",message);
            }
        });
        logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(logInterceptor)
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS);
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                if (!TextUtils.isEmpty(token)) {
                    Request request = chain.request().newBuilder().addHeader("token", token).build();
                    return chain.proceed(request);
                }else{
                    return null;
                }
            }
        });
        OkHttpClient client = builder.build();
        return client;
    }

    public static void get(String url, Callback callback) {

        Request request = new Request.Builder()
                .url(baseUrl+url)
                .tag(baseUrl+url)
                .build();
        getClient().dispatcher().setMaxRequests(8);
        getClient().newCall(request).enqueue(callback);
    }

    public static void post(String url, Map<String, Object> params, Callback callback) {
        List<String> kvs = new ArrayList<String>();
        for (String key : params.keySet()) {
            kvs.add(String.format("%s=%s", key, params.get(key)));
        }
        Collections.sort(kvs);
        kvs.add("key="+SYNC_API_KEY);
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < kvs.size(); i++) {
            stringBuilder.append(kvs.get(i) + "&");
        }
        if (stringBuilder.length() > 0){
            stringBuilder.deleteCharAt(stringBuilder.length() -1);
        }
        token = CDMD5Utils.getMD5(stringBuilder.toString());
        JSONObject jsonObject = new JSONObject(params);
        HashMap<String, Object> map = new HashMap<>();
        map.put("body",jsonObject);
        JSONObject jsonObject2 = new JSONObject(map);
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(JSON, jsonObject2.toString());
        Request request = new Request.Builder()
                .post(requestBody)
                .url(baseUrl+url)
                .tag(baseUrl+url)
                .build();
        getClient().dispatcher().setMaxRequests(8);
        getClient().newCall(request).enqueue(callback);

    }

    public static void cancel(String tag) {
        OkHttpUtils.getInstance().cancelTag(tag);
    }

    /**
     * 用户信息接口服务
     * @param userSign 用户标识
     * @param age 年龄
     * @param sex 性别  0-未知；1-男；2-女
     * @param userSource  用户来源  0-未知；1-和畅依APP；
     * @param remark 备注
     */
    public static void getInfo(String userSign,int age,int sex,String userSource,String remark){
        try {
            HashMap<String, Object> map = new HashMap<>();
            map.put("userSign",userSign);
            map.put("age",age+"");
            map.put("sex",sex+"");
            map.put("userSource",userSource);
            map.put("remark",remark);
            post("info", map, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 用户访问app记录
     * @param user_sign 用户标识
     * @param user_source 用户来源
     * @param channel 渠道
     * @param startTime 启动时间
     * @param quitTime 退出时间
     * @param flag  启动|退出标识，启动时把启动时间先缓存，退出的 时候把启动时间和退出时间 一起传过来
     * @param remark 备注
     */
    public static void getAccess(String user_sign, String user_source,String channel,String startTime,String quitTime,String flag,String remark){
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("userSign",user_sign);
            map.put("userSource",user_source);
            map.put("channel",channel);
            map.put("startTime",startTime);
            map.put("quitTime",quitTime);
            map.put("flag",flag);
            map.put("remark",remark);
            post("access", map, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *  记录用户使用app的设备信息
     * @param user_sign 用户标识
     * @param user_source 用户来源
     * @param brand 品牌
     * @param model 型号
     * @param system 系统
     * @param resolution 分辨率
     * @param operator 运营商  0-未知；1-中国移动；2-中国联通；3-中国电信
     * @param network_method 联网方式  0-未知；1-WIFI；2：3G/4G/5G
     * @param remark 备注
     */
    public static void getDevice(String user_sign,String user_source,String brand,String model,String system,String resolution,String operator,String network_method, String remark){
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("userSign",user_sign);
            map.put("userSource",user_source);
            map.put("brand",brand);
            map.put("model",model);
            map.put("system",system);
            map.put("resolution",resolution);
            map.put("operator",operator);
            map.put("networkMethod",network_method);
            map.put("remark",remark);
            post("device", map, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 用于记录用户使用app下载网站资源记录
     * @param user_sign 用户标识
     * @param user_source 用户来源
     * @param channel 渠道
     * @param version 版本
     * @param down_time 下载时间
     * @param remark 备注
     */
    public static void getDownload(String user_sign,String user_source,String channel,String version,String down_time,String remark){
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("userSign",user_sign);
            map.put("userSource",user_source);
            map.put("channel",channel);
            map.put("version",version);
            map.put("downTime",down_time);
            map.put("remark",remark);
            post("download", map, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

