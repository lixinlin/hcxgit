package com.hxlm.android.hcy.report;

/**
 * Created by lixinlin on 2019/1/9.
 */

/**
 * 全量档案
 */
public class ArchivesBean {

    /**
     * name : 阳虚质
     * createTime : 1478052375000
     * year : 2016
     * date : 11月02日
     * time : 10:06
     * type : TZBS
     * typeName : 体质
     * link : /member/service/reshow.jhtml?sn=TZBS-03&device=1
     * sn : TZBS-03
     */

    private String name;
    private long createTime;
    private String year;
    private String date;
    private String time;
    private String type;
    private String typeName;
    private String link;
    private String sn;
    private String path;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
