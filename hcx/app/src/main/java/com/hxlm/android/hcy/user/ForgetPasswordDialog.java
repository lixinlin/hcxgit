package com.hxlm.android.hcy.user;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.hcyandroid.util.IsMobile;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ContainsEmojiEditText;

public class ForgetPasswordDialog extends AlertDialog implements
        OnClickListener {
    private final OnCompleteListener onCompleteListener;
    private final UserManager userManager;
    private ContainsEmojiEditText et_account;// 账号
    private ContainsEmojiEditText et_password;// 密码
    private ContainsEmojiEditText et_confirm_password;// 确认密码
    private ContainsEmojiEditText et_test_number;// 验证码
    private String account;
    private String password;
    private Context context;

    public ForgetPasswordDialog(Context context, OnCompleteListener listener) {
        super(context);
        this.context = context;
        this.onCompleteListener = listener;
        userManager = new UserManager();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forget_password);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        initView();
    }

    private void initView() {
        et_account = (ContainsEmojiEditText) findViewById(R.id.et_account);
        et_password = (ContainsEmojiEditText) findViewById(R.id.et_password);
        et_confirm_password = (ContainsEmojiEditText) findViewById(R.id.et_confirm_password);
        et_test_number = (ContainsEmojiEditText) findViewById(R.id.et_test_number);

        LinearLayout linear_back = (LinearLayout) findViewById(R.id.linear_back);
        Button bt_get_test_number = (Button) findViewById(R.id.bt_get_test_number);
        Button bt_commit_and_login = (Button) findViewById(R.id.bt_commit_and_login);

        linear_back.setOnClickListener(this);
        bt_get_test_number.setOnClickListener(this);
        bt_commit_and_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linear_back:
                dismiss();
                break;

            // 获取验证码
            case R.id.bt_get_test_number:
                account = et_account.getText().toString();
                if (!TextUtils.isEmpty(account)) {
                    //验证手机号
                    boolean idMobile= IsMobile.isMobileNO(account);
                    if(idMobile)
                    {
                        userManager.getCaptchaAgain(account, new AbstractDefaultHttpHandlerCallback(getContext()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                ToastUtil.invokeShortTimeToast(getContext(), context.getString(R.string.login_has_been_send));
                            }
                        });
                    }else
                    {
                        ToastUtil.invokeShortTimeToast(getContext(), context.getString(R.string.login_phone_format_error));
                    }


                } else {
                    ToastUtil.invokeShortTimeToast(getContext(), getContext().getString(R.string.username_is_empty));
                }
                break;

            // 提交并登陆
            case R.id.bt_commit_and_login:
                account = et_account.getText().toString();
                password = et_password.getText().toString();
                String confirmPassword = et_confirm_password.getText().toString();
                String testNumber = et_test_number.getText().toString();

                if (TextUtils.isEmpty(account)) {
                    ToastUtil.invokeShortTimeToast(getContext(), context.getString(R.string.username_is_empty));
                } else if (TextUtils.isEmpty(password)) {
                    ToastUtil.invokeShortTimeToast(getContext(), context.getString(R.string.forget_pwd_mima_empty));
                } else if (TextUtils.isEmpty(confirmPassword)) {
                    ToastUtil.invokeShortTimeToast(getContext(), context.getString(R.string.forget_pwd_sure_mima_empty));
                } else if (TextUtils.isEmpty(testNumber)) {
                    ToastUtil.invokeShortTimeToast(getContext(), context.getString(R.string.forget_pwd_sms_empty));
                } else if (!password.equals(confirmPassword)) {
                    ToastUtil.invokeShortTimeToast(getContext(), context.getString(R.string.forget_pwd_dont_agree));
                } else {
                    userManager.createNewPassword(account, testNumber, password,
                            new AbstractDefaultHttpHandlerCallback(getContext()) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    ForgetPasswordDialog.this.dismiss();

                                    userManager.login(account, password, new AbstractDefaultHttpHandlerCallback(getContext()) {
                                        @Override
                                        protected void onResponseSuccess(Object obj) {
                                            ToastUtil.invokeShortTimeToast(getContext(), context.getString(R.string.forget_pwd_save_and_login));

                                            if (null != onCompleteListener) {
                                                onCompleteListener.onComplete();
                                            }
                                        }
                                    });
                                }
                            });
                }
                break;
        }
    }
}
