package com.hxlm.android.hcy;

import com.hxlm.android.hcy.asynchttp.AbstractHttpHandlerCallback;

/**
 * 逻辑处理类的基类
 * Created by Zhenyu on 2016/4/15.
 */
public abstract class AbstractBaseManager {
    protected final AbstractHttpHandlerCallback handler;

    public AbstractBaseManager(AbstractHttpHandlerCallback callback) {
        this.handler = callback;
    }

}
