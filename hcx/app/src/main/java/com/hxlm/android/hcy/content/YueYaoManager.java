package com.hxlm.android.hcy.content;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.*;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.Constant;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 提供乐药模块的基本功能。 Created by Zhenyu on 2015/8/16.
 */
public class YueYaoManager {
    private static final String TAG = "YueYaoManager";

    public final static int PLAYING = 1;
    public final static int STOP = 0;
    public final static int REFRESH_LIST = 2;
    public final static int REFRESH_COMPLETE = 3;

    private final Handler mHandler;
    private Map<Integer, ResourceItem> downloadingItems;//保存正在下载的乐药
    private List<ResourceItem> cartItems; //保存放入购物车的乐药
    private ResourceItem playingItem;
    private MediaPlayer player;

    public YueYaoManager(Handler mHandler) {
        this.mHandler = mHandler;
    }

    public void getItemsBySubjectSn(String subjectSn, AbstractDefaultHttpHandlerCallback handler) {
        String url = "/resources/listBySubject.jhtml";
        final RequestParams params = new RequestParams();

        params.put("mediaType", "audio");
        params.put("subjectSn", subjectSn);

        HcyHttpClient.submit(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(handler) {

            @Override
            protected Object contentParse(String content) {
                List<ResourceItem> items = JSON.parseArray(content, ResourceItem.class);
                Log.d(TAG, "YueYaoManagercontentParse: "+content);
                for (ResourceItem resourceItem : items) {
                    if (resourceItem.getPrice() == 0 || "paid".equals(resourceItem.getStatus())) {

                        resourceItem.setItemStatus(ResourceItem.NOT_DOWNLOAD);
                    } else if ("unpaid".equals(resourceItem.getStatus())) {
                        resourceItem.setItemStatus(ResourceItem.NOT_ORDERED);
                    }
                }
                processYueYaoItems(items);
                return items;
            }
        });
    }

    public void getOrderedItems(final AbstractDefaultHttpHandlerCallback handler) {
        LoginControllor.requestLogin(handler.getContext(), new OnCompleteListener() {
            @Override
            public void onComplete() {
                final String url = "/member/resources/list/" + LoginControllor.getLoginMember().getId() + ".jhtml";

                RequestParams params = new RequestParams();
                params.put("mediaType", "audio");

                HcyHttpClient.submit(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(handler) {

                    @Override
                    protected Object contentParse(String content) {

                        List<ResourceItem> items = JSON.parseArray(content, ResourceItem.class);

                        for (ResourceItem resourceItem : items) {
                            resourceItem.setItemStatus(ResourceItem.NOT_DOWNLOAD);
                        }
                        processYueYaoItems(items);
                        return items;
                    }
                });
            }
        });
    }

    public void orderItem(final ResourceItem item, final AbstractDefaultHttpHandlerCallback handler) {
        final String url = "/member/resources/downLoadRecord.jhtml";

        RequestParams params = new RequestParams();
        params.put("ids", String.valueOf(item.getId()));

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(handler) {
            @Override
            protected Object contentParse(String content) {
                return content;
            }
        }, LoginControllor.MEMBER, handler.getContext());
    }

   public void downloadItemFile(final ResourceItem resourceItem, final Context context) {
        HcyHttpClient.download(resourceItem.getSource(), new HcyBinaryResponseHandler(getItemLocalPath(resourceItem),
                new AbstractDefaultHttpHandlerCallback(context) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        if (Constant.isEnglish){
                            Toast.makeText(context, "Succeeded in downloading " + resourceItem.getName(),
                                    Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(context, "下载《" + resourceItem.getName() + "》成功",
                                    Toast.LENGTH_SHORT).show();
                        }

                        resourceItem.setItemStatus(ResourceItem.STOP);
                        removeDownloadingItem(resourceItem);
                        mHandler.sendEmptyMessage(REFRESH_COMPLETE);
                    }

                    @Override
                    protected void onResponseError(int errorCode, String errorDesc) {
                        if (Constant.isEnglish){
                            if (errorCode == AbstractHttpHandlerCallback.ON_CREATE_PATH_FAIL) {
                                Toast.makeText(context, "Fails to save " + resourceItem.getName() + ",Retry.",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Fails downloading " + resourceItem.getName() + ",Retry.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            if (errorCode == AbstractHttpHandlerCallback.ON_CREATE_PATH_FAIL) {
                                Toast.makeText(context, "保存《" + resourceItem.getName() + "》失败，请重试",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "下载《" + resourceItem.getName() + "》失败，请重试",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }

                        resourceItem.setItemStatus(ResourceItem.NOT_DOWNLOAD);
                        removeDownloadingItem(resourceItem);
                        mHandler.sendEmptyMessage(REFRESH_LIST);
                    }

                    @Override
                    protected void onProgress(int bytesWritten, int totalSize) {
                        resourceItem.setTotalSize(totalSize);
                        resourceItem.setDownloadedSize(bytesWritten);
                        mHandler.sendEmptyMessage(REFRESH_LIST);
                    }
                }));
    }


    private void processYueYaoItems(List<ResourceItem> items) {
        for (int i = 0; i < items.size(); i++) {
            ResourceItem resourceItem = items.get(i);

            if (resourceItem.getResourcesWarehouses().size() > 0) {
                resourceItem.setSource(resourceItem.getResourcesWarehouses().get(0).getSource());
            }

            String localStoragePath = getItemLocalPath(resourceItem);
            File yueyaoFile = new File(localStoragePath);

            if (yueyaoFile.exists()) {
                resourceItem.setItemStatus(ResourceItem.STOP);
                resourceItem.setLocalStoragePath(localStoragePath);
            }

            if (playingItem != null && playingItem.getId() == resourceItem.getId()) {
                resourceItem.setItemStatus(ResourceItem.PLAYING);
                playingItem = resourceItem;
            }

            if (downloadingItems != null && downloadingItems.containsKey(resourceItem.getId())) {
                items.remove(i);
                items.add(i, downloadingItems.get(resourceItem.getId()));
            }
        }
    }

    public void clear() {
        stopPlayer();
        if (downloadingItems != null) {
            downloadingItems.clear();
        }
    }

    public void playAudio(ResourceItem item) {
//        if (playingItem == null) {
//            //只有当没有播放任何歌曲的时候才向Activity发送开始播放的消息，便于Activity控制某些状态。
//            //如果需要在切换歌曲时进行某些操作，建议在前端的点击事件中操作。
//            mHandler.sendEmptyMessage(PLAYING);
//        } else if (playingItem.getId() != item.getId() && playingItem.getItemStatus() == ResourceItem.PLAYING) {
//            playingItem.setItemStatus(ResourceItem.STOP);
//            Logger.i("YueYaoActivity","在切换歌曲");
//
//        }

        if (playingItem == null) {
            //只有当没有播放任何歌曲的时候才向Activity发送开始播放的消息，便于Activity控制某些状态。
            //如果需要在切换歌曲时进行某些操作，建议在前端的点击事件中操作。
           // mHandler.sendEmptyMessage(PLAYING);
        } else if (playingItem.getId() != item.getId() && playingItem.getItemStatus() == ResourceItem.PLAYING) {
            stopPlayer();
            Logger.i("YueYaoActivity","在切换歌曲");

        }

        item.setItemStatus(ResourceItem.PLAYING);
        playingItem = item;

        mHandler.sendEmptyMessage(PLAYING);

        if (player == null) {
            player = new MediaPlayer();
            player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    player.start();
                }
            });

            //播放完成监听
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    playingItem.setItemStatus(ResourceItem.STOP);
                    //停止
                    mHandler.sendEmptyMessage(STOP);
                }
            });
        } else {
            player.reset();
        }

        try {
            player.setDataSource(getItemLocalPath(item));
            player.prepareAsync();
        } catch (IOException e) {
            Logger.e(TAG, e);
        }
    }

    public void stopPlayer() {
        if (playingItem != null) {
            playingItem.setItemStatus(ResourceItem.STOP);
            playingItem = null;
        }

        if (player != null) {
            player.stop();
            player.release();
            player = null;

            //停止
            mHandler.sendEmptyMessage(STOP);
        }
    }

    private String getItemLocalPath(ResourceItem item) {
        int cutPosition = item.getSource().lastIndexOf("/") + 1;

        Logger.i(TAG, "存放乐药下载地址-->" + Constant.YUEYAO_PATH + "/" + item.getSource().substring(cutPosition));

        return Constant.YUEYAO_PATH + "/" + item.getSource().substring(cutPosition);
    }


    public void setCartItems(List<ResourceItem> cartItems) {
        this.cartItems = cartItems;
    }

    public double getPrice() {
        double price = 0;
        if (cartItems != null) {
            for (ResourceItem item : cartItems) {
                price += item.getPrice();
            }
        }

        Log.i("Log.i","cartItems个数--->"+cartItems.size());

        return price;
    }

    public void addDownloadingItem(ResourceItem item) {
        if (downloadingItems == null) {
            downloadingItems = new HashMap<>();
        }
        this.downloadingItems.put(item.getId(), item);
    }

    private void removeDownloadingItem(ResourceItem item) {
        if (downloadingItems != null) {
            downloadingItems.remove(item.getId());
        }
    }

    public int getDownLoadingItemSize(){
        if(downloadingItems!= null){
            return downloadingItems.size();
        }
        return 0;
    }
}
