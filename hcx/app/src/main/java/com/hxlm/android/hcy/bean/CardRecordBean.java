package com.hxlm.android.hcy.bean;

import java.util.List;

/**
 * @author Lirong
 * @date 2018/12/20.
 * @description  消费记录bean
 */

public class CardRecordBean {
        /**
         * id : 2
         * member : 38
         * cardId : 1121212
         * cardNum : NGKHY99QMOKMOTMA7ZV
         * serviceName : 接口服务
         * unit : 1
         * value : 1
         * content : 内容测试
         * afterValue : 1
         * leftValue : 1
         * createTime : 2016-02-25 14:03:16
         * updateTime :2016-02-25 14:03:16
         */

        private String id;
        private String member;
        private String cardId;
        private String cardNum;
        private String serviceName;
        private String unit;
        private String value;
        private String content;
        private String afterValue;
        private String leftValue;
        private String createTime;
        private String updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAfterValue() {
        return afterValue;
    }

    public void setAfterValue(String afterValue) {
        this.afterValue = afterValue;
    }

    public String getLeftValue() {
        return leftValue;
    }

    public void setLeftValue(String leftValue) {
        this.leftValue = leftValue;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "CardRecordBean{" +
                "id='" + id + '\'' +
                ", member='" + member + '\'' +
                ", cardId='" + cardId + '\'' +
                ", cardNum='" + cardNum + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", unit='" + unit + '\'' +
                ", value='" + value + '\'' +
                ", content='" + content + '\'' +
                ", afterValue='" + afterValue + '\'' +
                ", leftValue='" + leftValue + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
