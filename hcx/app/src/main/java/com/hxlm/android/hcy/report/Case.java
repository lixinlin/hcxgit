package com.hxlm.android.hcy.report;

import java.util.Date;

/**
 * Created by lixinlin on 2018/12/14.
 */

public class Case {
    private String medicRecordId;
    private String mainSuit;
    private String doctorDept;
    private String doctorName;
    private Date createTime;

    public Case() {
    }

    public String getMedicRecordId() {
        return medicRecordId;
    }

    public void setMedicRecordId(String medicRecordId) {
        this.medicRecordId = medicRecordId;
    }

    public String getMainSuit() {
        return mainSuit;
    }

    public void setMainSuit(String mainSuit) {
        this.mainSuit = mainSuit;
    }

    public String getDoctorDept() {
        return doctorDept;
    }

    public void setDoctorDept(String doctorDept) {
        this.doctorDept = doctorDept;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
