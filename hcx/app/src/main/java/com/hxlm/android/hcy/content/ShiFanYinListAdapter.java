package com.hxlm.android.hcy.content;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.content.ResourceItem;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * 示范音的列表的基本功能
 */
public class ShiFanYinListAdapter extends BaseAdapter {
    private final LayoutInflater mInflater;
    private List<ResourceItem> mDatas;

    public ShiFanYinListAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
    }

    public void setDatas(List<ResourceItem> datas) {
        this.mDatas = datas;
    }

    @Override
    public int getCount() {
        if (mDatas == null) {
            return 0;
        } else {
            return mDatas.size();
        }
    }

    @Override
    public ResourceItem getItem(int position) {
        if (mDatas == null || mDatas.size() < position) {
            return null;
        }
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mDatas.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.activity_shifanyin_list_item, parent, false);

            holder = new ViewHolder();

            holder.quMingText = (TextView) convertView.findViewById(R.id.my_quming_text);
            holder.goumaishijian = (TextView) convertView.findViewById(R.id.my_goumai_shijian);
//            holder.goumaiText = (TextView) convertView.findViewById(R.id.my_goumai_text);
            holder.zhuangTaiView = (ImageView) convertView.findViewById(R.id.my_zhuangtai_view);
            holder.progressbar = (ProgressBar) convertView.findViewById(R.id.my_progressbar);
            holder.layout = (LinearLayout) convertView.findViewById(R.id.my_progress_bar_layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ResourceItem item = getItem(position);
        if (item == null) {
            return convertView;
        }

        holder.quMingText.setText(item.getName());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String dateString = dateFormat.format(item.getCreateDate());
        holder.goumaishijian.setText(dateString);

        switch (item.getItemStatus()) {
            case ResourceItem.NOT_DOWNLOAD:
                holder.zhuangTaiView.setImageResource(R.drawable.yueyao_xiazai);
                break;
            case ResourceItem.DOWNLOADING:
                holder.zhuangTaiView.setVisibility(View.GONE);
                holder.layout.setVisibility(View.VISIBLE);
                holder.progressbar.setVisibility(View.VISIBLE);
                holder.progressbar.setMax(item.getTotalSize());
                holder.progressbar.setProgress(item.getDownloadedSize());
                break;
            case ResourceItem.STOP:
                holder.layout.setVisibility(View.GONE);
                holder.zhuangTaiView.setVisibility(View.VISIBLE);
                holder.zhuangTaiView.setImageResource(R.drawable.card_checked);
                break;
            default:
                break;
        }

        return convertView;
    }

    private class ViewHolder {
        public TextView quMingText;
        public TextView goumaishijian;
        public ImageView zhuangTaiView;
        public ProgressBar progressbar;
        public LinearLayout layout;
    }
}