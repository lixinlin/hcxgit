package com.hxlm.android.hcy.user;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.AbstractHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.utils.SpUtils;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.MessageType;
import com.hxlm.hcyandroid.bean.HealthInformation;
import com.hxlm.hcyandroid.bean.HealthInformationCategory;
import com.hxlm.hcyandroid.util.AsyncHttpClientUtil;
import com.hxlm.hcyandroid.util.Md5Util;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyphone.event.EventUserLogin;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import net.sourceforge.simcpux.Constants;

import org.apache.http.Header;
import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 用户管理类
 * <p>
 * Created by Zhenyu on 2016/4/18.
 */
public class UserManager {
    /**
     * 登录接口
     *
     * @param userName 用户名
     * @param password 密码
     */
    public void login(final String userName, final String password, final AbstractHttpHandlerCallback callback) {
        LoginControllor.clearLastLoginInfo();

        String url = "/login/commit.jhtml";

        RequestParams params = new RequestParams();
        params.put("username", userName);
        params.put("password", password);

        HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            public Object contentParse(String content) {

                JSONObject data = JSON.parseObject(content);
                Member aMember = JSON.parseObject(data.getString("member"), Member.class);
                LoginControllor.setLoginMemberToken(data.getString("token"));
                LoginControllor.setLoginMemberJSESSIONID(data.getString("JSESSIONID"));
                LoginControllor.setLoginMember(aMember);
                List<ChildMember> mengberchild = aMember.getMengberchild();
                if(mengberchild != null){
                    LoginControllor.setChoosedChildMember(mengberchild.get(0));
                    for(ChildMember cm:mengberchild){
                        if(cm.getMobile().equals(aMember.getUserName())){
                            LoginControllor.setChoosedChildMember(cm);
                            break;
                        }
                    }
                }
                return null;
            }
        });
    }

    /**
     * 短信登录接口
     *
     * @param phone 用户名
     * @param code 密码
     */
    public void loginByDuanXin(final String phone, final String code, final AbstractHttpHandlerCallback callback) {

        String url = "/weiq/sms/login.jhtml";

        RequestParams params = new RequestParams();
        params.put("phone", phone);
        params.put("code", code);

        HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            public Object contentParse(String content) {
                JSONObject data = JSON.parseObject(content);
                Member aMember = JSON.parseObject(data.getString("member"), Member.class);
                LoginControllor.setLoginMemberToken(data.getString("token"));
                LoginControllor.setLoginMemberJSESSIONID(data.getString("JSESSIONID"));
                String token = data.getString("token");
                String JSESSIONID = data.getString("JSESSIONID");
                LoginControllor.setLoginMember(aMember);
                List<ChildMember> members = aMember.getMengberchild();
                LoginControllor.setChoosedChildMember(members.get(0));
                for (ChildMember member : members) {
                    if ((aMember.getUserName()).equals(member.getMobile())) {
                        LoginControllor.setChoosedChildMember(member);
                        break;
                    }
                }
                EventBus.getDefault().post(new EventUserLogin());
                return null;
            }
        });
    }


    /**
     * 短信免密登录接口
     *
     * @param phone 用户名
     * @param timestamp 时间

     */
    public void loginByDuanXinAuto(final String phone, Long timestamp, final AbstractHttpHandlerCallback callback) {

        String url = "/weiq/sms/relogin.jhtml";

        RequestParams params = new RequestParams();
        params.put("phone", phone);
        params.put("timestamp", timestamp);
        String s = Md5Util.stringMD5(phone+timestamp+"ky3h.com");
        params.put("token", s);


        HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            public Object contentParse(String content) {
                JSONObject data = JSON.parseObject(content);
                Member aMember = JSON.parseObject(data.getString("member"), Member.class);
                LoginControllor.setLoginMemberToken(data.getString("token"));
                LoginControllor.setLoginMemberJSESSIONID(data.getString("JSESSIONID"));
                String token = data.getString("token");
                String JSESSIONID = data.getString("JSESSIONID");
                LoginControllor.setLoginMember(aMember);
                List<ChildMember> members = aMember.getMengberchild();
                LoginControllor.setChoosedChildMember(members.get(0));
                for (ChildMember member : members) {
                    if ((aMember.getUserName()).equals(member.getMobile())) {
                        LoginControllor.setChoosedChildMember(member);
                        break;
                    }
                }
                EventBus.getDefault().post(new EventUserLogin());
                return null;
            }
        });
    }



    /**
     * 获取验证码
     *
     * @param userName 用户名
     */
    void getCaptcha(final String userName, final AbstractHttpHandlerCallback callback) {
        String url = "/register/captcha.jhtml";
        RequestParams params = new RequestParams();
        params.put("username", userName);
        params.put("UserPhoneKey", Md5Util.stringMD5(userName));

        HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                return content;
            }
        });
    }

    /**
     * 忘记密码后获取验证码的接口
     *
     * @param phone 用户名
     */
    public void getCaptchaAgain(String phone, final AbstractHttpHandlerCallback callback) {
        String url = "/weiq/sms/getsmsCode.jhtml";
        RequestParams params = new RequestParams();
        params.put("phone", phone);
        params.put("token", Md5Util.stringMD5(phone+"ky3h.com"));

        HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                Log.d("duanxin", "contentParse: "+content);
//                ToastUtil.invokeLongTimeToast(BaseApplication.instance,content);
                return content;
            }
        });
    }

    /**
     * 注册接口
     *
     * @param userName        用户名
     * @param password        密码
     * @param confirmPassword 确认密码
     * @param captcha         验证码
     */
    void regist(final String userName, final String password, String confirmPassword,
                String captcha, String cashcode, final AbstractHttpHandlerCallback callback) {
        String url = "/register/commit.jhtml?cashcode=" + cashcode;

        RequestParams params = new RequestParams();
        params.put("username", userName);
        params.put("password", password);
        params.put("password2", confirmPassword);
        params.put("code", captcha);

        HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                return content;
            }
        });
    }

    /**
     * 微信方式登录注册接口
     *
     */
    public void loginByWeiXin(
            String unionid,
            String screen_name,
            String gender,
            String profile_image_url,
            final AbstractHttpHandlerCallback callback) {
        String url = "/weiq/weiq/weix/authlogin.jhtml";
        RequestParams params = new RequestParams();
        params.put("unionid", unionid);
        params.put("screen_name", screen_name);
        params.put("gender", gender);
        params.put("profile_image_url", profile_image_url);

        HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                try {
                    JSONObject data = JSON.parseObject(content);
                    Member aMember = JSON.parseObject(data.getString("member"), Member.class);
                    LoginControllor.setLoginMemberToken(data.getString("token"));
                    LoginControllor.setLoginMemberJSESSIONID(data.getString("JSESSIONID"));
                    LoginControllor.setLoginMember(aMember);
                    List<ChildMember> mengberchild = aMember.getMengberchild();
                    if(mengberchild != null&&mengberchild.size()!=0){
                        LoginControllor.setChoosedChildMember(mengberchild.get(0));
                        for(ChildMember cm:mengberchild){
                            if(cm.getName().equals(aMember.getUserName())){
                                LoginControllor.setChoosedChildMember(cm);
                                break;
                            }
                        }
                    }
                    LoginControllor.setWeixinNickName(screen_name);
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        });
    }


    /**
     * 忘记密码接口
     *
     * @param userName    用户名
     * @param code        验证码
     * @param newPassword 新密码
     */
    void createNewPassword(final String userName, String code, final String newPassword,
                           final AbstractHttpHandlerCallback callback) {
        String url = "/password/resetPassword.jhtml";
        RequestParams params = new RequestParams();
        params.put("username", userName);
        params.put("code", code);
        params.put("newPassword", newPassword);

        HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                return content;
            }
        });
    }


    /**
     * 修改密码接口
     *
     * @param oldPassword     旧密码
     * @param newPassword     新密码
     * @param confirmPassword 确认密码
     */
    public void modifyPassword(String oldPassword, String newPassword, String confirmPassword,
                               AbstractDefaultHttpHandlerCallback callback) {

        String url = "/member/memberModifi/reset.jhtml";

        RequestParams params = new RequestParams();
        params.put("password", oldPassword);
        params.put("newPassword1", newPassword);
        params.put("newPassword2", confirmPassword);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            public Object contentParse(String content) {
                return content;
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

    /**
     * 添加家庭成员的接口
     *
     * @param name       姓名
     * @param sex        性别
     * @param birthday   生日
     * @param IDCard     身份证号
     * @param isMedicare 有无医疗保险
     * @param mobile     手机号
     */
    public void addMemberChild(String name, String sex, String birthday, String IDCard, String isMedicare,
                               String mobile, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/memberModifi/save.jhtml";

        RequestParams params = new RequestParams();

        params.put("name", name);
        params.put("gender", sex);
        params.put("birthday", birthday);
        params.put("IDCard", IDCard);
        params.put("isMedicare", isMedicare);
        params.put("mobile", mobile);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            public Object contentParse(String content) {
                List<ChildMember> mengberchilds = JSON.parseArray(content, ChildMember.class);
                return mengberchilds;
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

    /**
     * 删除家庭成员的接口
     *
     * @param memberChildId 家庭成员id
     */
    public void deleteMemberChild(final int memberChildId, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/memberModifi/delete.jhtml";

        RequestParams params = new RequestParams();
        params.put("id", memberChildId);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            public Object contentParse(String content) {
                List<ChildMember> childMembers = LoginControllor.getLoginMember().getMengberchild();
                for (int i = 0; i < childMembers.size(); i++) {
                    if (childMembers.get(i).getId() == memberChildId) {
                        childMembers.remove(i);
                    }
                }
                return content;
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

    /**
     * 修改家庭成员信息
     *
     * @param memberChildId 家庭成员id
     * @param name          姓名
     * @param sex           性别
     * @param birthday      生日
     * @param IDCard        身份证号码
     * @param isMedicare    是否有医疗保险
     * @param mobile        手机号码
     */
    public void modifyMemberChild(int memberChildId, String name, String sex, String birthday, String IDCard,
                                  String isMedicare, String mobile, AbstractDefaultHttpHandlerCallback callback) {

        String url = "/member/memberModifi/update.jhtml";

        RequestParams params = new RequestParams();
        params.put("id", memberChildId);
        params.put("name", name);
        params.put("gender", sex);
        params.put("birthday", birthday);
        params.put("IDCard", IDCard);
        params.put("isMedicare", isMedicare);
        params.put("mobile", mobile);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            public Object contentParse(String content) {
                return JSON.parseArray(content, ChildMember.class);
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }


    /**
     * 修改主账号信息
     *
     * @param memberImage  会员头像
     * @param gender       性别
     * @param name         姓名
     * @param mobile       手机号码
     * @param phone        电话号码
     * @param nation       民族
     * @param address      地址
     * @param isMarried    婚否
     * @param identityType 证件类型
     * @param idNumber     证件号码
     */
    public void updateMemberInfo(String memberImage, String gender,
                                 String name, String mobile, String phone, String nation,
                                 String address, Integer isMarried, String identityType,
                                 String idNumber, String birthday, AbstractDefaultHttpHandlerCallback callback) {

        String url = "/member/update.jhtml";

        RequestParams params = new RequestParams();
        params.put("memberImage", memberImage);
        params.put("gender", gender);
        params.put("name", name);
        params.put("mobile", mobile);
        params.put("phone", phone);
        params.put("nation", nation);
        params.put("address", address);

        if (isMarried != null && isMarried == 1) {
            params.put("isMarried", true);
        } else if (isMarried != null && isMarried == 0) {
            params.put("isMarried", false);
        }
        params.put("identityType", identityType);
        params.put("idNumber", idNumber);
        params.put("birthday", birthday);
        //Log.i("Log.i","params-->"+params);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {

                return JSON.parseObject(content, Member.class);
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

    /**
     * 完善信息
     */
    public void prefectInfo(String nickName, String infoName, String gender, String infoBirth, String cardId, AbstractDefaultHttpHandlerCallback callback) {

        String url = "/member/update.jhtml";

        RequestParams params = new RequestParams();
        params.put("memberImage", "");
        params.put("gender", gender);
        params.put("name", infoName);
        params.put("mobile", "");
        params.put("phone", "");
        params.put("nation", "");
        params.put("address", "");

//        if (isMarried != null && isMarried == 1) {
//            params.put("isMarried", true);
//        } else if (isMarried != null && isMarried == 0) {
        params.put("isMarried", false);
//        }
        params.put("identityType", "");
        params.put("idNumber", cardId);
        params.put("birthday", infoBirth);
        //Log.i("Log.i","params-->"+params);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {

                return JSON.parseObject(content, Member.class);
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

    /**
     * 判断用户是否登陆超时
     */
    public void checkLoginStatus(AbstractDefaultHttpHandlerCallback callback) {
        String url = "/login/logincheck.jhtml";

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, new RequestParams(), new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                if(!TextUtils.isEmpty(content)&&"true".equals(content)){
                    return true;
                }
                return false;
            }
        }, LoginControllor.MEMBER, callback.getContext());

    }

    // 计算年龄
    public int getAge(ChildMember childMember) {
        int age = 0;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        String str_birthday = childMember.getBirthday();

        try {
            if (!TextUtils.isEmpty(str_birthday)) {
                String[] strs_birthday;
                if (str_birthday.contains(".")) {
                    strs_birthday = str_birthday.split("\\.");
                } else {
                    strs_birthday = str_birthday.split("-");
                }
                int birthday_year = Integer.parseInt(strs_birthday[0]);
                int birthday_month = Integer.parseInt(strs_birthday[1]);
                int birthday_day = Integer.parseInt(strs_birthday[2]);

                long currentTime = System.currentTimeMillis();
                Date date_today = new Date(currentTime);
                String str_today = format.format(date_today);
                String[] strs_today = str_today.split("-");
                int today_year = Integer.parseInt(strs_today[0]);
                int today_month = Integer.parseInt(strs_today[1]);
                int today_day = Integer.parseInt(strs_today[2]);

                if (today_month > birthday_month) {

                    age = today_year - birthday_year;

                } else if (today_month < birthday_month) {

                    age = today_year - birthday_year - 1;

                } else if (today_month == birthday_month) {

                    if (today_day < birthday_day) {
                        age = today_year - birthday_year - 1;
                    } else {
                        age = today_year - birthday_year;
                    }
                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
//            生日不是日期格式
        }
        return age;
    }



    //意见反馈
    public void submitFeedback(String content, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member_suggest/commit.jhtml";
        RequestParams params = new RequestParams();
        params.put("content", content);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            public Object contentParse(String content) {
                return content;
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

    public void getHealthInformationCategory(AbstractDefaultHttpHandlerCallback callback){
        String url = "/article/healthCategoryList.jhtml";
        RequestParams params = new RequestParams();
        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                List<HealthInformationCategory> categories = new ArrayList<HealthInformationCategory>();
                try {
                    categories =  JSON.parseArray(content, HealthInformationCategory.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return categories;
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

    public void getNewInformation2(Context context,int pageNumber, AbstractDefaultHttpHandlerCallback callback){
        String url = "/article/healthArticleList.jhtml?pageNumber="+pageNumber;
         RequestParams params = new RequestParams();
        LoginControllor.requestLogin(context, new OnCompleteListener() {
            @Override
            public void onComplete() {
                HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET,url, params, new HcyHttpResponseHandler(callback) {
                    @Override
                    protected Object contentParse(String content) {
                        List<HealthInformation> informations= new ArrayList<>();
                        try {
//                            JSONObject jo = JSON.parseObject(content);
//
//                                JSONArray data = jo.getJSONArray("content");
//                            String jsonString = data.toJSONString();
                            Log.e("retrofit","==首页推荐阅读=="+content);

                            JSONObject jsonObject = JSON.parseObject(content);
                            String content1 = jsonObject.getString("content");
                            SharedPreferenceTempSave.saveString("informations",content1);
                            SharedPreferenceTempSave.saveLong("info_date",System.currentTimeMillis());
                            int totalPages = jsonObject.getIntValue("totalPages");
                            SpUtils.put(context,"totalPage",totalPages);
                            Log.e("retrofit","==totalPage=="+totalPages);
                            informations = JSON.parseArray(content1, HealthInformation.class);
                            Constants.hasInformations = true;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return informations;
                    }
                }, LoginControllor.MEMBER, callback.getContext());
            }
        });

    }

    public void getNewInformation(final Context context, final Handler handler) {
        String url = "/article/healthArticleList.jhtml";
        // RequestParams params = new RequestParams();
        // params.put("count", 10);
        RequestParams params = new RequestParams();
        AsyncHttpClientUtil.getClient().get(Constant.BASE_URL + url, params,
                new TextHttpResponseHandler() {
                    @Override
                    public void onSuccess(int arg0, Header[] arg1,
                                          String response) {

                        List<HealthInformation> informations = new ArrayList<HealthInformation>();
                        com.alibaba.fastjson.JSONObject jo = JSON.parseObject(response);
                        int status = jo.getInteger("status");
                        if (status == Constant.API_SUCCESS_CODE) {
                            try {
                                com.alibaba.fastjson.JSONArray data = jo.getJSONArray("data");
                                String jsonString = data.toJSONString();
                                informations = JSON.parseArray(jsonString, HealthInformation.class);
                                SharedPreferenceTempSave.saveString("informations",jsonString);
                                Constants.hasInformations = true;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (status == Constant.API_UNLOGIN) {
                        } else {
                        }
                        Message message = Message.obtain();
                        message.what = MessageType.MESSAGE_GET_NEW_INFORMATION;
                        message.arg1 = status;
                        message.obj = informations;
                        handler.sendMessage(message);
                    }

                    @Override
                    public void onFailure(int arg0, Header[] arg1, String arg2,
                                          Throwable arg3) {

                    }
                });

    }

    public void bindPhoneNumber(Context context,String phone,String unionid,String smscode,AbstractDefaultHttpHandlerCallback callback){
        String url = "/weiq/setUserInfo.jhtml";
        RequestParams params = new RequestParams();
        params.put("phone",phone);
        params.put("unionid",unionid);
        params.put("vftcode",smscode);
        params.put("name","");
        params.put("idCard","");
        Log.d("bindPhoneNumber", "bindPhoneNumber: "+params.toString());
        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_POST,url, params, new HcyHttpResponseHandler(callback) {
                    @Override
                    protected Object contentParse(String content) {
                        if("true".equals(content)){
                            return true;
                        }else {
                            return false;
                        }
                    }
                }, LoginControllor.MEMBER, callback.getContext());


    }
}
