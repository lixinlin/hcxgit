package com.hxlm.android.hcy.user;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.hcy.ky3h.R;
import com.hcy_futejia.activity.EnglishLoginActivity;
import com.hcy_futejia.activity.FtjLoginActivity;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.util.IsMobile;
import com.hxlm.hcyphone.MainActivity;

import java.util.Map;

public class SplashActivity extends BaseActivity {
    private boolean login_in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        autoLogin();
    }

    @Override
    public void setContentView() {

    }

    @Override
    public void initViews() {

    }

    @Override
    public void initDatas() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        autoLogin();
    }

    @Override
    protected void onPause() {
        super.onPause();
        login_in = false;
    }

    private void autoLogin() {
        if(login_in){
            return ;
        }
        login_in = true;
        boolean isAutologinBySms = LoginControllor.isIsAutologinBySms();
        boolean isAutologinByWeiChat = LoginControllor.isIsAutologinByWeiChat();
        boolean isAutologinByMiMa = LoginControllor.isIsAutologinByMiMa();
        String phone = LoginControllor.getPhone();
        if(isAutologinBySms){
            if(!TextUtils.isEmpty(phone)&& IsMobile.isMobileNO(phone)){
                long l = System.currentTimeMillis();
                new UserManager().loginByDuanXinAuto(phone, l, new AbstractDefaultHttpHandlerCallback(this) {
                    @Override
                    protected void onResponseSuccess(Object obj) {

                        //跳转主界面
                        toMainActivity();
                    }
                    @Override
                    protected void onResponseError(int errorCode, String errorDesc) {
                        super.onResponseError(errorCode, errorDesc);
//                        服务器相应失败，清除本地缓存，不再自动登录
                        LoginControllor.logout();
                        toLoginActivity();
                    }
                });
            }
        }else if(isAutologinByWeiChat){
            Map<String, String> weiChatInfo = LoginControllor.getWeiChatInfo();
            new UserManager().loginByWeiXin(
                    weiChatInfo.get("unionid")
                    , weiChatInfo.get("screen_name")
                    , weiChatInfo.get("gender")
                    , weiChatInfo.get("profile_image_url"), new AbstractDefaultHttpHandlerCallback(this) {
                        @Override
                        protected void onResponseSuccess(Object obj) {
                            boolean success = (boolean) obj;
                            if(success){
                                //跳转主界面
                                toMainActivity();
                            }
                        }
                        @Override
                        protected void onResponseError(int errorCode, String errorDesc) {
                            super.onResponseError(errorCode, errorDesc);
//                        服务器相应失败，清除本地缓存，不再自动登录
                            LoginControllor.logout();
                            toLoginActivity();
                        }
                    });

        }else if(isAutologinByMiMa){
            Map<String, String> miMaUserInfo = LoginControllor.getMiMaUserInfo();
            new UserManager().login(miMaUserInfo.get("mobile"), miMaUserInfo.get("password"), new AbstractDefaultHttpHandlerCallback(this) {
                @Override
                protected void onResponseSuccess(Object obj) {
                    //跳转主界面
                    toMainActivity();
                }
                @Override
                protected void onResponseError(int errorCode, String errorDesc) {
                    super.onResponseError(errorCode, errorDesc);
//                        服务器相应失败，清除本地缓存，不再自动登录
                    LoginControllor.logout();
                    toLoginActivity();
                }
            });
        }else{
            toLoginActivity();
        }
    }

    private void toMainActivity() {
        Intent intentMain = new Intent(this, MainActivity.class);
        startActivity(intentMain);
        this.finish();
    }

    private void toLoginActivity() {
//        Intent intentMain = new Intent(this, LoginActivity.class);
        if (Constant.isEnglish){
            Intent intentMain = new Intent(this, EnglishLoginActivity.class);
            startActivity(intentMain);
            this.finish();
        }else {
            Intent intentMain = new Intent(this, FtjLoginActivity.class);
            startActivity(intentMain);
            this.finish();
        }
    }
}
