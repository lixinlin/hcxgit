package com.hxlm.android.hcy.utils;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * view的工具类
 * Created by Zhenyu on 2016/4/21.
 */
public class ViewUtil {
    // 实现ListView在Scrollview中同时滑动
    public static void setListHeight(ListView listView) {
        ListAdapter list = listView.getAdapter();
        if (list == null) {
            return;
        }
        int height = 0;
        for (int i = 0; i < list.getCount(); i++) {
            View listItem = list.getView(i, null, listView);
            listItem.measure(0, 0); // 计算子项View 的宽高
            height += listItem.getMeasuredHeight(); // 统计所有子项的总高度
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = height + (listView.getDividerHeight() * (list.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
