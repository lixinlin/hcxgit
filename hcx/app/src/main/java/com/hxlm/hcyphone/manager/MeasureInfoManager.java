package com.hxlm.hcyphone.manager;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.hcyandroid.bean.BloodSugar;
import com.hxlm.hcyandroid.bean.ECGReport;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.hxlm.hcyphone.bean.HealthMonitorSuggest;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MeasureInfoManager {
    /**
     * 获取体征监测建议
     * @param memberChildId
     * @param callback
     */
    public void getMontiorSuggest(int memberChildId, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/myreport/view/" + memberChildId + ".jhtml?";
        RequestParams params = new RequestParams();
        params.put("memberChildId", memberChildId);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {

            @Override
            public Object contentParse(String content) {
                List<HealthMonitorSuggest> datas = new ArrayList<>();

                JSONObject data = JSON.parseObject(content);

                JSONObject jo_SAS = data.getJSONObject("SAS20");
                if (jo_SAS != null) {
                    HealthMonitorSuggest XLBS = JSON.parseObject(jo_SAS.toJSONString(), HealthMonitorSuggest.class);
                    datas.add(new HealthMonitorSuggest());
                }

                return datas;
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

    /**
     * 获得体检建议
     * @param memberChildId
     * @param callback
     */
    public void getDetectionSuggest(int memberChildId, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/myreport/view/" + memberChildId + ".jhtml?";
        RequestParams params = new RequestParams();
        params.put("memberChildId", memberChildId);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {

            @Override
            public Object contentParse(String content) {
                List<HealthMonitorSuggest> datas = new ArrayList<>();

                JSONObject data = JSON.parseObject(content);

                JSONObject jo_SAS = data.getJSONObject("SAS20");
                if (jo_SAS != null) {
                    HealthMonitorSuggest XLBS = JSON.parseObject(jo_SAS.toJSONString(), HealthMonitorSuggest.class);
                    datas.add(new HealthMonitorSuggest());
                }

                return datas;
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }


    /**
     * 获取心电报告的接口
     *
     * @param memberChildId 子账号id
     */
    public void getECGReport(int memberChildId, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/myreport/getEcgList/" + memberChildId + ".jhtml";

        RequestParams params = new RequestParams();

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                JSONObject jsonObject = JSON.parseObject(content);
                if (jsonObject != null) {
                    return JSON.parseArray(jsonObject.getString("content"), ECGReport.class);
                }
                return null;
            }
        }, LoginControllor.CHILD_MEMBER, callback.getContext());
    }

    /**
     * 获取血糖数据报告列表
     */
    public void getBloodSugarReport(AbstractDefaultHttpHandlerCallback callback) {
        String url = "/subject_report/findDate.jhtml";

        String memberChildId = SharedPreferenceUtil.getCurrnetMemberId();
        RequestParams params = new RequestParams();
        params.put("mcId", memberChildId);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                Map<String, List<BloodSugar>> map = new HashMap<>();
                List<BloodSugar> emptyData = new ArrayList<>();
                List<BloodSugar> fullData = new ArrayList<>();

                JSONObject data = JSON.parseObject(content);

                JSONArray empty = data.getJSONArray("type=empty");
                for (int i = 0; i < empty.size(); i++) {
                    JSONObject item = empty.getJSONObject(i);
                    emptyData.add(new BloodSugar(item.getInteger("id"),
                            item.getLong("createDate"), item
                            .getLong("modifyDate"), item
                            .getString("type"), item
                            .getDouble("levels"), item
                            .getBoolean("isAbnormity")));
                }

                JSONArray full = data.getJSONArray("type=full");
                for (int i = 0; i < full.size(); i++) {
                    JSONObject item = full.getJSONObject(i);
                    fullData.add(new BloodSugar(item.getInteger("id"), item
                            .getLong("createDate"), item
                            .getLong("modifyDate"), item.getString("type"),
                            item.getDouble("levels"), item
                            .getBoolean("isAbnormity")));
                }
                map.put("empty", emptyData);
                map.put("full", fullData);
                return map;
            }
        }, LoginControllor.MEMBER, callback.getContext());

    }





}
