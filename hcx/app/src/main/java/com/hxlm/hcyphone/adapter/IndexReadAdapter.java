package com.hxlm.hcyphone.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.utils.SpUtils;
import com.hxlm.hcyandroid.bean.HealthInformation;
import com.hxlm.hcyphone.utils.GlideApp;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lirong
 * @date 2019/1/2.
 * @description
 */

public class IndexReadAdapter extends RecyclerView.Adapter<IndexReadAdapter.IndexReadViewHolder> {

    private Context context ;
    private List<HealthInformation> list;

    public IndexReadAdapter(Context context,List<HealthInformation> list) {
        this.context = context;
        this.list = list;
    }


    @NonNull
    @Override
    public IndexReadViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.activity_player_recommend_read_item, parent, false);
        return new IndexReadViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull IndexReadViewHolder holder, int position) {
        HealthInformation healthInformation = list.get(position);
        holder.tv_recommend_reason.setText(healthInformation.getTitle());
        if (context != null) {
            GlideApp.with(context).load(healthInformation.getPicture()).into(holder.iv_image);
        }
        //点击监听
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onItem(holder.getPosition(),list.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    private OnItemClick onItemClick;

    public void setOnItemClick(OnItemClick onItemClick) {
        this.onItemClick = onItemClick;
    }

    public interface OnItemClick{
        void onItem(int position,HealthInformation healthInformation);
    }

    class IndexReadViewHolder extends RecyclerView.ViewHolder{

        private final ImageView iv_image;
        private final TextView tv_recommend_reason;

        public IndexReadViewHolder(View itemView) {
            super(itemView);
            iv_image = itemView.findViewById(R.id.iv_image);
            tv_recommend_reason = itemView.findViewById(R.id.tv_recommend_reason);
        }
    }

}
