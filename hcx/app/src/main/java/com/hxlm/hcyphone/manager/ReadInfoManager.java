package com.hxlm.hcyphone.manager;

import android.content.Context;

import com.alibaba.fastjson.JSON;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.SharedPreferenceTempSave;
import com.hxlm.hcyandroid.bean.HealthInformation;
import com.hxlm.hcyandroid.bean.HealthInformationCategory;
import com.loopj.android.http.RequestParams;

import net.sourceforge.simcpux.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lixinlin on 2018/12/31.
 */

public class ReadInfoManager  {


    public void getHealthInformationCategory(AbstractDefaultHttpHandlerCallback callback){
        String url = "/article/healthCategoryList.jhtml";
        RequestParams params = new RequestParams();
        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                List<HealthInformationCategory> categories = new ArrayList<HealthInformationCategory>();
                try {
                    categories =  JSON.parseArray(content, HealthInformationCategory.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return categories;
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

    public void getNewInformation(Context context, AbstractDefaultHttpHandlerCallback callback){
        String url = "/article/healthArticleList.jhtml";
        RequestParams params = new RequestParams();
        LoginControllor.requestLogin(context, new OnCompleteListener() {
            @Override
            public void onComplete() {
                HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
                    @Override
                    protected Object contentParse(String content) {
                        List<HealthInformation> informations= new ArrayList<>();
                        try {
//                            JSONObject jo = JSON.parseObject(content);
//
//                                JSONArray data = jo.getJSONArray("content");
//                            String jsonString = data.toJSONString();
                            SharedPreferenceTempSave.saveString("informations",content);
                            informations = JSON.parseArray(content, HealthInformation.class);
                            Constants.hasInformations = true;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return informations;
                    }
                }, LoginControllor.MEMBER, callback.getContext());
            }
        });

    }

    public void getNewInformationBeforeLogin(AbstractDefaultHttpHandlerCallback callback){
        String url = "/article/healthArticleList.jhtml";
        RequestParams params = new RequestParams();
        HcyHttpClient.submit(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                List<HealthInformation> informations= new ArrayList<>();
                try {
//                            JSONObject jo = JSON.parseObject(content);
//
//                                JSONArray data = jo.getJSONArray("content");
//                            String jsonString = data.toJSONString();
                    SharedPreferenceTempSave.saveString("informations",content);
                    informations = JSON.parseArray(content, HealthInformation.class);
                    Constants.hasInformations = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return informations;
            }
        });



    }



}
