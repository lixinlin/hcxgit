package com.hxlm.hcyphone;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by JQ on 2017/6/21.
 */
public abstract class BaseFragment extends Fragment {
    /** 调用顺序
     initView:
     initStartup:
     initData:
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(getContentViewResource(), container, false);

        initView(view);
        return view;
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initStartup();
        initData();
    }

    /**
     * 吐司提示
     *
     * @param message
     */
    public void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    /**
     * 布局
     *
     * @return
     */
    protected abstract int getContentViewResource();

    protected abstract void initView(View view);
    /**
     * 初始化启动项
     */
    protected abstract void initStartup();

    /**
     * 初始化数据
     */
    protected abstract void initData();
}
