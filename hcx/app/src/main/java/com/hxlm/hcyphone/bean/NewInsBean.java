package com.hxlm.hcyphone.bean;

/**
 * @author Lirong
 * @date 2018/12/28.
 * @description
 */

public class NewInsBean {

        /**
         * num : 2
         * name : 风寒束表证
         * score : 245.0
         */

        private int num;
        private String name;
        private double score;

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getScore() {
            return score;
        }

        public void setScore(double score) {
            this.score = score;
        }

    @Override
    public String toString() {
        return "NewInsBean{" +
                "num=" + num +
                ", name='" + name + '\'' +
                ", score=" + score +
                '}';
    }
}
