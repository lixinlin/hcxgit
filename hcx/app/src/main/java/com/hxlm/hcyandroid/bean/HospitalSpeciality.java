package com.hxlm.hcyandroid.bean;


import java.io.Serializable;

public class HospitalSpeciality implements Serializable {

    private int id;
    private int hospitalId;
    private int specialityId;
    private String name;//科室名称
    private String introduction;//介绍
    private boolean isStrongPoint;
    private boolean isNationalSpeciality;
    private boolean isCooperateSpeciality;


    @Override
    public String toString() {
        return "HospitalSpeciality [id=" + id + ", hospitalId=" + hospitalId
                + ", specialityId=" + specialityId + ", name=" + name
                + ", introduction=" + introduction + ", isStrongPoint="
                + isStrongPoint + ", isNationalSpeciality="
                + isNationalSpeciality + ", isCooperateSpeciality="
                + isCooperateSpeciality + "]";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(int hospitalId) {
        this.hospitalId = hospitalId;
    }

    public int getSpecialityId() {
        return specialityId;
    }

    public void setSpecialityId(int specialityId) {
        this.specialityId = specialityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public boolean isStrongPoint() {
        return isStrongPoint;
    }

    public void setStrongPoint(boolean isStrongPoint) {
        this.isStrongPoint = isStrongPoint;
    }

    public boolean isNationalSpeciality() {
        return isNationalSpeciality;
    }

    public void setNationalSpeciality(boolean isNationalSpeciality) {
        this.isNationalSpeciality = isNationalSpeciality;
    }

    public boolean isCooperateSpeciality() {
        return isCooperateSpeciality;
    }

    public void setCooperateSpeciality(boolean isCooperateSpeciality) {
        this.isCooperateSpeciality = isCooperateSpeciality;
    }


}
