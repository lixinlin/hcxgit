package com.hxlm.hcyandroid.tabbar.home_jczs;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

import com.hcy.ky3h.R;
import com.hcy_futejia.activity.FtjBodyQuestionActivity;
import com.hxlm.android.hcy.AbstractBaseActivity;
import com.hxlm.android.hcy.questionnair.QuestionnaireManager;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;

/**
 * Created by l on 2016/11/6.
 * 体质辨识
 */
public class OneClockActivity extends AbstractBaseActivity implements View.OnClickListener{
    private ImageView img_dian;
    @Override
    protected void setContentView() {
        setContentView(R.layout.layout_one_click);
    }

    @Override
    protected void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.one_clock_title), titleBar, 1);
        img_dian=(ImageView)findViewById(R.id.img_dian);
        img_dian.setOnClickListener(this);
    }

    @Override
    protected void initDatas() {
    }

    @Override
    public void onClick(View view) {
        SharedPreferenceUtil.saveString("oneClick","false");
        Intent intent = new Intent(this, FtjBodyQuestionActivity.class);
        intent.putExtra("title", getString(R.string.home_physical_identification));
        intent.putExtra("categorySn", QuestionnaireManager.SN_TZBS);
        startActivity(intent);
        this.finish();
    }
}