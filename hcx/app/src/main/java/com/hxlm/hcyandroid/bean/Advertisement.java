package com.hxlm.hcyandroid.bean;

/**
 * 广告类
 *
 * @author l
 */
public class Advertisement {

    private int id;
    private long createDate;
    private long modifyDate;
    private int order;//秩序
    private String title;//广告标题
    private String type;//广告类型，分text文本;image图片 两种；
    private String content;//广告文字说明，如果是图片广告的话无内容
    private String path;//图片地址
    private String url;//广告地址

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Advertisement [id=" + id + ", createDate=" + createDate
                + ", modifyDate=" + modifyDate + ", order=" + order
                + ", title=" + title + ", type=" + type + ", content="
                + content + ", path=" + path + ", url=" + url + "]";
    }


}
