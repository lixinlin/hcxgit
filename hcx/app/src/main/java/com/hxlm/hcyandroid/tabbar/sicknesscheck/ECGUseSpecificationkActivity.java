package com.hxlm.hcyandroid.tabbar.sicknesscheck;

import android.widget.ListView;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.bean.CheckStep;

import java.util.ArrayList;
import java.util.List;

/**
 * 心电使用规则
 *
 * @author l
 */
public class ECGUseSpecificationkActivity extends BaseActivity {


    private ListView lv_tips;
    private List<CheckStep> steps;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_ecgchec_use_specificationk);

    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.ecg_check_title), titleBar, 1);
        lv_tips = findViewById(R.id.lv_tips);

    }

    @Override
    public void initDatas() {
        steps = new ArrayList<CheckStep>();
        steps.add(new CheckStep(R.drawable.check_step_number_bg1, "", "请将设备插入仪器口指示位置"));
        steps.add(new CheckStep(R.drawable.check_step_number_bg2, "", "请将设备插入仪器口指示位置"));
        steps.add(new CheckStep(R.drawable.check_step_number_bg3, "", "请将设备插入仪器口指示位置"));
        steps.add(new CheckStep(R.drawable.check_step_number_bg4, "", "请将设备插入仪器口指示位置"));
        steps.add(new CheckStep(R.drawable.check_step_number_bg5, "", "请将设备插入仪器口指示位置"));
        steps.add(new CheckStep(R.drawable.check_step_number_bg6, "", "请将设备插入仪器口指示位置"));
        lv_tips.setAdapter(new StepAdapter(this, steps));

    }

}
