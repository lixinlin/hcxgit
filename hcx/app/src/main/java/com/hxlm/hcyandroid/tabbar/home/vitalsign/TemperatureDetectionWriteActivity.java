package com.hxlm.hcyandroid.tabbar.home.vitalsign;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.report.UploadManager;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.ChooseMemberDialog;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.CheckedDataType;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ContainsEmojiEditText;

import java.util.List;

public class TemperatureDetectionWriteActivity extends BaseActivity implements
        OnClickListener {

    private Context context;
    private ContainsEmojiEditText et_tw;// 体温
    private double wendu;// 当前温度
    private String isNormal;// 是否正常

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_temperature_detection_write);
        context = TemperatureDetectionWriteActivity.this;

    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.tdw_title), titleBar, 1);
        et_tw = (ContainsEmojiEditText) findViewById(R.id.et_tw);
        ImageView iv_save_tiwen = (ImageView) findViewById(R.id.iv_save_tiwen);
        iv_save_tiwen.setOnClickListener(this);
    }

    @Override
    public void initDatas() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_save_tiwen:
                String strtw = et_tw.getText().toString();

                if (TextUtils.isEmpty(strtw)) {
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.tdw_unInput_tiwen));
                } else {

                    wendu = Double.parseDouble(strtw);

                    if(wendu<25||wendu>50)
                    {
                        ToastUtil.invokeShortTimeToast(context, getString(R.string.tdw_input_current_range));
                    }else
                    {
                        if(wendu>=25&&wendu<36) {
                            isNormal = getString(R.string.tdw_isNormal_piandi);
                        }
                        else if (wendu >= 36 && wendu < 37.3) {
                            isNormal = getString(R.string.tdw_isNormal_zhengchang);
                        } else if (wendu >= 37.3 && wendu <= 50) {
                            isNormal = getString(R.string.tdw_isNormal_piangao);
                        }
                        //输入正常值才上传数据
                            LoginControllor.requestLogin(TemperatureDetectionWriteActivity.this, new OnCompleteListener() {
                                @Override
                                public void onComplete() {
                                    List<ChildMember> childMembers = LoginControllor.getLoginMember().getMengberchild();
                                    int size=childMembers.size();
                                    if(true){
                                        // 输入正常值才上传数据
                                        new UploadManager().uploadCheckedData(CheckedDataType.TEMPERATURE, wendu,0,
                                                new AbstractDefaultHttpHandlerCallback(TemperatureDetectionWriteActivity.this) {
                                                    @Override
                                                    protected void onResponseSuccess(Object obj) {
                                                        new TemperatureDialog(context, getString(R.string.tdw_dangqiantiwen) + wendu + "ºC", isNormal).show();
                                                    }
                                                });
                                    }else {
                                        new ChooseMemberDialog(TemperatureDetectionWriteActivity.this, new OnCompleteListener() {
                                            @Override
                                            public void onComplete() {

                                                // 输入正常值才上传数据
                                                new UploadManager().uploadCheckedData(CheckedDataType.TEMPERATURE, wendu,0,
                                                        new AbstractDefaultHttpHandlerCallback(TemperatureDetectionWriteActivity.this) {
                                                            @Override
                                                            protected void onResponseSuccess(Object obj) {
                                                                new TemperatureDialog(context, getString(R.string.tdw_dangqiantiwen) + wendu + "ºC", isNormal).show();
                                                            }
                                                        });

                                            }
                                        }).show();
                                    }

                                }
                            });

                    }
                }
                break;

            default:
                break;
        }
    }

    public class TemperatureDialog extends AlertDialog implements
            OnClickListener {

        Context context;
        String tishi;
        String isnormal;

        String str_tishi = "";

        public TemperatureDialog(Context context, String tishi, String isnormal) {
            super(context);
            this.context = context;
            this.tishi = tishi;
            this.isnormal = isnormal;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.temperaturedetectionwrite_submit_prompt);

            TextView tv_breath_prompt_tishi = (TextView) findViewById(R.id.tv_breath_prompt_tishi);
            tv_breath_prompt_tishi.setText(tishi);

            // 体温
            TextView tv_is_normal = (TextView) findViewById(R.id.tv_is_normal);

            // 确定
            ImageView image_submit = (ImageView) findViewById(R.id.image_submit);
            image_submit.setOnClickListener(this);

            if (getString(R.string.tdw_isNormal_zhengchang).equals(isnormal)) {
                str_tishi = getString(R.string.tdw_dialog_tiwen_zhengchang);
                SpannableStringBuilder style = new SpannableStringBuilder(
                        str_tishi);
                style.setSpan(
                        new ForegroundColorSpan(getResources().getColor(
                                R.color.submit_normal)), 6, 8,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv_is_normal.setText(style);

            } else if (getString(R.string.tdw_isNormal_piangao).equals(isnormal)) {
                str_tishi = getString(R.string.tdw_dialog_tiwen_piangao);
                SpannableStringBuilder style = new SpannableStringBuilder(
                        str_tishi);
                style.setSpan(
                        new ForegroundColorSpan(getResources().getColor(
                                R.color.submit_not_normal)), 3, 5,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv_is_normal.setText(style);

            } else if (getString(R.string.tdw_isNormal_piandi).equals(isnormal)) {
                str_tishi = getString(R.string.tdw_dialog_tiwen_piandi);
                SpannableStringBuilder style = new SpannableStringBuilder(
                        str_tishi);
                style.setSpan(
                        new ForegroundColorSpan(getResources().getColor(
                                R.color.submit_not_normal)), 3, 5,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv_is_normal.setText(style);
            }

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                // 确定
                case R.id.image_submit:
                    this.dismiss();
                    break;

            }
        }

    }

}
