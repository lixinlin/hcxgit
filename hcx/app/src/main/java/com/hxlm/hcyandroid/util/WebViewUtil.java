package com.hxlm.hcyandroid.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.UserManager;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyphone.MainActivity;

import java.util.HashMap;

public class WebViewUtil {
    private final static String TAG = "WebViewUtil";
    private boolean loadError = false;//是否加载失败

    //private Dialog waittingDialog;

    private LinearLayout webParentView;//父类
    private WebView webviewInit;
    private ProgressBar progressBar;
    private View mErrorView;
    private Context context;
    // 用于记录出错页面的url 方便重新加载
    private String mFailingUrl = null;

    // 设置cookie
    public static void syncCookie(Context context) {
        try {
            CookieSyncManager.createInstance(context);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.removeAllCookie();

            String cookieUrl = Constant.PRODUCTION_SYSTEM_COOKIE;
            if (Constant.BASE_URL.equals(Constant.TEST_SYSTEM_URL)) {
                cookieUrl = Constant.TEST_SYSTEM_COOKIE;
            }

            cookieManager.setCookie(cookieUrl, "JSESSIONID=" + HcyHttpClient.getCookie("JSESSIONID"));
            cookieManager.setCookie(cookieUrl, "token=" + HcyHttpClient.getCookie("token"));
            CookieSyncManager.getInstance().sync();

        } catch (Exception e) {
            Logger.e(TAG, e);
        }
    }

    public void setWebViewInit(WebView webView, ProgressBar  aProgressBar,Context context, String strUrl) {
        this.webviewInit = webView;
        this.progressBar=aProgressBar;
        this.context = context;
        new UserManager().checkLoginStatus(new AbstractDefaultHttpHandlerCallback(context) {
            @Override
            protected void onResponseSuccess(Object obj) {
                boolean status = (boolean) obj;
                if(!status){
                    LoginControllor.autoLoginAndChooseChildMember(context, new OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            Log.d(TAG, "onComplete: 未登录状态访问网页");
                        }
                    });
                }

            }
        });
        progressBar.setMax(100);//最大值为100

        setWebView(webView, context);


        if (NetWorkUtils.isNetworkConnected(context)) {
//            webviewInit.loadUrl(strUrl);
            if (Constant.isEnglish) {
                HashMap<String, String> header = new HashMap<>();
                header.put("language", "us-en");
                webviewInit.loadUrl(strUrl, header);
            }else {
                webviewInit.loadUrl(strUrl);
            }
        } else {
            mFailingUrl = strUrl;
            showErrorPage();
        }
    }


    @SuppressLint("SetJavaScriptEnabled")
    public void setWebView(final WebView webView, final Context context) {
        // webView.setWebChromeClient(new WebChromeClient());
        final WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);

        syncCookie(context);

        if (Build.VERSION.SDK_INT >= 19) {
            settings.setLoadsImagesAutomatically(true);
        } else {
            settings.setLoadsImagesAutomatically(false);
        }
//-----------------------------------加--------------------

        webView.setWebChromeClient(new WebChromeClient() {

            //显示进度条
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);

                Logger.i("Log.i", "newProgress-->" + newProgress);
                progressBar.setProgress(newProgress);
                if (newProgress == 100) {
                    progressBar.setVisibility(View.GONE);
                }
            }

            //显示标题
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);

                if (Constant.DEBUG) {
                    Log.i("Log.i", "网页标题title-->" + title);
                }
                //判断标题 title 中是否包含有“error”字段，如果包含“error”字段，则设置加载失败，显示加载失败的视图
                if (!TextUtils.isEmpty(title) && (title.toLowerCase().contains("error") || title.contains("页面不存在") || title.contains("找不到网页"))) {
                    loadError = true;
                }
            }
        });
        //-----------------------------------

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
               if(url.contains("mobileIndex.html")){
                   Intent intent=new Intent(context, MainActivity.class);
                   intent.putExtra("mainId",3);
                   context.startActivity(intent);
               }
//                view.loadUrl(url);
                if (Constant.isEnglish) {
                    HashMap<String, String> header = new HashMap<>();
                    header.put("language", "us-en");
                    view.loadUrl(url, header);
                }else {
                    view.loadUrl(url);
                }
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if(Constant.DEBUG) {
                    Log.i("Log.i", "onPageStarted-->" + url);
                }
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);//显示progressbar
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.GONE);
                if(Constant.DEBUG)
                {
                    Log.i("Log.i", "onPageFinished-->" + url);
                }

                if (loadError == false) {
                    if (webParentView != null && mErrorView != null) {
                        mErrorView.setVisibility(View.GONE);
                        // webParentView.removeView(mErrorView);
                        webviewInit.setVisibility(View.VISIBLE);

                    }
                }

                if (!settings.getLoadsImagesAutomatically()) {
                    settings.setLoadsImagesAutomatically(true);
                }

                super.onPageFinished(view, url);
            }

            //-----------------------------------加--------------------
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                loadError = true;
                mFailingUrl = failingUrl;
                showErrorPage();//显示错误页面
                if (Constant.DEBUG) {
                    Log.i("Log.i", "onReceivedError-->errorCode-->" + errorCode);
                    Log.i("Log.i", "onReceivedError-->mFailingUrl-->" + mFailingUrl);
                }
            }
            //---------------------------------------------
        });
    }

    /**
     * 显示自定义错误提示页面，用一个View覆盖在WebView
     */
    protected void showErrorPage() {
        webParentView = (LinearLayout) webviewInit.getParent();
        initErrorPage();//初始化自定义页面
        while (webParentView.getChildCount() > 1) {
            webParentView.removeViewAt(0);


        }
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        webParentView.addView(mErrorView, 0, lp);
    }

    /***
     * 显示加载失败时自定义的网页
     */
    protected void initErrorPage() {
        if (mErrorView == null) {
            mErrorView = View.inflate(context, R.layout.request_error, null);
            ImageView button = (ImageView) mErrorView.findViewById(R.id.img_request_again);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (NetWorkUtils.isNetworkConnected(context)) {
                        loadError = false;
//                        webviewInit.loadUrl(mFailingUrl);
                        if (Constant.isEnglish) {
                            HashMap<String, String> header = new HashMap<>();
                            header.put("language", "us-en");
                            webviewInit.loadUrl(mFailingUrl, header);
                        }else {
                            webviewInit.loadUrl(mFailingUrl);
                        }
                        //Toast.makeText(context,"reload",Toast.LENGTH_SHORT).show();
                    }
                }
            });
            mErrorView.setOnClickListener(null);
        } else {
            // mErrorView.setVisibility(View.VISIBLE);
            //测试在Activity上没问题，但是Fragment上有些小问题，加上下面的没事了
            ViewParent parent = mErrorView.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup)parent).removeView(mErrorView);
            }

            mErrorView = View.inflate(context, R.layout.request_error, null);
            ImageView button = (ImageView) mErrorView.findViewById(R.id.img_request_again);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (NetWorkUtils.isNetworkConnected(context)) {

                        loadError = false;
//                        webviewInit.loadUrl(mFailingUrl);
                        if (Constant.isEnglish) {
                            HashMap<String, String> header = new HashMap<>();
                            header.put("language", "us-en");
                            webviewInit.loadUrl(mFailingUrl, header);
                        }else {
                            webviewInit.loadUrl(mFailingUrl);
                        }
                        //Toast.makeText(context,"reload",Toast.LENGTH_SHORT).show();
                    }
                }
            });
            mErrorView.setOnClickListener(null);
        }
    }

}
