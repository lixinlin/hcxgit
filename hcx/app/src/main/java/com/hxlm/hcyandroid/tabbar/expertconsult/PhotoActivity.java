package com.hxlm.hcyandroid.tabbar.expertconsult;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.util.Bimp;
import com.hxlm.hcyandroid.util.FileUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 显示点击的大图显示，可以滑动查看所有的
 *
 * @author dell
 */
public class PhotoActivity extends Activity {

    public List<Bitmap> bmp = new ArrayList<>();
    public List<String> drr = new ArrayList<>();
    public List<String> del = new ArrayList<>();
    public int max;
    LinearLayout photo_relativeLayout;
    private ArrayList<View> listViews = null;
    private ViewPager pager;
    private MyPageAdapter adapter;
    private int count;
    private int ScaleAngle = 0;
    private OnPageChangeListener pageChangeListener = new OnPageChangeListener() {

        @Override
        public void onPageSelected(int arg0) {// 页面选择响应函数
            count = arg0;
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {// 滑动中。。。

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {// 滑动状态改变

        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_photo);

        photo_relativeLayout = (LinearLayout) findViewById(R.id.photo_relativeLayout);
        photo_relativeLayout.setBackgroundColor(0x70000000);

        for (int i = 0; i < Bimp.bmp.size(); i++) {
            bmp.add(Bimp.bmp.get(i));
        }
        for (int i = 0; i < Bimp.drr.size(); i++) {
            drr.add(Bimp.drr.get(i));
        }
        max = Bimp.max;

        Button photo_bt_exit = (Button) findViewById(R.id.photo_bt_exit);
        photo_bt_exit.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                finish();
            }
        });
        Button photo_bt_del = (Button) findViewById(R.id.photo_bt_del);
        photo_bt_del.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (listViews.size() == 1) {
                    Bimp.bmp.clear();
                    Bimp.drr.clear();
                    Bimp.max = 0;
                    FileUtils.deleteDir();
                    finish();
                } else {
                    String newStr = drr.get(count).substring(
                            drr.get(count).lastIndexOf("/") + 1,
                            drr.get(count).lastIndexOf("."));
                    bmp.remove(count);
                    drr.remove(count);
                    del.add(newStr);
                    max--;
                    pager.removeAllViews();
                    listViews.remove(count);
                    adapter.setListViews(listViews);
                    adapter.notifyDataSetChanged();
                }
            }
        });
        Button photo_bt_enter = (Button) findViewById(R.id.photo_bt_enter);
        photo_bt_enter.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                Bimp.bmp = bmp;
                Bimp.drr = drr;
                Bimp.max = max;
                for (String aDel : del) {
                    FileUtils.delFile(aDel + ".JPEG");
                }
                finish();
            }
        });

        Button photo_bt_xuan = (Button) findViewById(R.id.photo_bt_xuan);
        photo_bt_xuan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String newStr1 = drr.get(count).substring(
                        drr.get(count).lastIndexOf("/") + 1,
                        drr.get(count).lastIndexOf("."));

                Bitmap mySourceBmp = bmp.get(count);
                int widthOrig = mySourceBmp.getWidth();
                int heightOrig = mySourceBmp.getHeight();

                ScaleAngle = ScaleAngle + 90;
                Matrix matrix = new Matrix();
                /* 使用Matrix.postScale 设置维度 */
                matrix.postScale(1.0f, 1.0f);
                /* 使用Matrix.setRotate 方法旋转Bitmap */
                matrix.setRotate(ScaleAngle);
                /* 创建新的Bitmap对象 */
                Bitmap resizedBitmap = Bitmap.createBitmap(mySourceBmp, 0, 0,
                        widthOrig, heightOrig, matrix, true);

                FileUtils.saveBitmap(resizedBitmap, newStr1);

                ((ImageView) listViews.get(count)).setImageBitmap(resizedBitmap);

                // listViews.clear();
                // for (int i = 0; i < Bimp.bmp.size(); i++) {
                // initListViews(Bimp.bmp.get(i));//
                // }
                //
                // pager.removeAllViews();
                // adapter = new MyPageAdapter(listViews);// 构造adapter
                // pager.setAdapter(adapter);// 设置适配器

            }
        });

        pager = (ViewPager) findViewById(R.id.viewpager);
        pager.setOnPageChangeListener(pageChangeListener);
        for (int i = 0; i < bmp.size(); i++) {
            initListViews(bmp.get(i), i);//
        }

        adapter = new MyPageAdapter(listViews);// 构造adapter
        pager.setAdapter(adapter);// 设置适配器
        Intent intent = getIntent();
        int id = intent.getIntExtra("ID", 0);
        pager.setCurrentItem(id);
    }

    private void initListViews(Bitmap bm, int i) {
        if (listViews == null)
            listViews = new ArrayList<>();
        ImageView img = new ImageView(this);
        img.setBackgroundColor(0xff000000);
        img.setImageBitmap(bm);
        img.setId(i);
        img.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));
        listViews.add(img);// 添加view
    }

    class MyPageAdapter extends PagerAdapter {

        private ArrayList<View> listViews;// content

        private int size;// 页数

        public MyPageAdapter(ArrayList<View> listViews) {// 构造函数
            // 初始化viewpager的时候给的一个页面
            this.listViews = listViews;
            size = listViews == null ? 0 : listViews.size();
        }

        public void setListViews(ArrayList<View> listViews) {// 自己写的一个方法用来添加数据
            this.listViews = listViews;
            size = listViews == null ? 0 : listViews.size();
        }

        public int getCount() {// 返回数量
            return size;
        }

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(listViews.get(position % size));
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(listViews.get(position % size), 0);
            return listViews.get(position % size);
        }

        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }
    }
}
