package com.hxlm.hcyandroid.tabbar.sicknesscheck;

import android.util.Log;
import android.view.SurfaceView;
import android.widget.Toast;
import com.hcy.ky3h.R;
import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.comm.Error;
import com.hxlm.android.comm.Error_English;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;
import com.hxlm.android.health.device.message.ecg.EcgDataMessage;
import com.hxlm.android.health.device.message.ecg.EcgWaveOutputCommand;
import com.hxlm.android.health.device.message.ecg.EcgWaveQueueMessage;
import com.hxlm.android.health.device.model.ChargePal;
import com.hxlm.android.health.device.model.HcyPhone;
import com.hxlm.android.health.device.view.ECGCompareManager;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.util.ToastUtil;

import java.io.IOException;

/**
 * 用于比较滤波和不滤波两种心电图效果
 * Created by Zhenyu on 2016/3/30.
 */
public class ECGCompareActivity extends AbstractDeviceActivity {
    private final static String TAG = "ECGCheckActivity";

    private HcyPhone hcyPhone;
    private ECGCompareManager ecgCompareManager;

    private SurfaceView svEcg;

    private EcgWaveQueueMessage ecgWaveQueueMessage = null;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_ecg_compare);

        hcyPhone = new HcyPhone();

        if (hcyPhone.getPowerStatus() != HcyPhone.PowerStatus.UNSUPPORT) {
            try {
                hcyPhone.setPowerStatus(HcyPhone.PowerStatus.ON);
                ioSession = hcyPhone.getIOSession(this);

            } catch (IOException e) {
                ioSession = new ChargePal().getIOSession(this);
                onExceptionCaught(e);
            }
        } else {
            ioSession = new ChargePal().getIOSession(this);
        }
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.ecg_check_title), titleBar, 1);

        svEcg = (SurfaceView) findViewById(R.id.sv_ecg);
        svEcg.setZOrderOnTop(true);
    }

    @Override
    public void initDatas() {

    }

    @Override
    public void onResume() {
        super.onResume();

        if (ioSession != null && ioSession.status != AbstractIOSession.Status.CONNECTED) {
            ioSession.connect();
        }
    }

    @Override
    protected void onConnected() {
        ToastUtil.invokeShortTimeToast(this, "设备已连接");

        ioSession.sendMessage(new EcgWaveOutputCommand(true));
    }

    @Override
    protected void onConnectFailed(Error error) {
        if (hcyPhone.getPowerStatus() == HcyPhone.PowerStatus.ON) {
            try {
                hcyPhone.setPowerStatus(HcyPhone.PowerStatus.OFF);
            } catch (IOException e) {
                onExceptionCaught(e);
            }

            ioSession = new ChargePal().getIOSession(this);
            ioSession.connect();
        }
        Toast.makeText(this, error.getDesc(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onConnectFailedEnglist(Error_English error) {
        if (hcyPhone.getPowerStatus() == HcyPhone.PowerStatus.ON) {
            try {
                hcyPhone.setPowerStatus(HcyPhone.PowerStatus.OFF);
            } catch (IOException e) {
                onExceptionCaught(e);
            }

            ioSession = new ChargePal().getIOSession(this);
            ioSession.connect();
        }
        Toast.makeText(this, error.getDesc(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDisconnected() {
        if (ecgCompareManager != null) {
            ecgCompareManager.stopDraw();
            ecgCompareManager = null;
        }

        ToastUtil.invokeShortTimeToast(this, "设备已断开");
    }

    @Override
    protected void onExceptionCaught(Throwable e) {
        Log.e(TAG, e.getMessage());
    }

    @Override
    protected void onMessageReceived(AbstractMessage message) {
        switch ((HealthDeviceMessageType) message.getMessageType()) {
            case ECG_WAVE:
                if (ecgWaveQueueMessage == null) {
                    ecgWaveQueueMessage = (EcgWaveQueueMessage) message;

                    try {
                        ecgCompareManager = new ECGCompareManager(svEcg, ecgWaveQueueMessage, Constant.BASE_PATH);
                    } catch (IOException e) {
                        onExceptionCaught(e);
                    }
                    ecgCompareManager.startDraw();
                    svEcg.invalidate();
                }
                break;

            case ECG_DATA:
                EcgDataMessage ecgDataMessage = (EcgDataMessage) message;
                if (ecgDataMessage.getConnection() == 1) {
                    Toast.makeText(this, "导联线路脱落", Toast.LENGTH_SHORT).show();
                }
                if (ecgDataMessage.getSignalQuality() == 1) {
                    Toast.makeText(this, "心电信号太弱", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (ecgCompareManager != null) {
            ecgCompareManager.stopDraw();
            ecgCompareManager = null;
        }

        try {
            hcyPhone.setPowerStatus(HcyPhone.PowerStatus.OFF);
        } catch (IOException e) {
            onExceptionCaught(e);
        }
    }

    @Override
    protected void onDestroy() {
        try {
            hcyPhone.setPowerStatus(HcyPhone.PowerStatus.OFF);
        } catch (IOException e) {
            onExceptionCaught(e);
        }

        super.onDestroy();
    }
}
