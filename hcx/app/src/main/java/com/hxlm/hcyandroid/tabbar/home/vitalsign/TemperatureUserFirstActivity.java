package com.hxlm.hcyandroid.tabbar.home.vitalsign;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.bean.CheckStep;
import com.hxlm.hcyandroid.tabbar.sicknesscheck.StepAdapter;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 体温首次进入界面
 *
 * @author l
 */
public class TemperatureUserFirstActivity extends BaseActivity implements OnClickListener {

    private ListView lv_tips;
    private List<CheckStep> steps;

    private ImageView iv_now_check;//立即检测
    private ImageView iv_no_prompt;//今后不再提示

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_temperature_user_first);

    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, "体温检测", titleBar, 1);
        lv_tips = findViewById(R.id.lv_tips);
        iv_now_check = findViewById(R.id.iv_now_check);
        iv_now_check.setOnClickListener(this);
        iv_no_prompt = findViewById(R.id.iv_no_prompt);
        iv_no_prompt.setOnClickListener(this);




    }

    @Override
    public void initDatas() {
        steps = new ArrayList<CheckStep>();
        steps.add(new CheckStep(R.drawable.check_step_number_bg1, "", "请将设备插入仪器口指示位置"));
        steps.add(new CheckStep(R.drawable.check_step_number_bg2, "", "请将设备插入仪器口指示位置"));
        steps.add(new CheckStep(R.drawable.check_step_number_bg3, "", "请将设备插入仪器口指示位置"));
        steps.add(new CheckStep(R.drawable.check_step_number_bg4, "", "请将设备插入仪器口指示位置"));
        steps.add(new CheckStep(R.drawable.check_step_number_bg5, "", "请将设备插入仪器口指示位置"));
        steps.add(new CheckStep(R.drawable.check_step_number_bg6, "", "请将设备插入仪器口指示位置"));
        lv_tips.setAdapter(new StepAdapter(this, steps));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //立即检测
            case R.id.iv_now_check:
                TemperatureUserFirstActivity.this.finish();
                Intent intent = new Intent(TemperatureUserFirstActivity.this, TemperatureDetectionActivity.class);
                startActivity(intent);

                break;
            //今后不再提示
            case R.id.iv_no_prompt:
                TemperatureUserFirstActivity.this.finish();
                SharedPreferenceUtil.saveString("Temperature", "0");
                Intent intent2 = new Intent(TemperatureUserFirstActivity.this, TemperatureDetectionActivity.class);
                startActivity(intent2);
                break;
            default:
                break;
        }


    }

}
