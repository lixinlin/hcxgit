package com.hxlm.hcyandroid.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Gallery;
import android.widget.ImageView;
import com.hxlm.hcyandroid.bean.GongFaDatas;
import com.hxlm.hcyandroid.util.ImageUtils;

/**
 * 显示功法图册的Adapter
 * Created by Zhenyu on 2015/8/23.
 */
public class ImageListAdapter extends BaseListAdapter {
    //由于屏幕上可以显示三张图片，此处最少应该设为3。
    private static final int CACHE_COUNT = 3;

    private Context mContext;
    private int group_index;
    //采用固定数量的ImageView来进行Gallery的图片展示，经测试内存占用为之前三分之一。
    private ImageView[] gongfaImgs;

    public ImageListAdapter(Context context) {
        mContext = context;
        Gallery.LayoutParams params = new Gallery.LayoutParams(
                Gallery.LayoutParams.MATCH_PARENT,
                Gallery.LayoutParams.MATCH_PARENT);

        gongfaImgs = new ImageView[CACHE_COUNT];

        for (int i = 0; i < gongfaImgs.length; i++) {
            gongfaImgs[i] = new ImageView(mContext);
            gongfaImgs[i].setAdjustViewBounds(true);
            gongfaImgs[i].setLayoutParams(params);
        }
    }

    public void setGroup_index(int group_index) {
        if (this.group_index != group_index) {
            this.group_index = group_index;
        }
    }

    public int getRealPosition(int position) {
        return GongFaDatas.GONGFA_GROUP[group_index][0] + position;
    }

    @Override
    public int getCount() {
        return GongFaDatas.GONGFA_GROUP[group_index][1];
    }

    @Override
    public Object getItem(int position) {
        int arrayPosition = GongFaDatas.GONGFA_GROUP[group_index][0] + position;
        return GongFaDatas.GONG_FA_BIG_IMAGE[arrayPosition];
    }

    @Override
    public long getItemId(int position) {
        return GongFaDatas.GONGFA_GROUP[group_index][0] + position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int cachePosition = position % CACHE_COUNT;
        int arrayPosition = GongFaDatas.GONGFA_GROUP[group_index][0] + position;

        Bitmap bitmap = ImageUtils.readBitMap(mContext, GongFaDatas.GONG_FA_BIG_IMAGE[arrayPosition]);
        gongfaImgs[cachePosition].setImageBitmap(bitmap);

        return gongfaImgs[cachePosition];
    }
}
