package com.hxlm.hcyandroid.datamanager;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.hcyandroid.MessageType;
import com.hxlm.hcyandroid.ResponseStatus;
import com.hxlm.hcyandroid.bean.*;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

public class ReserveBaseManager extends BaseManager {

    public ReserveBaseManager(Handler handler) {
        super(handler);
    }

    /**
     * 判断预约是否取消
     *
     * @param id   预约id
     * @param type 预约类型
     */
    public void checkIsCancled(int id, int type) {
        String url = "/member/doctorSubscribe/checkStatus/" + id + ".jhtml";
        String method = METHOD_GET;
        RequestParams params = new RequestParams();
        params.put("id", id);
        params.put("type", type);
        queryRemoteWithHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                boolean isCancled = false;
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS) {
                    isCancled = jo.getBoolean("data");
                }
                Message message = Message.obtain();
                message.arg1 = status;
                message.obj = isCancled;
                message.what = MessageType.MESSAGE_CHECK_IS_CANCLED;
                mHandler.sendMessage(message);
            }
        });
    }

    /**
     * 取消预约的接口
     *
     * @param id
     * @param reserveType
     */
    public void cancle(final int id, int reserveType) {
        String method = METHOD_GET;
        String url = "/member/doctorSubscribe/cancel/" + id + ".jhtml";
        RequestParams params = new RequestParams();
        params.put("id", id);
        params.put("type", reserveType);
        queryRemoteWithHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                String data = "";
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS || status == 1) {
                    data = jo.getString("data");
                }
                Message message = Message.obtain();
                message.arg1 = status;
                message.arg2 = id;
                message.what = MessageType.MESSAGE_CANCLE_ORDER;
                message.obj = data;
                mHandler.sendMessage(message);
            }
        });
    }

    /**
     * 获取所有地区的接口
     *
     * @param id   地区id，可以为null
     * @param type type为“video”的时候，查询只能提供视频服务的地区
     */
    public void getAllArea(Integer id, String type) {
        String method = METHOD_GET;
        final RequestParams params = new RequestParams();
        if (id != null) {
            params.put("parentId", id);
        }
        if (!TextUtils.isEmpty(type)) {
            params.put("type", type);
        }
        String url = "/doctor/getAreaList.jhtml";
        queryRemoteWithoutHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                int myWhat = MessageType.MESSAGE_GET_PARENT_AREA;
                if (params.has("parentId")) {
                    myWhat = MessageType.MESSAGE_GET_AREA;
                }
                List<Area> areas = new ArrayList<Area>();
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS) {
                    JSONArray data = jo.getJSONArray("data");
                    areas = JSON.parseArray(data.toJSONString(), Area.class);
                }
                Message message = Message.obtain();
                message.arg1 = status;
                message.what = myWhat;
                message.obj = areas;
                mHandler.sendMessage(message);
            }
        });
    }

    /**
     * 获取所有可预约的医院
     *
     * @param areaId       地区id
     * @param specialityId 科室id
     */
    public void getAllHospitals(Integer areaId, Integer specialityId) {
        String method = METHOD_GET;
        String url = "/doctor/getHospitalList.jhtml";
        RequestParams params = new RequestParams();
        if (areaId != null) {
            params.put("areaId", areaId);
        }
        if (specialityId != null) {
            params.put("specialityId", specialityId);
        }
        queryRemoteWithoutHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                List<HospitalToBeReserve> hospitals = new ArrayList<HospitalToBeReserve>();
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS) {
                    JSONObject data = jo.getJSONObject("data");
                    hospitals = JSON.parseArray(data.getJSONArray("content")
                            .toJSONString(), HospitalToBeReserve.class);
                }
                Message message = Message.obtain();
                message.arg1 = status;
                message.what = MessageType.MESSAGE_GET_HOSPITAL;
                message.obj = hospitals;
                mHandler.sendMessage(message);
            }
        });
    }

    /**
     * 获取所有的科室
     */
    public void getAllDepartments() {
        String method = METHOD_GET;
        String url = "/doctor/getSpecialityList.jhtml";
        queryRemoteWithoutHeader(method, url, null, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                List<Department> departments = new ArrayList<Department>();
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS) {
                    JSONArray data = jo.getJSONArray("data");
                    departments = JSON.parseArray(data.toJSONString(),
                            Department.class);
                }

                Message message = Message.obtain();
                message.arg1 = status;
                message.what = MessageType.MESSAGE_GET_DEPARTMENT;
                message.obj = departments;
                mHandler.sendMessage(message);
            }
        });
    }

    /**
     * 获取医生列表
     *
     * @param type         预约类型 挂号为bespoke，视频预约为video
     * @param areaId       预约类型 挂号为bespoke，视频预约为video
     * @param hospitalId   医院id
     * @param departmentId 科室id
     * @param isReservable 是否可预约
     * @param pageNumber   页码
     * @param name         姓名（用于模糊查询）
     * @param isImportant  是否专病专科
     */
    public void getDoctors(String type, Integer areaId, Integer hospitalId,
                           Integer departmentId, Integer isReservable, int pageNumber,
                           String name, Integer isImportant) {
        String method = METHOD_GET;
        String url = "/doctor/list.jhtml";
        RequestParams params = new RequestParams();
        params.put("type", type);
        if (areaId != null) {
            params.put("areaId", areaId);
        }
        if (hospitalId != null) {
            params.put("hospitalId", hospitalId);
        }
        if (departmentId != null) {
            params.put("specialityId", departmentId);
        }
        if (isReservable != null) {
            if (isReservable == 1) {
                params.put("isSubscribe", true);
            } else if (isReservable == 0) {
                params.put("isSubscribe", false);
            }
        }
        params.put("pageNumber", pageNumber);

        if (name != null || "".equals(name)) {
            params.put("name", name);
        }
        if (isImportant != null) {
            if (isImportant == 1) {
                params.put("isImportant", true);
            } else if (isImportant == 0) {
                params.put("isImportant", false);
            }
        }
        queryRemoteWithoutHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                List<ReserveDoctor> doctors = new ArrayList<ReserveDoctor>();
                int totalPages = -1;
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS) {
                    JSONObject data = jo.getJSONObject("data");
                    JSONArray ja_doctor = data.getJSONArray("content");
                    doctors = JSON.parseArray(ja_doctor.toJSONString(),
                            ReserveDoctor.class);
                    totalPages = data.getInteger("totalPages");
                }
                Message message = Message.obtain();
                message.arg1 = status;
                message.arg2 = totalPages;
                message.what = MessageType.MESSAGE_GET_DOCTORS;
                message.obj = doctors;
                mHandler.sendMessage(message);
            }
        });
    }

    /**
     * 根据地区名字获取id
     *
     * @param areaName 地区名字
     */
    public void getIdByAreaName(String areaName) {
        String method = METHOD_GET;
        String url = "/doctor/get_area_by_name.jhtml";
        RequestParams params = new RequestParams();
        params.put("name", areaName);
        queryRemoteWithoutHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                int areaId = -1;
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS) {
                    areaId = jo.getInteger("data");
                }
                Message message = Message.obtain();
                message.arg1 = status;
                message.what = MessageType.MESSAGE_GET_ID_BY_AREA;
                message.obj = areaId;
                mHandler.sendMessage(message);
            }
        });
    }

    /**
     * 预约医生（预约挂号和视频预约）
     *
     * @param doctorScheduleId 医生排班id
     * @param patientName      患者姓名
     * @param patientPhone     患者电话
     * @param patientId        患者身份证号码
     * @param isInsuranceCard  是否有医保
     */
    public void reserveDoctor(int doctorScheduleId, String patientName,
                              String patientPhone, String patientId, Integer isInsuranceCard) {
        String method = METHOD_GET;
        String url = "/member/doctorSubscribe/reserve.jhtml";
        //String memberId = SharedPreferenceUtil.getString("memberId");
        int memberId = LoginControllor.getLoginMember().getId();
        RequestParams params = new RequestParams();
        params.put("memberId", memberId);
        params.put("doctorScheduleId", doctorScheduleId);
        if (!TextUtils.isEmpty(patientName)) {
            params.put("patientName", patientName);
        }
        if (!TextUtils.isEmpty(patientPhone)) {
            params.put("patientPhone", patientPhone);
        }
        if (!TextUtils.isEmpty(patientId)) {
            params.put("patientId", patientId);
        }
        if (isInsuranceCard == 1) {
            params.put("isInsuranceCard", true);
        } else if (isInsuranceCard == 0) {
            params.put("isInsuranceCard", false);
        }
        queryRemoteWithHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                ReserveDoctorResponse response = new ReserveDoctorResponse();
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");

                String faileInfo = "";

                Message message = Message.obtain();
                message.arg1 = status;
                message.what = MessageType.MESSAGE_RESERVE_DOCTOR;

                if (status == ResponseStatus.SUCCESS) {
                    JSONObject data = jo.getJSONObject("data");
                    response = JSON.parseObject(data.toJSONString(),
                            ReserveDoctorResponse.class);
                    message.obj = response;
                } else {
                    //返回失败信息
                    faileInfo = jo.getString("data");
                    message.obj = faileInfo;

                }

                mHandler.sendMessage(message);
            }
        });
    }

    /**
     * 根据医生id获取预约挂号或者预约视频列表
     *
     * @param doctorId 医生id
     * @param type     获取数据的类型，bespoke:挂号预约 ，video：视频预约d
     */
    public void getScheduleList(int doctorId, String type) {
        String url = "/doctor/getScheduleList.jhtml";
        RequestParams params = new RequestParams();
        Log.e("Log.e", "doctorId = " + doctorId);
        params.put("doctorId", doctorId);
        params.put("type", type);
        queryRemoteWithoutHeader(METHOD_GET, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                List<DoctorSchedule> list = new ArrayList<DoctorSchedule>();
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS) {
                    JSONArray data = jo.getJSONArray("data");
                    list = JSON.parseArray(data.toJSONString(),
                            DoctorSchedule.class);
                    for (int i = 0; i < list.size(); i++) {
                        boolean isSubscribe = data.getJSONObject(i).getBoolean("isSubscribe");
                        list.get(i).setSubscribe(isSubscribe);
                    }
                }
                Message message = Message.obtain();
                message.arg1 = status;
                message.obj = list;
                message.what = MessageType.MESSAGE_GET_SCHEDULE_LIST;
                mHandler.sendMessage(message);
            }
        });
    }
}
