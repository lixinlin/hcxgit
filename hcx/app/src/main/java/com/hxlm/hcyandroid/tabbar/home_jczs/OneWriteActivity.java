package com.hxlm.hcyandroid.tabbar.home_jczs;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

import com.hcy.ky3h.R;
import com.hcy_futejia.activity.FtjVisceraIdentityActivity;
import com.hxlm.android.hcy.AbstractBaseActivity;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;

/**
 * Created by l on 2016/11/6.
 * 一写
 */
public class OneWriteActivity extends AbstractBaseActivity implements View.OnClickListener{

private ImageView img_wirte;
    @Override
    protected void setContentView() {
        setContentView(R.layout.layout_one_write);
    }

    @Override
    protected void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.one_write_title), titleBar, 1);
        img_wirte=(ImageView)findViewById(R.id.img_wirte);
        img_wirte.setOnClickListener(this);
    }

    @Override
    protected void initDatas() {

    }

    @Override
    public void onClick(View view) {
        SharedPreferenceUtil.saveString("oneWrite","false");
        Intent intent = new Intent(OneWriteActivity.this, FtjVisceraIdentityActivity.class);
        startActivity(intent);
        this.finish();
    }
}