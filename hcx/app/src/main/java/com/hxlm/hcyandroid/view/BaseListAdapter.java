package com.hxlm.hcyandroid.view;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * 无限循环滚动
 */
public class BaseListAdapter extends BaseAdapter {

    private List<Object> mDatas;

    public void setDatas(List<Object> datas) {
        this.mDatas = datas;
    }

    @Override
    public int getCount() {
        if (mDatas == null) {
            return 0;
        }
        return Integer.MAX_VALUE;
    }

    public int getRealCount() {
        if (mDatas == null) {
            return 0;
        }
        return mDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }

}
