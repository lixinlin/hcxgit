package com.hxlm.hcyandroid.bean;

public class HealthAdvisorCategory {
    private int id;
    private long createDate;
    private long modifyDate;
    private int order;
    private String name;
    private int serviceNum;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getServiceNum() {
        return serviceNum;
    }

    public void setServiceNum(int serviceNum) {
        this.serviceNum = serviceNum;
    }

    @Override
    public String toString() {
        return "HealthAdvisorCategory{" +
                "id=" + id +
                ", createDate=" + createDate +
                ", modifyDate=" + modifyDate +
                ", order=" + order +
                ", name='" + name + '\'' +
                ", serviceNum=" + serviceNum +
                '}';
    }
}
