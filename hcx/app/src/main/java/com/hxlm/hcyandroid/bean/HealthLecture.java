package com.hxlm.hcyandroid.bean;

import java.io.Serializable;

/**
 * 健康讲座
 *
 * @author Administrator
 */
public class HealthLecture implements Serializable {
    private int id;
    private long createDate;
    private long modifyDate;
    private String title; // 标题
    private String supportPhone; // 联系电话
    private boolean isPublication;
    private String area; // 讲座地点
    private String picture; // 讲座的图片
    private int listeners; // 听众总人数限制
    private int reserveLimit; // 单人预定门票的数量限制
    private int laveListeners; // 剩余票数
    private double price; // 价格，免费讲座价格为0
    private long beginDate; // 讲座开始日期
    private long endDate; // 讲座结束日期
    private long lectureDate; // 开讲时间
    private int productId; // 商品id，如果收费的话有值，不收费为null
    private String path; // 讲座详情的webview的路径
    private String talker; // 主讲人

//
//
//	public HealthLecture() {
//		super();
//	}
//
//	public HealthLecture(Parcel in) {
//		id = in.readInt();
//		createDate = in.readLong();
//		modifyDate = in.readLong();
//		title = in.readString();
//		supportPhone = in.readString();
//		isPublication = (in.readInt() == 1) ? true : false;
//		area = in.readString();
//		picture = in.readString();
//		listeners = in.readInt();
//		reserveLimit = in.readInt();
//		laveListeners = in.readInt();
//		price = in.readDouble();
//		beginDate = in.readLong();
//		endDate = in.readLong();
//		lectureDate = in.readLong();
//		productId = in.readInt();
//		path = in.readString();
//		talker = in.readString();
//	}
//
//	@Override
//	public void writeToParcel(Parcel dest, int flags) {
//		dest.writeInt(id);
//		dest.writeLong(createDate);
//		dest.writeLong(modifyDate);
//		dest.writeString(title);
//		dest.writeString(supportPhone);
//		dest.writeInt(isPublication ? 1 : 0);
//		dest.writeString(area);
//		dest.writeString(picture);
//		dest.writeInt(listeners);
//		dest.writeInt(reserveLimit);
//		dest.writeInt(laveListeners);
//		dest.writeDouble(price);
//		dest.writeLong(beginDate);
//		dest.writeLong(endDate);
//		dest.writeLong(lectureDate);
//		dest.writeLong(productId);
//		dest.writeString(path);
//		dest.writeString(talker);
//	}
//
//	public static final Parcelable.Creator<HealthLecture> CREATOR = new Creator<HealthLecture>() {
//
//		@Override
//		public HealthLecture createFromParcel(Parcel source) {
//			return new HealthLecture(source);
//		}
//
//		@Override
//		public HealthLecture[] newArray(int size) {
//			return new HealthLecture[size];
//		}
//
//	};

    public String getTalker() {
        return talker;
    }

    public void setTalker(String talker) {
        this.talker = talker;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSupportPhone() {
        return supportPhone;
    }

    public void setSupportPhone(String supportPhone) {
        this.supportPhone = supportPhone;
    }

    public boolean isPublication() {
        return isPublication;
    }

    public void setPublication(boolean isPublication) {
        this.isPublication = isPublication;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getListeners() {
        return listeners;
    }

    public void setListeners(int listeners) {
        this.listeners = listeners;
    }

    public int getReserveLimit() {
        return reserveLimit;
    }

    public void setReserveLimit(int reserveLimit) {
        this.reserveLimit = reserveLimit;
    }

    public int getLaveListeners() {
        return laveListeners;
    }

    public void setLaveListeners(int laveListeners) {
        this.laveListeners = laveListeners;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(long beginDate) {
        this.beginDate = beginDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public long getLectureDate() {
        return lectureDate;
    }

    public void setLectureDate(long lectureDate) {
        this.lectureDate = lectureDate;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

//	@Override
//	public int describeContents() {
//		return 0;
//	}

}
