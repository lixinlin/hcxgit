package com.hxlm.hcyandroid.bean;

/**
 * 我的预约挂号
 *
 * @author Administrator
 */
public class MyReserve {

    public static final String STATUS_CONFIRM = "confirm";
    public static final String STATUS_SUCCESS = "sucess";
    public static final String STATUS_UNPAID = "unpaid";
    public static final String STATUS_SERVER_OVER = "serverOver";
    public static final String STATUS_CANCELLED = "cancelled";

    private int id;//预约id
    private String status;// 预约状态（sucess:预约成功 ,unpaid:未付款, serverOver:服务结束,
    // cancelled:已取消）
    private double actualPayment;//未付款状态需要支付的金额
    private String patientName;
    private String patientPhone;
    private String patientId;
    private boolean isInsuranceCard;//是否有医疗保险
    private DoctorSchedule doctorSchedule;
    private ReserveDoctor doctor;
    private OrderItem orderItem;

    public MyReserve(int id, String status, double actualPayment,
                     String patientName, String patientPhone, String patientId,
                     boolean isInsuranceCard, DoctorSchedule doctorSchedule,
                     ReserveDoctor doctor, OrderItem orderItem) {
        super();
        this.id = id;
        this.status = status;
        this.actualPayment = actualPayment;
        this.patientName = patientName;
        this.patientPhone = patientPhone;
        this.patientId = patientId;
        this.isInsuranceCard = isInsuranceCard;
        this.doctorSchedule = doctorSchedule;
        this.doctor = doctor;
        this.orderItem = orderItem;
    }

    public MyReserve() {
        super();
        // TODO Auto-generated constructor stub
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getActualPayment() {
        return actualPayment;
    }

    public void setActualPayment(double actualPayment) {
        this.actualPayment = actualPayment;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientPhone() {
        return patientPhone;
    }

    public void setPatientPhone(String patientPhone) {
        this.patientPhone = patientPhone;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public boolean isInsuranceCard() {
        return isInsuranceCard;
    }

    public void setInsuranceCard(boolean isInsuranceCard) {
        this.isInsuranceCard = isInsuranceCard;
    }

    public DoctorSchedule getDoctorSchedule() {
        return doctorSchedule;
    }

    public void setDoctorSchedule(DoctorSchedule doctorSchedule) {
        this.doctorSchedule = doctorSchedule;
    }

    public ReserveDoctor getDoctor() {
        return doctor;
    }

    public void setDoctor(ReserveDoctor doctor) {
        this.doctor = doctor;
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }

    @Override
    public String toString() {
        return "MyReserve [id=" + id + ", status=" + status
                + ", actualPayment=" + actualPayment + ", patientName="
                + patientName + ", patientPhone=" + patientPhone
                + ", patientId=" + patientId + ", isInsuranceCard="
                + isInsuranceCard + ", doctorSchedule=" + doctorSchedule
                + ", doctor=" + doctor + ", orderItem=" + orderItem + "]";
    }


}
