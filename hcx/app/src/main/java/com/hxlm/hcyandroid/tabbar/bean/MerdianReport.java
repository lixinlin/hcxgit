package com.hxlm.hcyandroid.tabbar.bean;

/**
 * Created by lixinlin on 2018/11/6.
 */

public class MerdianReport {
    private String type;
    private String time;

    public MerdianReport() {
    }

    public MerdianReport(String type, String time) {
        this.type = type;
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
