package com.hxlm.hcyandroid.tabbar.home_jczs;

import com.hxlm.hcyandroid.Constant;

/**
 * Created by l on 2016/10/18.
 * 检测跳转，根据不同的拦截指令，跳转不同的检测界面
 */
public class DetectionJump {

    //跳转检测页面，同意url
    public static final String DETECTIOB_JUMP_URL= Constant.BASE_URL+"/hcy/member/action";
    public static final String DETECTIOB_JUMP_URL2= Constant.BASE_URL+"/member/action";


    public static final String MAN_BING="manbing";//慢病检测
    public static final String SHENG_MING="shengming";//生命体征
    public static final String YU_YUE="yuyue";//预约挂号
    public static final String ZHUAN_BING="zhuanbing";//专病专科
    public static final String SHI_PIN="shiping";//视频咨询
    public static final String TU_WEN="tuwen";//图文咨询
    public static final String ER_XUE="erxue";//耳穴处方
    public static final String YIN_YUE="yinyue";//音乐处方
    public static final String YUN_DONG="yundong";//进入运动
    public static final String TI_ZHI="tizhi";//体质辨识
    public static final String WEN_YIN="wenyin";//闻音辨识
    public static final String ZANG_FU="zangfu";//脏腑辨识
    public static final String GAN_YU_FANG_AN="ganyufangan";//干预方案
    public static final String GAN_YU="fangan";//干预
    public static final String JIU_LIAO="aijiu";//干预方案


}
