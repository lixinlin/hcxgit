package com.hxlm.hcyandroid.tabbar.archives;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.health.device.view.ECGDrawWaveManager;
import com.hxlm.android.health.device.view.ECGWaveImageView;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * 心电心率检测
 *
 * @author l
 */
public class ECGReviewActivity extends BaseActivity implements View.OnClickListener {
    public final static String ECG_FILE_BUNDLE_NAME = "ecgFileName";
    private final static String TAG = "ECGReviewActivity";
    private final static int POINTS_OF_STEP = 200;
    private final static int POINTS_BUFFER = 50000;

    private final byte[] data = new byte[POINTS_BUFFER];

    private ECGWaveImageView ivEcgWave;
    private String ecgFileName;
    private int step = 0;
    private int dataLength = 0;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_ecg_review);
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.archives_heart_chakan), titleBar, 1);
        ivEcgWave = (ECGWaveImageView) findViewById(R.id.ecg_wave_iv);
        ImageView ivEcgPrevs = (ImageView) findViewById(R.id.iv_ecg_prevs);
        ImageView ivEcgNext = (ImageView) findViewById(R.id.iv_ecg_next);
        ImageButton ivArrowLeft = (ImageButton) findViewById(R.id.iv_arrowLeft);
        ImageButton ivArrowRight = (ImageButton) findViewById(R.id.iv_arrowRight);

        ivEcgNext.setOnClickListener(this);
        ivEcgPrevs.setOnClickListener(this);
        ivArrowLeft.setOnClickListener(this);
        ivArrowRight.setOnClickListener(this);
    }

    @Override
    public void initDatas() {
        Bundle bundle = this.getIntent().getExtras();
        ecgFileName = bundle.getString(ECG_FILE_BUNDLE_NAME);
        Log.i(TAG, "ecgFileName-->" + ecgFileName);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData(step);
        ivEcgWave.setData(data, step * POINTS_OF_STEP);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //上一页
            case R.id.iv_ecg_prevs:
                if (step > 0) {
                    step--;
                    ivEcgWave.setData(data, step * POINTS_OF_STEP);
                }
                break;
            //下一页
            case R.id.iv_ecg_next:
                if ((step * POINTS_OF_STEP + ivEcgWave.getPointsPerView()) < dataLength) {
                    Log.i(TAG, "step = " + step + ",  PointsPerView = " + ivEcgWave.getPointsPerView()
                            + ",  data length = " + dataLength);
                    step++;
                    ivEcgWave.setData(data, step * POINTS_OF_STEP);
                }
                break;
            //左箭头
            case R.id.iv_arrowLeft:
                if (step > 0) {
                    step--;
                    ivEcgWave.setData(data, step * POINTS_OF_STEP / 2);
                }
                break;
            //右箭头
            case R.id.iv_arrowRight:
                if ((step * POINTS_OF_STEP + ivEcgWave.getPointsPerView()) < dataLength) {
                    Log.i(TAG, "step = " + step + ",  PointsPerView = " + ivEcgWave.getPointsPerView()
                            + ",  data length = " + dataLength);
                    step++;
                    ivEcgWave.setData(data, step * POINTS_OF_STEP / 2);
                }
                break;
        }
    }

    private void loadData(final long aSkipCount) {
        File ecgFile = new File(Constant.BASE_PATH + ECGDrawWaveManager.ECG_FILE_PATH, ecgFileName);
        if (ecgFile.exists()) {

            Logger.i(TAG,"要画心电图行的路径-->"+ecgFile.getAbsolutePath());

            BufferedInputStream bis = null;
            try {
                bis = new BufferedInputStream(
                        new FileInputStream(Constant.BASE_PATH + ECGDrawWaveManager.ECG_FILE_PATH + "/" + ecgFileName));

                Logger.i(TAG,"要画心电图行的路径222-->"+Constant.BASE_PATH + ECGDrawWaveManager.ECG_FILE_PATH + "/" + ecgFileName);
                long skipCount = 0;
                if (aSkipCount > 0) {
                    skipCount = bis.skip(aSkipCount);
                }
                if (skipCount == aSkipCount) {
                    dataLength = bis.read(data);
                }

                Log.i(TAG,"data---->"+data.toString());
//                for (int i=0;i<data.length;i++)
//                {
//                    Log.i(TAG,"data i---->"+data[i]);
//                }


            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            } finally {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage());
                    }
                }
            }
        }
    }
}
