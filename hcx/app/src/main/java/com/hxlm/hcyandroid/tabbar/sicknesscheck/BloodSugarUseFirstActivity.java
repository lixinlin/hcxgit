package com.hxlm.hcyandroid.tabbar.sicknesscheck;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.hcy.ky3h.R;
import com.hcy_futejia.activity.FtjBloodSugarCheckActivity;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 血糖首次进入界面
 *
 * @author l
 */
public class BloodSugarUseFirstActivity extends BaseActivity implements
        OnClickListener {

    private ListView lv_sick_BloodSugar;
    private List<BloodSugarText> list;

    private ImageView iv_now_check;// 立即检测
    private ImageView iv_no_prompt;// 今后不再提示

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_blood_sugar_use_first);

    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.bsc_title), titleBar, 1);
        lv_sick_BloodSugar = (ListView) findViewById(R.id.lv_sick_BloodSugar);
        iv_now_check = (ImageView) findViewById(R.id.iv_now_check);
        iv_now_check.setOnClickListener(this);
        iv_no_prompt = (ImageView) findViewById(R.id.iv_no_prompt);
        iv_no_prompt.setOnClickListener(this);

        list = new ArrayList<BloodSugarText>();
        list.add(new BloodSugarText(R.drawable.breath_10,
                getString(R.string.bsuf_list_add1)));
        list.add(new BloodSugarText(R.drawable.breath_102,
                getString(R.string.bsuf_list_add2)));
        list.add(new BloodSugarText(R.drawable.breath_103,
                getString(R.string.bsuf_list_add3)));

        BloodSugarAdapter adapter = new BloodSugarAdapter(
                BloodSugarUseFirstActivity.this, list);
        lv_sick_BloodSugar.setAdapter(adapter);

    }

    @Override
    public void initDatas() {

    }

    // 实体类
    class BloodSugarText {

        private int id;
        private String text;

        public BloodSugarText(int id, String text) {
            super();
            this.id = id;
            this.text = text;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }

    // 适配器
    class BloodSugarAdapter extends BaseAdapter {

        Context context;
        List<BloodSugarText> list;
        LayoutInflater inflater;

        public BloodSugarAdapter(Context context, List<BloodSugarText> list) {
            this.context = context;
            this.list = list;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int arg0) {
            return list.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int position, View view, ViewGroup arg2) {
            ViewHolder holder = null;
            if (view == null) {
                holder = new ViewHolder();
                view = inflater.inflate(R.layout.breath_list_item, null);
                holder.ivid = (ImageView) view.findViewById(R.id.ivid);
                holder.text = (TextView) view.findViewById(R.id.tv_breath);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            holder.ivid.setImageResource(list.get(position).getId());
            holder.text.setText(list.get(position).getText());
            return view;
        }

    }

    class ViewHolder {
        ImageView ivid;
        TextView text;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // 立即检测
            case R.id.iv_now_check:
                BloodSugarUseFirstActivity.this.finish();
                Intent intent = new Intent(BloodSugarUseFirstActivity.this,
                        FtjBloodSugarCheckActivity.class);
                startActivity(intent);

                break;
            // 今后不再提示
            case R.id.iv_no_prompt:
                BloodSugarUseFirstActivity.this.finish();
                SharedPreferenceUtil.saveString("BloodSugar", "0");
                Intent intent2 = new Intent(BloodSugarUseFirstActivity.this,
                        FtjBloodSugarCheckActivity.class);
                startActivity(intent2);
                break;
            default:
                break;
        }

    }

}
