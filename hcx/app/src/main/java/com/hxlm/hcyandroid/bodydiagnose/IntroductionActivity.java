package com.hxlm.hcyandroid.bodydiagnose;

import android.content.Intent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.util.WebViewUtil;

/**
 * 介绍
 *
 * @author l
 */
public class IntroductionActivity extends BaseActivity {

    private ProgressBar progressBar;
    private WebView wv_introduce;
    private String title = "";
    private String introduceCategory = "";
    private String path = "";

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_webview);
    }

    @Override
    public void initViews() {

        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        introduceCategory = intent.getStringExtra("introduceCategory");

        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, title, titleBar, 1);
        progressBar=(ProgressBar)findViewById(R.id.progressbar);
        wv_introduce = (WebView) findViewById(R.id.wv);

        WebSettings setting = wv_introduce.getSettings();
        setting.setJavaScriptEnabled(true);
        // wv_introduce.loadUrl(introduceCategory);
        wv_introduce.loadUrl(introduceCategory);

        //重新定义
//        new WebViewUtil().setWebViewInit(wv_introduce,progressBar,IntroductionActivity.this, introduceCategory);
    }

    @Override
    public void initDatas() {

    }
}
