package com.hxlm.hcyandroid.datamanager;

import android.os.Handler;
import android.os.Message;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hxlm.hcyandroid.MessageType;
import com.hxlm.hcyandroid.ResponseStatus;
import com.hxlm.hcyandroid.bean.DoctorSchedule;
import com.hxlm.hcyandroid.bean.MyReserve;
import com.hxlm.hcyandroid.bean.OrderItem;
import com.hxlm.hcyandroid.bean.ReserveDoctor;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

public class RegistrationReserveManager extends ReserveBaseManager {

    public RegistrationReserveManager(Handler handler) {
        super(handler);
    }

    /**
     * 获取我的预约挂号列表
     */
    public void getMyReserve(Integer pageNumber) {
        String method = METHOD_GET;
        String memberId = SharedPreferenceUtil.getMemberId();
        String url = "/member/doctorSubscribe/list_bespoke/" + memberId
                + ".jhtml";
        RequestParams params = new RequestParams();
        params.put("id", memberId);
        params.put("pageNumber", pageNumber);
        queryRemoteWithHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                List<MyReserve> myReserves = new ArrayList<MyReserve>();
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                int totalPages = -1;
                if (status == ResponseStatus.SUCCESS) {
                    JSONObject data = jo.getJSONObject("data");
                    JSONArray ja_content = data.getJSONArray("content");
                    if (ja_content != null) {
                        for (int i = 0; i < ja_content.size(); i++) {
                            JSONObject jo_reservation = ja_content.getJSONObject(i);
                            int id = jo_reservation.getInteger("id");
                            String reserveStatus = jo_reservation
                                    .getString("status");
                            double actualPayment = jo_reservation
                                    .getDouble("actualPayment");
                            String patientName = jo_reservation
                                    .getString("patientName");
                            String patientPhone = jo_reservation
                                    .getString("patientPhone");
                            String patientId = jo_reservation
                                    .getString("patientId");
                            boolean isInsuranceCard = jo_reservation
                                    .getBoolean("isInsuranceCard");
                            JSONObject jo_doctorSchedule = jo_reservation
                                    .getJSONObject("doctorSchedule");
                            DoctorSchedule doctorSchedule = JSON.parseObject(
                                    jo_doctorSchedule.toJSONString(),
                                    DoctorSchedule.class);
                            JSONObject jo_doctor = jo_reservation
                                    .getJSONObject("doctor");
                            ReserveDoctor doctor = JSON.parseObject(
                                    jo_doctor.toJSONString(),
                                    ReserveDoctor.class);
                            JSONObject jo_orderItem = jo_reservation
                                    .getJSONObject("orderItem");
                            OrderItem orderItem = JSON.parseObject(
                                    jo_orderItem.toJSONString(),
                                    OrderItem.class);
                            MyReserve myReserve = new MyReserve(id,
                                    reserveStatus, actualPayment, patientName,
                                    patientPhone, patientId, isInsuranceCard,
                                    doctorSchedule, doctor, orderItem);
                            myReserves.add(myReserve);

                            totalPages = data.getInteger("totalPages");
                        }
                    }
                }
                Message message = Message.obtain();
                message.arg1 = status;
                message.arg2 = totalPages;
                message.what = MessageType.MESSAGE_GET_RESERVES;
                message.obj = myReserves;
                mHandler.sendMessage(message);
            }
        });
    }
}
