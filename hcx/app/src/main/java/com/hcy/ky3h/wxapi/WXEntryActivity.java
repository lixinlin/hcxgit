package com.hcy.ky3h.wxapi;

import android.os.Bundle;

import com.hcy.ky3h.R;
import com.umeng.socialize.weixin.view.WXCallbackActivity;

/**
 * Created by lixinlin on 2018/9/21.
 */

public class WXEntryActivity extends WXCallbackActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wxentry);
    }
}
