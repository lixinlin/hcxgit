package com.hhmedic.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Window;
import android.view.WindowManager;

public class BaseActivity extends com.hxlm.hcyandroid.BaseActivity {

    public String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(contentViewId());
        initUI();
    }


    protected int contentViewId() {
        return 0;
    }

    protected void initUI() {

    }


    @Override
    public void setContentView() {
//        无需实现
    }

    @Override
    public void initViews() {
//无需事先
    }

    @Override
    public void initDatas() {
//无需实现
    }
}
