package com.hcy_futejia.activity;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hcy_futejia.bean.FtjRecordIndexData;
import com.hcy_futejia.utils.DateUtil;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.report.RecordManager;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.CheckedDataType;
import com.hxlm.hcyandroid.tabbar.MyHealthFileBroadcast;
import com.hxlm.hcyandroid.tabbar.sicknesscheck.BloodPressureUseFirstActivity;
import com.hxlm.hcyandroid.tabbar.sicknesscheck.BloodSugarUseFirstActivity;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.hxlm.hcyphone.MainActivity;
import com.hxlm.hcyphone.adapter.HealthDetectionSuggestAdapter;
import com.hxlm.hcyphone.adapter.HealthMonitorSuggestAdapter;
import com.hxlm.hcyphone.manager.MeasureInfoManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FtjMeasureInfoActivity extends BaseActivity implements OnCompleteListener {
    private static final String TAG = "MeasureInfoActivity";
    private TextView tvBloodPressureDetectionDate;
//    private TextView tvBloodPressureDetectionValue;
    private TextView tvBloodGlucoseDetectionDate;
//    private TextView tvBloodGlucoseDetectionValue;
    private TextView tvMonitorSuggest;
    private TextView tvDetectionSuggest;
    private ListView lvMonitorSuggest;
    private ListView lvDetectionSuggest;
    private RecordManager mRecordManager;
    private MeasureInfoManager mMeasureInfoManager;
    private int memberChildId;
    private List<FtjRecordIndexData> mRecordIndexdatas;
    private HealthMonitorSuggestAdapter healthMonitorSuggestAdapter;
    private HealthDetectionSuggestAdapter healthDetectionSuggestAdapter;
    private ScrollView scrollview;
    private Runnable runnable;
    private TextView tv_gaoya_value;
    private TextView tv_diya_value;
    private TextView tv_xinlv_value;
    private TextView tv_sugar_value;
    private ImageView iv_blood_sugar_daosanjiao1;
    private ImageView iv_blood_sugar_daosanjiao2;
    private ImageView iv_blood_sugar_daosanjiao3;
    private ImageView iv_blood_pressure_daosanjiao3;
    private ImageView iv_blood_pressure_daosanjiao2;
    private ImageView iv_blood_pressure_daosanjiao1;
    private TextView tv_shijianduan;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_ftj_measure_info);
    }

    @Override
    public void initViews() {
        //初始化titleBar
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.MeasureInfoActivity_tizhengjianjiance), titleBar, 1, this);

        tv_gaoya_value = findViewById(R.id.tv_gaoya_value);
        tv_diya_value = findViewById(R.id.tv_diya_value);
        tv_xinlv_value = findViewById(R.id.tv_xinlv_value);
        tv_sugar_value = findViewById(R.id.tv_sugar_value);
        tv_shijianduan = findViewById(R.id.tv_shijianduan);

        tvBloodPressureDetectionDate = (TextView) findViewById(R.id.tv_blood_pressure_date);
        tvBloodGlucoseDetectionDate = (TextView) findViewById(R.id.tv_blood_sugar_date);

        iv_blood_pressure_daosanjiao1 = findViewById(R.id.iv_blood_pressure_daosanjiao1);
        iv_blood_pressure_daosanjiao2 = findViewById(R.id.iv_blood_pressure_daosanjiao2);
        iv_blood_pressure_daosanjiao3 = findViewById(R.id.iv_blood_pressure_daosanjiao3);
        iv_blood_sugar_daosanjiao1 = findViewById(R.id.iv_blood_sugar_daosanjiao1);
        iv_blood_sugar_daosanjiao2 = findViewById(R.id.iv_blood_sugar_daosanjiao2);
        iv_blood_sugar_daosanjiao3 = findViewById(R.id.iv_blood_sugar_daosanjiao3);

        //监测建议控件
        tvMonitorSuggest = (TextView) findViewById(R.id.tv_monitor_suggest);
        lvMonitorSuggest = (ListView) findViewById(R.id.lv_monitor_suggest);
        lvMonitorSuggest.setAdapter(healthMonitorSuggestAdapter);

        //检测建议控件
        tvDetectionSuggest = (TextView) findViewById(R.id.tv_detection_suggest);
        lvDetectionSuggest = (ListView) findViewById(R.id.lv_detection_suggest);
        lvDetectionSuggest.setAdapter(healthDetectionSuggestAdapter);

        scrollview = (ScrollView) findViewById(R.id.scrollView_test);


    }

    @Override
    public void initDatas() {

        fetchChildMemberId();
        mRecordManager = new RecordManager();
        mMeasureInfoManager = new MeasureInfoManager();
        healthMonitorSuggestAdapter = new HealthMonitorSuggestAdapter(this);
        healthDetectionSuggestAdapter = new HealthDetectionSuggestAdapter(this);

        runnable = new Runnable() {
            @Override
            public void run() {
                scrollview.scrollTo(0, 0);// 改变滚动条的位置
            }
        };
        new Handler().postDelayed(runnable, 20);
    }

    /**
     * 获取选择的用户id;
     */

    private void fetchChildMemberId() {
        ChildMember choosedChildMember = LoginControllor.getChoosedChildMember();
        if (choosedChildMember == null) {
            memberChildId = LoginControllor.getLoginMember().getMengberchild().get(0).getId();
        } else {
            memberChildId = choosedChildMember.getId();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLastDataOnResumeOrMemberChange();
    }

    /**
     * 在onResume中调用，确保展示的是最新的信息；
     */
    private void getLastDataOnResumeOrMemberChange() {
        //获取最新检测记录
        mRecordManager.oldgetRecordIndexNew(memberChildId, new AbstractDefaultHttpHandlerCallback(FtjMeasureInfoActivity.this) {
            @Override
            protected void onResponseSuccess(Object obj) {
                mRecordIndexdatas = (List<FtjRecordIndexData>) obj;
                Log.e("retrofit","===一测列表数据==="+mRecordIndexdatas.toString());
                handleResponseRecord(mRecordIndexdatas);
            }
        });

        List<String> monitorSuggest = new ArrayList();
        monitorSuggest.add("XL");
        monitorSuggest.add("XYa");
        monitorSuggest.add("XT");
        healthMonitorSuggestAdapter.setmDatas(monitorSuggest);
        String msStr = getString(R.string.measure_info_msstr);
        tvMonitorSuggest.setText(msStr);

        List<String> detectionSuggest = new ArrayList();
        detectionSuggest.add(getString(R.string.measure_info_gangongneng));
        detectionSuggest.add(getString(R.string.measure_info_feigongneng));
        healthDetectionSuggestAdapter.setmDatas(detectionSuggest);
        String dsStr = getString(R.string.measure_info_dsstr);
        tvDetectionSuggest.setText(dsStr);
    }

    /**
     * 将获取的体检建议展示在布局
     *
     * @param ds 获取的检测记录；
     */
    private void handleResponseDetectionSuggest(List<String> ds) {
        healthDetectionSuggestAdapter.setmDatas(ds);
        String dsStr = getString(R.string.measure_info_dsstr);
        tvDetectionSuggest.setText(dsStr);
    }

    /**
     * 将获取的监测建议展示在布局
     *
     * @param ms 获取的检测记录；
     */
    private void handleResponseMontiorSuggest(List<String> ms) {
        healthMonitorSuggestAdapter.setmDatas(ms);
        String msStr = getString(R.string.measure_info_msstr);
        tvMonitorSuggest.setText(msStr);
    }

    /**
     * 血压范围判断
     * @param iv
     */
    private void setVisibleImg(ImageView iv){
        iv_blood_pressure_daosanjiao1.setVisibility(View.INVISIBLE);
        iv_blood_pressure_daosanjiao2.setVisibility(View.INVISIBLE);
        iv_blood_pressure_daosanjiao3.setVisibility(View.INVISIBLE);
        iv.setVisibility(View.VISIBLE);
    }

    /**
     * 血糖范围判断
     * @param iv
     */
    private void setVisibleImgSugar(ImageView iv){
        iv_blood_sugar_daosanjiao1.setVisibility(View.INVISIBLE);
        iv_blood_sugar_daosanjiao2.setVisibility(View.INVISIBLE);
        iv_blood_sugar_daosanjiao3.setVisibility(View.INVISIBLE);
        iv.setVisibility(View.VISIBLE);
    }

    /**
     * 将获取的最新检测记录展示在布局
     *
     * @param mRecordIndexdatas 获取的检测记录；
     */
    private void handleResponseRecord(List<FtjRecordIndexData> mRecordIndexdatas) {

        tvBloodPressureDetectionDate.setText("");
        tvBloodGlucoseDetectionDate.setText("");
        tv_gaoya_value.setText("_mmHg");
        tv_diya_value.setText("_mmHg");
        tv_xinlv_value.setText("_BMP");
        tv_sugar_value.setText("_mmol");
        if (mRecordIndexdatas != null && mRecordIndexdatas.size() > 0) {
            for (FtjRecordIndexData data : mRecordIndexdatas) {
                String category = data.getCategory();
               if (category.equalsIgnoreCase(getString(R.string.MeasureInfoActivity_xueya))){
                   String s = DateUtil.dateToString(data.getInputDate(), "yyyy-MM-dd");
                   tvBloodPressureDetectionDate.setText(s);
                   int highPressure = data.getHighPressure();
                   int lowPressure = data.getLowPressure();
                   tv_gaoya_value.setText(highPressure+"mmHg");
                    tv_diya_value.setText(lowPressure+"mmHg");
                    tv_xinlv_value.setText(data.getPulse()+"BMP");
                    if (lowPressure>=0 && lowPressure<=60){
                        setVisibleImg(iv_blood_pressure_daosanjiao1);
                    }else if (lowPressure >= 90){
                        setVisibleImg(iv_blood_pressure_daosanjiao3);
                    }else {
                        if (highPressure >= 140){
                            setVisibleImg(iv_blood_pressure_daosanjiao3);
                        }else if (highPressure <= 90){
                            setVisibleImg(iv_blood_pressure_daosanjiao1);
                        }else {
                            setVisibleImg(iv_blood_pressure_daosanjiao2);
                        }
                    }
                }else if (category.equalsIgnoreCase(getString(R.string.MeasureInfoActivity_xuetang))){
                    String result = data.getResult();
                    tv_sugar_value.setText(result+"mmol");
                    String s = DateUtil.dateToString(data.getInputDate(), "yyyy-MM-dd");
                    tvBloodGlucoseDetectionDate.setText(s);
                   String type = data.getType();
                   if (type.equals("empty")){
                       tv_shijianduan.setText(getString(R.string.sugar_wucanqian));
                   }else if (type.equals("full")){
                       tv_shijianduan.setText(getString(R.string.sugar_wucanhou));
                   }else if (type.equals("beforeDawn")){
                       tv_shijianduan.setText(getString(R.string.sugar_lingchen));
                   }else if (type.equals("beforeBreakfast")){
                       tv_shijianduan.setText(getString(R.string.sugar_zaocanqian));
                   }else if (type.equals("afterBreakfast")){
                       tv_shijianduan.setText(getString(R.string.sugar_zaocanhou));
                   }else if (type.equals("beforeDinner")){
                       tv_shijianduan.setText(getString(R.string.sugar_wancanqian));
                   }else if (type.equals("afterDinner")){
                       tv_shijianduan.setText(getString(R.string.sugar_wancanhou));
                   }else if (type.equals("beforeSleep")){
                       tv_shijianduan.setText(getString(R.string.sugar_shuiqian));
                   }

                   double i = Double.parseDouble(result);
                   if (i >0 && i < 3.9){
                       setVisibleImgSugar(iv_blood_sugar_daosanjiao1);
                   }else if (i > 6.1){
                       setVisibleImgSugar(iv_blood_sugar_daosanjiao3);
                   }else {
                       setVisibleImgSugar(iv_blood_sugar_daosanjiao2);
                   }
               }

            }
        }
    }

    public void doDetection(View view) {
        switch (view.getId()) {
            case R.id.tv_blood_pressure_detection:
                //是否需要进入血压首次进入界面 0表示不显示
                if ("0".equals(SharedPreferenceUtil.getBloodPressure())) {
                    startActivity(new Intent(this, FtjBloodPressureCheckActivity.class));
                } else {
                    startActivity(new Intent(this, BloodPressureUseFirstActivity.class));
                }

                break;
            case R.id.tv_blood_sugar_detection:
                //是否需要进入血糖首次进入界面 0表示不显示
                if ("0".equals(SharedPreferenceUtil.getBloodSugar())) {
                    startActivity(new Intent(this, FtjBloodSugarCheckActivity.class));
                } else {
                    startActivity(new Intent(this, BloodSugarUseFirstActivity.class));
                }
                break;

            case R.id.tv_blood_pressure_monitoring:
                Intent intent1 = new Intent(FtjMeasureInfoActivity.this, BloodMonitorActivity.class);
                intent1.putExtra("title",getString(R.string.blood_pressure_monitoring));
                intent1.putExtra("b",CheckedDataType.BLOOD_PRESSURE);
                startActivity(intent1);
                break;
            case R.id.tv_blood_sugar_monitoring:
                Intent intent2 = new Intent(FtjMeasureInfoActivity.this, BloodMonitorActivity.class);
                intent2.putExtra("title",getString(R.string.blood_sugar_monitoring));
                intent2.putExtra("b",CheckedDataType.BLOOD_SUGAR);
                startActivity(intent2);
                break;
            default:
                break;
        }
    }

    /**
     * TitleBar选择用户回调
     * 用于完成用户切换时拉取信息
     */
    @Override
    public void onComplete() {
        fetchChildMemberId();
        getLastDataOnResumeOrMemberChange();
    }
}

