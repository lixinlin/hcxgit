package com.hcy_futejia.activity;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hcy_futejia.utils.SingleListSymple;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.database.GreenDaoManager;
import com.hxlm.database.ICD10;
import com.hxlm.database.dao.DaoSession;
import com.hxlm.database.dao.ICD10Dao;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.bean.ICDItem;
import com.hxlm.hcyandroid.bean.Symptom;
import com.hxlm.hcyandroid.tabbar.home.visceraidentity.AlertDialogPrompt;

import com.hxlm.hcyandroid.tabbar.home.visceraidentity.ListCenterAdapter;
import com.hxlm.hcyandroid.util.LanguageUtil;
import com.hxlm.hcyandroid.view.ContainsEmojiEditText;

import org.greenrobot.greendao.query.QueryBuilder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FtjVisceraDiseaseListActivity extends BaseActivity implements View.OnClickListener{
    private ContainsEmojiEditText et_sickness_name;//搜索疾病

    private List<Symptom> list_left_data;// 左边的list

    private ListView listview_center;// 中间

    public ListCenterAdapter centeradapter;// 中间adapter

    private ICDItem itemToBeDelete;//删除选中项

    private ImageView image_search;// 搜索



//    private ICD10OpenHelper openHelper;
    private String sicknessName;
//    private ICD10DAO dao;
    private List<ICDItem> centerItems = new ArrayList<ICDItem>();
    private List<ICDItem> rightItems = new ArrayList<ICDItem>();

    // 接口调用参数
    private String symptomStr = "";
    private String icds = "";
    private int level = 2;
    private int zxNum;
    private int disNum;
    private int sex;
    private boolean isMan;

    private int REQUEST_DCODE_GRAINT_URI = 11;
    private TextView tv_disease_sure;

    private static String DB_NAME = "ICD10.db";
    private static String ASSETS_NAME = "ICD10.db";

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_ftj_viscera_disease_list);
    }

    @Override
    public void initViews() {
        initDatas();
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.vdl_title), titleBar, 1);


        listview_center = (ListView) findViewById(R.id.listview_center);
        tv_disease_sure = findViewById(R.id.tv_disease_sure);
        tv_disease_sure.setOnClickListener(this);
        et_sickness_name = (ContainsEmojiEditText) findViewById(R.id.et_list);
        Intent intent = getIntent();
        isMan = intent.getBooleanExtra("isMan", true);
        list_left_data = SingleListSymple.getSingleListSymple().getSymptomList();

        boolean isDBExist = checkDataBase();
        if (!isDBExist) {
            copyDataBase();
        }

        et_sickness_name.addTextChangedListener(new TextWatcher() {

            private List<ICD10> icdBeans;

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                sicknessName = s.toString();
                if (!TextUtils.isEmpty(sicknessName)) {
                    if (centerItems != null) {
                        centerItems.clear();
                    }
                    List<ICDItem> itemsFromDb = new ArrayList<ICDItem>();
                    DaoSession daoSession = GreenDaoManager.getDaoSession();
                    if (LanguageUtil.getInstance().getAppLocale(FtjVisceraDiseaseListActivity.this).getLanguage().equals("en")){
                        icdBeans =
                                daoSession.getICD10Dao().queryBuilder()
                                        .where(ICD10Dao.Properties.Content_en.like("%" + sicknessName + "%"))
                                        .limit(200).build().list();
                    }else {
                        icdBeans =
                                daoSession.getICD10Dao().queryBuilder()
                                .where(ICD10Dao.Properties.Content.like("%" + sicknessName + "%"))
                                .limit(200).build().list();
                    }
                    int sexFromActivity = 0;
                    for (int i = 0; i < icdBeans.size(); i++) {
                        ICD10 icd10 = icdBeans.get(i);
                        int sex = icd10.getSEX();
                        if (sex == sexFromActivity || sex == 2) {
                            String micd = icd10.getMICD();
                            String mtji = icd10.getMTJI();
                            String desc = "";
                            if (LanguageUtil.getInstance().getAppLocale(FtjVisceraDiseaseListActivity.this).getLanguage().equals("en")){
                                desc = icd10.getContent_en();
                            }else {
                                desc = icd10.getContent();
                            }
                            String[] descs = desc.split("_");
                            String name = descs[0];
                            String sp = "";
                            if (descs.length == 2) {
                                sp = descs[1];
                            }
                            ICDItem item = new ICDItem(name, sp, micd, mtji, sex);
                            itemsFromDb.add(item);
                        }
                    }

//                    List<ICDItem> itemsFromDb = dao.queryByNameAndSex(
//                            sicknessName, isMan);
                    if (rightItems != null && rightItems.size() > 0) {
                        for (ICDItem centerIcdItem : itemsFromDb) {
                            for (ICDItem rightIcdItem : rightItems) {
                                if (rightIcdItem.getName().equals(
                                        centerIcdItem.getName())) {
                                    centerIcdItem.setChoosed(true);
                                }
                            }
                        }
                    }
                    centerItems.addAll(itemsFromDb);
                    if (centeradapter == null) {
                        centeradapter = new ListCenterAdapter(
                                FtjVisceraDiseaseListActivity.this, centerItems);
                        listview_center.setAdapter(centeradapter);
                    } else {
                        centeradapter.notifyDataSetChanged();
                    }
                } else {
                    centerItems.clear();
                    if (centeradapter == null) {
                        centeradapter = new ListCenterAdapter(
                                FtjVisceraDiseaseListActivity.this, centerItems);
                        listview_center.setAdapter(centeradapter);
                    } else {
                        centeradapter.notifyDataSetChanged();
                    }
                }
            }
        });

        // 中间listview
        listview_center.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


                ICDItem item = centerItems.get(position);
                boolean isChoosed = item.isChoosed();
                if (isChoosed) {
                    item.setChoosed(!isChoosed);
                    for (ICDItem IcdItem : rightItems) {
                        if (IcdItem.getName().equals(item.getName())) {
                            itemToBeDelete = IcdItem;
                        }
                    }
                    if (itemToBeDelete != null) {
                        rightItems.remove(itemToBeDelete);
                        itemToBeDelete = null;
                    }
                    centeradapter.notifyDataSetChanged();

                } else {
                    if (rightItems.size() < 5) {
                        item.setChoosed(!isChoosed);
                        rightItems.add(item);
                        centeradapter.notifyDataSetChanged();
                    } else {
                        AlertDialogPrompt cloce = new AlertDialogPrompt(
                                FtjVisceraDiseaseListActivity.this);
                        Dialog dialogcloce = cloce.createAlartDialog(getString(R.string.vdl_tips_max_disease), "");
                        dialogcloce.show();
                        dialogcloce.setCanceledOnTouchOutside(true);
                    }
                }
                centeradapter.notifyDataSetChanged();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_DCODE_GRAINT_URI){
            updateDirectoryEntries(data.getData());
            Log.d("retrofit", "onActivityResult:Uri= "+data.getData());
        }
    }

    private static final String[] DIRECTORY_SELECTION = new String[]{
            DocumentsContract.Document.COLUMN_DISPLAY_NAME,
            DocumentsContract.Document.COLUMN_MIME_TYPE,
            DocumentsContract.Document.COLUMN_DOCUMENT_ID,
    };

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void updateDirectoryEntries(Uri uri) {
        ContentResolver contentResolver = this.getContentResolver();
        Uri docUri = DocumentsContract.buildDocumentUriUsingTree(uri, DocumentsContract.getTreeDocumentId(uri));
        Uri childrenUri = DocumentsContract.buildChildDocumentsUriUsingTree(uri, DocumentsContract.getTreeDocumentId(uri));
        try (Cursor docCursor = contentResolver .query(docUri, DIRECTORY_SELECTION, null, null, null))
        {
            while (docCursor != null && docCursor.moveToNext()) {
//                et_sickness_name.setText(docCursor.getString(docCursor.getColumnIndex( DocumentsContract.Document.COLUMN_DISPLAY_NAME)));
            }
        }
        try (Cursor childCursor = contentResolver .query(childrenUri, DIRECTORY_SELECTION, null, null, null)) {
            while (childCursor != null && childCursor.moveToNext()) {
                String fileName = childCursor.getString(childCursor.getColumnIndex( DocumentsContract.Document.COLUMN_DISPLAY_NAME));
                String mimeType = childCursor.getString(childCursor.getColumnIndex( DocumentsContract.Document.COLUMN_MIME_TYPE));
                Log.e("retrofit", "Directory: "+fileName+"\n"+mimeType);
            }
        }
    }

    @Override
    public void initDatas() {
//        openHelper = new ICD10OpenHelper(this);
//        dao = new ICD10DAO(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (rightItems != null){
            rightItems.clear();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // 已选疾病与症状列表
            case R.id.tv_disease_sure:
                Intent intent = new Intent(FtjVisceraDiseaseListActivity.this,HpiActivity.class);
//                intent.putExtra("list_left",(Serializable) list_left_data);
                intent.putExtra("list_right", (Serializable) rightItems);
                intent.putExtra("isMan",isMan);
                startActivity(intent);
                BaseApplication.addDestoryActivity(FtjVisceraDiseaseListActivity.this, "VisceraListActivity");
                break;
            default:
                break;
        }
    }

    /**
     * 检查本地是否存在该数据库
     *
     * @return 如果存在返回true，否则返回false
     */
    public boolean checkDataBase() {
        boolean isExit = false;
        String path = getDataBasePath();
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            db = SQLiteDatabase.openDatabase(path + DB_NAME, null,
                    SQLiteDatabase.OPEN_READONLY);
            cursor = db.rawQuery("select content from ICD10 limit 3", null);
            isExit = true;
        } catch (SQLiteException e) {
            isExit = false;
            // TODO: handle exception
        }
        if(cursor!=null){
            cursor.close();
        }
        if (db != null) {
            db.close();
        }
        return isExit;
    }

    private void copyDataBase() {
        try {
            InputStream myInput = this.getAssets().open(ASSETS_NAME);
            String outFileName = getDataBasePath() + DB_NAME;
            OutputStream myOutput = new FileOutputStream(outFileName);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }
            myOutput.flush();
            myOutput.close();
            myInput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public String getDataBasePath() {
        String packageName = this.getPackageName();
        return "/data/data/" + packageName + "/databases/";
    }

}
