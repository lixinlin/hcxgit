package com.hcy_futejia.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.utils.SpUtils;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.util.IsMobile;
import com.hxlm.hcyandroid.util.StatusBarUtils;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ContainsEmojiEditText;

import org.w3c.dom.Text;

/**
 * @author Administrator
 */
public class CreateAccountActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout ll_account_phone;
    private ContainsEmojiEditText et_account_phone;
    private Button bt_mobile_next;
    private Button bt_to_email;
    private LinearLayout ll_account_email;
    private ContainsEmojiEditText et_account_email;
    private Button bt_email_next;
    private Button bt_to_mobile;
    private Context context;
    private TextView tv_have_account;

    @Override
    public void setContentView() {
        StatusBarUtils.setWindowStatusBarColor(this, R.color.white);
        setContentView(R.layout.activity_create_account);
    }

    @Override
    public void initViews() {

        context = CreateAccountActivity.this;

        ll_account_phone = findViewById(R.id.ll_account_phone);
        et_account_phone = findViewById(R.id.et_account_phone);
        bt_mobile_next = findViewById(R.id.bt_mobile_next);
        bt_to_email = findViewById(R.id.bt_to_email);

        ll_account_email = findViewById(R.id.ll_account_email);
        et_account_email = findViewById(R.id.et_account_email);
        bt_email_next = findViewById(R.id.bt_email_next);
        bt_to_mobile = findViewById(R.id.bt_to_mobile);

        tv_have_account = findViewById(R.id.tv_have_account);

        bt_mobile_next.setOnClickListener(this);
        bt_email_next.setOnClickListener(this);
        bt_to_email.setOnClickListener(this);
        bt_to_mobile.setOnClickListener(this);
        tv_have_account.setOnClickListener(this);

        et_account_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s == null || s.length() == 0) {
                    bt_email_next.setBackgroundResource(R.drawable.yuanjiaojuxing);
                } else {
                    bt_email_next.setBackgroundResource(R.drawable.yuanjiaojuxing_login_blue);
                }
            }
        });

        et_account_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s == null || s.length() == 0) {
                    bt_mobile_next.setBackgroundResource(R.drawable.yuanjiaojuxing);
                } else {
                    bt_mobile_next.setBackgroundResource(R.drawable.yuanjiaojuxing_login_blue);
                }
            }
        });
    }

    @Override
    public void initDatas() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_mobile_next:
                String mobile = et_account_phone.getText().toString().trim();
                if (!TextUtils.isEmpty(mobile) ) {
                    boolean englishMobileNO = IsMobile.isEnglishMobileNO(mobile);
                    if (englishMobileNO) {
                        SpUtils.put(context,"createAccount",mobile);
                        Intent intent = new Intent(CreateAccountActivity.this, CreatePasswordActivity.class);
                        intent.putExtra("userType",1);
                        startActivity(intent);
                        BaseApplication.addDestoryActivity(CreateAccountActivity.this,"accountActivity");
                    }else{
                        ToastUtil.invokeShortTimeToast(context, getString(R.string.login_phone_format_error));
                    }
                }
                break;
            case R.id.bt_email_next:
                String email = et_account_email.getText().toString().trim();
                if (!TextUtils.isEmpty(email)) {
                    boolean email1 = IsMobile.isEmail(email);
                    if (email1) {
                        SpUtils.put(context,"createAccount",email);
                        Intent intent1 = new Intent(CreateAccountActivity.this, CreatePasswordActivity.class);
                        intent1.putExtra("userType",2);
                        startActivity(intent1);
                        BaseApplication.addDestoryActivity(CreateAccountActivity.this,"accountActivity");
                    }else{
                        ToastUtil.invokeShortTimeToast(context, getString(R.string.email_format_error));
                    }
                }
                break;
            case R.id.bt_to_email:
                ll_account_phone.setVisibility(View.GONE);
                ll_account_email.setVisibility(View.VISIBLE);
                break;
            case R.id.bt_to_mobile:
                ll_account_email.setVisibility(View.GONE);
                ll_account_phone.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_have_account:
                Intent intent = new Intent(CreateAccountActivity.this, EnglishLoginActivity.class);
                startActivity(intent);
                CreateAccountActivity.this.finish();
                break;
            default:
                break;
        }
    }
}
