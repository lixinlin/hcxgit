package com.hcy_futejia.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hcy.ky3h.R;
import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.comm.Error;
import com.hxlm.android.comm.Error_English;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.report.UploadManager;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.ChooseMemberDialog;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;
import com.hxlm.android.health.device.message.bloodpressure.BloodPressureBatteryPercentageMessage;
import com.hxlm.android.health.device.message.bloodpressure.BloodPressureCommand;
import com.hxlm.android.health.device.message.bloodpressure.BloodPressureERR;
import com.hxlm.android.health.device.message.bloodpressure.BloodPressureResponseMessage;
import com.hxlm.android.health.device.message.bloodpressure.BloodPressureValueMessage;
import com.hxlm.android.health.device.model.BloodPressureModel;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.CheckedDataType;
import com.hxlm.hcyandroid.bean.BloodPressureData;
import com.hxlm.hcyandroid.bean.CheckStep;
import com.hxlm.hcyandroid.tabbar.MyHealthFileBroadcast;
import com.hxlm.hcyandroid.util.StatusBarUtils;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyphone.MainActivity;

import java.util.List;

public class FtjBloodPressureCheckActivity extends AbstractDeviceActivity
        implements View.OnClickListener {
    private String TAG = "BluetoothBleIOSession";
    private TextView tv_MB;// 脉搏
    private TextView tv_SSY;// 收缩压
    private TextView tv_SZY;// 舒张压
    private TextView iv_up_button;// 开始检测
    private TextView iv_down_button;// 非设备检测
    private String  tv_device_connect_status = BaseApplication.getContext().getString(R.string.no_access);


    private TextView iv_xueya_restart;// 重新检测

    private List<CheckStep> steps;

    private String strssy;
    private String strszy;
    private String strmb;
    private String isNormal;// 是否正常
    private Dialog waittingDialog;

    private Context context;
    private UploadManager uploadManager;

    private int myCount = 0;//蓝牙返回数据的次数

    @Override
    public void setContentView() {

        //保持背光常亮的设置方法
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        StatusBarUtils.setWindowStatusBarColor(this,R.color.white);
        setContentView(R.layout.activity_ftj_blood_pressure_check);
        context = FtjBloodPressureCheckActivity.this;
        checkBlePermission();

    }

    /**
     * 检查蓝牙权限
     */
    public void checkBlePermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    1);
        } else {
            Log.i("tag","已申请权限");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                checkBleDevice();
            }
            ioSession = new BloodPressureModel().getIOSession(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                // 如果请求被取消，则结果数组为空。
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("tag","同意申请");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                        checkBleDevice();
                    }
                    ioSession = new BloodPressureModel().getIOSession(this);
                } else {
                    Log.i("tag","拒绝申请");
                }
                return;
            }
            default:
                break;
        }
    }

    /**
     * 判断是否支持蓝牙，并打开蓝牙
     * 获取到BluetoothAdapter之后，还需要判断是否支持蓝牙，以及蓝牙是否打开。
     * 如果没打开，需要让用户打开蓝牙：
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void checkBleDevice() {
        //首先获取BluetoothManager
        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        //获取BluetoothAdapter
        if (bluetoothManager != null) {
            BluetoothAdapter mBluetoothAdapter = bluetoothManager.getAdapter();
            if (mBluetoothAdapter != null) {
                if (!mBluetoothAdapter.isEnabled()) {
                    //调用enable()方法直接打开蓝牙
                    if (!mBluetoothAdapter.enable()){
                        Log.i("tag","蓝牙打开失败");
                    }
                    else{
                        Log.i("tag","蓝牙已打开");
                    }
                }
            } else {
                Log.i("tag","同意申请");
            }
        }
    }


    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.bpc_title_new), titleBar, 1);
        tv_MB = (TextView) findViewById(R.id.tv_MB);
        tv_SSY = (TextView) findViewById(R.id.tv_SSY);
        tv_SZY = (TextView) findViewById(R.id.tv_SZY);
        iv_up_button = findViewById(R.id.iv_up_button);
        iv_down_button = findViewById(R.id.iv_down_button);

        iv_xueya_restart = findViewById(R.id.iv_xueya_restart);

        iv_xueya_restart.setOnClickListener(this);//重新检测
        iv_up_button.setOnClickListener(this);
        iv_down_button.setOnClickListener(this);
    }


    @Override
    public void initDatas() {
        uploadManager = new UploadManager();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ioSession == null) {
            ToastUtil.invokeShortTimeToast(FtjBloodPressureCheckActivity.this, getString(R.string.bpc_lanya_init_failure));
        } else if (ioSession.status != AbstractIOSession.Status.CONNECTED) {
            ioSession.connect();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // 开始检测
            case R.id.iv_up_button:
                myCount = 0;//重新进行设置
                if (ioSession != null) {
                    if (getString(R.string.no_access).equals(tv_device_connect_status.toString())) {
                        //ToastUtil.invokeShortTimeToast(BloodPressureCheckActivity.this,"血压计设备未连接");
                        tv_device_connect_status = getString(R.string.no_access);

                    } else if (getString(R.string.bpc_connected).equals(tv_device_connect_status.toString())) {
                        BloodPressureCommand bloodPressureCommand = new BloodPressureCommand();
                        //开始测量
                        bloodPressureCommand.setCommandType(BloodPressureCommand.CommandType.Bl_PRESSURE_START_MEASUTING);
                        ioSession.sendMessage(bloodPressureCommand);
                        //开始检测不显示，重新检测显示
                        iv_up_button.setVisibility(View.GONE);
                        iv_xueya_restart.setVisibility(View.VISIBLE);
                    }

                }
                break;
            // 非设备检测
            case R.id.iv_down_button:
                startActivity(new Intent(this, FtjBloodPressureWriteActivity.class));
                break;

            // 重新检测
            case R.id.iv_xueya_restart:
                myCount = 0;//重新进行设置

                tv_MB.setText("");
                tv_SSY.setText("");
                tv_SZY.setText("");
                if (ioSession != null) {

                    if (getString(R.string.no_access).equals(tv_device_connect_status.toString())) {
                        // ToastUtil.invokeShortTimeToast(BloodPressureCheckActivity.this,"血压计设备未连接");
                        tv_device_connect_status = getString(R.string.no_access);

                    } else if (getString(R.string.bpc_connected).equals(tv_device_connect_status.toString())) {
                        BloodPressureCommand bloodPressureCommand = new BloodPressureCommand();
                        //开始测量
                        bloodPressureCommand.setCommandType(BloodPressureCommand.CommandType.Bl_PRESSURE_START_MEASUTING);
                        ioSession.sendMessage(bloodPressureCommand);
                    }
                }
                break;
            default:
                break;
        }

    }

    //计时结束提交血压数据
    private void submitBloodPressure() {

        if (ioSession != null) {
            BloodPressureCommand bloodPressureCommand = new BloodPressureCommand();
            //结束
            bloodPressureCommand.setCommandType(BloodPressureCommand.CommandType.Bl_PRESSURE_STOP_MEASUTING);
            ioSession.sendMessage(bloodPressureCommand);
        }

        // 高压收缩压
        strssy = tv_SSY.getText().toString();
        // 低压舒张压
        strszy = tv_SZY.getText().toString();

        strmb = tv_MB.getText().toString();


        if (TextUtils.isEmpty(strssy) || TextUtils.isEmpty(strszy) || TextUtils.isEmpty(strmb)) {
            ToastUtil.invokeShortTimeToast(context, getString(R.string.bpc_data_empty));
        } else if ("_".equals(strssy) || "_".equals(strszy) || "_".equals(strmb)) {
            ToastUtil.invokeShortTimeToast(context, getString(R.string.bpc_data_empty));

        }
        // 如果高压不能超过250，低压不能超过150，脉搏不能超过150
        else {
            if (Integer.parseInt(strssy) > 250) {
                ToastUtil.invokeShortTimeToast(context, getString(R.string.bpc_range_gaoya));
            } else if (Integer.parseInt(strszy) > 150) {
                ToastUtil.invokeShortTimeToast(context, getString(R.string.bpc_range_diya));
            } else if (Integer.parseInt(strmb) > 150) {
                ToastUtil.invokeShortTimeToast(context, getString(R.string.bpc_range_maibo));
            } else {

                // 高压收缩压
                int intssy = Integer.parseInt(strssy);
                // 低压舒张压
                int intszy = Integer.parseInt(strszy);

                if ((intssy >= 180 && intszy >= 110)
                        || (intssy >= 180 || intszy >= 110)) {
                    isNormal = getString(R.string.bpc_isNormal1);


                } else if (((intssy >= 160 && intssy <= 179) && (intszy >= 100 && intszy <= 109))
                        || ((intssy >= 160 && intssy <= 179) || (intszy >= 100 && intszy <= 109))) {

                    isNormal = getString(R.string.bpc_isNormal2);

//
                } else if (((intssy >= 140 && intssy <= 159) && (intszy >= 90 && intszy <= 99))
                        || ((intssy >= 140 && intssy <= 159) || (intszy >= 90 && intszy <= 99))) {

                    isNormal = getString(R.string.bpc_isNormal3);


                } else if (((intssy >= 120 && intssy <= 139) && (intszy >= 80 && intszy <= 89))
                        || ((intssy >= 120 && intssy <= 139) || (intszy >= 80 && intszy <= 89))) {
                    isNormal = getString(R.string.bpc_isNormal4);


                } else if ((intssy >= 90 && intssy < 120)
                        && (intszy >= 60 && intszy < 80)) {
                    isNormal = getString(R.string.bpc_isNormal5);


                } else if ((intssy > 0 && intssy < 90)
                        && (intszy > 0 && intszy < 60)
                        || ((intssy > 0 && intssy < 90) || (intszy > 0 && intszy < 60))) {
                    isNormal = getString(R.string.bpc_isNormal6);


                } else if (((intssy >= 120 && intssy <= 139) && (intszy >= 80 && intszy <= 89))
                        || ((intssy >= 120 && intssy <= 139) || (intszy >= 80 && intszy <= 89))) {
                    isNormal = getString(R.string.bpc_isNormal4);


                } else if ((intssy >= 90 && intssy < 120)
                        && (intszy >= 60 && intszy < 80)) {
                    isNormal = getString(R.string.bpc_isNormal5);


                } else {
                    if ((intssy > 0 && intssy < 90)
                            && (intszy > 0 && intszy < 60)
                            || ((intssy > 0 && intssy < 90) || (intszy > 0 && intszy < 60))) {
                        isNormal = getString(R.string.bpc_isNormal6);

                    }
                }


                LoginControllor.requestLogin(FtjBloodPressureCheckActivity.this, new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        List<ChildMember> childMembers = LoginControllor.getLoginMember().getMengberchild();
                        int size=childMembers.size();
                        if(true){
                            final BloodPressureData bloodPressureData = new BloodPressureData();
                            bloodPressureData.setHighPressure(strssy);
                            bloodPressureData.setLowPressure(strszy);
                            bloodPressureData.setPulse(strmb);

                            // 输入正常值才上传数据
                            uploadManager.uploadCheckedData(CheckedDataType.BLOOD_PRESSURE, bloodPressureData, 0,
                                    new AbstractDefaultHttpHandlerCallback(FtjBloodPressureCheckActivity.this) {
                                        @Override
                                        protected void onResponseSuccess(Object obj) {
                                            FtjBloodPressureCheckActivity.BloodPressureDialog bloodPressureDialog = new FtjBloodPressureCheckActivity.BloodPressureDialog(FtjBloodPressureCheckActivity.this, getString(R.string.bpw_dialog_text_dangqianmaibo) + strmb
                                                    + getString(R.string.bpc_unit_count), strssy + "mmHg", strszy + "mmHg", isNormal);
                                            bloodPressureDialog.setCanceledOnTouchOutside(false);
                                            bloodPressureDialog.show();

                                        }
                                    });
                        }else{
                            ChooseMemberDialog chooseMemberDialog = new ChooseMemberDialog(FtjBloodPressureCheckActivity.this, new OnCompleteListener() {
                                @Override
                                public void onComplete() {
                                    final BloodPressureData bloodPressureData = new BloodPressureData();
                                    bloodPressureData.setHighPressure(strssy);
                                    bloodPressureData.setLowPressure(strszy);
                                    bloodPressureData.setPulse(strmb);

                                    // 输入正常值才上传数据
                                    uploadManager.uploadCheckedData(CheckedDataType.BLOOD_PRESSURE, bloodPressureData, 0,
                                            new AbstractDefaultHttpHandlerCallback(FtjBloodPressureCheckActivity.this) {
                                                @Override
                                                protected void onResponseSuccess(Object obj) {
                                                    FtjBloodPressureCheckActivity.BloodPressureDialog bloodPressureDialog = new FtjBloodPressureCheckActivity.BloodPressureDialog(FtjBloodPressureCheckActivity.this, getString(R.string.bpw_dialog_text_dangqianmaibo) + strmb
                                                            + getString(R.string.bpc_unit_count), strssy + "mmHg", strszy + "mmHg", isNormal);
                                                    bloodPressureDialog.setCanceledOnTouchOutside(false);
                                                    bloodPressureDialog.show();

                                                }
                                            });
                                }
                            });
                            chooseMemberDialog.show();
                        }

                    }
                });
            }
        }
    }

    @Override
    public void onConnectFailed(Error error) {
        Toast.makeText(this, error.getDesc(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onConnectFailedEnglist(Error_English error_english) {
        Toast.makeText(this, error_english.getDesc(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onConnected() {
        // ToastUtil.invokeShortTimeToast(BloodPressureCheckActivity.this, "设备已连接");
        tv_device_connect_status = getString(R.string.bpc_connected);

    }

    @Override
    protected void onDisconnected() {
        //ToastUtil.invokeShortTimeToast(BloodPressureCheckActivity.this, "设备已断开");
        tv_device_connect_status = getString(R.string.no_access);

        iv_up_button.setVisibility(View.VISIBLE);// 开始检测
        iv_xueya_restart.setVisibility(View.GONE);// 重新检测
    }

    @Override
    protected void onExceptionCaught(Throwable e) {
        Log.e(TAG, e.getMessage());

    }

    // 接收设备返回的数据
    @Override
    protected void onMessageReceived(AbstractMessage message) {

        Log.d(TAG, "message received..........." + message.toString());

        switch ((HealthDeviceMessageType) message.getMessageType()) {
            // 压力值数据
            case CHECKSUM_ERROR:
                break;
            case BOARD_LEVEL_COMMAND:
                break;
            case BOARD_COMMAND_RESPONSE:
                break;
            case CHAIR_COMMAND:
                break;
            case CHAIR_COMMAND_RESPONSE:
                break;
            case ECG_DATA:
                break;
            case ECG_WAVE:
                break;
            case ECG_SIGNAL_GAIN_COMMAND:
                break;
            case ECG_WAVE_OUTPUT_COMMAND:
                break;
            case ECG_DATA_OUTPUT_COMMAND:
                break;
            case ECG_FILTER_TYPE_COMMAND:
                break;
            case SPO2_DATA:
                break;
            case SPO2_DATA_OUTPUT_COMMAND:
                break;
            case SPO2_WAVE_OUTPUT_COMMAND:
                break;
            case TEMPERATURE_DATA_OUTPUT_COMMAND:
                break;
            case TEMPERATURE_DATA:
                break;
            case RESPIRATORY_DATA_OUTPUT_COMMAND:
                break;
            case RESPIRATORY_SIGNAL_GAIN_COMMAND:
                break;
            case BLOOD_PRESSURE_COMMAND:
                break;
            case BLOOD_PRESSURE_VALUE:
                BloodPressureValueMessage bloodPressureValue = (BloodPressureValueMessage) message;
                //ToastUtil.invokeShortTimeToast(context, "压力值数据-->" + bloodPressureValue.getPressureValue());
                break;
            // 血压计电量百分比
            case BLOOD_PRESSURE_BATTERY_PERCENTAGE:
                BloodPressureBatteryPercentageMessage batteryPercentageMessage = (BloodPressureBatteryPercentageMessage) message;
                // ToastUtil.invokeShortTimeToast(context, "血压计电量百分比-->" + batteryPercentageMessage.getBatteryPercentage());

                break;
            // 结果数据格式
            case BLOOD_PRESSURE_RESPONSE:
                BloodPressureResponseMessage bloodPressureResponseMessage = (BloodPressureResponseMessage) message;
                tv_SSY.setText(String.valueOf(bloodPressureResponseMessage.getSystol_ic()));//收缩压
                tv_SZY.setText(String.valueOf(bloodPressureResponseMessage.getDiastol_ic()));//舒张压
                tv_MB.setText(String.valueOf(bloodPressureResponseMessage.getPulse_rate()));//心率

//                count++;
//                text_cishu.setText("--->"+count);
//
//
//                //--------------------------每隔30分钟开启测试
//                if (ioSession != null) {
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            BloodPressureCommand bloodPressureCommand = new BloodPressureCommand();
//                            //开始测量
//                            bloodPressureCommand.setCommandType(BloodPressureCommand.CommandType.Bl_PRESSURE_START_MEASUTING);
//                            ioSession.sendMessage(bloodPressureCommand);
//                        }
//                    },30*60*1000);
//                }


                myCount++;
                if (myCount == 1) {

                    //计时结束之后，提交数据
                    submitBloodPressure();
                }

                break;
            // 测量失败返回数据
            case BLOOD_PRESSURE_ERR:
                BloodPressureERR bloodPressureERR = (BloodPressureERR) message;
                int bit5 = bloodPressureERR.getErr();
                switch (bit5) {
                    case 1:
                        ToastUtil.invokeLongTimeToast(FtjBloodPressureCheckActivity.this, getString(R.string.bpc_tips_exception));
                        break;
                    case 2:
                        ToastUtil.invokeLongTimeToast(FtjBloodPressureCheckActivity.this, getString(R.string.bpc_tips_not_measure));
                        break;
                    case 3:
                        ToastUtil.invokeLongTimeToast(FtjBloodPressureCheckActivity.this, getString(R.string.bpc_tips_measure_exception));
                        break;
                    case 4:
                        ToastUtil.invokeLongTimeToast(FtjBloodPressureCheckActivity.this, getString(R.string.bpc_tips_guosong_or_louqi));
                        break;
                    case 5:
                        ToastUtil.invokeLongTimeToast(FtjBloodPressureCheckActivity.this, getString(R.string.bpc_tips_guojin_or_zuse));
                        break;
                    case 6:
                        ToastUtil.invokeLongTimeToast(FtjBloodPressureCheckActivity.this, getString(R.string.bpc_tips_ganraoyanzhong));
                        break;
                    case 7:
                        ToastUtil.invokeLongTimeToast(FtjBloodPressureCheckActivity.this, getString(R.string.bpc_tips_yali));
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public class BloodPressureDialog extends AlertDialog implements
            View.OnClickListener {

        Context context;
        String mb;
        String ssy;
        String szy;
        String isnormal;

        String str_tishi = "";

        public BloodPressureDialog(Context context, String mb, String ssy,
                                   String szy, String isnormal) {
            super(context);
            this.context = context;
            this.mb = mb;
            this.ssy = ssy;
            this.szy = szy;
            this.isnormal = isnormal;

        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.bloodpressure_submit_prompt);

            TextView tv_breath_prompt_tishi_mb = (TextView) findViewById(R.id.tv_breath_prompt_tishi_mb);
            tv_breath_prompt_tishi_mb.setText(mb);

            TextView tv_breath_prompt_tishi_ssy = (TextView) findViewById(R.id.tv_breath_prompt_tishi_ssy);
            tv_breath_prompt_tishi_ssy.setText(ssy);

            TextView tv_breath_prompt_tishi_szy = (TextView) findViewById(R.id.tv_breath_prompt_tishi_szy);
            tv_breath_prompt_tishi_szy.setText(szy);


            TextView tv_is_normal = (TextView) findViewById(R.id.tv_is_normal);
            // tv_is_normal.setText("脉搏：60－100次/分");

            //显示高压范围
            TextView text_gaoya = (TextView) findViewById(R.id.text_gaoya);
            text_gaoya.setText("90 < "+getString(R.string.bpw_text_range_gaoya)+"< 140");
            //显示低压范围
            TextView text_diya = (TextView) findViewById(R.id.text_diya);
            text_diya.setText("60 < "+getString(R.string.bpw_text_range_diya)+"< 90");

            // 返回检测
            TextView text_back = (TextView) findViewById(R.id.text_back);
            text_back.setOnClickListener(this);

            //查看档案
            TextView text_see_dangan = (TextView) findViewById(R.id.text_see_dangan);
            text_see_dangan.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                // 返回检测
                case R.id.text_back:
                    this.dismiss();
                    iv_up_button.setVisibility(View.VISIBLE);// 开始检测
                    iv_xueya_restart.setVisibility(View.GONE);// 重新检测

                    break;
                //查看档案
                case R.id.text_see_dangan:
                    this.dismiss();

                    iv_up_button.setVisibility(View.VISIBLE);// 开始检测
                    iv_xueya_restart.setVisibility(View.GONE);// 重新检测

                    // 动态注册广播使用隐士Intent
                    Intent intent = new Intent(MyHealthFileBroadcast.ACTION);
                    intent.putExtra("ArchivesFragment", "3");
                    intent.putExtra("otherReport", "true");
                    intent.putExtra("Jump", 6);
                    FtjBloodPressureCheckActivity.this.sendBroadcast(intent);

                    Intent intent2 = new Intent(FtjBloodPressureCheckActivity.this, MainActivity.class);

                    intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//清除MainActivity之前所有的activity
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP); //沿用之前的MainActivity
                    startActivity(intent2);
                    break;

            }
        }

    }
}
