package com.hcy_futejia.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.bean.MyConsultation;
import com.hxlm.hcyandroid.bean.ReplyUserConsultation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Lirong
 * @date 2019/5/16.
 * @description   我的咨询  适配器
 */

public class FtjMyConsultationAdapter extends BaseAdapter{

    private Context context;
    private List<MyConsultation> consultationList;

    public FtjMyConsultationAdapter(Context context, List<MyConsultation> consultations) {
        this.context = context;
        this.consultationList = consultations;
    }

    @Override
    public int getCount() {
        return consultationList.size();
    }

    @Override
    public Object getItem(int position) {
        return consultationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_ftj_my_consultation, null);
            holder.tv_content = convertView.findViewById(R.id.tv_content);
            holder.tv_date = convertView.findViewById(R.id.tv_date);
            holder.tv_reply = convertView.findViewById(R.id.tv_reply);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        MyConsultation myConsultation = consultationList.get(position);
        holder.tv_content.setText(myConsultation.getContent());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        holder.tv_date.setText(format.format(new Date(myConsultation.getCreateDate())));
        ReplyUserConsultation replyUserConsultation = myConsultation.getReplyUserConsultations();
        if (replyUserConsultation == null) {
            holder.tv_reply.setText( context.getString(R.string.consult_weihuifu));
            holder.tv_reply.setTextColor(Color.parseColor("#8e8e93"));
        } else {
            holder.tv_reply.setText(context.getString(R.string.consult_yihuifu));
            holder.tv_reply.setTextColor(Color.parseColor("#d81e06"));
        }

        return convertView;
    }

    class ViewHolder{
        TextView tv_content;
        TextView tv_date;
        TextView tv_reply;
    }
}
