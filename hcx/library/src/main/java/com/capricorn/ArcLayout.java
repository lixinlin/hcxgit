/*
 * Copyright (C) 2012 Capricorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.capricorn;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

/**
 * A Layout that arranges its children around its center. The arc can be set by
 * calling {@link #setArc(float, float) setArc()}. You can override the method
 * {@link #onMeasure(int, int) onMeasure()}, otherwise it is always
 * WRAP_CONTENT.
 * 
 * @author Capricorn
 * 
 */
public class ArcLayout extends ViewGroup {
	private final static String TAG = "ArcLayout";
	/**
	 * children will be set the same size.
	 */
	private int mChildSize;

	private int mChildPadding = 10;

	private int mLayoutPadding = 0;

	public static float DEFAULT_FROM_DEGREES = 270.0f;

	public static float DEFAULT_TO_DEGREES = 360.0f;

	private float mFromDegrees = DEFAULT_FROM_DEGREES;

	private float mToDegrees = DEFAULT_TO_DEGREES;

	private static int MIN_RADIUS = 70;

	/* the distance between the layout's center and any child's center */
	private int mRadius;

	private static float TIMES = 1.55f;

	private boolean mExpanded = false;

	private boolean isLayoutEnded = false;

	public ArcLayout(Context context) {
		super(context);
	}

	public ArcLayout(Context context, AttributeSet attrs) {
		super(context, attrs);

		if (attrs != null) {
			TypedArray a = getContext().obtainStyledAttributes(attrs,
					R.styleable.ArcLayout, 0, 0);
			mFromDegrees = a.getFloat(R.styleable.ArcLayout_fromDegrees,
					DEFAULT_FROM_DEGREES);
			mToDegrees = a.getFloat(R.styleable.ArcLayout_toDegrees,
					DEFAULT_TO_DEGREES);
			mChildSize = Math
					.max(a.getDimensionPixelSize(
							R.styleable.ArcLayout_childSize, 0), 0);

			a.recycle();
		}
	}

	private static int computeRadius(float arcDegrees, int childCount,
			int childSize, int childPadding, int minRadius) {
		if (childCount < 2) {
			return minRadius;
		}

		float perDegrees = arcDegrees / childCount;
		float perHalfDegrees = perDegrees / 2;
		int perSize = childSize + childPadding;

		int radius = (int) ((perSize / 2) / Math.sin(Math
				.toRadians(perHalfDegrees)));

		return Math.max(radius, minRadius);
	}

	private static Rect computeChildFrame(int centerX, int centerY, int radius,
			float degrees, int size) {

		double childCenterX = centerX + radius
				* Math.cos(Math.toRadians(degrees));
		double childCenterY = centerY + radius
				* Math.sin(Math.toRadians(degrees));

		return new Rect((int) (childCenterX - size / 2),
				(int) (childCenterY - size / 2),
				(int) (childCenterX + size / 2),
				(int) (childCenterY + size / 2));
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int radius = mRadius = computeRadius(
				Math.abs(mToDegrees - mFromDegrees), getChildCount(),
				mChildSize, mChildPadding, MIN_RADIUS);

		int size = radius * 2 + mChildSize + mChildPadding + mLayoutPadding * 2;
		Log.d(TAG, "the ChildCount is " + getChildCount() );
		Log.d(TAG, "the layout size is " + size);

		int count = getChildCount();
		for (int i = 0; i < count; i++) {
			getChildAt(i)
					.measure(
							MeasureSpec.makeMeasureSpec(mChildSize,
									MeasureSpec.EXACTLY),
							MeasureSpec.makeMeasureSpec(mChildSize,
									MeasureSpec.EXACTLY));
		}
		Log.d(TAG, "mesure is over.");

		setMeasuredDimension(size, size);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {

		int centerX = getWidth() / 2;
		int centerY = getHeight() / 2;
		int radius = mExpanded ? mRadius : 0;

		int childCount = getChildCount();
		float perDegrees = (mToDegrees - mFromDegrees) / childCount;

		float degrees = mFromDegrees;
//		if(!isLayoutEnded) {

			for (int i = 0; i < childCount; i++) {
				Rect frame = computeChildFrame(centerX, centerY, radius, degrees,
						mChildSize);
				degrees += perDegrees;
				getChildAt(i).layout(frame.left, frame.top, frame.right,
						frame.bottom);
			}

//			isLayoutEnded = true;
//		}
	}

	// �Ŵ󶯻�
	private static Animation createExpandAnimation(float fromXDelta,
			float toXDelta, float fromYDelta, float toYDelta, long duration) {

		Animation animation = new TranslateAnimation(fromXDelta, toXDelta,
				fromYDelta, toYDelta);
		animation.setDuration(duration);
		animation.setFillAfter(true);

		return animation;
	}

	// ��������
	private static Animation createShrinkAnimation(float fromXDelta,
			float toXDelta, float fromYDelta, float toYDelta, long duration) {

		long preDuration = duration / 2;

		Animation translateAnimation = new TranslateAnimation(fromXDelta, toXDelta, fromYDelta,
				toYDelta);
		translateAnimation.setDuration(duration - preDuration);
		translateAnimation.setFillAfter(true);

		return translateAnimation;
	}

	@SuppressLint("NewApi")
	private void bindChildAnimation(View child, int index, long duration) {

		boolean expanded = mExpanded;
		int centerX = getWidth() / 2;
		int centerY = getHeight() / 2;
		int radius = expanded ? 0 : mRadius;

		int childCount = getChildCount();
		float perDegrees = (mToDegrees - mFromDegrees) / childCount;

		Rect frame = computeChildFrame(centerX, centerY, radius, mFromDegrees
				+ index * perDegrees, mChildSize);

		int toXDelta = frame.left - child.getLeft();
		int toYDelta = frame.top - child.getTop();

		Animation animation = mExpanded ? createShrinkAnimation(0, toXDelta, 0,
				toYDelta, duration) : createExpandAnimation(0, toXDelta, 0,
				toYDelta, duration);

		animation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				onAllAnimationsEnd();
			}
		});

		if (mExpanded) {
			child.setVisibility(View.INVISIBLE);
		} else {
			child.setVisibility(View.VISIBLE);
		}
		child.setAnimation(animation);
	}

	public boolean isExpanded() {
		return mExpanded;
	}

	public void setArc(float fromDegrees, float toDegrees) {
		if (mFromDegrees == fromDegrees && mToDegrees == toDegrees) {
			return;
		}

		mFromDegrees = fromDegrees;
		mToDegrees = toDegrees;

		requestLayout();
	}

	public void setChildSize(int size) {
		if (mChildSize == size || size < 0) {
			return;
		}

		mChildSize = size;

		requestLayout();
	}

	public int getChildSize() {
		return mChildSize;
	}

	/**
	 * switch between expansion and shrinkage
	 * 
	 * @param circleNormal
	 */
	public void switchState(ImageView circleNormal) {
		if (mExpanded) {
			Log.i("Log.i","缩小");
			//缩小
			ScaleAnimation scaleAnimation = new ScaleAnimation(TIMES, 1.0f,
					TIMES, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f,
					Animation.RELATIVE_TO_SELF, 0.5f);
			scaleAnimation.setDuration(200);
			scaleAnimation.setFillAfter(true);
			circleNormal.startAnimation(scaleAnimation);
		} else {
			Log.i("Log.i","放大");
           //放大
			ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, TIMES,
					1.0f, TIMES, Animation.RELATIVE_TO_SELF, 0.5f,
					Animation.RELATIVE_TO_SELF, 0.5f);
			scaleAnimation.setDuration(200);
			scaleAnimation.setFillAfter(true);
			circleNormal.startAnimation(scaleAnimation);
		}

		int childCount = getChildCount();
		for (int i = 0; i < childCount; i++) {
			bindChildAnimation(getChildAt(i), i, 300);
		}

		mExpanded = !mExpanded;

		invalidate();
	}

	/**
	 * switch between expansion and shrinkage
	 *
	 * @param circleNormal
	 */
	public void switchState(ImageView circleNormal,int index,ImageView mHintView,ImageView mHintViewJun) {
		if (mExpanded) {
			//mHintView.setVisibility(View.VISIBLE);
			//mHintViewJun.setVisibility(View.INVISIBLE);
			//缩小
			ScaleAnimation scaleAnimation = new ScaleAnimation(TIMES, 1.0f,
					TIMES, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f,
					Animation.RELATIVE_TO_SELF, 0.5f);
			scaleAnimation.setDuration(200);
			scaleAnimation.setFillAfter(true);
			circleNormal.startAnimation(scaleAnimation);

		} else {
			//mHintView.setVisibility(View.INVISIBLE);
			//mHintViewJun.setVisibility(View.VISIBLE);
			//放大
			ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, TIMES,
					1.0f, TIMES, Animation.RELATIVE_TO_SELF, 0.5f,
					Animation.RELATIVE_TO_SELF, 0.5f);
			scaleAnimation.setDuration(200);
			scaleAnimation.setFillAfter(true);
			circleNormal.startAnimation(scaleAnimation);


		}

		int childCount = getChildCount();
		for (int i = 0; i < childCount; i++) {
			bindChildAnimation(getChildAt(i), i, 300);
		}

		mExpanded = !mExpanded;

		invalidate();
	}


	//得到当前boolean值
	public boolean getmExpanded()
	{
		return mExpanded;
	}

	private void onAllAnimationsEnd() {
		int childCount = getChildCount();
		for (int i = 0; i < childCount; i++) {
			getChildAt(i).clearAnimation();
		}

		requestLayout();
	}

	public void close(boolean showAnimation, ImageView circleNormal) {

		if (mExpanded) {

			if (showAnimation) {
				int childCount = getChildCount();
				for (int i = 0; i < childCount; i++) {
					bindChildAnimation(getChildAt(i), i, 300);
				}
			}

			ScaleAnimation scaleAnimation = new ScaleAnimation(TIMES, 1.0f,
					TIMES, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f,
					Animation.RELATIVE_TO_SELF, 0.5f);
			scaleAnimation.setDuration(200);
			scaleAnimation.setFillAfter(true);
			circleNormal.startAnimation(scaleAnimation);

			mExpanded = !mExpanded;
		}

	}

}
