package com.hxlm.android.comm;

import android.util.Log;
import android.widget.Toast;
import com.hxlm.android.utils.ByteUtil;
import com.hxlm.android.utils.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 协议处理抽象类。
 * <p/>
 * Created by Zhenyu on 2015/11/23.
 */
public abstract class AbstractCodec {
    private final static int BUFFER_SIZE = 512;

    protected final byte[] readPacketHeader;
    protected final byte[] writePackHeader;
    private final int minContentLength; //最小处理包体长度，避免子类方法处理时出现异常
    private final int maxContentLength; //最大处理包体长度，避免数据包出现异常时，不处理数据

    public final ReentrantLock lock;

    protected byte[] dataBuffer;//缓冲区初始长度为512，所有操作均在此buffer中进行
    public int startIndex = 0;  //未处理数据在缓冲字节数组中的起点位置
    public int endIndex = 0;  //已接收数据在缓冲字节数组中的终点位置

    //protected final String tag = this.getClass().getSimpleName();
    protected final String tag = "UsbSerialIOSession";

    /**
     * 构造函数，针对发送和接收包头一致的情况，只需要输入一个包头
     *
     * @param packetHeader      包头字节数据
     * @param aMinContentLength 最小包长度，如果收到的数据小于这个长度，不进行处理
     * @param aMaxContentLength 最大包长度，如果包中的长度标识超过此长度，认为数据出现错误
     */
    public AbstractCodec(final byte[] packetHeader, final int aMinContentLength,
                         final int aMaxContentLength) {
        this.readPacketHeader = packetHeader;
        this.writePackHeader = packetHeader;
        this.minContentLength = aMinContentLength;
        this.maxContentLength = aMaxContentLength;

        dataBuffer = new byte[BUFFER_SIZE];
        lock = new ReentrantLock(false);
    }

    /**
     * 构造函数，针对发送和接收包头不同的情况，只需要输入接收数据包头和发送数据包头
     *
     * @param readPacketHeader  接收数据包头字节数据
     * @param writePackHeader   发送数据包头字节数据
     * @param aMinContentLength 最小包长度，如果收到的数据小于这个长度，不进行处理
     * @param aMaxContentLength 最大包长度，如果包中的长度标识超过此长度，认为数据出现错误
     */
    public AbstractCodec(byte[] readPacketHeader, byte[] writePackHeader,
                         int aMinContentLength, int aMaxContentLength) {
        this.readPacketHeader = readPacketHeader;
        this.writePackHeader = writePackHeader;
        this.minContentLength = aMinContentLength;
        this.maxContentLength = aMaxContentLength;

        dataBuffer = new byte[BUFFER_SIZE];
        lock = new ReentrantLock(false);
    }

    /**
     * 将消息最终封装成设备识别的十六进制包， 此方法有个基本使用前提，数据包的基本
     * 结构需要为： 包头 + （包体 = 包体长度 + 命令字节 + 传输内容 + 校验和）
     *
     * @param message 待封装的消息
     */
    protected byte[] getPacket(final AbstractMessage message) {
        byte[] parketBody = encodeMessage(message);// 数据包内容

        byte[] packet = new byte[writePackHeader.length + parketBody.length];
        // 拷贝包头到packet数组
        System.arraycopy(writePackHeader, 0, packet, 0, writePackHeader.length);
        // 拷贝数据包内容到packet数组
        System.arraycopy(parketBody, 0, packet, writePackHeader.length, parketBody.length);

        return packet;
    }

    /**
     * 对输入的byte数据进行解析，如果发现包头，则将包头后的内容提交内部抽象接口由实例化的子类进行处理。
     *
     * @param data       设备传来的新数据
     * @param dataLength 新数据在缓冲字节数组中的长度
     * @return Collection<AbstractMessage> 解析出的消息集合
     */
   public Collection<AbstractMessage> getMessages(final byte[] data, final int dataLength) throws InterruptedException {

        final ReentrantLock lock = this.lock;
        lock.lockInterruptibly();

        try {
            ArrayList<AbstractMessage> messages = null;
            writeToBuffer(data, dataLength);//缓冲



            //如果剩余未处理的字节数大于（包头 + 最小包体长度），才进行处理，要保证最小包体长度设置可以取到包长度字节。
            while ((endIndex - startIndex) >= (readPacketHeader.length + minContentLength)) {

                if (isPacketHeader(startIndex)) {

                    int bodyLength = getBodyLength(startIndex + readPacketHeader.length);


                    // 如果获得的包长度大于定义的最大包长度，数据应该是出现了错误，略过此次的包头继续处理
                    if (bodyLength < maxContentLength) {
                        // 只有当剩余数据包含一个完整消息包时，才进行处理；否则留待下次处理
                        if (startIndex + readPacketHeader.length + bodyLength <= endIndex) {
                            AbstractMessage message = decodeMessage(startIndex + readPacketHeader.length);
                            if (message != null) {
                                if (messages == null) {
                                    messages = new ArrayList<>();
                                }
                                messages.add(message);
                            }
                            startIndex += readPacketHeader.length + bodyLength;
                        } else {
                            // 如果剩余数据没有一个完整数据包，中断处理过程
                            break;
                        }
                    } else {
                        // 如果获得的包长度大于定义的最大包长度，数据应该是出现了错误，略过此次的包头继续处理
                        startIndex += readPacketHeader.length;
                    }
                } else {
                    // 如果不是包头，则向前移动一位
                    startIndex++;
                }

            }

            return messages;
        } finally {
            lock.unlock();
        }
    }

    /**
     * 将新数据写入缓冲，等待进行处理。
     * 缓冲策略： 1.如果（进入的字节数+未处理字节数）大于（缓冲区总长度），给缓冲区扩容，并把内容拷贝到缓冲区。
     * 2.如果（进入的字节数+现在处理指针位置+未处理字节数）大于（缓冲区总长度），则将剩余字节移到缓 冲区头部，然后拷入新字节
     * 3.如果无需进行调整，则将新字节直接放入原有数据尾部。
     *
     * @param data       从设备获得的新数据
     * @param dataLength 新数据在缓冲字节数组中的长度
     */
    public void writeToBuffer(final byte[] data, final int dataLength) {
        int remainDataLength = endIndex - startIndex;

        Logger.d(tag, "remain Data Length = " + remainDataLength + "; new Data Length = " + dataLength);
        Logger.d(tag, "data = " + ByteUtil.bytesToHexString(dataBuffer, startIndex, remainDataLength)
                + " | " + ByteUtil.bytesToHexString(data, 0, dataLength));

        if (remainDataLength + dataLength > dataBuffer.length) {
            //如果（进入的字节数+未处理字节数）大于（缓冲区总长度），给缓冲区扩容，并把内容拷贝到缓冲区。
            Logger.d(tag, "Enlarge the data buffer.");

            byte[] newDataBuffer = new byte[remainDataLength + dataLength];

            System.arraycopy(dataBuffer, startIndex, newDataBuffer, 0, remainDataLength);
            System.arraycopy(data, 0, newDataBuffer, remainDataLength, dataLength);//内容拷贝到缓冲区。

            dataBuffer = newDataBuffer;
            endIndex = remainDataLength + dataLength;
            startIndex = 0;
        } else if (startIndex + remainDataLength + dataLength > dataBuffer.length) {
            //如果（进入的字节数+现在处理指针位置+未处理字节数）大于（缓冲区总长度），则将剩余字节移到缓 冲区头部，然后拷入新字节
            Logger.d(tag, "Reuse the data buffer.");

            System.arraycopy(dataBuffer, startIndex, dataBuffer, 0, remainDataLength);
            System.arraycopy(data, 0, dataBuffer, remainDataLength, dataLength);

            endIndex = remainDataLength + dataLength;
            startIndex = 0;
        } else {
            //如果无需进行调整，则将新字节直接放入原有数据尾部。
            System.arraycopy(data, 0, dataBuffer, endIndex, dataLength);
            endIndex += dataLength;
        }
    }

    /**
     * 检查某一字节数组指定位置开始的连续字节是否与指定的消息头字节数组相等。
     *
     * @param startIndex 本次处理在字节数组的起始位置
     */
    public boolean isPacketHeader(int startIndex) {

        for (int i = 0; i < readPacketHeader.length; i++) {
            if (readPacketHeader[i] != dataBuffer[startIndex + i]) {
                Logger.i(tag, "未检测到包头！startIndex = " + startIndex);
                return false;
            }
        }
        return true;
    }

    /**
     * 获得dataBuffer中指定位置开始、指定长度的校验和的值  求和
     *
     * @param data   计算校验和的byte数组
     * @param index  起始位置
     * @param length 计算长度
     * @return 校验和结果
     */
    protected int getCheckSumByteAdd(final byte[] data, final int index, final int length) {

        int sum = 0;
        for (int i = index; i < (index + length); i++) {
            sum += (data[i] & 0x000000FF);  // 确保没有负数
        }
        return sum;
    }

    /**
     * 获得dataBuffer中指定位置开始、指定长度的校验和的值  求和按位取反
     *
     * @param data   计算校验和的byte数组
     * @param index  起始位置
     * @param length 计算长度
     * @return 校验和结果
     */
    protected byte getCheckSumByte(final byte[] data, final int index, final int length) {
        int sum = 0;
        for (int i = index; i < (index + length); i++) {
            sum += (data[i] & 0x000000FF);  // 确保没有负数
        }

        return (byte) ((~sum) & 0xFF);
    }

    /**
     * 获得dataBuffer中指定位置开始、指定长度的校验和的值，进行异或校验
     *
     * @param data   计算校验和的byte数组
     * @param index  起始位置
     * @param length 计算长度
     * @return 校验和结果
     */
    protected byte getCheckSumByteXOR(final byte[] data, final int index, final int length) {
        //异或校验
        byte xor = 0x0;
        for (int i = index; i < (index + length); i++) {
            xor ^= data[i];
        }
        return xor;
    }

    /**
     * 获得消息包长度，输入消息体的起始位置，便于根据不同的协议格式获得包长度。
     *
     * @param bodyStartIndex 消息体在字节数组里的起始位置
     */
    protected abstract int getBodyLength(int bodyStartIndex);

    /**
     * 解析实际的协议内容，不需要再处理包头，从包体处理即可。
     *
     * @param bodyStartIndex 消息体在字节数组里的起始位置
     */
    protected abstract AbstractMessage decodeMessage(final int bodyStartIndex) throws InterruptedException;

    /**
     * 按照不同的协议要求封装消息内容
     *
     * @param message 待封装的消息
     */
    protected abstract byte[] encodeMessage(final AbstractMessage message);
}
