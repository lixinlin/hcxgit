package com.hxlm.android.health.device.message.bodytempergun;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * Created by l on 2016/12/26.
 * 体温枪接收设备返回数据
 */
public class BodyTemperGunMessage extends AbstractMessage {

    private int bodyTemperNum1;//温度数值第一位
    private int bodyTemperNum2;//温度数值第二位
    private int bodyTemperNum3;//温度数值第三位
    private int bodyTemperNum4;//温度数值第四位
    public BodyTemperGunMessage() {

        super(HealthDeviceMessageType.BODYTEMPERGUN_COMMAND_RESPONSE);
    }


    public int getBodyTemperNum4() {
        return bodyTemperNum4;
    }

    public void setBodyTemperNum4(int bodyTemperNum4) {
        this.bodyTemperNum4 = bodyTemperNum4;
    }

    public int getBodyTemperNum1() {
        return bodyTemperNum1;
    }

    public void setBodyTemperNum1(int bodyTemperNum1) {
        this.bodyTemperNum1 = bodyTemperNum1;
    }

    public int getBodyTemperNum2() {
        return bodyTemperNum2;
    }

    public void setBodyTemperNum2(int bodyTemperNum2) {
        this.bodyTemperNum2 = bodyTemperNum2;
    }

    public int getBodyTemperNum3() {
        return bodyTemperNum3;
    }

    public void setBodyTemperNum3(int bodyTemperNum3) {
        this.bodyTemperNum3 = bodyTemperNum3;
    }

    @Override
    public String toString() {
        return "BodyTemperGunMessage{" +
                "bodyTemperNum1=" + bodyTemperNum1 +
                ", bodyTemperNum2=" + bodyTemperNum2 +
                ", bodyTemperNum3=" + bodyTemperNum3 +
                ", bodyTemperNum4=" + bodyTemperNum4 +
                '}';
    }
}
