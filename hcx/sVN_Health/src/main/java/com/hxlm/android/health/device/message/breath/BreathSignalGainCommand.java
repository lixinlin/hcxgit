package com.hxlm.android.health.device.message.breath;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * 用于设置呼吸模块的呼吸波形增益，增益值定义如下
 * 0x01 x0.25 增益
 * 0x02 x0.5 增益
 * 0x03 x1 增益
 * 0x04 x2 增益
 */
public class BreathSignalGainCommand extends AbstractMessage {

    private int signalGain;         //呼吸模块增益    00： x0.25 增益；01： x0.5 增益；2： x1 增益；3： x2 增益

    public BreathSignalGainCommand() {
        super(HealthDeviceMessageType.RESPIRATORY_SIGNAL_GAIN_COMMAND);
    }

    public int getSignalGain() {
        return signalGain;
    }

    public void setSignalGain(int signalGain) {
        this.signalGain = signalGain;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("呼吸波形模块增益：");
        switch (signalGain) {
            case 0:
                sb.append("x0.25 增益\n");
                break;
            case 1:
                sb.append("x0.5 增益\n");
                break;
            case 2:
                sb.append("x1 增益\n");
                break;
            case 3:
                sb.append("x2 增益\n");
                break;
        }
        return sb.toString();
    }
}
