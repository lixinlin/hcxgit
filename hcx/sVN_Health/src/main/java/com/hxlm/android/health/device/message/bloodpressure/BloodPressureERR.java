package com.hxlm.android.health.device.message.bloodpressure;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * 错误码
 * 
 * @author l
 * 
 */
public class BloodPressureERR extends AbstractMessage {

	public BloodPressureERR() {
		super(HealthDeviceMessageType.BLOOD_PRESSURE_ERR);
	}

	private int err;// 错误码

	public int getErr() {
		return err;
	}

	public void setErr(int err) {
		this.err = err;
	}

}
