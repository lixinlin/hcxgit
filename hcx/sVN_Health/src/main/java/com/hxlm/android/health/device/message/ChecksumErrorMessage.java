package com.hxlm.android.health.device.message;

import com.hxlm.android.comm.AbstractMessage;

/**
 * 站版驱动板发送数据
 *
 * @author l
 */
public class ChecksumErrorMessage extends AbstractMessage {
    public ChecksumErrorMessage() {
        super(HealthDeviceMessageType.CHECKSUM_ERROR);
    }
}
