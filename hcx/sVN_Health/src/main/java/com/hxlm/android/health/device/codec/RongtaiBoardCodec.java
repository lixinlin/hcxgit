package com.hxlm.android.health.device.codec;

import com.hxlm.android.comm.AbstractCodec;
import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;
import com.hxlm.android.health.device.message.board.BoardLevelCommand;
import com.hxlm.android.health.device.message.board.BoardResponseMessage;

/**
 * 站版驱动板的协议处理类
 * 平板APP里设计两个按钮，一个是角度升，一个是角度降，点一下角度升就发1给驱动板，点一下角度降就发送2给驱动板
 * 一直点击角度升，每点击一次角度升就发1给驱动板，点一下角度降就发送2给驱动板
 * 平板还可以定时发查询命令，比如发值3，驱动板收到后就回传当前档位值
 *
 * @author l
 */
public class RongtaiBoardCodec extends AbstractCodec {
    public RongtaiBoardCodec() {
        super(new byte[]{(byte) 0x55, (byte) 0xAB}, 3, 5);
    }

    @Override
    protected int getBodyLength(int bodyStartIndex) {
        return dataBuffer[bodyStartIndex] & 0x000000FF;
    }

    // 接收设备传回的数据
    @Override
    protected AbstractMessage decodeMessage(int startIndex) {
        BoardResponseMessage boardResponseMessage = new BoardResponseMessage();
        switch (dataBuffer[startIndex + 1]) {
            case 0x01:
                boardResponseMessage.setResult(BoardResponseMessage.ResultType.SETTING_LEVEL_1_SUCCESS);
                break;
            case 0x02:
                boardResponseMessage.setResult(BoardResponseMessage.ResultType.SETTING_LEVEL_2_SUCCESS);
                break;
            case 0x03:
                boardResponseMessage.setResult(BoardResponseMessage.ResultType.SETTING_LEVEL_3_SUCCESS);
                break;
            case 0x04:
                boardResponseMessage.setResult(BoardResponseMessage.ResultType.SETTING_LEVEL_4_SUCCESS);
                break;
            case 0x05:
                boardResponseMessage.setResult(BoardResponseMessage.ResultType.SETTING_LEVEL_5_SUCCESS);
                break;
            case 0x06:
                boardResponseMessage.setResult(BoardResponseMessage.ResultType.SETTING_LEVEL_6_SUCCESS);
                break;
        }
        return boardResponseMessage;
    }

    // 向设备发送数据
    @Override
    protected byte[] encodeMessage(AbstractMessage message) {
        byte[] packBody = new byte[3];
        packBody[0] = (byte) 0x03;

        //数据包内容
        switch ((HealthDeviceMessageType) message.getMessageType()) {
            case BOARD_LEVEL_COMMAND:
                BoardLevelCommand msg = (BoardLevelCommand) message;

                switch (msg.getCommandType()) {
                    case SETTING_LEVEL_1:
                        packBody[1] = 0x01;
                        break;
                    case SETTING_LEVEL_2:
                        packBody[1] = 0x02;
                        break;
                    case SETTING_LEVEL_3:
                        packBody[1] = 0x03;
                        break;
                    case SETTING_LEVEL_4:
                        packBody[1] = 0x04;
                        break;
                    case SETTING_LEVEL_5:
                        packBody[1] = 0x05;
                        break;
                    case SETTING_LEVEL_6:
                        packBody[1] = 0x06;
                        break;
                }
                break;
        }

        packBody[2] = getCheckSumByte(packBody, 0, 2);
        return packBody;
    }
}
