package com.hxlm.android.utils;

import android.text.TextUtils;

import java.util.Locale;

@SuppressWarnings("unused")
public class ByteUtil {
    /**
     * bytes字符串转换为Byte值
     *
     * @param src String Byte字符串，每个Byte之间没有分隔符(字符范围:0-9 A-F)
     * @return byte[]
     */
    public static byte[] hexStr2Bytes(String src) {
        if (TextUtils.isEmpty(src)) {
            return null;
        }
        /* 对输入值进行规范化整理 */
        src = src.trim().replace(" ", "").toUpperCase(Locale.US);
        // 处理值初始化
        int m, n;
        int iLen = src.length() / 2; // 计算长度
        byte[] ret = new byte[iLen]; // 分配存储空间

        for (int i = 0; i < iLen; i++) {
            m = i * 2 + 1;
            n = m + 1;
            ret[i] = (byte) (Integer.decode("0x" + src.substring(i * 2, m)
                    + src.substring(m, n)) & 0xFF);
        }
        return ret;
    }

    //定义一个功能将字符数组转化成字符串
    public static String toString(char[] chs, int index) {
        String s = "";
        for (int i = index; i < chs.length; i++) {
            s = s + chs[i];
        }
        return s;
    }

    //btye to 16 byte转变为16进制字符串
    public static String bytesToHexString(byte[] bArray) {
        StringBuilder sb = new StringBuilder(bArray.length);
        String sTemp;
        for (byte aBArray : bArray) {
            sTemp = Integer.toHexString(0xFF & aBArray);
            if (sTemp.length() < 2)
                sb.append(0);
            sb.append(sTemp.toUpperCase()).append(" ");
        }
        return sb.toString();
    }

    //btye to 16 byte转变为16进制字符串
    public static String bytesToHexString(byte[] bArray, int offset, int len) {
        int length = len;
        if (len > bArray.length - offset) {
            length = bArray.length - offset;
        }
        StringBuilder sb = new StringBuilder(length);
        String sTemp;
        for (int i = offset; i < (offset + length); i++) {
            sTemp = Integer.toHexString(0xFF & bArray[i]);
            if (sTemp.length() < 2)
                sb.append(0);
            sb.append(sTemp.toUpperCase()).append(" ");
        }
        return sb.toString();
    }

    public static byte[] intToByte(int i) {
        byte[] bt = new byte[4];
        bt[0] = (byte) (0xff & i);
        bt[1] = (byte) ((0xff00 & i) >> 8);
        bt[2] = (byte) ((0xff0000 & i) >> 16);
        bt[3] = (byte) ((0xff000000 & i) >> 24);
        return bt;
    }

    public static int bytes2Int(byte[] bys, int start, int len, boolean isBigEndian) {
        int n = 0;
        for (int i = start, k = start + len % (Integer.SIZE / Byte.SIZE + 1); i < k; i++) {
            n |= (bys[i] & 0xff) << ((isBigEndian ? (k - i - 1) : i) * Byte.SIZE);
        }
        return n;
    }

    public static long bytes2Long(byte[] bys, int start, int len,
                                  boolean isBigEndian) {
        long n = 0;
        for (int i = start, k = start + len; i < k; i++) {
            n <<= 8;
            n |= (bys[i] & 0xff);
        }
        return n;
    }

    public static String intToIp(int i) {
        if (i == 0)
            return null;
        return (i & 0xFF) + "." + ((i >> 8) & 0xFF) + "." + ((i >> 16) & 0xFF)
                + "." + ((i >> 24) & 0xFF);
    }

    public static long macToLong(String macStr) {
        if (macStr == null || "".equals(macStr))
            return 0;

        String macStr1 = macStr.substring(0, 2) + macStr.substring(3, 5)
                + macStr.substring(6, 8) + macStr.substring(9, 11)
                + macStr.substring(12, 14) + macStr.substring(15, 17);
        return Long.parseLong(macStr1, 16);
    }
}
