package com.hxlm.android.health.device.message.bloodpressure;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * 压力值数据格式
 * @author l
 *
 */
public class BloodPressureValueMessage extends AbstractMessage{

	private int pressureValue;//压力值
	
	
	public BloodPressureValueMessage() {
		super(HealthDeviceMessageType.BLOOD_PRESSURE_VALUE);
	}


	public int getPressureValue() {
		return pressureValue;
	}


	public void setPressureValue(int pressureValue) {
		this.pressureValue = pressureValue;
	}

	
}
