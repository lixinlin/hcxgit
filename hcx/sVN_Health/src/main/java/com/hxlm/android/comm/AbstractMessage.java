package com.hxlm.android.comm;

/**
 * 消息抽象类，主要定义消息的类型
 * <p/>
 * Created by Zhenyu on 2015/11/24.
 */
public class AbstractMessage {
    private final Enum messageType;
    public AbstractMessage(Enum messageType) {
        this.messageType = messageType;
    }
    public Enum getMessageType() {
        return messageType;
    }
}
