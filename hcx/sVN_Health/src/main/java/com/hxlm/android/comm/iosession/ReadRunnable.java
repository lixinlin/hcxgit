package com.hxlm.android.comm.iosession;

import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.utils.Logger;

import java.io.IOException;

/**
 * 从IOSession中读取数据，并进行解析。
 */
class ReadRunnable implements Runnable {
    private static final String TAG = "ReadRunnable";
    private static final int BUFF_SIZE = 128;
    private AbstractIOSession ioSession;

    ReadRunnable(AbstractIOSession ioSession) {
        this.ioSession = ioSession;
    }

    @Override
    public void run() {
        Logger.d(TAG,"******************************启动数据读取线程，连接方式为：" + ioSession.getClass().getSimpleName());

        final byte[] rbuf = new byte[BUFF_SIZE];

        // 如果为true 读取数据
        while (ioSession.status == AbstractIOSession.Status.CONNECTED) {

            try {
                final int len = ioSession.read(rbuf);

                if (len > 0) {
                    ioSession.parseData(rbuf, len);
                }
            } catch (IOException e) {
                ioSession.exceptionCaught(e);
            }
        }

        ioSession = null;

        Logger.d(TAG,"******************************退出数据读取线程。");
    }
}
