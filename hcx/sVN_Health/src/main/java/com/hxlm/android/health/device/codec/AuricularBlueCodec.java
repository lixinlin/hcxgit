package com.hxlm.android.health.device.codec;

import com.hxlm.android.comm.AbstractCodec;
import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;
import com.hxlm.android.health.device.message.auricular.AuricularBlueCommand;
import com.hxlm.android.health.device.message.auricular.AuricularBlueResponseMessage;
import com.hxlm.android.health.device.message.auricular.CommandID;

/**
 * Created by l on 2016/9/28.
 * 普通蓝牙
 */
public class AuricularBlueCodec extends AbstractCodec {
    private static final byte BYTE_END = (byte) 0x00;//数据

    public AuricularBlueCodec() {
        ////定义两个返回数据读取的包头,读包头，最小包长度，最大包长度
        super(new byte[]{(byte) 0xFA}, 2, 3);
    }

    @Override
    protected int getBodyLength(int bodyStartIndex) {
        return 2;
    }

    //接收设备返回信息
    @Override
    protected AbstractMessage decodeMessage(int bodyStartIndex) throws InterruptedException {
        AuricularBlueResponseMessage message = new AuricularBlueResponseMessage();
        switch (dataBuffer[bodyStartIndex]) {
            case (byte) 0xEE:
                message.setResposeMessage("服务器端返回数据成功");
                break;
            case (byte) 0xEF:
                message.setResposeMessage("服务器端返回数据失败");
                break;
        }
        return message;
    }

    // 向设备发送指令
    @Override
    protected byte[] encodeMessage(AbstractMessage message) {

        byte[] data = new byte[2];
        // 数据包内容
        switch ((HealthDeviceMessageType) message.getMessageType()) {
            case AURICULAR_COMMON_BLUETOOTH:
                AuricularBlueCommand blueCommand = (AuricularBlueCommand) message;
                switch (blueCommand.getCommandType()) {
                    //开始命令
                    case BLUE_MESSAGE_BEGIN:
                        data[0] = CommandID.REQUEST_BEGIN;
                        data[1] = BYTE_END;
                        break;
                    //停止命令
                    case BLUE_MESSAGE_STOP:
                        data[0] = CommandID.REQUEST_STOP;
                        data[1] = BYTE_END;
                        break;
                    //强度增大命令
                    case BLUE_MESSAGE_ADD:
                        data[0] = CommandID.REQUEST_ADD;
                        data[1] = (byte) blueCommand.getQiangdu();

                        break;
                    //强度减小命令
                    case BLUE_MESSAGE_REDUCE:
                        data[0] = CommandID.REQUEST_REDUCE;
                        data[1] = (byte) blueCommand.getQiangdu();
                        break;
                    //暂停命令
                    case BLUE_MESSAGE_PAUSE:
                        data[0] = CommandID.REQUEST_PAUSE;
                        data[1] = BYTE_END;
                        break;
                    //暂停恢复命令
                    case BLUE_MESSAGE_RESUME:
                        data[0] = CommandID.REQUEST_RESUME;
                        data[1] = BYTE_END;
                        break;
                }
        }

        return data;
    }
}
