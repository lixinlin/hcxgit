package com.hxlm.android.health.device.message;

import com.hxlm.android.health.device.message.bodytempergun.BodyTemperGunCommand;

/**
 * 支持的消息类型
 *
 * Created by Zhenyu on 2015/11/24.
 */
public enum HealthDeviceMessageType {
    CHECKSUM_ERROR,

    BOARD_LEVEL_COMMAND,
    BOARD_COMMAND_RESPONSE,

    CHAIR_COMMAND,
    CHAIR_COMMAND_RESPONSE,

    ECG_DATA,
    ECG_WAVE,
    ECG_SIGNAL_GAIN_COMMAND,
    ECG_WAVE_OUTPUT_COMMAND,
    ECG_DATA_OUTPUT_COMMAND,
    ECG_FILTER_TYPE_COMMAND,

    SPO2_DATA,
    SPO2_DATA_OUTPUT_COMMAND,
    SPO2_WAVE_OUTPUT_COMMAND,

    TEMPERATURE_DATA_OUTPUT_COMMAND,
    TEMPERATURE_DATA,

    RESPIRATORY_DATA_OUTPUT_COMMAND,
    RESPIRATORY_SIGNAL_GAIN_COMMAND,
    
    BLOOD_PRESSURE_COMMAND,//血压发送数据
    BLOOD_PRESSURE_VALUE,//血压压力值
    BLOOD_PRESSURE_BATTERY_PERCENTAGE,//血压计电量百分比
    BLOOD_PRESSURE_RESPONSE,//血压返回数据（收缩压，舒张压以及心率）
    BLOOD_PRESSURE_ERR,//血压检测错误码


    AURICULAR_COMMON_BLUETOOTH, //普通蓝牙 耳针仪 发送命令
    AURICULAR_COMMON_BLUETOOTH_RESP_MESSAGE, // 普通蓝牙 耳针仪 接收命令

    BODYTEMPERGUN_COMMAND,//体温枪 发送命令
    BODYTEMPERGUN_COMMAND_RESPONSE,//体温枪 接收数据

    AIR_QUALITY_RESPONSE//空气质量检测 接收数据
 }
