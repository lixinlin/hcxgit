package com.jiudaifu.moxademo.view;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.TextView;
import android.widget.Toast;

import com.jiudaifu.moxademo.R;

/**
 * author: mojinshu
 * date: 2018/8/20 下午3:00
 * description:
 */
public class MoxaClueDialog {

    private static Toast sToast;

    public static void ShowMsgForClue(Context c, String clueText)
    {
        if (sToast != null) {
            sToast.cancel();
            sToast = null;
        }
        Toast tst = Toast.makeText(c, clueText, Toast.LENGTH_SHORT);

        LayoutInflater lin = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView tv = (TextView)lin.inflate(R.layout.clue_view, null, false);
        tv.setText(clueText);
        tst.setView(tv);
        tst.setGravity(Gravity.CENTER, 0, 0);
        tst.show();

        sToast = tst;
    }
}
