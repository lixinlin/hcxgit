package com.jiudaifu.moxademo.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.jiudaifu.moxademo.R;
import com.jiudaifu.moxademo.adapter.NumericWheelAdapter;
import com.jiudaifu.moxademo.interfaces.OnChangeStateListener;
import com.jiudaifu.moxademo.interfaces.OnWheelChangedListener;
import com.jiudaifu.moxademo.utils.AppConfig;
import com.jiudaifu.moxademo.utils.Utils;
import com.telink.ibluetooth.expose.ChannelsPresenter;
import com.telink.ibluetooth.model.Channel;
import com.telink.ibluetooth.model.Channels;
import com.telink.ibluetooth.utils.VoiceUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author: mojinshu
 * date: 2018/8/20 下午2:20
 * description:
 */
public class WheelControlDialog implements View.OnClickListener {

    private PopupWindow window;
    private Context context;
    private TextView mChannel, mTempHint;
    private WheelView mTempSetting, mTimeSetting;
    private CheckBox mTimeSyncBtn, mTempSyncBtn;
    private TextView mSyncConfirm;
    private ImageView closeView;

    private View outSide;
    private Channel mDeviceChannel;
    private ChannelsPresenter mPresenter;

    private OnChangeStateListener listener;

    //温度单位为℃
    public static final int MIN_TEMP = 38;
    public static final int MAX_TEMP = 56;
    public static final int MAX_CHILD_TEMP = 39;

    //时间单位为min
    public static final int MIN_TIME = 1;
    public static final int MAX_TIME = 70;

    public int screenWidth = 0;

    public int currentTime = 0;
    public int currentTemp = 0;
    private int position = 0;

    private Map<Integer, String> specialNumMap;

//    private boolean isSyncTime,isSyncTemp;

    public WheelControlDialog(Context context, Channel mDeviceChannel, ChannelsPresenter mPresenter) {
        this.context = context;
        this.mDeviceChannel = mDeviceChannel;
        this.mPresenter = mPresenter;
        init(context);
    }

    public WheelControlDialog(Context context, int which, OnChangeStateListener stateListener) {
        this.context = context;
        this.position = which;
        this.listener = stateListener;
        this.mDeviceChannel = Channels.getInstance().get(which);
        init(context);
    }

    private void init(Context context) {
        intSpecialNum();
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_wheel_control, null);
        screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
        window = new PopupWindow(view, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        // 设置popWindow弹出窗体可点击，这句话必须添加，并且是true
        window.setFocusable(true);
        ColorDrawable dw = new ColorDrawable(0x10000000);
        window.setBackgroundDrawable(dw);
        initView(view);
        setData(mDeviceChannel);
    }

    /**
     * 实例化特殊编号
     */
    private void intSpecialNum() {
        if (specialNumMap == null) {
            specialNumMap = new HashMap<>();
        }
        specialNumMap.put(7, "金");
        specialNumMap.put(8, "木");
        specialNumMap.put(9, "水");
        specialNumMap.put(10, "火");
        specialNumMap.put(11, "土");
        specialNumMap.put(12, "灸");
    }

    private void initView(View view) {
        mChannel = (TextView) view.findViewById(R.id.text_channel_number_dialog);
        mTempHint = (TextView) view.findViewById(R.id.text_temp_status_dialog);

        mTempSyncBtn = (CheckBox) view.findViewById(R.id.check_temp_sync_dialog);
        mTempSyncBtn.setOnClickListener(this);

        mTimeSyncBtn = (CheckBox) view.findViewById(R.id.check_time_sync_dialog);
        mTimeSyncBtn.setOnClickListener(this);

        mSyncConfirm = (TextView) view.findViewById(R.id.text_setting_ensure_dialog);
        mSyncConfirm.setOnClickListener(this);

        mTempSetting = (WheelView) view.findViewById(R.id.wheelview_temp_setting_dialog);
        int color = Color.parseColor("#FF0000");
        int currentMaxTemp;
        int moxaMode = AppConfig.getMoxaMode(context);
        if (moxaMode != 0) {
            currentMaxTemp = MAX_CHILD_TEMP;
        } else {
            currentMaxTemp = MAX_TEMP;
        }
        NumericWheelAdapter tempAdapter = new NumericWheelAdapter(MIN_TEMP, currentMaxTemp, "%02d");
        mTempSetting.setAdapter(tempAdapter);
        mTempSetting.setLabel(context.getString(R.string.degree_centigrade));
        mTempSetting.setCyclic(true);
        mTempSetting.setCenterDrawable(R.drawable.wheel_center_drawable);
        mTempSetting.setTextSize(adjustFontSize(screenWidth));
        mTempSetting.isNeedItemShadows(false);
        mTempSetting.setWarnNum(48, 56, color);
        mTempSetting.setValueTextColor(context.getResources().getColor(R.color.blue_title));
        mTempSetting.setVisibleItems(3);

        mTimeSetting = (WheelView) view.findViewById(R.id.wheelview_time_setting_dialog);
        mTimeSetting.setAdapter(new NumericWheelAdapter(MIN_TIME, MAX_TIME, "%02d"));
//        mTimeSetting.setLabel(context.getString(R.string.fen));
        mTimeSetting.setLabel("\'  ");
        mTimeSetting.setCyclic(true);
        mTimeSetting.setVisibleItems(3);
        mTimeSetting.isNeedItemShadows(false);
        mTimeSetting.setValueTextColor(context.getResources().getColor(R.color.blue_title));
        mTimeSetting.setCenterDrawable(R.drawable.wheel_center_drawable);
        mTimeSetting.setTextSize(adjustFontSize(screenWidth));

        mTempSetting.addChangingListener(wheelListener);
        mTimeSetting.addChangingListener(wheelListener);

        closeView = (ImageView) view.findViewById(R.id.image_close_dialog);
        closeView.setOnClickListener(this);

        outSide = view.findViewById(R.id.view_outside_control);
        outSide.setOnClickListener(this);
    }

    private void setData(Channel mDeviceChannel) {
        Channel.Info targetState = mDeviceChannel.getTargetState();
        int temp = targetState.getTemp();
        int timeMin = targetState.getTimeSeconds() / 60;

        int tempPos = findTempPosition(temp);
        if (tempPos == -1) {
            tempPos = (MAX_TEMP - MIN_TEMP) / 2;
        }
        mTempSetting.setCurrentItem(tempPos);

        int timePos = findTimePosition(Math.max(1, timeMin));
        if (timePos == -1) {
            timePos = (MAX_TIME - MIN_TIME) / 2;
        }

        mTimeSetting.setCurrentItem(timePos);

        int num = mDeviceChannel.getNumber();

        if (num >= 7 && num <= 12) {
            String specialStr = specialNumMap.get(num);
            mChannel.setText(context.getString(R.string.channel_special_number, specialStr));
        } else {
            mChannel.setText(context.getString(R.string.channel_number, mDeviceChannel.getNumber()));
        }


//        updateHintText(targetState);

    }

    public int adjustFontSize(int screenWidth) {
        return (int) (screenWidth / 12);//X*13==X/20*1.5
    }

    private OnWheelChangedListener wheelListener = new OnWheelChangedListener() {
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            if (wheel == mTempSetting) {
                currentTemp = wheel.getCurrentValue();
                updateHintText(currentTemp);
            } else if (wheel == mTimeSetting) {
                currentTime = wheel.getCurrentValue();
                if (currentTime == 0) {
                    //默认设置一分钟
                    currentTime = 1 * 60;
                }
            }

        }
    };

    private int findTempPosition(int temp) {

        if (temp < MIN_TEMP || temp > MAX_TEMP) {
            return -1;
        }
        int pointer = -1;
        for (int i = MIN_TEMP; i <= MAX_TEMP; i++) {
            pointer++;
            if (temp == i) {
                return pointer;
            }
        }
        return -1;
    }

    /**
     * @param time
     * @return
     */
    private int findTimePosition(int time) {

        if (time < MIN_TIME || time > MAX_TIME) {
            return -1;
        }
        int pointer = -1;
        for (int i = MIN_TIME; i <= MAX_TIME; i++) {
            pointer++;
            if (time == i) {
                return pointer;
            }
        }
        return -1;
    }

    private void updateHintText(int temp) {
        mTempHint.setVisibility(View.VISIBLE);
        String statusText = "";
        int colorRes;
//        if (targetState.getState() == Channel.State.WORKING) {
        if (temp >= VoiceUtils.WARNNING_WENDU) {
            statusText = context.getString(R.string.chl_status_high_temp);
            colorRes = R.color.warnning_text;
        } else if (temp >= VoiceUtils.WARNNING_TEMP_ORANGE) {
            statusText = context.getString(R.string.chl_status_high_temp_1);
            colorRes = R.color.chl_text_orange;
        } else {
            statusText = context.getString(R.string.chl_status_normal_temp);
            mTempHint.setVisibility(View.INVISIBLE);
            colorRes = R.color.gray;
        }
//        } else {
////            statusText = context.getString(R.string.chl_status_stop);
//            colorRes = R.color.gray;
//        }
        mTempHint.setText(statusText);
        mTempHint.setTextColor(context.getResources().getColor(colorRes));
    }

    /**
     * 显示popupWindow
     */
    public void showPopwindow(View v) {
//        window.setAnimationStyle(R.style.mypopwindow_anim_style);
        int tabLayoutHeight = AppConfig.getTabLayoutHeight(context);
        if (tabLayoutHeight == 0) {
            tabLayoutHeight = Utils.dip2px(context, 58);
        }
        int heightPixels = Resources.getSystem().getDisplayMetrics().heightPixels;
        window.setHeight(heightPixels - (tabLayoutHeight + getStatusBarHeight(context) + 2));
        // 在底部显示
        window.showAtLocation(v, Gravity.BOTTOM, 0, tabLayoutHeight + 2);
    }

    public void setOnDismissListener(PopupWindow.OnDismissListener onDismissListener) {
        if (onDismissListener != null) {
            window.setOnDismissListener(onDismissListener);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == outSide) {
            dismiss();
        } else if (v == mTempSyncBtn) {
            tempSync();
        } else if (v == mTimeSyncBtn) {
            timeSync();
        } else if (v == mSyncConfirm) {
            confirmSetting();
        } else if (v == closeView) {
            dismiss();
        }
    }

    private void tempSync() {

    }

    private void timeSync() {

    }

    private void confirmSetting() {
        boolean isSyncTemp = mTempSyncBtn.isChecked();
        boolean isSyncTime = mTimeSyncBtn.isChecked();

        if (currentTime == 0) {
            currentTime = MIN_TIME;
        }

        if (currentTemp == 0) {
            currentTemp = MIN_TEMP;
        }

        if (listener != null) {
            listener.onChangeState(position, currentTemp, currentTime * 60, isSyncTemp, isSyncTime);
            dismiss();
            return;
        }

        if (isSyncTemp || isSyncTime) {
            List<Channel> channels = Channels.getInstance().get();
            if (channels != null && channels.size() > 0) {
                for (Channel mChannel : channels) {
                    Channel.Info targetState = mChannel.getTargetState();
                    if (isSyncTemp) {
                        targetState.setTemp(currentTemp);
                        mPresenter.setTemp(mChannel);
                    }

                    if (isSyncTime) {
                        targetState.setTimeMilliseconds(currentTime * 60 * 1000);
                        mPresenter.setTime(mChannel);
                    }
                    if (!mChannel.isWorking()) {
                        mPresenter.openChannel(mChannel);
                    }
                }
            }

        } else {
            if (currentTime != 0) {
                updateTime(currentTime);
            }

            if (currentTemp != 0) {
                updateTemp(currentTemp);
            }

            if (!mDeviceChannel.isWorking()) {
                mPresenter.openChannel(mDeviceChannel);
            }
        }

        dismiss();
    }

    private void dismiss() {
        if (window != null && window.isShowing()) {
            window.dismiss();
        }
    }


    private void updateTemp(int progress) {
        Channel.Info targetState = mDeviceChannel.getTargetState();
        targetState.setTemp(progress);
        mPresenter.setTemp(mDeviceChannel);
//        updateHintText(targetState);
    }


    private void updateTime(int progress) {
        Channel.Info targetState = mDeviceChannel.getTargetState();
        targetState.setTimeMilliseconds(progress * 60 * 1000);
        mPresenter.setTime(mDeviceChannel);
    }

    private int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
