package com.jiudaifu.moxademo.jiuliaoData;


/**
 * Created by lixinlin on 2018/10/25.
 */

public class ErrorLog  {
    private String member;
    private String deviceSn;
    private String msgType;
    private String exceTime;
    private String exceDesc;

    public ErrorLog() {
    }

    public ErrorLog( String member, String deviceSn, String msgType, String exceTime, String exceDesc) {
        this.member = member;
        this.deviceSn = deviceSn;
        this.msgType = msgType;
        this.exceTime = exceTime;
        this.exceDesc = exceDesc;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public String getDeviceSn() {
        return deviceSn;
    }

    public void setDeviceSn(String deviceSn) {
        this.deviceSn = deviceSn;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getExceTime() {
        return exceTime;
    }

    public void setExceTime(String exceTime) {
        this.exceTime = exceTime;
    }

    public String getExceDesc() {
        return exceDesc;
    }

    public void setExceDesc(String exceDesc) {
        this.exceDesc = exceDesc;
    }

    @Override
    public String toString() {
        return "ErrorLog{" +
                ", member='" + member + '\'' +
                ", deviceSn='" + deviceSn + '\'' +
                ", msgType='" + msgType + '\'' +
                ", exceTime='" + exceTime + '\'' +
                ", exceDesc='" + exceDesc + '\'' +
                '}';
    }
}
